<?php

if(PHP_OS == 'WINNT'){
    return [
        'pdf' => [
            'enabled' => true,
            'binary'  => '"C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe"',
            'options' => [],
        ],
        'image' => [
            'enabled' => false,
            'binary'  => '"C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltoimage.exe"',
            'options' => [],
        ]
    ];
}else{
    return [
        'pdf' => [
            'enabled' => true,
            'binary'  => '/usr/local/bin/wkhtmltopdf',
            'options' => [],
        ],
        'image' => [
            'enabled' => false,
            'binary'  => '/usr/local/bin/wkhtmltoimage',
            'options' => [],
        ]
    ];
}