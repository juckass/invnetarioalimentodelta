/*
Navicat MySQL Data Transfer

Source Server         : servidor
Source Server Version : 50505
Source Host           : e-bolivar.gob.ve:3306
Source Database       : inf_delta

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-10-09 11:02:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for app_historico
-- ----------------------------
DROP TABLE IF EXISTS `app_historico`;
CREATE TABLE `app_historico` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tabla` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `concepto` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `idregistro` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `usuario` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33484 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of app_historico
-- ----------------------------
INSERT INTO `app_historico` VALUES ('31467', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-16 19:59:34', '2017-03-16 19:59:34');
INSERT INTO `app_historico` VALUES ('31468', 'autenticacion', 'autenticacion', '', 'lmata', '2017-03-16 21:51:41', '2017-03-16 21:51:41');
INSERT INTO `app_historico` VALUES ('31469', 'autenticacion', 'autenticacion', '', 'lmata', '2017-03-17 08:22:43', '2017-03-17 08:22:43');
INSERT INTO `app_historico` VALUES ('31470', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-17 08:28:17', '2017-03-17 08:28:17');
INSERT INTO `app_historico` VALUES ('31471', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-17 08:33:48', '2017-03-17 08:33:48');
INSERT INTO `app_historico` VALUES ('31472', 'categorias', 'creado', '8', 'mjgutierrez', '2017-03-17 08:51:54', '2017-03-17 08:51:54');
INSERT INTO `app_historico` VALUES ('31473', 'autenticacion', 'autenticacion', '', 'rquintana', '2017-03-17 09:55:28', '2017-03-17 09:55:28');
INSERT INTO `app_historico` VALUES ('31474', 'app_usuario', 'actualizado', '20', 'rquintana', '2017-03-17 09:56:48', '2017-03-17 09:56:48');
INSERT INTO `app_historico` VALUES ('31475', 'autenticacion', 'autenticacion', '', 'ajbetancourt', '2017-03-17 10:46:50', '2017-03-17 10:46:50');
INSERT INTO `app_historico` VALUES ('31476', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-17 11:00:01', '2017-03-17 11:00:01');
INSERT INTO `app_historico` VALUES ('31477', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-17 12:31:19', '2017-03-17 12:31:19');
INSERT INTO `app_historico` VALUES ('31478', 'categorias', 'eliminado', '8', 'mjgutierrez', '2017-03-17 12:36:21', '2017-03-17 12:36:21');
INSERT INTO `app_historico` VALUES ('31481', 'ingresos_rubros', 'creado', '3', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31482', 'ingresos_requerimiento', 'creado', '1', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31483', 'ingresos_requerimiento', 'creado', '2', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31484', 'ingresos_requerimiento', 'creado', '3', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31485', 'ingresos_requerimiento', 'creado', '4', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31486', 'ingresos_requerimiento', 'creado', '5', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31487', 'ingresos_requerimiento', 'creado', '6', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31488', 'ingresos_requerimiento', 'creado', '7', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31489', 'ingresos_requerimiento', 'creado', '8', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31490', 'ingresos_requerimiento', 'creado', '9', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31491', 'ingresos_requerimiento', 'creado', '10', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31492', 'ingresos_requerimiento', 'creado', '11', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31493', 'ingresos_requerimiento', 'creado', '12', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31494', 'ingresos_requerimiento', 'creado', '13', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31495', 'ingresos_requerimiento', 'creado', '14', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31496', 'ingresos_requerimiento', 'creado', '15', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31497', 'ingresos_requerimiento', 'creado', '16', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31498', 'ingresos_requerimiento', 'creado', '17', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31499', 'ingresos_requerimiento', 'creado', '18', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31500', 'ingresos_requerimiento', 'creado', '19', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31501', 'ingresos_requerimiento', 'creado', '20', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31502', 'ingresos_requerimiento', 'creado', '21', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31503', 'ingresos_requerimiento', 'creado', '22', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31504', 'ingresos_requerimiento', 'creado', '23', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31505', 'ingresos_requerimiento', 'creado', '24', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31506', 'ingresos_requerimiento', 'creado', '25', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31507', 'ingresos_requerimiento', 'creado', '26', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31508', 'ingresos_requerimiento', 'creado', '27', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31509', 'ingresos_requerimiento', 'creado', '28', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31510', 'ingresos_requerimiento', 'creado', '29', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31511', 'ingresos_requerimiento', 'creado', '30', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31512', 'ingresos_requerimiento', 'creado', '31', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31513', 'ingresos_requerimiento', 'creado', '32', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31514', 'ingresos_requerimiento', 'creado', '33', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31515', 'ingresos_requerimiento', 'creado', '34', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31516', 'ingresos_requerimiento', 'creado', '35', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31517', 'ingresos_requerimiento', 'creado', '36', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31518', 'ingresos_requerimiento', 'creado', '37', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31519', 'ingresos_requerimiento', 'creado', '38', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31520', 'ingresos_requerimiento', 'creado', '39', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31521', 'ingresos_requerimiento', 'creado', '40', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31522', 'ingresos_requerimiento', 'creado', '41', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31523', 'ingresos_requerimiento', 'creado', '42', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31524', 'ingresos_requerimiento', 'creado', '43', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31525', 'ingresos_requerimiento', 'creado', '44', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31526', 'ingresos_requerimiento', 'creado', '45', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31527', 'ingresos_requerimiento', 'creado', '46', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31528', 'ingresos_requerimiento', 'creado', '47', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31529', 'ingresos_requerimiento', 'creado', '48', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31530', 'ingresos_requerimiento', 'creado', '49', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31531', 'ingresos_requerimiento', 'creado', '50', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31532', 'ingresos_requerimiento', 'creado', '51', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31533', 'ingresos_requerimiento', 'creado', '52', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31534', 'ingresos_requerimiento', 'creado', '53', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31535', 'ingresos_requerimiento', 'creado', '54', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31536', 'ingresos_requerimiento', 'creado', '55', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31537', 'ingresos_requerimiento', 'creado', '56', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31538', 'ingresos_requerimiento', 'creado', '57', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31539', 'ingresos_requerimiento', 'creado', '58', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31540', 'ingresos_requerimiento', 'creado', '59', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31541', 'ingresos_requerimiento', 'creado', '60', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31542', 'ingresos_requerimiento', 'creado', '61', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31543', 'ingresos_requerimiento', 'creado', '62', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31544', 'ingresos_requerimiento', 'creado', '63', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31545', 'ingresos_requerimiento', 'creado', '64', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31546', 'ingresos_requerimiento', 'creado', '65', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31547', 'ingresos_requerimiento', 'creado', '66', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31548', 'ingresos_requerimiento', 'creado', '67', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31549', 'ingresos_requerimiento', 'creado', '68', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31550', 'ingresos_requerimiento', 'creado', '69', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31551', 'ingresos_requerimiento', 'creado', '70', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31552', 'ingresos_requerimiento', 'creado', '71', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31553', 'ingresos_requerimiento', 'creado', '72', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31554', 'ingresos_requerimiento', 'creado', '73', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31555', 'ingresos_requerimiento', 'creado', '74', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31556', 'ingresos_requerimiento', 'creado', '75', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31557', 'ingresos_requerimiento', 'creado', '76', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31558', 'ingresos_requerimiento', 'creado', '77', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31559', 'ingresos_requerimiento', 'creado', '78', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31560', 'ingresos_requerimiento', 'creado', '79', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31561', 'ingresos_requerimiento', 'creado', '80', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31562', 'ingresos_requerimiento', 'creado', '81', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31563', 'ingresos_requerimiento', 'creado', '82', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31564', 'ingresos_requerimiento', 'creado', '83', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31565', 'ingresos_requerimiento', 'creado', '84', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31566', 'ingresos_requerimiento', 'creado', '85', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31567', 'ingresos_requerimiento', 'creado', '86', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31568', 'ingresos_requerimiento', 'creado', '87', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31569', 'ingresos_requerimiento', 'creado', '88', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31570', 'ingresos_requerimiento', 'creado', '89', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31571', 'ingresos_requerimiento', 'creado', '90', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31572', 'ingresos_requerimiento', 'creado', '91', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31573', 'ingresos_requerimiento', 'creado', '92', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31574', 'ingresos_requerimiento', 'creado', '93', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31575', 'ingresos_requerimiento', 'creado', '94', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31576', 'ingresos_requerimiento', 'creado', '95', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31577', 'ingresos_requerimiento', 'creado', '96', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31578', 'ingresos_requerimiento', 'creado', '97', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31579', 'ingresos_requerimiento', 'creado', '98', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31580', 'ingresos_requerimiento', 'creado', '99', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31581', 'ingresos_requerimiento', 'creado', '100', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31582', 'ingresos_requerimiento', 'creado', '101', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31583', 'ingresos_requerimiento', 'creado', '102', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31584', 'ingresos_requerimiento', 'creado', '103', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31585', 'ingresos_requerimiento', 'creado', '104', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31586', 'ingresos_requerimiento', 'creado', '105', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31587', 'ingresos_requerimiento', 'creado', '106', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31588', 'ingresos_requerimiento', 'creado', '107', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31589', 'ingresos_requerimiento', 'creado', '108', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31590', 'ingresos_requerimiento', 'creado', '109', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31591', 'ingresos_requerimiento', 'creado', '110', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31592', 'ingresos_requerimiento', 'creado', '111', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31593', 'ingresos_requerimiento', 'creado', '112', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31594', 'ingresos_requerimiento', 'creado', '113', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31595', 'ingresos_requerimiento', 'creado', '114', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31596', 'ingresos_requerimiento', 'creado', '115', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31597', 'ingresos_requerimiento', 'creado', '116', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31598', 'ingresos_requerimiento', 'creado', '117', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31599', 'ingresos_requerimiento', 'creado', '118', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31600', 'ingresos_requerimiento', 'creado', '119', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31601', 'ingresos_requerimiento', 'creado', '120', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31602', 'ingresos_requerimiento', 'creado', '121', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31603', 'ingresos_requerimiento', 'creado', '122', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31604', 'ingresos_requerimiento', 'creado', '123', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31605', 'ingresos_requerimiento', 'creado', '124', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31606', 'ingresos_requerimiento', 'creado', '125', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31607', 'ingresos_requerimiento', 'creado', '126', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31608', 'ingresos_requerimiento', 'creado', '127', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31609', 'ingresos_requerimiento', 'creado', '128', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31610', 'ingresos_requerimiento', 'creado', '129', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31611', 'ingresos_requerimiento', 'creado', '130', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31612', 'ingresos_requerimiento', 'creado', '131', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31613', 'ingresos_requerimiento', 'creado', '132', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31614', 'ingresos_requerimiento', 'creado', '133', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31615', 'ingresos_requerimiento', 'creado', '134', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31616', 'ingresos_requerimiento', 'creado', '135', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31617', 'ingresos_requerimiento', 'creado', '136', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31618', 'ingresos_requerimiento', 'creado', '137', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31619', 'ingresos_requerimiento', 'creado', '138', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31620', 'ingresos_requerimiento', 'creado', '139', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31621', 'ingresos_requerimiento', 'creado', '140', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31622', 'ingresos_requerimiento', 'creado', '141', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31623', 'ingresos_requerimiento', 'creado', '142', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31624', 'ingresos_requerimiento', 'creado', '143', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31625', 'ingresos_requerimiento', 'creado', '144', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31626', 'ingresos_requerimiento', 'creado', '145', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31627', 'ingresos_requerimiento', 'creado', '146', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31628', 'ingresos_requerimiento', 'creado', '147', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31629', 'ingresos_requerimiento', 'creado', '148', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31630', 'ingresos_requerimiento', 'creado', '149', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31631', 'ingresos_requerimiento', 'creado', '150', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31632', 'ingresos_requerimiento', 'creado', '151', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31633', 'ingresos_requerimiento', 'creado', '152', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31634', 'ingresos_requerimiento', 'creado', '153', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31635', 'ingresos_requerimiento', 'creado', '154', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31636', 'ingresos_requerimiento', 'creado', '155', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31637', 'ingresos_requerimiento', 'creado', '156', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31638', 'ingresos_requerimiento', 'creado', '157', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31639', 'ingresos_requerimiento', 'creado', '158', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31640', 'ingresos_requerimiento', 'creado', '159', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31641', 'ingresos_requerimiento', 'creado', '160', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31642', 'ingresos_requerimiento', 'creado', '161', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31643', 'ingresos_requerimiento', 'creado', '162', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31644', 'ingresos_requerimiento', 'creado', '163', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31645', 'ingresos_requerimiento', 'creado', '164', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31646', 'ingresos_requerimiento', 'creado', '165', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31647', 'ingresos_requerimiento', 'creado', '166', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31648', 'ingresos_requerimiento', 'creado', '167', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31649', 'ingresos_requerimiento', 'creado', '168', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31650', 'ingresos_requerimiento', 'creado', '169', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31651', 'ingresos_requerimiento', 'creado', '170', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31652', 'ingresos_requerimiento', 'creado', '171', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31653', 'ingresos_requerimiento', 'creado', '172', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31654', 'ingresos_requerimiento', 'creado', '173', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31655', 'ingresos_requerimiento', 'creado', '174', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31656', 'ingresos_requerimiento', 'creado', '175', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31657', 'ingresos_requerimiento', 'creado', '176', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31658', 'ingresos_requerimiento', 'creado', '177', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31659', 'ingresos_requerimiento', 'creado', '178', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31660', 'ingresos_requerimiento', 'creado', '179', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31661', 'ingresos_requerimiento', 'creado', '180', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31662', 'ingresos_requerimiento', 'creado', '181', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31663', 'ingresos_requerimiento', 'creado', '182', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31664', 'ingresos_requerimiento', 'creado', '183', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31665', 'ingresos_requerimiento', 'creado', '184', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31666', 'ingresos_requerimiento', 'creado', '185', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31667', 'ingresos_requerimiento', 'creado', '186', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31668', 'ingresos_requerimiento', 'creado', '187', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31669', 'ingresos_requerimiento', 'creado', '188', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31670', 'ingresos_requerimiento', 'creado', '189', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31671', 'ingresos_requerimiento', 'creado', '190', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31672', 'ingresos_requerimiento', 'creado', '191', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31673', 'ingresos_requerimiento', 'creado', '192', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31674', 'ingresos_requerimiento', 'creado', '193', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31675', 'ingresos_requerimiento', 'creado', '194', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31676', 'ingresos_requerimiento', 'creado', '195', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31677', 'ingresos_requerimiento', 'creado', '196', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31678', 'ingresos_requerimiento', 'creado', '197', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31679', 'ingresos_requerimiento', 'creado', '198', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31680', 'ingresos_requerimiento', 'creado', '199', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31681', 'ingresos_requerimiento', 'creado', '200', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31682', 'ingresos_requerimiento', 'creado', '201', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31683', 'ingresos_requerimiento', 'creado', '202', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31684', 'ingresos_requerimiento', 'creado', '203', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31685', 'ingresos_requerimiento', 'creado', '204', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31686', 'ingresos_requerimiento', 'creado', '205', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31687', 'ingresos_requerimiento', 'creado', '206', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31688', 'ingresos_requerimiento', 'creado', '207', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31689', 'ingresos_requerimiento', 'creado', '208', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31690', 'ingresos_requerimiento', 'creado', '209', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31691', 'ingresos_requerimiento', 'creado', '210', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31692', 'ingresos_requerimiento', 'creado', '211', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31693', 'ingresos_requerimiento', 'creado', '212', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31694', 'ingresos_requerimiento', 'creado', '213', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31695', 'ingresos_requerimiento', 'creado', '214', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31696', 'ingresos_requerimiento', 'creado', '215', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31697', 'ingresos_requerimiento', 'creado', '216', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31698', 'ingresos_requerimiento', 'creado', '217', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31699', 'ingresos_requerimiento', 'creado', '218', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31700', 'ingresos_requerimiento', 'creado', '219', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31701', 'ingresos_requerimiento', 'creado', '220', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31702', 'ingresos_requerimiento', 'creado', '221', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31703', 'ingresos_requerimiento', 'creado', '222', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31704', 'ingresos_requerimiento', 'creado', '223', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31705', 'ingresos_requerimiento', 'creado', '224', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31706', 'ingresos_requerimiento', 'creado', '225', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31707', 'ingresos_requerimiento', 'creado', '226', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31708', 'ingresos_requerimiento', 'creado', '227', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31709', 'ingresos_requerimiento', 'creado', '228', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31710', 'ingresos_requerimiento', 'creado', '229', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31711', 'ingresos_requerimiento', 'creado', '230', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31712', 'ingresos_requerimiento', 'creado', '231', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31713', 'ingresos_requerimiento', 'creado', '232', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31714', 'ingresos_requerimiento', 'creado', '233', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31715', 'ingresos_requerimiento', 'creado', '234', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31716', 'ingresos_requerimiento', 'creado', '235', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31717', 'ingresos_requerimiento', 'creado', '236', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31718', 'ingresos_requerimiento', 'creado', '237', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31719', 'ingresos_requerimiento', 'creado', '238', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31720', 'ingresos_requerimiento', 'creado', '239', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31721', 'ingresos_requerimiento', 'creado', '240', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31722', 'ingresos_requerimiento', 'creado', '241', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31723', 'ingresos_requerimiento', 'creado', '242', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31724', 'ingresos_requerimiento', 'creado', '243', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31725', 'ingresos_requerimiento', 'creado', '244', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31726', 'ingresos_requerimiento', 'creado', '245', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31727', 'ingresos_requerimiento', 'creado', '246', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31728', 'ingresos_requerimiento', 'creado', '247', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31729', 'ingresos_requerimiento', 'creado', '248', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31730', 'ingresos_requerimiento', 'creado', '249', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31731', 'ingresos_requerimiento', 'creado', '250', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31732', 'ingresos_requerimiento', 'creado', '251', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31733', 'ingresos_requerimiento', 'creado', '252', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31734', 'ingresos_requerimiento', 'creado', '253', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31735', 'ingresos_requerimiento', 'creado', '254', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31736', 'ingresos_requerimiento', 'creado', '255', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31737', 'ingresos_requerimiento', 'creado', '256', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31738', 'ingresos_requerimiento', 'creado', '257', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31739', 'ingresos_requerimiento', 'creado', '258', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31740', 'ingresos_requerimiento', 'creado', '259', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31741', 'ingresos_requerimiento', 'creado', '260', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31742', 'ingresos_requerimiento', 'creado', '261', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31743', 'ingresos_requerimiento', 'creado', '262', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31744', 'ingresos_requerimiento', 'creado', '263', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31745', 'ingresos_requerimiento', 'creado', '264', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31746', 'ingresos_requerimiento', 'creado', '265', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31747', 'ingresos_requerimiento', 'creado', '266', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31748', 'ingresos_requerimiento', 'creado', '267', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31749', 'ingresos_requerimiento', 'creado', '268', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31750', 'ingresos_requerimiento', 'creado', '269', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31751', 'ingresos_requerimiento', 'creado', '270', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31752', 'ingresos_requerimiento', 'creado', '271', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31753', 'ingresos_requerimiento', 'creado', '272', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31754', 'ingresos_requerimiento', 'creado', '273', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31755', 'ingresos_requerimiento', 'creado', '274', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31756', 'ingresos_requerimiento', 'creado', '275', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31757', 'ingresos_requerimiento', 'creado', '276', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31758', 'ingresos_requerimiento', 'creado', '277', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31759', 'ingresos_requerimiento', 'creado', '278', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31760', 'ingresos_requerimiento', 'creado', '279', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31761', 'ingresos_requerimiento', 'creado', '280', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31762', 'ingresos_requerimiento', 'creado', '281', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31763', 'ingresos_requerimiento', 'creado', '282', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31764', 'ingresos_requerimiento', 'creado', '283', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31765', 'ingresos_requerimiento', 'creado', '284', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31766', 'ingresos_requerimiento', 'creado', '285', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31767', 'ingresos_requerimiento', 'creado', '286', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31768', 'ingresos_requerimiento', 'creado', '287', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31769', 'ingresos_requerimiento', 'creado', '288', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31770', 'ingresos_requerimiento', 'creado', '289', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31771', 'ingresos_requerimiento', 'creado', '290', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31772', 'ingresos_requerimiento', 'creado', '291', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31773', 'ingresos_requerimiento', 'creado', '292', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31774', 'ingresos_requerimiento', 'creado', '293', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31775', 'ingresos_requerimiento', 'creado', '294', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31776', 'ingresos_requerimiento', 'creado', '295', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31777', 'ingresos_requerimiento', 'creado', '296', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31778', 'ingresos_requerimiento', 'creado', '297', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31779', 'ingresos_requerimiento', 'creado', '298', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31780', 'ingresos_requerimiento', 'creado', '299', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31781', 'ingresos_requerimiento', 'creado', '300', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31782', 'ingresos_requerimiento', 'creado', '301', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31783', 'ingresos_requerimiento', 'creado', '302', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31784', 'ingresos_requerimiento', 'creado', '303', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31785', 'ingresos_requerimiento', 'creado', '304', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31786', 'ingresos_requerimiento', 'creado', '305', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31787', 'ingresos_requerimiento', 'creado', '306', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31788', 'ingresos_requerimiento', 'creado', '307', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31789', 'ingresos_requerimiento', 'creado', '308', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31790', 'ingresos_requerimiento', 'creado', '309', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31791', 'ingresos_requerimiento', 'creado', '310', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31792', 'ingresos_requerimiento', 'creado', '311', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31793', 'ingresos_requerimiento', 'creado', '312', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31794', 'ingresos_requerimiento', 'creado', '313', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31795', 'ingresos_requerimiento', 'creado', '314', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31796', 'ingresos_requerimiento', 'creado', '315', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31797', 'ingresos_requerimiento', 'creado', '316', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31798', 'ingresos_requerimiento', 'creado', '317', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31799', 'ingresos_requerimiento', 'creado', '318', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31800', 'ingresos_requerimiento', 'creado', '319', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31801', 'ingresos_requerimiento', 'creado', '320', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31802', 'ingresos_requerimiento', 'creado', '321', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31803', 'ingresos_requerimiento', 'creado', '322', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31804', 'ingresos_requerimiento', 'creado', '323', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31805', 'ingresos_requerimiento', 'creado', '324', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31806', 'ingresos_requerimiento', 'creado', '325', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31807', 'ingresos_requerimiento', 'creado', '326', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31808', 'ingresos_requerimiento', 'creado', '327', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31809', 'ingresos_requerimiento', 'creado', '328', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31810', 'ingresos_requerimiento', 'creado', '329', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31811', 'ingresos_requerimiento', 'creado', '330', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31812', 'ingresos_requerimiento', 'creado', '331', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31813', 'ingresos_requerimiento', 'creado', '332', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31814', 'ingresos_requerimiento', 'creado', '333', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31815', 'ingresos_requerimiento', 'creado', '334', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31816', 'ingresos_requerimiento', 'creado', '335', 'mjgutierrez', '2017-03-17 12:43:02', '2017-03-17 12:43:02');
INSERT INTO `app_historico` VALUES ('31817', 'ingresos_rubros', 'actualizado', '3', 'mjgutierrez', '2017-03-17 12:43:15', '2017-03-17 12:43:15');
INSERT INTO `app_historico` VALUES ('31818', 'ingresos_rubros', 'eliminado', '3', 'mjgutierrez', '2017-03-17 12:43:22', '2017-03-17 12:43:22');
INSERT INTO `app_historico` VALUES ('31819', 'ingresos_categoria', 'creado', '4', 'mjgutierrez', '2017-03-17 12:43:38', '2017-03-17 12:43:38');
INSERT INTO `app_historico` VALUES ('31820', 'ingresos_rubros', 'creado', '4', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31821', 'ingresos_requerimiento', 'creado', '336', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31822', 'ingresos_requerimiento', 'creado', '337', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31823', 'ingresos_requerimiento', 'creado', '338', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31824', 'ingresos_requerimiento', 'creado', '339', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31825', 'ingresos_requerimiento', 'creado', '340', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31826', 'ingresos_requerimiento', 'creado', '341', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31827', 'ingresos_requerimiento', 'creado', '342', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31828', 'ingresos_requerimiento', 'creado', '343', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31829', 'ingresos_requerimiento', 'creado', '344', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31830', 'ingresos_requerimiento', 'creado', '345', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31831', 'ingresos_requerimiento', 'creado', '346', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31832', 'ingresos_requerimiento', 'creado', '347', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31833', 'ingresos_requerimiento', 'creado', '348', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31834', 'ingresos_requerimiento', 'creado', '349', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31835', 'ingresos_requerimiento', 'creado', '350', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31836', 'ingresos_requerimiento', 'creado', '351', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31837', 'ingresos_requerimiento', 'creado', '352', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31838', 'ingresos_requerimiento', 'creado', '353', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31839', 'ingresos_requerimiento', 'creado', '354', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31840', 'ingresos_requerimiento', 'creado', '355', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31841', 'ingresos_requerimiento', 'creado', '356', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31842', 'ingresos_requerimiento', 'creado', '357', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31843', 'ingresos_requerimiento', 'creado', '358', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31844', 'ingresos_requerimiento', 'creado', '359', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31845', 'ingresos_requerimiento', 'creado', '360', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31846', 'ingresos_requerimiento', 'creado', '361', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31847', 'ingresos_requerimiento', 'creado', '362', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31848', 'ingresos_requerimiento', 'creado', '363', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31849', 'ingresos_requerimiento', 'creado', '364', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31850', 'ingresos_requerimiento', 'creado', '365', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31851', 'ingresos_requerimiento', 'creado', '366', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31852', 'ingresos_requerimiento', 'creado', '367', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31853', 'ingresos_requerimiento', 'creado', '368', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31854', 'ingresos_requerimiento', 'creado', '369', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31855', 'ingresos_requerimiento', 'creado', '370', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31856', 'ingresos_requerimiento', 'creado', '371', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31857', 'ingresos_requerimiento', 'creado', '372', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31858', 'ingresos_requerimiento', 'creado', '373', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31859', 'ingresos_requerimiento', 'creado', '374', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31860', 'ingresos_requerimiento', 'creado', '375', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31861', 'ingresos_requerimiento', 'creado', '376', 'mjgutierrez', '2017-03-17 13:21:49', '2017-03-17 13:21:49');
INSERT INTO `app_historico` VALUES ('31862', 'ingresos_requerimiento', 'creado', '377', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31863', 'ingresos_requerimiento', 'creado', '378', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31864', 'ingresos_requerimiento', 'creado', '379', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31865', 'ingresos_requerimiento', 'creado', '380', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31866', 'ingresos_requerimiento', 'creado', '381', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31867', 'ingresos_requerimiento', 'creado', '382', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31868', 'ingresos_requerimiento', 'creado', '383', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31869', 'ingresos_requerimiento', 'creado', '384', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31870', 'ingresos_requerimiento', 'creado', '385', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31871', 'ingresos_requerimiento', 'creado', '386', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31872', 'ingresos_requerimiento', 'creado', '387', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31873', 'ingresos_requerimiento', 'creado', '388', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31874', 'ingresos_requerimiento', 'creado', '389', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31875', 'ingresos_requerimiento', 'creado', '390', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31876', 'ingresos_requerimiento', 'creado', '391', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31877', 'ingresos_requerimiento', 'creado', '392', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31878', 'ingresos_requerimiento', 'creado', '393', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31879', 'ingresos_requerimiento', 'creado', '394', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31880', 'ingresos_requerimiento', 'creado', '395', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31881', 'ingresos_requerimiento', 'creado', '396', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31882', 'ingresos_requerimiento', 'creado', '397', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31883', 'ingresos_requerimiento', 'creado', '398', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31884', 'ingresos_requerimiento', 'creado', '399', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31885', 'ingresos_requerimiento', 'creado', '400', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31886', 'ingresos_requerimiento', 'creado', '401', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31887', 'ingresos_requerimiento', 'creado', '402', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31888', 'ingresos_requerimiento', 'creado', '403', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31889', 'ingresos_requerimiento', 'creado', '404', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31890', 'ingresos_requerimiento', 'creado', '405', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31891', 'ingresos_requerimiento', 'creado', '406', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31892', 'ingresos_requerimiento', 'creado', '407', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31893', 'ingresos_requerimiento', 'creado', '408', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31894', 'ingresos_requerimiento', 'creado', '409', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31895', 'ingresos_requerimiento', 'creado', '410', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31896', 'ingresos_requerimiento', 'creado', '411', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31897', 'ingresos_requerimiento', 'creado', '412', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31898', 'ingresos_requerimiento', 'creado', '413', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31899', 'ingresos_requerimiento', 'creado', '414', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31900', 'ingresos_requerimiento', 'creado', '415', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31901', 'ingresos_requerimiento', 'creado', '416', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31902', 'ingresos_requerimiento', 'creado', '417', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31903', 'ingresos_requerimiento', 'creado', '418', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31904', 'ingresos_requerimiento', 'creado', '419', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31905', 'ingresos_requerimiento', 'creado', '420', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31906', 'ingresos_requerimiento', 'creado', '421', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31907', 'ingresos_requerimiento', 'creado', '422', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31908', 'ingresos_requerimiento', 'creado', '423', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31909', 'ingresos_requerimiento', 'creado', '424', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31910', 'ingresos_requerimiento', 'creado', '425', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31911', 'ingresos_requerimiento', 'creado', '426', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31912', 'ingresos_requerimiento', 'creado', '427', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31913', 'ingresos_requerimiento', 'creado', '428', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31914', 'ingresos_requerimiento', 'creado', '429', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31915', 'ingresos_requerimiento', 'creado', '430', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31916', 'ingresos_requerimiento', 'creado', '431', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31917', 'ingresos_requerimiento', 'creado', '432', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31918', 'ingresos_requerimiento', 'creado', '433', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31919', 'ingresos_requerimiento', 'creado', '434', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31920', 'ingresos_requerimiento', 'creado', '435', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31921', 'ingresos_requerimiento', 'creado', '436', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31922', 'ingresos_requerimiento', 'creado', '437', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31923', 'ingresos_requerimiento', 'creado', '438', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31924', 'ingresos_requerimiento', 'creado', '439', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31925', 'ingresos_requerimiento', 'creado', '440', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31926', 'ingresos_requerimiento', 'creado', '441', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31927', 'ingresos_requerimiento', 'creado', '442', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31928', 'ingresos_requerimiento', 'creado', '443', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31929', 'ingresos_requerimiento', 'creado', '444', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31930', 'ingresos_requerimiento', 'creado', '445', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31931', 'ingresos_requerimiento', 'creado', '446', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31932', 'ingresos_requerimiento', 'creado', '447', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31933', 'ingresos_requerimiento', 'creado', '448', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31934', 'ingresos_requerimiento', 'creado', '449', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31935', 'ingresos_requerimiento', 'creado', '450', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31936', 'ingresos_requerimiento', 'creado', '451', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31937', 'ingresos_requerimiento', 'creado', '452', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31938', 'ingresos_requerimiento', 'creado', '453', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31939', 'ingresos_requerimiento', 'creado', '454', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31940', 'ingresos_requerimiento', 'creado', '455', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31941', 'ingresos_requerimiento', 'creado', '456', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31942', 'ingresos_requerimiento', 'creado', '457', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31943', 'ingresos_requerimiento', 'creado', '458', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31944', 'ingresos_requerimiento', 'creado', '459', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31945', 'ingresos_requerimiento', 'creado', '460', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31946', 'ingresos_requerimiento', 'creado', '461', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31947', 'ingresos_requerimiento', 'creado', '462', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31948', 'ingresos_requerimiento', 'creado', '463', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31949', 'ingresos_requerimiento', 'creado', '464', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31950', 'ingresos_requerimiento', 'creado', '465', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31951', 'ingresos_requerimiento', 'creado', '466', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31952', 'ingresos_requerimiento', 'creado', '467', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31953', 'ingresos_requerimiento', 'creado', '468', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31954', 'ingresos_requerimiento', 'creado', '469', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31955', 'ingresos_requerimiento', 'creado', '470', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31956', 'ingresos_requerimiento', 'creado', '471', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31957', 'ingresos_requerimiento', 'creado', '472', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31958', 'ingresos_requerimiento', 'creado', '473', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31959', 'ingresos_requerimiento', 'creado', '474', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31960', 'ingresos_requerimiento', 'creado', '475', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31961', 'ingresos_requerimiento', 'creado', '476', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31962', 'ingresos_requerimiento', 'creado', '477', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31963', 'ingresos_requerimiento', 'creado', '478', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31964', 'ingresos_requerimiento', 'creado', '479', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31965', 'ingresos_requerimiento', 'creado', '480', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31966', 'ingresos_requerimiento', 'creado', '481', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31967', 'ingresos_requerimiento', 'creado', '482', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31968', 'ingresos_requerimiento', 'creado', '483', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31969', 'ingresos_requerimiento', 'creado', '484', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31970', 'ingresos_requerimiento', 'creado', '485', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31971', 'ingresos_requerimiento', 'creado', '486', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31972', 'ingresos_requerimiento', 'creado', '487', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31973', 'ingresos_requerimiento', 'creado', '488', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31974', 'ingresos_requerimiento', 'creado', '489', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31975', 'ingresos_requerimiento', 'creado', '490', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31976', 'ingresos_requerimiento', 'creado', '491', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31977', 'ingresos_requerimiento', 'creado', '492', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31978', 'ingresos_requerimiento', 'creado', '493', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31979', 'ingresos_requerimiento', 'creado', '494', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31980', 'ingresos_requerimiento', 'creado', '495', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31981', 'ingresos_requerimiento', 'creado', '496', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31982', 'ingresos_requerimiento', 'creado', '497', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31983', 'ingresos_requerimiento', 'creado', '498', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31984', 'ingresos_requerimiento', 'creado', '499', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31985', 'ingresos_requerimiento', 'creado', '500', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31986', 'ingresos_requerimiento', 'creado', '501', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31987', 'ingresos_requerimiento', 'creado', '502', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31988', 'ingresos_requerimiento', 'creado', '503', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31989', 'ingresos_requerimiento', 'creado', '504', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31990', 'ingresos_requerimiento', 'creado', '505', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31991', 'ingresos_requerimiento', 'creado', '506', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31992', 'ingresos_requerimiento', 'creado', '507', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31993', 'ingresos_requerimiento', 'creado', '508', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31994', 'ingresos_requerimiento', 'creado', '509', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31995', 'ingresos_requerimiento', 'creado', '510', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31996', 'ingresos_requerimiento', 'creado', '511', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31997', 'ingresos_requerimiento', 'creado', '512', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31998', 'ingresos_requerimiento', 'creado', '513', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('31999', 'ingresos_requerimiento', 'creado', '514', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32000', 'ingresos_requerimiento', 'creado', '515', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32001', 'ingresos_requerimiento', 'creado', '516', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32002', 'ingresos_requerimiento', 'creado', '517', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32003', 'ingresos_requerimiento', 'creado', '518', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32004', 'ingresos_requerimiento', 'creado', '519', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32005', 'ingresos_requerimiento', 'creado', '520', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32006', 'ingresos_requerimiento', 'creado', '521', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32007', 'ingresos_requerimiento', 'creado', '522', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32008', 'ingresos_requerimiento', 'creado', '523', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32009', 'ingresos_requerimiento', 'creado', '524', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32010', 'ingresos_requerimiento', 'creado', '525', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32011', 'ingresos_requerimiento', 'creado', '526', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32012', 'ingresos_requerimiento', 'creado', '527', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32013', 'ingresos_requerimiento', 'creado', '528', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32014', 'ingresos_requerimiento', 'creado', '529', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32015', 'ingresos_requerimiento', 'creado', '530', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32016', 'ingresos_requerimiento', 'creado', '531', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32017', 'ingresos_requerimiento', 'creado', '532', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32018', 'ingresos_requerimiento', 'creado', '533', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32019', 'ingresos_requerimiento', 'creado', '534', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32020', 'ingresos_requerimiento', 'creado', '535', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32021', 'ingresos_requerimiento', 'creado', '536', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32022', 'ingresos_requerimiento', 'creado', '537', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32023', 'ingresos_requerimiento', 'creado', '538', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32024', 'ingresos_requerimiento', 'creado', '539', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32025', 'ingresos_requerimiento', 'creado', '540', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32026', 'ingresos_requerimiento', 'creado', '541', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32027', 'ingresos_requerimiento', 'creado', '542', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32028', 'ingresos_requerimiento', 'creado', '543', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32029', 'ingresos_requerimiento', 'creado', '544', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32030', 'ingresos_requerimiento', 'creado', '545', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32031', 'ingresos_requerimiento', 'creado', '546', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32032', 'ingresos_requerimiento', 'creado', '547', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32033', 'ingresos_requerimiento', 'creado', '548', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32034', 'ingresos_requerimiento', 'creado', '549', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32035', 'ingresos_requerimiento', 'creado', '550', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32036', 'ingresos_requerimiento', 'creado', '551', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32037', 'ingresos_requerimiento', 'creado', '552', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32038', 'ingresos_requerimiento', 'creado', '553', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32039', 'ingresos_requerimiento', 'creado', '554', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32040', 'ingresos_requerimiento', 'creado', '555', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32041', 'ingresos_requerimiento', 'creado', '556', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32042', 'ingresos_requerimiento', 'creado', '557', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32043', 'ingresos_requerimiento', 'creado', '558', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32044', 'ingresos_requerimiento', 'creado', '559', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32045', 'ingresos_requerimiento', 'creado', '560', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32046', 'ingresos_requerimiento', 'creado', '561', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32047', 'ingresos_requerimiento', 'creado', '562', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32048', 'ingresos_requerimiento', 'creado', '563', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32049', 'ingresos_requerimiento', 'creado', '564', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32050', 'ingresos_requerimiento', 'creado', '565', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32051', 'ingresos_requerimiento', 'creado', '566', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32052', 'ingresos_requerimiento', 'creado', '567', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32053', 'ingresos_requerimiento', 'creado', '568', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32054', 'ingresos_requerimiento', 'creado', '569', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32055', 'ingresos_requerimiento', 'creado', '570', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32056', 'ingresos_requerimiento', 'creado', '571', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32057', 'ingresos_requerimiento', 'creado', '572', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32058', 'ingresos_requerimiento', 'creado', '573', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32059', 'ingresos_requerimiento', 'creado', '574', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32060', 'ingresos_requerimiento', 'creado', '575', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32061', 'ingresos_requerimiento', 'creado', '576', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32062', 'ingresos_requerimiento', 'creado', '577', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32063', 'ingresos_requerimiento', 'creado', '578', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32064', 'ingresos_requerimiento', 'creado', '579', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32065', 'ingresos_requerimiento', 'creado', '580', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32066', 'ingresos_requerimiento', 'creado', '581', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32067', 'ingresos_requerimiento', 'creado', '582', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32068', 'ingresos_requerimiento', 'creado', '583', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32069', 'ingresos_requerimiento', 'creado', '584', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32070', 'ingresos_requerimiento', 'creado', '585', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32071', 'ingresos_requerimiento', 'creado', '586', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32072', 'ingresos_requerimiento', 'creado', '587', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32073', 'ingresos_requerimiento', 'creado', '588', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32074', 'ingresos_requerimiento', 'creado', '589', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32075', 'ingresos_requerimiento', 'creado', '590', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32076', 'ingresos_requerimiento', 'creado', '591', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32077', 'ingresos_requerimiento', 'creado', '592', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32078', 'ingresos_requerimiento', 'creado', '593', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32079', 'ingresos_requerimiento', 'creado', '594', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32080', 'ingresos_requerimiento', 'creado', '595', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32081', 'ingresos_requerimiento', 'creado', '596', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32082', 'ingresos_requerimiento', 'creado', '597', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32083', 'ingresos_requerimiento', 'creado', '598', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32084', 'ingresos_requerimiento', 'creado', '599', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32085', 'ingresos_requerimiento', 'creado', '600', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32086', 'ingresos_requerimiento', 'creado', '601', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32087', 'ingresos_requerimiento', 'creado', '602', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32088', 'ingresos_requerimiento', 'creado', '603', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32089', 'ingresos_requerimiento', 'creado', '604', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32090', 'ingresos_requerimiento', 'creado', '605', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32091', 'ingresos_requerimiento', 'creado', '606', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32092', 'ingresos_requerimiento', 'creado', '607', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32093', 'ingresos_requerimiento', 'creado', '608', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32094', 'ingresos_requerimiento', 'creado', '609', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32095', 'ingresos_requerimiento', 'creado', '610', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32096', 'ingresos_requerimiento', 'creado', '611', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32097', 'ingresos_requerimiento', 'creado', '612', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32098', 'ingresos_requerimiento', 'creado', '613', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32099', 'ingresos_requerimiento', 'creado', '614', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32100', 'ingresos_requerimiento', 'creado', '615', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32101', 'ingresos_requerimiento', 'creado', '616', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32102', 'ingresos_requerimiento', 'creado', '617', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32103', 'ingresos_requerimiento', 'creado', '618', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32104', 'ingresos_requerimiento', 'creado', '619', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32105', 'ingresos_requerimiento', 'creado', '620', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32106', 'ingresos_requerimiento', 'creado', '621', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32107', 'ingresos_requerimiento', 'creado', '622', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32108', 'ingresos_requerimiento', 'creado', '623', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32109', 'ingresos_requerimiento', 'creado', '624', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32110', 'ingresos_requerimiento', 'creado', '625', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32111', 'ingresos_requerimiento', 'creado', '626', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32112', 'ingresos_requerimiento', 'creado', '627', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32113', 'ingresos_requerimiento', 'creado', '628', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32114', 'ingresos_requerimiento', 'creado', '629', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32115', 'ingresos_requerimiento', 'creado', '630', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32116', 'ingresos_requerimiento', 'creado', '631', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32117', 'ingresos_requerimiento', 'creado', '632', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32118', 'ingresos_requerimiento', 'creado', '633', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32119', 'ingresos_requerimiento', 'creado', '634', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32120', 'ingresos_requerimiento', 'creado', '635', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32121', 'ingresos_requerimiento', 'creado', '636', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32122', 'ingresos_requerimiento', 'creado', '637', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32123', 'ingresos_requerimiento', 'creado', '638', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32124', 'ingresos_requerimiento', 'creado', '639', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32125', 'ingresos_requerimiento', 'creado', '640', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32126', 'ingresos_requerimiento', 'creado', '641', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32127', 'ingresos_requerimiento', 'creado', '642', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32128', 'ingresos_requerimiento', 'creado', '643', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32129', 'ingresos_requerimiento', 'creado', '644', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32130', 'ingresos_requerimiento', 'creado', '645', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32131', 'ingresos_requerimiento', 'creado', '646', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32132', 'ingresos_requerimiento', 'creado', '647', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32133', 'ingresos_requerimiento', 'creado', '648', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32134', 'ingresos_requerimiento', 'creado', '649', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32135', 'ingresos_requerimiento', 'creado', '650', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32136', 'ingresos_requerimiento', 'creado', '651', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32137', 'ingresos_requerimiento', 'creado', '652', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32138', 'ingresos_requerimiento', 'creado', '653', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32139', 'ingresos_requerimiento', 'creado', '654', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32140', 'ingresos_requerimiento', 'creado', '655', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32141', 'ingresos_requerimiento', 'creado', '656', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32142', 'ingresos_requerimiento', 'creado', '657', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32143', 'ingresos_requerimiento', 'creado', '658', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32144', 'ingresos_requerimiento', 'creado', '659', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32145', 'ingresos_requerimiento', 'creado', '660', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32146', 'ingresos_requerimiento', 'creado', '661', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32147', 'ingresos_requerimiento', 'creado', '662', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32148', 'ingresos_requerimiento', 'creado', '663', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32149', 'ingresos_requerimiento', 'creado', '664', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32150', 'ingresos_requerimiento', 'creado', '665', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32151', 'ingresos_requerimiento', 'creado', '666', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32152', 'ingresos_requerimiento', 'creado', '667', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32153', 'ingresos_requerimiento', 'creado', '668', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32154', 'ingresos_requerimiento', 'creado', '669', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32155', 'ingresos_requerimiento', 'creado', '670', 'mjgutierrez', '2017-03-17 13:21:50', '2017-03-17 13:21:50');
INSERT INTO `app_historico` VALUES ('32156', 'ingresos_rubros', 'actualizado', '4', 'mjgutierrez', '2017-03-17 13:21:54', '2017-03-17 13:21:54');
INSERT INTO `app_historico` VALUES ('32157', 'ingresos_rubros', 'eliminado', '4', 'mjgutierrez', '2017-03-17 13:22:02', '2017-03-17 13:22:02');
INSERT INTO `app_historico` VALUES ('32158', 'ingresos_rubros', 'creado', '5', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32159', 'ingresos_requerimiento', 'creado', '671', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32160', 'ingresos_requerimiento', 'creado', '672', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32161', 'ingresos_requerimiento', 'creado', '673', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32162', 'ingresos_requerimiento', 'creado', '674', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32163', 'ingresos_requerimiento', 'creado', '675', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32164', 'ingresos_requerimiento', 'creado', '676', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32165', 'ingresos_requerimiento', 'creado', '677', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32166', 'ingresos_requerimiento', 'creado', '678', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32167', 'ingresos_requerimiento', 'creado', '679', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32168', 'ingresos_requerimiento', 'creado', '680', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32169', 'ingresos_requerimiento', 'creado', '681', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32170', 'ingresos_requerimiento', 'creado', '682', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32171', 'ingresos_requerimiento', 'creado', '683', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32172', 'ingresos_requerimiento', 'creado', '684', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32173', 'ingresos_requerimiento', 'creado', '685', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32174', 'ingresos_requerimiento', 'creado', '686', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32175', 'ingresos_requerimiento', 'creado', '687', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32176', 'ingresos_requerimiento', 'creado', '688', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32177', 'ingresos_requerimiento', 'creado', '689', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32178', 'ingresos_requerimiento', 'creado', '690', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32179', 'ingresos_requerimiento', 'creado', '691', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32180', 'ingresos_requerimiento', 'creado', '692', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32181', 'ingresos_requerimiento', 'creado', '693', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32182', 'ingresos_requerimiento', 'creado', '694', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32183', 'ingresos_requerimiento', 'creado', '695', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32184', 'ingresos_requerimiento', 'creado', '696', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32185', 'ingresos_requerimiento', 'creado', '697', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32186', 'ingresos_requerimiento', 'creado', '698', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32187', 'ingresos_requerimiento', 'creado', '699', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32188', 'ingresos_requerimiento', 'creado', '700', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32189', 'ingresos_requerimiento', 'creado', '701', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32190', 'ingresos_requerimiento', 'creado', '702', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32191', 'ingresos_requerimiento', 'creado', '703', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32192', 'ingresos_requerimiento', 'creado', '704', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32193', 'ingresos_requerimiento', 'creado', '705', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32194', 'ingresos_requerimiento', 'creado', '706', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32195', 'ingresos_requerimiento', 'creado', '707', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32196', 'ingresos_requerimiento', 'creado', '708', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32197', 'ingresos_requerimiento', 'creado', '709', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32198', 'ingresos_requerimiento', 'creado', '710', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32199', 'ingresos_requerimiento', 'creado', '711', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32200', 'ingresos_requerimiento', 'creado', '712', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32201', 'ingresos_requerimiento', 'creado', '713', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32202', 'ingresos_requerimiento', 'creado', '714', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32203', 'ingresos_requerimiento', 'creado', '715', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32204', 'ingresos_requerimiento', 'creado', '716', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32205', 'ingresos_requerimiento', 'creado', '717', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32206', 'ingresos_requerimiento', 'creado', '718', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32207', 'ingresos_requerimiento', 'creado', '719', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32208', 'ingresos_requerimiento', 'creado', '720', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32209', 'ingresos_requerimiento', 'creado', '721', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32210', 'ingresos_requerimiento', 'creado', '722', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32211', 'ingresos_requerimiento', 'creado', '723', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32212', 'ingresos_requerimiento', 'creado', '724', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32213', 'ingresos_requerimiento', 'creado', '725', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32214', 'ingresos_requerimiento', 'creado', '726', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32215', 'ingresos_requerimiento', 'creado', '727', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32216', 'ingresos_requerimiento', 'creado', '728', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32217', 'ingresos_requerimiento', 'creado', '729', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32218', 'ingresos_requerimiento', 'creado', '730', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32219', 'ingresos_requerimiento', 'creado', '731', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32220', 'ingresos_requerimiento', 'creado', '732', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32221', 'ingresos_requerimiento', 'creado', '733', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32222', 'ingresos_requerimiento', 'creado', '734', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32223', 'ingresos_requerimiento', 'creado', '735', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32224', 'ingresos_requerimiento', 'creado', '736', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32225', 'ingresos_requerimiento', 'creado', '737', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32226', 'ingresos_requerimiento', 'creado', '738', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32227', 'ingresos_requerimiento', 'creado', '739', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32228', 'ingresos_requerimiento', 'creado', '740', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32229', 'ingresos_requerimiento', 'creado', '741', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32230', 'ingresos_requerimiento', 'creado', '742', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32231', 'ingresos_requerimiento', 'creado', '743', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32232', 'ingresos_requerimiento', 'creado', '744', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32233', 'ingresos_requerimiento', 'creado', '745', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32234', 'ingresos_requerimiento', 'creado', '746', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32235', 'ingresos_requerimiento', 'creado', '747', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32236', 'ingresos_requerimiento', 'creado', '748', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32237', 'ingresos_requerimiento', 'creado', '749', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32238', 'ingresos_requerimiento', 'creado', '750', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32239', 'ingresos_requerimiento', 'creado', '751', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32240', 'ingresos_requerimiento', 'creado', '752', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32241', 'ingresos_requerimiento', 'creado', '753', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32242', 'ingresos_requerimiento', 'creado', '754', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32243', 'ingresos_requerimiento', 'creado', '755', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32244', 'ingresos_requerimiento', 'creado', '756', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32245', 'ingresos_requerimiento', 'creado', '757', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32246', 'ingresos_requerimiento', 'creado', '758', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32247', 'ingresos_requerimiento', 'creado', '759', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32248', 'ingresos_requerimiento', 'creado', '760', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32249', 'ingresos_requerimiento', 'creado', '761', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32250', 'ingresos_requerimiento', 'creado', '762', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32251', 'ingresos_requerimiento', 'creado', '763', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32252', 'ingresos_requerimiento', 'creado', '764', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32253', 'ingresos_requerimiento', 'creado', '765', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32254', 'ingresos_requerimiento', 'creado', '766', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32255', 'ingresos_requerimiento', 'creado', '767', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32256', 'ingresos_requerimiento', 'creado', '768', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32257', 'ingresos_requerimiento', 'creado', '769', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32258', 'ingresos_requerimiento', 'creado', '770', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32259', 'ingresos_requerimiento', 'creado', '771', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32260', 'ingresos_requerimiento', 'creado', '772', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32261', 'ingresos_requerimiento', 'creado', '773', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32262', 'ingresos_requerimiento', 'creado', '774', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32263', 'ingresos_requerimiento', 'creado', '775', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32264', 'ingresos_requerimiento', 'creado', '776', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32265', 'ingresos_requerimiento', 'creado', '777', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32266', 'ingresos_requerimiento', 'creado', '778', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32267', 'ingresos_requerimiento', 'creado', '779', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32268', 'ingresos_requerimiento', 'creado', '780', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32269', 'ingresos_requerimiento', 'creado', '781', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32270', 'ingresos_requerimiento', 'creado', '782', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32271', 'ingresos_requerimiento', 'creado', '783', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32272', 'ingresos_requerimiento', 'creado', '784', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32273', 'ingresos_requerimiento', 'creado', '785', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32274', 'ingresos_requerimiento', 'creado', '786', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32275', 'ingresos_requerimiento', 'creado', '787', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32276', 'ingresos_requerimiento', 'creado', '788', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32277', 'ingresos_requerimiento', 'creado', '789', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32278', 'ingresos_requerimiento', 'creado', '790', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32279', 'ingresos_requerimiento', 'creado', '791', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32280', 'ingresos_requerimiento', 'creado', '792', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32281', 'ingresos_requerimiento', 'creado', '793', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32282', 'ingresos_requerimiento', 'creado', '794', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32283', 'ingresos_requerimiento', 'creado', '795', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32284', 'ingresos_requerimiento', 'creado', '796', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32285', 'ingresos_requerimiento', 'creado', '797', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32286', 'ingresos_requerimiento', 'creado', '798', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32287', 'ingresos_requerimiento', 'creado', '799', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32288', 'ingresos_requerimiento', 'creado', '800', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32289', 'ingresos_requerimiento', 'creado', '801', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32290', 'ingresos_requerimiento', 'creado', '802', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32291', 'ingresos_requerimiento', 'creado', '803', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32292', 'ingresos_requerimiento', 'creado', '804', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32293', 'ingresos_requerimiento', 'creado', '805', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32294', 'ingresos_requerimiento', 'creado', '806', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32295', 'ingresos_requerimiento', 'creado', '807', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32296', 'ingresos_requerimiento', 'creado', '808', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32297', 'ingresos_requerimiento', 'creado', '809', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32298', 'ingresos_requerimiento', 'creado', '810', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32299', 'ingresos_requerimiento', 'creado', '811', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32300', 'ingresos_requerimiento', 'creado', '812', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32301', 'ingresos_requerimiento', 'creado', '813', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32302', 'ingresos_requerimiento', 'creado', '814', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32303', 'ingresos_requerimiento', 'creado', '815', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32304', 'ingresos_requerimiento', 'creado', '816', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32305', 'ingresos_requerimiento', 'creado', '817', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32306', 'ingresos_requerimiento', 'creado', '818', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32307', 'ingresos_requerimiento', 'creado', '819', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32308', 'ingresos_requerimiento', 'creado', '820', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32309', 'ingresos_requerimiento', 'creado', '821', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32310', 'ingresos_requerimiento', 'creado', '822', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32311', 'ingresos_requerimiento', 'creado', '823', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32312', 'ingresos_requerimiento', 'creado', '824', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32313', 'ingresos_requerimiento', 'creado', '825', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32314', 'ingresos_requerimiento', 'creado', '826', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32315', 'ingresos_requerimiento', 'creado', '827', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32316', 'ingresos_requerimiento', 'creado', '828', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32317', 'ingresos_requerimiento', 'creado', '829', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32318', 'ingresos_requerimiento', 'creado', '830', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32319', 'ingresos_requerimiento', 'creado', '831', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32320', 'ingresos_requerimiento', 'creado', '832', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32321', 'ingresos_requerimiento', 'creado', '833', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32322', 'ingresos_requerimiento', 'creado', '834', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32323', 'ingresos_requerimiento', 'creado', '835', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32324', 'ingresos_requerimiento', 'creado', '836', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32325', 'ingresos_requerimiento', 'creado', '837', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32326', 'ingresos_requerimiento', 'creado', '838', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32327', 'ingresos_requerimiento', 'creado', '839', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32328', 'ingresos_requerimiento', 'creado', '840', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32329', 'ingresos_requerimiento', 'creado', '841', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32330', 'ingresos_requerimiento', 'creado', '842', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32331', 'ingresos_requerimiento', 'creado', '843', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32332', 'ingresos_requerimiento', 'creado', '844', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32333', 'ingresos_requerimiento', 'creado', '845', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32334', 'ingresos_requerimiento', 'creado', '846', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32335', 'ingresos_requerimiento', 'creado', '847', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32336', 'ingresos_requerimiento', 'creado', '848', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32337', 'ingresos_requerimiento', 'creado', '849', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32338', 'ingresos_requerimiento', 'creado', '850', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32339', 'ingresos_requerimiento', 'creado', '851', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32340', 'ingresos_requerimiento', 'creado', '852', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32341', 'ingresos_requerimiento', 'creado', '853', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32342', 'ingresos_requerimiento', 'creado', '854', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32343', 'ingresos_requerimiento', 'creado', '855', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32344', 'ingresos_requerimiento', 'creado', '856', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32345', 'ingresos_requerimiento', 'creado', '857', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32346', 'ingresos_requerimiento', 'creado', '858', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32347', 'ingresos_requerimiento', 'creado', '859', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32348', 'ingresos_requerimiento', 'creado', '860', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32349', 'ingresos_requerimiento', 'creado', '861', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32350', 'ingresos_requerimiento', 'creado', '862', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32351', 'ingresos_requerimiento', 'creado', '863', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32352', 'ingresos_requerimiento', 'creado', '864', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32353', 'ingresos_requerimiento', 'creado', '865', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32354', 'ingresos_requerimiento', 'creado', '866', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32355', 'ingresos_requerimiento', 'creado', '867', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32356', 'ingresos_requerimiento', 'creado', '868', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32357', 'ingresos_requerimiento', 'creado', '869', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32358', 'ingresos_requerimiento', 'creado', '870', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32359', 'ingresos_requerimiento', 'creado', '871', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32360', 'ingresos_requerimiento', 'creado', '872', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32361', 'ingresos_requerimiento', 'creado', '873', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32362', 'ingresos_requerimiento', 'creado', '874', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32363', 'ingresos_requerimiento', 'creado', '875', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32364', 'ingresos_requerimiento', 'creado', '876', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32365', 'ingresos_requerimiento', 'creado', '877', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32366', 'ingresos_requerimiento', 'creado', '878', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32367', 'ingresos_requerimiento', 'creado', '879', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32368', 'ingresos_requerimiento', 'creado', '880', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32369', 'ingresos_requerimiento', 'creado', '881', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32370', 'ingresos_requerimiento', 'creado', '882', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32371', 'ingresos_requerimiento', 'creado', '883', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32372', 'ingresos_requerimiento', 'creado', '884', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32373', 'ingresos_requerimiento', 'creado', '885', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32374', 'ingresos_requerimiento', 'creado', '886', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32375', 'ingresos_requerimiento', 'creado', '887', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32376', 'ingresos_requerimiento', 'creado', '888', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32377', 'ingresos_requerimiento', 'creado', '889', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32378', 'ingresos_requerimiento', 'creado', '890', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32379', 'ingresos_requerimiento', 'creado', '891', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32380', 'ingresos_requerimiento', 'creado', '892', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32381', 'ingresos_requerimiento', 'creado', '893', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32382', 'ingresos_requerimiento', 'creado', '894', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32383', 'ingresos_requerimiento', 'creado', '895', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32384', 'ingresos_requerimiento', 'creado', '896', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32385', 'ingresos_requerimiento', 'creado', '897', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32386', 'ingresos_requerimiento', 'creado', '898', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32387', 'ingresos_requerimiento', 'creado', '899', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32388', 'ingresos_requerimiento', 'creado', '900', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32389', 'ingresos_requerimiento', 'creado', '901', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32390', 'ingresos_requerimiento', 'creado', '902', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32391', 'ingresos_requerimiento', 'creado', '903', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32392', 'ingresos_requerimiento', 'creado', '904', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32393', 'ingresos_requerimiento', 'creado', '905', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32394', 'ingresos_requerimiento', 'creado', '906', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32395', 'ingresos_requerimiento', 'creado', '907', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32396', 'ingresos_requerimiento', 'creado', '908', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32397', 'ingresos_requerimiento', 'creado', '909', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32398', 'ingresos_requerimiento', 'creado', '910', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32399', 'ingresos_requerimiento', 'creado', '911', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32400', 'ingresos_requerimiento', 'creado', '912', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32401', 'ingresos_requerimiento', 'creado', '913', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32402', 'ingresos_requerimiento', 'creado', '914', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32403', 'ingresos_requerimiento', 'creado', '915', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32404', 'ingresos_requerimiento', 'creado', '916', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32405', 'ingresos_requerimiento', 'creado', '917', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32406', 'ingresos_requerimiento', 'creado', '918', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32407', 'ingresos_requerimiento', 'creado', '919', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32408', 'ingresos_requerimiento', 'creado', '920', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32409', 'ingresos_requerimiento', 'creado', '921', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32410', 'ingresos_requerimiento', 'creado', '922', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32411', 'ingresos_requerimiento', 'creado', '923', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32412', 'ingresos_requerimiento', 'creado', '924', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32413', 'ingresos_requerimiento', 'creado', '925', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32414', 'ingresos_requerimiento', 'creado', '926', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32415', 'ingresos_requerimiento', 'creado', '927', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32416', 'ingresos_requerimiento', 'creado', '928', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32417', 'ingresos_requerimiento', 'creado', '929', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32418', 'ingresos_requerimiento', 'creado', '930', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32419', 'ingresos_requerimiento', 'creado', '931', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32420', 'ingresos_requerimiento', 'creado', '932', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32421', 'ingresos_requerimiento', 'creado', '933', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32422', 'ingresos_requerimiento', 'creado', '934', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32423', 'ingresos_requerimiento', 'creado', '935', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32424', 'ingresos_requerimiento', 'creado', '936', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32425', 'ingresos_requerimiento', 'creado', '937', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32426', 'ingresos_requerimiento', 'creado', '938', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32427', 'ingresos_requerimiento', 'creado', '939', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32428', 'ingresos_requerimiento', 'creado', '940', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32429', 'ingresos_requerimiento', 'creado', '941', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32430', 'ingresos_requerimiento', 'creado', '942', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32431', 'ingresos_requerimiento', 'creado', '943', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32432', 'ingresos_requerimiento', 'creado', '944', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32433', 'ingresos_requerimiento', 'creado', '945', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32434', 'ingresos_requerimiento', 'creado', '946', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32435', 'ingresos_requerimiento', 'creado', '947', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32436', 'ingresos_requerimiento', 'creado', '948', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32437', 'ingresos_requerimiento', 'creado', '949', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32438', 'ingresos_requerimiento', 'creado', '950', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32439', 'ingresos_requerimiento', 'creado', '951', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32440', 'ingresos_requerimiento', 'creado', '952', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32441', 'ingresos_requerimiento', 'creado', '953', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32442', 'ingresos_requerimiento', 'creado', '954', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32443', 'ingresos_requerimiento', 'creado', '955', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32444', 'ingresos_requerimiento', 'creado', '956', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32445', 'ingresos_requerimiento', 'creado', '957', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32446', 'ingresos_requerimiento', 'creado', '958', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32447', 'ingresos_requerimiento', 'creado', '959', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32448', 'ingresos_requerimiento', 'creado', '960', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32449', 'ingresos_requerimiento', 'creado', '961', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32450', 'ingresos_requerimiento', 'creado', '962', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32451', 'ingresos_requerimiento', 'creado', '963', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32452', 'ingresos_requerimiento', 'creado', '964', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32453', 'ingresos_requerimiento', 'creado', '965', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32454', 'ingresos_requerimiento', 'creado', '966', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32455', 'ingresos_requerimiento', 'creado', '967', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32456', 'ingresos_requerimiento', 'creado', '968', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32457', 'ingresos_requerimiento', 'creado', '969', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32458', 'ingresos_requerimiento', 'creado', '970', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32459', 'ingresos_requerimiento', 'creado', '971', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32460', 'ingresos_requerimiento', 'creado', '972', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32461', 'ingresos_requerimiento', 'creado', '973', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32462', 'ingresos_requerimiento', 'creado', '974', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32463', 'ingresos_requerimiento', 'creado', '975', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32464', 'ingresos_requerimiento', 'creado', '976', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32465', 'ingresos_requerimiento', 'creado', '977', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32466', 'ingresos_requerimiento', 'creado', '978', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32467', 'ingresos_requerimiento', 'creado', '979', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32468', 'ingresos_requerimiento', 'creado', '980', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32469', 'ingresos_requerimiento', 'creado', '981', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32470', 'ingresos_requerimiento', 'creado', '982', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32471', 'ingresos_requerimiento', 'creado', '983', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32472', 'ingresos_requerimiento', 'creado', '984', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32473', 'ingresos_requerimiento', 'creado', '985', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32474', 'ingresos_requerimiento', 'creado', '986', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32475', 'ingresos_requerimiento', 'creado', '987', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32476', 'ingresos_requerimiento', 'creado', '988', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32477', 'ingresos_requerimiento', 'creado', '989', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32478', 'ingresos_requerimiento', 'creado', '990', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32479', 'ingresos_requerimiento', 'creado', '991', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32480', 'ingresos_requerimiento', 'creado', '992', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32481', 'ingresos_requerimiento', 'creado', '993', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32482', 'ingresos_requerimiento', 'creado', '994', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32483', 'ingresos_requerimiento', 'creado', '995', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32484', 'ingresos_requerimiento', 'creado', '996', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32485', 'ingresos_requerimiento', 'creado', '997', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32486', 'ingresos_requerimiento', 'creado', '998', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32487', 'ingresos_requerimiento', 'creado', '999', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32488', 'ingresos_requerimiento', 'creado', '1000', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32489', 'ingresos_requerimiento', 'creado', '1001', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32490', 'ingresos_requerimiento', 'creado', '1002', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32491', 'ingresos_requerimiento', 'creado', '1003', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32492', 'ingresos_requerimiento', 'creado', '1004', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32493', 'ingresos_requerimiento', 'creado', '1005', 'mjgutierrez', '2017-03-17 13:41:51', '2017-03-17 13:41:51');
INSERT INTO `app_historico` VALUES ('32494', 'ingresos_rubros', 'actualizado', '5', 'mjgutierrez', '2017-03-17 13:41:57', '2017-03-17 13:41:57');
INSERT INTO `app_historico` VALUES ('32495', 'ingresos_requerimiento', 'creado', '1006', 'mjgutierrez', '2017-03-17 13:42:12', '2017-03-17 13:42:12');
INSERT INTO `app_historico` VALUES ('32496', 'ingresos_requerimiento', 'actualizado', '1006', 'mjgutierrez', '2017-03-17 13:45:40', '2017-03-17 13:45:40');
INSERT INTO `app_historico` VALUES ('32497', 'redes_alimentacion', 'actualizado', '3', 'mjgutierrez', '2017-03-17 13:46:55', '2017-03-17 13:46:55');
INSERT INTO `app_historico` VALUES ('32498', 'redes_alimentacion', 'actualizado', '3', 'mjgutierrez', '2017-03-17 13:47:00', '2017-03-17 13:47:00');
INSERT INTO `app_historico` VALUES ('32499', 'redes_alimentacion', 'creado', '4', 'mjgutierrez', '2017-03-17 13:47:04', '2017-03-17 13:47:04');
INSERT INTO `app_historico` VALUES ('32500', 'redes_alimentacion', 'eliminado', '4', 'mjgutierrez', '2017-03-17 13:47:10', '2017-03-17 13:47:10');
INSERT INTO `app_historico` VALUES ('32501', 'ingresos_categoria', 'eliminado', '4', 'mjgutierrez', '2017-03-17 13:47:30', '2017-03-17 13:47:30');
INSERT INTO `app_historico` VALUES ('32502', 'ingresos_categoria', 'creado', '5', 'mjgutierrez', '2017-03-17 13:48:21', '2017-03-17 13:48:21');
INSERT INTO `app_historico` VALUES ('32503', 'autenticacion', 'autenticacion', 'Clave:Frangel1233', 'frangel', '2017-03-17 14:20:48', '2017-03-17 14:20:48');
INSERT INTO `app_historico` VALUES ('32504', 'autenticacion', 'autenticacion', '', 'frangel', '2017-03-17 14:21:00', '2017-03-17 14:21:00');
INSERT INTO `app_historico` VALUES ('32505', 'app_usuario', 'actualizado', '2', 'frangel', '2017-03-17 14:22:07', '2017-03-17 14:22:07');
INSERT INTO `app_historico` VALUES ('32506', 'autenticacion', 'autenticacion', 'Clave:Ajbetancourt123', 'ajbetancourt', '2017-03-17 14:22:37', '2017-03-17 14:22:37');
INSERT INTO `app_historico` VALUES ('32507', 'autenticacion', 'autenticacion', 'Clave:Ajbetancourt2016', 'ajbetancourt', '2017-03-17 14:22:40', '2017-03-17 14:22:40');
INSERT INTO `app_historico` VALUES ('32508', 'autenticacion', 'autenticacion', '', 'rquintana', '2017-03-17 14:23:23', '2017-03-17 14:23:23');
INSERT INTO `app_historico` VALUES ('32509', 'app_usuario', 'actualizado', '20', 'rquintana', '2017-03-17 14:24:59', '2017-03-17 14:24:59');
INSERT INTO `app_historico` VALUES ('32510', 'app_usuario', 'actualizado', '20', 'rquintana', '2017-03-17 14:25:01', '2017-03-17 14:25:01');
INSERT INTO `app_historico` VALUES ('32511', 'autenticacion', 'autenticacion', '', 'rquintana', '2017-03-17 14:26:34', '2017-03-17 14:26:34');
INSERT INTO `app_historico` VALUES ('32512', 'app_usuario', 'actualizado', '20', 'rquintana', '2017-03-17 14:27:01', '2017-03-17 14:27:01');
INSERT INTO `app_historico` VALUES ('32513', 'app_usuario', 'actualizado', '20', 'rquintana', '2017-03-17 14:27:04', '2017-03-17 14:27:04');
INSERT INTO `app_historico` VALUES ('32514', 'autenticacion', 'autenticacion', '', 'rquintana', '2017-03-17 14:27:15', '2017-03-17 14:27:15');
INSERT INTO `app_historico` VALUES ('32515', 'autenticacion', 'autenticacion', '', 'lmata', '2017-03-17 14:33:38', '2017-03-17 14:33:38');
INSERT INTO `app_historico` VALUES ('32516', 'empresas', 'creado', '1', 'rquintana', '2017-03-17 14:36:15', '2017-03-17 14:36:15');
INSERT INTO `app_historico` VALUES ('32517', 'empresas', 'creado', '2', 'rquintana', '2017-03-17 14:36:30', '2017-03-17 14:36:30');
INSERT INTO `app_historico` VALUES ('32518', 'ingresos_rubros', 'creado', '6', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32519', 'ingresos_requerimiento', 'creado', '1007', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32520', 'ingresos_requerimiento', 'creado', '1008', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32521', 'ingresos_requerimiento', 'creado', '1009', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32522', 'ingresos_requerimiento', 'creado', '1010', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32523', 'ingresos_requerimiento', 'creado', '1011', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32524', 'ingresos_requerimiento', 'creado', '1012', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32525', 'ingresos_requerimiento', 'creado', '1013', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32526', 'ingresos_requerimiento', 'creado', '1014', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32527', 'ingresos_requerimiento', 'creado', '1015', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32528', 'ingresos_requerimiento', 'creado', '1016', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32529', 'ingresos_requerimiento', 'creado', '1017', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32530', 'ingresos_requerimiento', 'creado', '1018', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32531', 'ingresos_requerimiento', 'creado', '1019', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32532', 'ingresos_requerimiento', 'creado', '1020', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32533', 'ingresos_requerimiento', 'creado', '1021', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32534', 'ingresos_requerimiento', 'creado', '1022', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32535', 'ingresos_requerimiento', 'creado', '1023', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32536', 'ingresos_requerimiento', 'creado', '1024', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32537', 'ingresos_requerimiento', 'creado', '1025', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32538', 'ingresos_requerimiento', 'creado', '1026', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32539', 'ingresos_requerimiento', 'creado', '1027', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32540', 'ingresos_requerimiento', 'creado', '1028', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32541', 'ingresos_requerimiento', 'creado', '1029', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32542', 'ingresos_requerimiento', 'creado', '1030', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32543', 'ingresos_requerimiento', 'creado', '1031', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32544', 'ingresos_requerimiento', 'creado', '1032', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32545', 'ingresos_requerimiento', 'creado', '1033', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32546', 'ingresos_requerimiento', 'creado', '1034', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32547', 'ingresos_requerimiento', 'creado', '1035', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32548', 'ingresos_requerimiento', 'creado', '1036', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32549', 'ingresos_requerimiento', 'creado', '1037', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32550', 'ingresos_requerimiento', 'creado', '1038', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32551', 'ingresos_requerimiento', 'creado', '1039', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32552', 'ingresos_requerimiento', 'creado', '1040', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32553', 'ingresos_requerimiento', 'creado', '1041', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32554', 'ingresos_requerimiento', 'creado', '1042', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32555', 'ingresos_requerimiento', 'creado', '1043', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32556', 'ingresos_requerimiento', 'creado', '1044', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32557', 'ingresos_requerimiento', 'creado', '1045', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32558', 'ingresos_requerimiento', 'creado', '1046', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32559', 'ingresos_requerimiento', 'creado', '1047', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32560', 'ingresos_requerimiento', 'creado', '1048', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32561', 'ingresos_requerimiento', 'creado', '1049', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32562', 'ingresos_requerimiento', 'creado', '1050', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32563', 'ingresos_requerimiento', 'creado', '1051', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32564', 'ingresos_requerimiento', 'creado', '1052', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32565', 'ingresos_requerimiento', 'creado', '1053', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32566', 'ingresos_requerimiento', 'creado', '1054', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32567', 'ingresos_requerimiento', 'creado', '1055', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32568', 'ingresos_requerimiento', 'creado', '1056', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32569', 'ingresos_requerimiento', 'creado', '1057', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32570', 'ingresos_requerimiento', 'creado', '1058', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32571', 'ingresos_requerimiento', 'creado', '1059', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32572', 'ingresos_requerimiento', 'creado', '1060', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32573', 'ingresos_requerimiento', 'creado', '1061', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32574', 'ingresos_requerimiento', 'creado', '1062', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32575', 'ingresos_requerimiento', 'creado', '1063', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32576', 'ingresos_requerimiento', 'creado', '1064', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32577', 'ingresos_requerimiento', 'creado', '1065', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32578', 'ingresos_requerimiento', 'creado', '1066', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32579', 'ingresos_requerimiento', 'creado', '1067', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32580', 'ingresos_requerimiento', 'creado', '1068', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32581', 'ingresos_requerimiento', 'creado', '1069', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32582', 'ingresos_requerimiento', 'creado', '1070', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32583', 'ingresos_requerimiento', 'creado', '1071', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32584', 'ingresos_requerimiento', 'creado', '1072', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32585', 'ingresos_requerimiento', 'creado', '1073', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32586', 'ingresos_requerimiento', 'creado', '1074', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32587', 'ingresos_requerimiento', 'creado', '1075', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32588', 'ingresos_requerimiento', 'creado', '1076', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32589', 'ingresos_requerimiento', 'creado', '1077', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32590', 'ingresos_requerimiento', 'creado', '1078', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32591', 'ingresos_requerimiento', 'creado', '1079', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32592', 'ingresos_requerimiento', 'creado', '1080', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32593', 'ingresos_requerimiento', 'creado', '1081', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32594', 'ingresos_requerimiento', 'creado', '1082', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32595', 'ingresos_requerimiento', 'creado', '1083', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32596', 'ingresos_requerimiento', 'creado', '1084', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32597', 'ingresos_requerimiento', 'creado', '1085', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32598', 'ingresos_requerimiento', 'creado', '1086', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32599', 'ingresos_requerimiento', 'creado', '1087', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32600', 'ingresos_requerimiento', 'creado', '1088', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32601', 'ingresos_requerimiento', 'creado', '1089', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32602', 'ingresos_requerimiento', 'creado', '1090', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32603', 'ingresos_requerimiento', 'creado', '1091', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32604', 'ingresos_requerimiento', 'creado', '1092', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32605', 'ingresos_requerimiento', 'creado', '1093', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32606', 'ingresos_requerimiento', 'creado', '1094', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32607', 'ingresos_requerimiento', 'creado', '1095', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32608', 'ingresos_requerimiento', 'creado', '1096', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32609', 'ingresos_requerimiento', 'creado', '1097', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32610', 'ingresos_requerimiento', 'creado', '1098', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32611', 'ingresos_requerimiento', 'creado', '1099', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32612', 'ingresos_requerimiento', 'creado', '1100', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32613', 'ingresos_requerimiento', 'creado', '1101', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32614', 'ingresos_requerimiento', 'creado', '1102', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32615', 'ingresos_requerimiento', 'creado', '1103', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32616', 'ingresos_requerimiento', 'creado', '1104', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32617', 'ingresos_requerimiento', 'creado', '1105', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32618', 'ingresos_requerimiento', 'creado', '1106', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32619', 'ingresos_requerimiento', 'creado', '1107', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32620', 'ingresos_requerimiento', 'creado', '1108', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32621', 'ingresos_requerimiento', 'creado', '1109', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32622', 'ingresos_requerimiento', 'creado', '1110', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32623', 'ingresos_requerimiento', 'creado', '1111', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32624', 'ingresos_requerimiento', 'creado', '1112', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32625', 'ingresos_requerimiento', 'creado', '1113', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32626', 'ingresos_requerimiento', 'creado', '1114', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32627', 'ingresos_requerimiento', 'creado', '1115', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32628', 'ingresos_requerimiento', 'creado', '1116', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32629', 'ingresos_requerimiento', 'creado', '1117', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32630', 'ingresos_requerimiento', 'creado', '1118', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32631', 'ingresos_requerimiento', 'creado', '1119', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32632', 'ingresos_requerimiento', 'creado', '1120', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32633', 'ingresos_requerimiento', 'creado', '1121', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32634', 'ingresos_requerimiento', 'creado', '1122', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32635', 'ingresos_requerimiento', 'creado', '1123', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32636', 'ingresos_requerimiento', 'creado', '1124', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32637', 'ingresos_requerimiento', 'creado', '1125', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32638', 'ingresos_requerimiento', 'creado', '1126', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32639', 'ingresos_requerimiento', 'creado', '1127', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32640', 'ingresos_requerimiento', 'creado', '1128', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32641', 'ingresos_requerimiento', 'creado', '1129', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32642', 'ingresos_requerimiento', 'creado', '1130', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32643', 'ingresos_requerimiento', 'creado', '1131', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32644', 'ingresos_requerimiento', 'creado', '1132', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32645', 'ingresos_requerimiento', 'creado', '1133', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32646', 'ingresos_requerimiento', 'creado', '1134', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32647', 'ingresos_requerimiento', 'creado', '1135', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32648', 'ingresos_requerimiento', 'creado', '1136', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32649', 'ingresos_requerimiento', 'creado', '1137', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32650', 'ingresos_requerimiento', 'creado', '1138', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32651', 'ingresos_requerimiento', 'creado', '1139', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32652', 'ingresos_requerimiento', 'creado', '1140', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32653', 'ingresos_requerimiento', 'creado', '1141', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32654', 'ingresos_requerimiento', 'creado', '1142', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32655', 'ingresos_requerimiento', 'creado', '1143', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32656', 'ingresos_requerimiento', 'creado', '1144', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32657', 'ingresos_requerimiento', 'creado', '1145', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32658', 'ingresos_requerimiento', 'creado', '1146', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32659', 'ingresos_requerimiento', 'creado', '1147', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32660', 'ingresos_requerimiento', 'creado', '1148', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32661', 'ingresos_requerimiento', 'creado', '1149', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32662', 'ingresos_requerimiento', 'creado', '1150', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32663', 'ingresos_requerimiento', 'creado', '1151', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32664', 'ingresos_requerimiento', 'creado', '1152', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32665', 'ingresos_requerimiento', 'creado', '1153', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32666', 'ingresos_requerimiento', 'creado', '1154', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32667', 'ingresos_requerimiento', 'creado', '1155', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32668', 'ingresos_requerimiento', 'creado', '1156', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32669', 'ingresos_requerimiento', 'creado', '1157', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32670', 'ingresos_requerimiento', 'creado', '1158', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32671', 'ingresos_requerimiento', 'creado', '1159', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32672', 'ingresos_requerimiento', 'creado', '1160', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32673', 'ingresos_requerimiento', 'creado', '1161', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32674', 'ingresos_requerimiento', 'creado', '1162', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32675', 'ingresos_requerimiento', 'creado', '1163', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32676', 'ingresos_requerimiento', 'creado', '1164', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32677', 'ingresos_requerimiento', 'creado', '1165', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32678', 'ingresos_requerimiento', 'creado', '1166', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32679', 'ingresos_requerimiento', 'creado', '1167', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32680', 'ingresos_requerimiento', 'creado', '1168', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32681', 'ingresos_requerimiento', 'creado', '1169', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32682', 'ingresos_requerimiento', 'creado', '1170', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32683', 'ingresos_requerimiento', 'creado', '1171', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32684', 'ingresos_requerimiento', 'creado', '1172', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32685', 'ingresos_requerimiento', 'creado', '1173', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32686', 'ingresos_requerimiento', 'creado', '1174', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32687', 'ingresos_requerimiento', 'creado', '1175', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32688', 'ingresos_requerimiento', 'creado', '1176', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32689', 'ingresos_requerimiento', 'creado', '1177', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32690', 'ingresos_requerimiento', 'creado', '1178', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32691', 'ingresos_requerimiento', 'creado', '1179', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32692', 'ingresos_requerimiento', 'creado', '1180', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32693', 'ingresos_requerimiento', 'creado', '1181', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32694', 'ingresos_requerimiento', 'creado', '1182', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32695', 'ingresos_requerimiento', 'creado', '1183', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32696', 'ingresos_requerimiento', 'creado', '1184', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32697', 'ingresos_requerimiento', 'creado', '1185', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32698', 'ingresos_requerimiento', 'creado', '1186', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32699', 'ingresos_requerimiento', 'creado', '1187', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32700', 'ingresos_requerimiento', 'creado', '1188', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32701', 'ingresos_requerimiento', 'creado', '1189', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32702', 'ingresos_requerimiento', 'creado', '1190', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32703', 'ingresos_requerimiento', 'creado', '1191', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32704', 'ingresos_requerimiento', 'creado', '1192', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32705', 'ingresos_requerimiento', 'creado', '1193', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32706', 'ingresos_requerimiento', 'creado', '1194', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32707', 'ingresos_requerimiento', 'creado', '1195', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32708', 'ingresos_requerimiento', 'creado', '1196', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32709', 'ingresos_requerimiento', 'creado', '1197', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32710', 'ingresos_requerimiento', 'creado', '1198', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32711', 'ingresos_requerimiento', 'creado', '1199', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32712', 'ingresos_requerimiento', 'creado', '1200', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32713', 'ingresos_requerimiento', 'creado', '1201', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32714', 'ingresos_requerimiento', 'creado', '1202', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32715', 'ingresos_requerimiento', 'creado', '1203', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32716', 'ingresos_requerimiento', 'creado', '1204', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32717', 'ingresos_requerimiento', 'creado', '1205', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32718', 'ingresos_requerimiento', 'creado', '1206', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32719', 'ingresos_requerimiento', 'creado', '1207', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32720', 'ingresos_requerimiento', 'creado', '1208', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32721', 'ingresos_requerimiento', 'creado', '1209', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32722', 'ingresos_requerimiento', 'creado', '1210', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32723', 'ingresos_requerimiento', 'creado', '1211', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32724', 'ingresos_requerimiento', 'creado', '1212', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32725', 'ingresos_requerimiento', 'creado', '1213', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32726', 'ingresos_requerimiento', 'creado', '1214', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32727', 'ingresos_requerimiento', 'creado', '1215', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32728', 'ingresos_requerimiento', 'creado', '1216', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32729', 'ingresos_requerimiento', 'creado', '1217', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32730', 'ingresos_requerimiento', 'creado', '1218', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32731', 'ingresos_requerimiento', 'creado', '1219', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32732', 'ingresos_requerimiento', 'creado', '1220', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32733', 'ingresos_requerimiento', 'creado', '1221', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32734', 'ingresos_requerimiento', 'creado', '1222', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32735', 'ingresos_requerimiento', 'creado', '1223', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32736', 'ingresos_requerimiento', 'creado', '1224', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32737', 'ingresos_requerimiento', 'creado', '1225', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32738', 'ingresos_requerimiento', 'creado', '1226', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32739', 'ingresos_requerimiento', 'creado', '1227', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32740', 'ingresos_requerimiento', 'creado', '1228', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32741', 'ingresos_requerimiento', 'creado', '1229', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32742', 'ingresos_requerimiento', 'creado', '1230', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32743', 'ingresos_requerimiento', 'creado', '1231', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32744', 'ingresos_requerimiento', 'creado', '1232', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32745', 'ingresos_requerimiento', 'creado', '1233', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32746', 'ingresos_requerimiento', 'creado', '1234', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32747', 'ingresos_requerimiento', 'creado', '1235', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32748', 'ingresos_requerimiento', 'creado', '1236', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32749', 'ingresos_requerimiento', 'creado', '1237', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32750', 'ingresos_requerimiento', 'creado', '1238', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32751', 'ingresos_requerimiento', 'creado', '1239', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32752', 'ingresos_requerimiento', 'creado', '1240', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32753', 'ingresos_requerimiento', 'creado', '1241', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32754', 'ingresos_requerimiento', 'creado', '1242', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32755', 'ingresos_requerimiento', 'creado', '1243', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32756', 'ingresos_requerimiento', 'creado', '1244', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32757', 'ingresos_requerimiento', 'creado', '1245', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32758', 'ingresos_requerimiento', 'creado', '1246', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32759', 'ingresos_requerimiento', 'creado', '1247', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32760', 'ingresos_requerimiento', 'creado', '1248', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32761', 'ingresos_requerimiento', 'creado', '1249', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32762', 'ingresos_requerimiento', 'creado', '1250', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32763', 'ingresos_requerimiento', 'creado', '1251', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32764', 'ingresos_requerimiento', 'creado', '1252', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32765', 'ingresos_requerimiento', 'creado', '1253', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32766', 'ingresos_requerimiento', 'creado', '1254', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32767', 'ingresos_requerimiento', 'creado', '1255', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32768', 'ingresos_requerimiento', 'creado', '1256', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32769', 'ingresos_requerimiento', 'creado', '1257', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32770', 'ingresos_requerimiento', 'creado', '1258', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32771', 'ingresos_requerimiento', 'creado', '1259', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32772', 'ingresos_requerimiento', 'creado', '1260', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32773', 'ingresos_requerimiento', 'creado', '1261', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32774', 'ingresos_requerimiento', 'creado', '1262', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32775', 'ingresos_requerimiento', 'creado', '1263', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32776', 'ingresos_requerimiento', 'creado', '1264', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32777', 'ingresos_requerimiento', 'creado', '1265', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32778', 'ingresos_requerimiento', 'creado', '1266', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32779', 'ingresos_requerimiento', 'creado', '1267', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32780', 'ingresos_requerimiento', 'creado', '1268', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32781', 'ingresos_requerimiento', 'creado', '1269', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32782', 'ingresos_requerimiento', 'creado', '1270', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32783', 'ingresos_requerimiento', 'creado', '1271', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32784', 'ingresos_requerimiento', 'creado', '1272', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32785', 'ingresos_requerimiento', 'creado', '1273', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32786', 'ingresos_requerimiento', 'creado', '1274', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32787', 'ingresos_requerimiento', 'creado', '1275', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32788', 'ingresos_requerimiento', 'creado', '1276', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32789', 'ingresos_requerimiento', 'creado', '1277', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32790', 'ingresos_requerimiento', 'creado', '1278', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32791', 'ingresos_requerimiento', 'creado', '1279', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32792', 'ingresos_requerimiento', 'creado', '1280', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32793', 'ingresos_requerimiento', 'creado', '1281', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32794', 'ingresos_requerimiento', 'creado', '1282', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32795', 'ingresos_requerimiento', 'creado', '1283', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32796', 'ingresos_requerimiento', 'creado', '1284', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32797', 'ingresos_requerimiento', 'creado', '1285', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32798', 'ingresos_requerimiento', 'creado', '1286', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32799', 'ingresos_requerimiento', 'creado', '1287', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32800', 'ingresos_requerimiento', 'creado', '1288', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32801', 'ingresos_requerimiento', 'creado', '1289', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32802', 'ingresos_requerimiento', 'creado', '1290', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32803', 'ingresos_requerimiento', 'creado', '1291', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32804', 'ingresos_requerimiento', 'creado', '1292', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32805', 'ingresos_requerimiento', 'creado', '1293', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32806', 'ingresos_requerimiento', 'creado', '1294', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32807', 'ingresos_requerimiento', 'creado', '1295', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32808', 'ingresos_requerimiento', 'creado', '1296', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32809', 'ingresos_requerimiento', 'creado', '1297', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32810', 'ingresos_requerimiento', 'creado', '1298', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32811', 'ingresos_requerimiento', 'creado', '1299', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32812', 'ingresos_requerimiento', 'creado', '1300', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32813', 'ingresos_requerimiento', 'creado', '1301', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32814', 'ingresos_requerimiento', 'creado', '1302', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32815', 'ingresos_requerimiento', 'creado', '1303', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32816', 'ingresos_requerimiento', 'creado', '1304', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32817', 'ingresos_requerimiento', 'creado', '1305', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32818', 'ingresos_requerimiento', 'creado', '1306', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32819', 'ingresos_requerimiento', 'creado', '1307', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32820', 'ingresos_requerimiento', 'creado', '1308', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32821', 'ingresos_requerimiento', 'creado', '1309', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32822', 'ingresos_requerimiento', 'creado', '1310', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32823', 'ingresos_requerimiento', 'creado', '1311', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32824', 'ingresos_requerimiento', 'creado', '1312', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32825', 'ingresos_requerimiento', 'creado', '1313', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32826', 'ingresos_requerimiento', 'creado', '1314', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32827', 'ingresos_requerimiento', 'creado', '1315', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32828', 'ingresos_requerimiento', 'creado', '1316', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32829', 'ingresos_requerimiento', 'creado', '1317', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32830', 'ingresos_requerimiento', 'creado', '1318', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32831', 'ingresos_requerimiento', 'creado', '1319', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32832', 'ingresos_requerimiento', 'creado', '1320', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32833', 'ingresos_requerimiento', 'creado', '1321', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32834', 'ingresos_requerimiento', 'creado', '1322', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32835', 'ingresos_requerimiento', 'creado', '1323', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32836', 'ingresos_requerimiento', 'creado', '1324', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32837', 'ingresos_requerimiento', 'creado', '1325', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32838', 'ingresos_requerimiento', 'creado', '1326', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32839', 'ingresos_requerimiento', 'creado', '1327', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32840', 'ingresos_requerimiento', 'creado', '1328', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32841', 'ingresos_requerimiento', 'creado', '1329', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32842', 'ingresos_requerimiento', 'creado', '1330', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32843', 'ingresos_requerimiento', 'creado', '1331', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32844', 'ingresos_requerimiento', 'creado', '1332', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32845', 'ingresos_requerimiento', 'creado', '1333', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32846', 'ingresos_requerimiento', 'creado', '1334', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32847', 'ingresos_requerimiento', 'creado', '1335', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32848', 'ingresos_requerimiento', 'creado', '1336', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32849', 'ingresos_requerimiento', 'creado', '1337', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32850', 'ingresos_requerimiento', 'creado', '1338', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32851', 'ingresos_requerimiento', 'creado', '1339', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32852', 'ingresos_requerimiento', 'creado', '1340', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32853', 'ingresos_requerimiento', 'creado', '1341', 'rquintana', '2017-03-17 14:41:04', '2017-03-17 14:41:04');
INSERT INTO `app_historico` VALUES ('32854', 'ingresos_inventario', 'creado', '1', 'rquintana', '2017-03-17 14:43:09', '2017-03-17 14:43:09');
INSERT INTO `app_historico` VALUES ('32855', 'ingresos_inventario', 'actualizado', '1', 'rquintana', '2017-03-17 14:43:09', '2017-03-17 14:43:09');
INSERT INTO `app_historico` VALUES ('32856', 'ingresos_inventario', 'creado', '2', 'rquintana', '2017-03-17 14:43:09', '2017-03-17 14:43:09');
INSERT INTO `app_historico` VALUES ('32857', 'ingresos_inventario', 'actualizado', '2', 'rquintana', '2017-03-17 14:43:09', '2017-03-17 14:43:09');
INSERT INTO `app_historico` VALUES ('32858', 'ingresos_inventario', 'creado', '3', 'rquintana', '2017-03-17 14:43:09', '2017-03-17 14:43:09');
INSERT INTO `app_historico` VALUES ('32859', 'ingresos_inventario', 'actualizado', '3', 'rquintana', '2017-03-17 14:43:09', '2017-03-17 14:43:09');
INSERT INTO `app_historico` VALUES ('32860', 'ingresos_inventario', 'creado', '4', 'rquintana', '2017-03-17 14:43:09', '2017-03-17 14:43:09');
INSERT INTO `app_historico` VALUES ('32861', 'ingresos_inventario', 'actualizado', '4', 'rquintana', '2017-03-17 14:43:09', '2017-03-17 14:43:09');
INSERT INTO `app_historico` VALUES ('32862', 'ingresos_requerimiento', 'creado', '1342', 'rquintana', '2017-03-17 14:44:00', '2017-03-17 14:44:00');
INSERT INTO `app_historico` VALUES ('32863', 'ingresos_inventario', 'creado', '5', 'rquintana', '2017-03-17 14:44:39', '2017-03-17 14:44:39');
INSERT INTO `app_historico` VALUES ('32864', 'ingresos_inventario', 'actualizado', '5', 'rquintana', '2017-03-17 14:44:39', '2017-03-17 14:44:39');
INSERT INTO `app_historico` VALUES ('32865', 'ingresos_inventario', 'creado', '6', 'rquintana', '2017-03-17 14:44:39', '2017-03-17 14:44:39');
INSERT INTO `app_historico` VALUES ('32866', 'ingresos_inventario', 'actualizado', '6', 'rquintana', '2017-03-17 14:44:39', '2017-03-17 14:44:39');
INSERT INTO `app_historico` VALUES ('32867', 'ingresos_inventario', 'creado', '7', 'rquintana', '2017-03-17 14:44:39', '2017-03-17 14:44:39');
INSERT INTO `app_historico` VALUES ('32868', 'ingresos_inventario', 'actualizado', '7', 'rquintana', '2017-03-17 14:44:39', '2017-03-17 14:44:39');
INSERT INTO `app_historico` VALUES ('32869', 'ingresos_inventario', 'creado', '8', 'rquintana', '2017-03-17 14:44:39', '2017-03-17 14:44:39');
INSERT INTO `app_historico` VALUES ('32870', 'ingresos_inventario', 'actualizado', '8', 'rquintana', '2017-03-17 14:44:39', '2017-03-17 14:44:39');
INSERT INTO `app_historico` VALUES ('32871', 'ingresos_inventario', 'creado', '9', 'rquintana', '2017-03-17 14:44:50', '2017-03-17 14:44:50');
INSERT INTO `app_historico` VALUES ('32872', 'ingresos_inventario', 'actualizado', '9', 'rquintana', '2017-03-17 14:44:50', '2017-03-17 14:44:50');
INSERT INTO `app_historico` VALUES ('32873', 'ingresos_inventario', 'creado', '10', 'rquintana', '2017-03-17 14:44:50', '2017-03-17 14:44:50');
INSERT INTO `app_historico` VALUES ('32874', 'ingresos_inventario', 'actualizado', '10', 'rquintana', '2017-03-17 14:44:50', '2017-03-17 14:44:50');
INSERT INTO `app_historico` VALUES ('32875', 'ingresos_inventario', 'creado', '11', 'rquintana', '2017-03-17 14:44:50', '2017-03-17 14:44:50');
INSERT INTO `app_historico` VALUES ('32876', 'ingresos_inventario', 'actualizado', '11', 'rquintana', '2017-03-17 14:44:50', '2017-03-17 14:44:50');
INSERT INTO `app_historico` VALUES ('32877', 'ingresos_inventario', 'creado', '12', 'rquintana', '2017-03-17 14:44:50', '2017-03-17 14:44:50');
INSERT INTO `app_historico` VALUES ('32878', 'ingresos_inventario', 'actualizado', '12', 'rquintana', '2017-03-17 14:44:50', '2017-03-17 14:44:50');
INSERT INTO `app_historico` VALUES ('32879', 'autenticacion', 'autenticacion', '', 'ajbetancourt', '2017-03-17 14:45:15', '2017-03-17 14:45:15');
INSERT INTO `app_historico` VALUES ('32880', 'app_usuario', 'actualizado', '21', 'ajbetancourt', '2017-03-17 14:48:23', '2017-03-17 14:48:23');
INSERT INTO `app_historico` VALUES ('32881', 'autenticacion', 'autenticacion', '', 'ajbetancourt', '2017-03-17 14:48:37', '2017-03-17 14:48:37');
INSERT INTO `app_historico` VALUES ('32882', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-17 14:57:36', '2017-03-17 14:57:36');
INSERT INTO `app_historico` VALUES ('32883', 'app_usuario', 'actualizado', '19', 'mjgutierrez', '2017-03-17 14:59:59', '2017-03-17 14:59:59');
INSERT INTO `app_historico` VALUES ('32884', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-17 15:02:21', '2017-03-17 15:02:21');
INSERT INTO `app_historico` VALUES ('32885', 'app_usuario', 'actualizado', '19', 'mjgutierrez', '2017-03-17 15:07:53', '2017-03-17 15:07:53');
INSERT INTO `app_historico` VALUES ('32886', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-17 15:07:58', '2017-03-17 15:07:58');
INSERT INTO `app_historico` VALUES ('32887', 'app_usuario', 'actualizado', '19', 'mjgutierrez', '2017-03-17 15:10:02', '2017-03-17 15:10:02');
INSERT INTO `app_historico` VALUES ('32888', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-17 15:14:19', '2017-03-17 15:14:19');
INSERT INTO `app_historico` VALUES ('32889', 'app_usuario', 'actualizado', '19', 'mjgutierrez', '2017-03-17 15:19:02', '2017-03-17 15:19:02');
INSERT INTO `app_historico` VALUES ('32890', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-17 15:20:34', '2017-03-17 15:20:34');
INSERT INTO `app_historico` VALUES ('32891', 'app_usuario', 'actualizado', '19', 'mjgutierrez', '2017-03-17 15:22:11', '2017-03-17 15:22:11');
INSERT INTO `app_historico` VALUES ('32892', 'autenticacion', 'autenticacion', '', 'ftorres', '2017-03-17 15:23:42', '2017-03-17 15:23:42');
INSERT INTO `app_historico` VALUES ('32893', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-17 15:25:07', '2017-03-17 15:25:07');
INSERT INTO `app_historico` VALUES ('33060', 'app_usuario', 'actualizado', '23', 'lmata', '2017-03-17 15:30:48', '2017-03-17 15:30:48');
INSERT INTO `app_historico` VALUES ('33061', 'autenticacion', 'autenticacion', '', 'lmata', '2017-03-17 15:30:53', '2017-03-17 15:30:53');
INSERT INTO `app_historico` VALUES ('33228', 'app_usuario', 'actualizado', '20', 'rquintana', '2017-03-17 16:02:22', '2017-03-17 16:02:22');
INSERT INTO `app_historico` VALUES ('33229', 'app_usuario', 'actualizado', '19', 'mjgutierrez', '2017-03-17 16:32:23', '2017-03-17 16:32:23');
INSERT INTO `app_historico` VALUES ('33230', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-17 16:32:29', '2017-03-17 16:32:29');
INSERT INTO `app_historico` VALUES ('33231', 'sector', 'creado', '1', 'mjgutierrez', '2017-03-17 16:36:11', '2017-03-17 16:36:11');
INSERT INTO `app_historico` VALUES ('33232', 'autenticacion', 'autenticacion', 'Clave:Rquintana2016', 'rquintanaj', '2017-03-17 16:43:48', '2017-03-17 16:43:48');
INSERT INTO `app_historico` VALUES ('33233', 'autenticacion', 'autenticacion', '', 'rquintana', '2017-03-17 16:43:54', '2017-03-17 16:43:54');
INSERT INTO `app_historico` VALUES ('33234', 'app_usuario', 'actualizado', '20', 'rquintana', '2017-03-17 16:44:22', '2017-03-17 16:44:22');
INSERT INTO `app_historico` VALUES ('33235', 'app_usuario', 'actualizado', '19', 'mjgutierrez', '2017-03-17 16:52:18', '2017-03-17 16:52:18');
INSERT INTO `app_historico` VALUES ('33236', 'autenticacion', 'autenticacion', '', 'frangel', '2017-03-17 19:51:43', '2017-03-17 19:51:43');
INSERT INTO `app_historico` VALUES ('33237', 'app_usuario', 'actualizado', '2', 'frangel', '2017-03-17 19:52:36', '2017-03-17 19:52:36');
INSERT INTO `app_historico` VALUES ('33238', 'autenticacion', 'autenticacion', 'Clave:Equintana2016', 'rquintana', '2017-03-17 22:22:33', '2017-03-17 22:22:33');
INSERT INTO `app_historico` VALUES ('33239', 'autenticacion', 'autenticacion', 'Clave:Equintana2016', 'rquintana', '2017-03-17 22:22:36', '2017-03-17 22:22:36');
INSERT INTO `app_historico` VALUES ('33240', 'autenticacion', 'autenticacion', '', 'rquintana', '2017-03-17 22:22:54', '2017-03-17 22:22:54');
INSERT INTO `app_historico` VALUES ('33241', 'app_usuario', 'actualizado', '20', 'rquintana', '2017-03-17 22:30:52', '2017-03-17 22:30:52');
INSERT INTO `app_historico` VALUES ('33242', 'autenticacion', 'autenticacion', 'Clave:Rquinatana2016', 'rquintana', '2017-03-19 20:50:50', '2017-03-19 20:50:50');
INSERT INTO `app_historico` VALUES ('33243', 'autenticacion', 'autenticacion', 'Clave:Rquinatana2016', 'rquintana', '2017-03-19 20:51:17', '2017-03-19 20:51:17');
INSERT INTO `app_historico` VALUES ('33244', 'autenticacion', 'autenticacion', '', 'rquintana', '2017-03-19 20:51:44', '2017-03-19 20:51:44');
INSERT INTO `app_historico` VALUES ('33245', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-19 21:13:16', '2017-03-19 21:13:16');
INSERT INTO `app_historico` VALUES ('33246', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-19 21:13:17', '2017-03-19 21:13:17');
INSERT INTO `app_historico` VALUES ('33247', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-19 21:13:26', '2017-03-19 21:13:26');
INSERT INTO `app_historico` VALUES ('33248', 'autenticacion', 'autenticacion', '', 'rquintana', '2017-03-19 21:25:19', '2017-03-19 21:25:19');
INSERT INTO `app_historico` VALUES ('33249', 'app_perfil', 'actualizado', '4', 'mjgutierrez', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_historico` VALUES ('33250', 'app_perfil', 'actualizado', '2', 'mjgutierrez', '2017-03-19 21:26:46', '2017-03-19 21:26:46');
INSERT INTO `app_historico` VALUES ('33251', 'app_perfil', 'actualizado', '3', 'mjgutierrez', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_historico` VALUES ('33252', 'app_perfil', 'actualizado', '7', 'mjgutierrez', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_historico` VALUES ('33253', 'app_perfil', 'actualizado', '1', 'mjgutierrez', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_historico` VALUES ('33254', 'app_usuario', 'actualizado', '19', 'mjgutierrez', '2017-03-19 21:27:24', '2017-03-19 21:27:24');
INSERT INTO `app_historico` VALUES ('33266', 'autenticacion', 'autenticacion', '', 'rquintana', '2017-03-20 00:08:45', '2017-03-20 00:08:45');
INSERT INTO `app_historico` VALUES ('33267', 'autenticacion', 'autenticacion', '', 'rquintana', '2017-03-20 09:00:04', '2017-03-20 09:00:04');
INSERT INTO `app_historico` VALUES ('33268', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-20 09:00:21', '2017-03-20 09:00:21');
INSERT INTO `app_historico` VALUES ('33269', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-20 09:23:13', '2017-03-20 09:23:13');
INSERT INTO `app_historico` VALUES ('33270', 'autenticacion', 'autenticacion', '', 'rquintana', '2017-03-20 09:35:01', '2017-03-20 09:35:01');
INSERT INTO `app_historico` VALUES ('33271', 'app_usuario', 'actualizado', '19', 'mjgutierrez', '2017-03-20 09:36:41', '2017-03-20 09:36:41');
INSERT INTO `app_historico` VALUES ('33272', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-20 09:37:05', '2017-03-20 09:37:05');
INSERT INTO `app_historico` VALUES ('33273', 'autenticacion', 'autenticacion', '', 'ajbetancourt', '2017-03-20 09:51:29', '2017-03-20 09:51:29');
INSERT INTO `app_historico` VALUES ('33274', 'app_usuario', 'actualizado', '20', 'rquintana', '2017-03-20 10:48:33', '2017-03-20 10:48:33');
INSERT INTO `app_historico` VALUES ('33275', 'autenticacion', 'autenticacion', '', 'ftorres', '2017-03-20 10:48:46', '2017-03-20 10:48:46');
INSERT INTO `app_historico` VALUES ('33276', 'inventario', 'creado', '1', 'ftorres', '2017-03-20 11:02:31', '2017-03-20 11:02:31');
INSERT INTO `app_historico` VALUES ('33277', 'inventario', 'creado', '2', 'ftorres', '2017-03-20 11:02:31', '2017-03-20 11:02:31');
INSERT INTO `app_historico` VALUES ('33278', 'inventario', 'creado', '3', 'ftorres', '2017-03-20 11:02:31', '2017-03-20 11:02:31');
INSERT INTO `app_historico` VALUES ('33279', 'inventario', 'creado', '4', 'ftorres', '2017-03-20 11:02:31', '2017-03-20 11:02:31');
INSERT INTO `app_historico` VALUES ('33280', 'inventario', 'creado', '5', 'ftorres', '2017-03-20 11:02:31', '2017-03-20 11:02:31');
INSERT INTO `app_historico` VALUES ('33281', 'inventario', 'creado', '6', 'ftorres', '2017-03-20 11:02:31', '2017-03-20 11:02:31');
INSERT INTO `app_historico` VALUES ('33282', 'inventario', 'creado', '7', 'ftorres', '2017-03-20 11:02:31', '2017-03-20 11:02:31');
INSERT INTO `app_historico` VALUES ('33283', 'inventario', 'creado', '8', 'ftorres', '2017-03-20 11:02:31', '2017-03-20 11:02:31');
INSERT INTO `app_historico` VALUES ('33284', 'inventario', 'creado', '9', 'ftorres', '2017-03-20 11:02:31', '2017-03-20 11:02:31');
INSERT INTO `app_historico` VALUES ('33285', 'inventario', 'creado', '10', 'ftorres', '2017-03-20 11:02:31', '2017-03-20 11:02:31');
INSERT INTO `app_historico` VALUES ('33286', 'inventario', 'creado', '11', 'ftorres', '2017-03-20 11:02:31', '2017-03-20 11:02:31');
INSERT INTO `app_historico` VALUES ('33287', 'inventario', 'creado', '12', 'ftorres', '2017-03-20 11:02:31', '2017-03-20 11:02:31');
INSERT INTO `app_historico` VALUES ('33288', 'inventario', 'creado', '13', 'ftorres', '2017-03-20 11:02:31', '2017-03-20 11:02:31');
INSERT INTO `app_historico` VALUES ('33289', 'inventario', 'creado', '14', 'ftorres', '2017-03-20 11:02:31', '2017-03-20 11:02:31');
INSERT INTO `app_historico` VALUES ('33290', 'inventario', 'creado', '15', 'ftorres', '2017-03-20 11:02:31', '2017-03-20 11:02:31');
INSERT INTO `app_historico` VALUES ('33291', 'inventario', 'creado', '16', 'ftorres', '2017-03-20 11:02:31', '2017-03-20 11:02:31');
INSERT INTO `app_historico` VALUES ('33292', 'inventario', 'creado', '17', 'ftorres', '2017-03-20 11:02:31', '2017-03-20 11:02:31');
INSERT INTO `app_historico` VALUES ('33293', 'inventario', 'creado', '18', 'ftorres', '2017-03-20 11:02:31', '2017-03-20 11:02:31');
INSERT INTO `app_historico` VALUES ('33294', 'inventario', 'creado', '19', 'ftorres', '2017-03-20 11:02:31', '2017-03-20 11:02:31');
INSERT INTO `app_historico` VALUES ('33295', 'inventario', 'creado', '20', 'ftorres', '2017-03-20 11:02:31', '2017-03-20 11:02:31');
INSERT INTO `app_historico` VALUES ('33296', 'inventario', 'creado', '21', 'ftorres', '2017-03-20 11:02:31', '2017-03-20 11:02:31');
INSERT INTO `app_historico` VALUES ('33297', 'inventario', 'creado', '22', 'ftorres', '2017-03-20 11:02:31', '2017-03-20 11:02:31');
INSERT INTO `app_historico` VALUES ('33298', 'inventario', 'creado', '23', 'ftorres', '2017-03-20 11:02:31', '2017-03-20 11:02:31');
INSERT INTO `app_historico` VALUES ('33299', 'inventario', 'creado', '24', 'ftorres', '2017-03-20 11:02:32', '2017-03-20 11:02:32');
INSERT INTO `app_historico` VALUES ('33300', 'inventario', 'creado', '25', 'ftorres', '2017-03-20 11:02:32', '2017-03-20 11:02:32');
INSERT INTO `app_historico` VALUES ('33301', 'inventario', 'creado', '26', 'ftorres', '2017-03-20 11:02:32', '2017-03-20 11:02:32');
INSERT INTO `app_historico` VALUES ('33302', 'inventario', 'creado', '27', 'ftorres', '2017-03-20 11:02:32', '2017-03-20 11:02:32');
INSERT INTO `app_historico` VALUES ('33303', 'inventario', 'creado', '28', 'ftorres', '2017-03-20 11:02:32', '2017-03-20 11:02:32');
INSERT INTO `app_historico` VALUES ('33304', 'inventario', 'creado', '29', 'ftorres', '2017-03-20 11:02:32', '2017-03-20 11:02:32');
INSERT INTO `app_historico` VALUES ('33305', 'inventario', 'creado', '30', 'ftorres', '2017-03-20 11:02:32', '2017-03-20 11:02:32');
INSERT INTO `app_historico` VALUES ('33306', 'inventario', 'creado', '31', 'ftorres', '2017-03-20 11:02:32', '2017-03-20 11:02:32');
INSERT INTO `app_historico` VALUES ('33307', 'inventario', 'creado', '32', 'ftorres', '2017-03-20 11:02:32', '2017-03-20 11:02:32');
INSERT INTO `app_historico` VALUES ('33308', 'inventario', 'creado', '33', 'ftorres', '2017-03-20 11:02:32', '2017-03-20 11:02:32');
INSERT INTO `app_historico` VALUES ('33309', 'inventario', 'creado', '34', 'ftorres', '2017-03-20 11:02:32', '2017-03-20 11:02:32');
INSERT INTO `app_historico` VALUES ('33310', 'inventario', 'creado', '35', 'ftorres', '2017-03-20 11:02:32', '2017-03-20 11:02:32');
INSERT INTO `app_historico` VALUES ('33311', 'inventario', 'creado', '36', 'ftorres', '2017-03-20 11:02:32', '2017-03-20 11:02:32');
INSERT INTO `app_historico` VALUES ('33312', 'inventario', 'creado', '37', 'ftorres', '2017-03-20 11:02:32', '2017-03-20 11:02:32');
INSERT INTO `app_historico` VALUES ('33313', 'inventario', 'creado', '38', 'ftorres', '2017-03-20 11:02:32', '2017-03-20 11:02:32');
INSERT INTO `app_historico` VALUES ('33314', 'inventario', 'creado', '39', 'ftorres', '2017-03-20 11:02:32', '2017-03-20 11:02:32');
INSERT INTO `app_historico` VALUES ('33315', 'inventario', 'creado', '40', 'ftorres', '2017-03-20 11:02:32', '2017-03-20 11:02:32');
INSERT INTO `app_historico` VALUES ('33316', 'inventario', 'creado', '41', 'ftorres', '2017-03-20 11:02:32', '2017-03-20 11:02:32');
INSERT INTO `app_historico` VALUES ('33317', 'inventario', 'creado', '42', 'ftorres', '2017-03-20 11:02:32', '2017-03-20 11:02:32');
INSERT INTO `app_historico` VALUES ('33318', 'inventario', 'creado', '43', 'ftorres', '2017-03-20 11:02:32', '2017-03-20 11:02:32');
INSERT INTO `app_historico` VALUES ('33319', 'inventario', 'creado', '44', 'ftorres', '2017-03-20 11:02:32', '2017-03-20 11:02:32');
INSERT INTO `app_historico` VALUES ('33320', 'inventario', 'creado', '45', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33321', 'inventario', 'creado', '46', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33322', 'inventario', 'creado', '47', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33323', 'inventario', 'creado', '48', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33324', 'inventario', 'creado', '49', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33325', 'inventario', 'creado', '50', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33326', 'inventario', 'creado', '51', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33327', 'inventario', 'creado', '52', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33328', 'inventario', 'creado', '53', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33329', 'inventario', 'creado', '54', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33330', 'inventario', 'creado', '55', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33331', 'inventario', 'creado', '56', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33332', 'inventario', 'creado', '57', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33333', 'inventario', 'creado', '58', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33334', 'inventario', 'creado', '59', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33335', 'inventario', 'creado', '60', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33336', 'inventario', 'creado', '61', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33337', 'inventario', 'creado', '62', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33338', 'inventario', 'creado', '63', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33339', 'inventario', 'creado', '64', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33340', 'inventario', 'creado', '65', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33341', 'inventario', 'creado', '66', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33342', 'inventario', 'creado', '67', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33343', 'inventario', 'creado', '68', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33344', 'inventario', 'creado', '69', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33345', 'inventario', 'creado', '70', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33346', 'inventario', 'creado', '71', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33347', 'inventario', 'creado', '72', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33348', 'inventario', 'creado', '73', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33349', 'inventario', 'creado', '74', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33350', 'inventario', 'creado', '75', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33351', 'inventario', 'creado', '76', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33352', 'inventario', 'creado', '77', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33353', 'inventario', 'creado', '78', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33354', 'inventario', 'creado', '79', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33355', 'inventario', 'creado', '80', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33356', 'inventario', 'creado', '81', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33357', 'inventario', 'creado', '82', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33358', 'inventario', 'creado', '83', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33359', 'inventario', 'creado', '84', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33360', 'inventario', 'creado', '85', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33361', 'inventario', 'creado', '86', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33362', 'inventario', 'creado', '87', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33363', 'inventario', 'creado', '88', 'ftorres', '2017-03-20 11:03:52', '2017-03-20 11:03:52');
INSERT INTO `app_historico` VALUES ('33364', 'inventario', 'creado', '89', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33365', 'inventario', 'creado', '90', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33366', 'inventario', 'creado', '91', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33367', 'inventario', 'creado', '92', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33368', 'inventario', 'creado', '93', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33369', 'inventario', 'creado', '94', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33370', 'inventario', 'creado', '95', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33371', 'inventario', 'creado', '96', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33372', 'inventario', 'creado', '97', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33373', 'inventario', 'creado', '98', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33374', 'inventario', 'creado', '99', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33375', 'inventario', 'creado', '100', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33376', 'inventario', 'creado', '101', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33377', 'inventario', 'creado', '102', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33378', 'inventario', 'creado', '103', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33379', 'inventario', 'creado', '104', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33380', 'inventario', 'creado', '105', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33381', 'inventario', 'creado', '106', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33382', 'inventario', 'creado', '107', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33383', 'inventario', 'creado', '108', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33384', 'inventario', 'creado', '109', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33385', 'inventario', 'creado', '110', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33386', 'inventario', 'creado', '111', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33387', 'inventario', 'creado', '112', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33388', 'inventario', 'creado', '113', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33389', 'inventario', 'creado', '114', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33390', 'inventario', 'creado', '115', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33391', 'inventario', 'creado', '116', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33392', 'inventario', 'creado', '117', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33393', 'inventario', 'creado', '118', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33394', 'inventario', 'creado', '119', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33395', 'inventario', 'creado', '120', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33396', 'inventario', 'creado', '121', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33397', 'inventario', 'creado', '122', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33398', 'inventario', 'creado', '123', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33399', 'inventario', 'creado', '124', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33400', 'inventario', 'creado', '125', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33401', 'inventario', 'creado', '126', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33402', 'inventario', 'creado', '127', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33403', 'inventario', 'creado', '128', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33404', 'inventario', 'creado', '129', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33405', 'inventario', 'creado', '130', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33406', 'inventario', 'creado', '131', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33407', 'inventario', 'creado', '132', 'ftorres', '2017-03-20 11:04:14', '2017-03-20 11:04:14');
INSERT INTO `app_historico` VALUES ('33408', 'autenticacion', 'autenticacion', '', 'rquintana', '2017-03-20 11:34:54', '2017-03-20 11:34:54');
INSERT INTO `app_historico` VALUES ('33409', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-20 11:40:22', '2017-03-20 11:40:22');
INSERT INTO `app_historico` VALUES ('33410', 'app_usuario', 'actualizado', '20', 'rquintana', '2017-03-20 11:47:04', '2017-03-20 11:47:04');
INSERT INTO `app_historico` VALUES ('33411', 'autenticacion', 'autenticacion', '', 'rquintana', '2017-03-20 11:48:07', '2017-03-20 11:48:07');
INSERT INTO `app_historico` VALUES ('33412', 'app_usuario', 'actualizado', '19', 'mjgutierrez', '2017-03-20 11:55:11', '2017-03-20 11:55:11');
INSERT INTO `app_historico` VALUES ('33413', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-20 11:55:20', '2017-03-20 11:55:20');
INSERT INTO `app_historico` VALUES ('33414', 'autenticacion', 'autenticacion', '', 'ftorres', '2017-03-20 11:56:29', '2017-03-20 11:56:29');
INSERT INTO `app_historico` VALUES ('33415', 'app_usuario', 'actualizado', '19', 'mjgutierrez', '2017-03-20 12:02:46', '2017-03-20 12:02:46');
INSERT INTO `app_historico` VALUES ('33416', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-20 12:02:49', '2017-03-20 12:02:49');
INSERT INTO `app_historico` VALUES ('33417', 'sector', 'creado', '2', 'mjgutierrez', '2017-03-20 12:44:53', '2017-03-20 12:44:53');
INSERT INTO `app_historico` VALUES ('33418', 'clap', 'creado', '1', 'mjgutierrez', '2017-03-20 12:49:37', '2017-03-20 12:49:37');
INSERT INTO `app_historico` VALUES ('33419', 'clap_contacto', 'creado', '0', 'mjgutierrez', '2017-03-20 12:49:37', '2017-03-20 12:49:37');
INSERT INTO `app_historico` VALUES ('33426', 'resumen', 'creado', '20', 'mjgutierrez', '2017-03-20 13:24:07', '2017-03-20 13:24:07');
INSERT INTO `app_historico` VALUES ('33427', 'resumen_distribucion', 'creado', '331', 'mjgutierrez', '2017-03-20 13:24:07', '2017-03-20 13:24:07');
INSERT INTO `app_historico` VALUES ('33428', 'resumen_distribucion', 'creado', '332', 'mjgutierrez', '2017-03-20 13:24:07', '2017-03-20 13:24:07');
INSERT INTO `app_historico` VALUES ('33429', 'resumen_distribucion', 'creado', '333', 'mjgutierrez', '2017-03-20 13:24:07', '2017-03-20 13:24:07');
INSERT INTO `app_historico` VALUES ('33430', 'resumen_distribucion', 'creado', '334', 'mjgutierrez', '2017-03-20 13:24:07', '2017-03-20 13:24:07');
INSERT INTO `app_historico` VALUES ('33431', 'resumen_distribucion_empresas', 'creado', '1', 'mjgutierrez', '2017-03-20 13:24:07', '2017-03-20 13:24:07');
INSERT INTO `app_historico` VALUES ('33432', 'resumen_distribucion_empresas', 'creado', '2', 'mjgutierrez', '2017-03-20 13:24:07', '2017-03-20 13:24:07');
INSERT INTO `app_historico` VALUES ('33433', 'resumen', 'creado', '21', 'mjgutierrez', '2017-03-20 13:24:56', '2017-03-20 13:24:56');
INSERT INTO `app_historico` VALUES ('33434', 'resumen_distribucion', 'creado', '335', 'mjgutierrez', '2017-03-20 13:24:56', '2017-03-20 13:24:56');
INSERT INTO `app_historico` VALUES ('33435', 'resumen_distribucion', 'creado', '336', 'mjgutierrez', '2017-03-20 13:24:56', '2017-03-20 13:24:56');
INSERT INTO `app_historico` VALUES ('33436', 'resumen_distribucion', 'creado', '337', 'mjgutierrez', '2017-03-20 13:24:56', '2017-03-20 13:24:56');
INSERT INTO `app_historico` VALUES ('33437', 'resumen_distribucion', 'creado', '338', 'mjgutierrez', '2017-03-20 13:24:56', '2017-03-20 13:24:56');
INSERT INTO `app_historico` VALUES ('33438', 'resumen_distribucion_empresas', 'creado', '3', 'mjgutierrez', '2017-03-20 13:24:56', '2017-03-20 13:24:56');
INSERT INTO `app_historico` VALUES ('33439', 'resumen_distribucion_empresas', 'creado', '4', 'mjgutierrez', '2017-03-20 13:24:56', '2017-03-20 13:24:56');
INSERT INTO `app_historico` VALUES ('33440', 'autenticacion', 'autenticacion', '', 'lmata', '2017-03-20 13:41:08', '2017-03-20 13:41:08');
INSERT INTO `app_historico` VALUES ('33441', 'autenticacion', 'autenticacion', 'Clave:Mg7857172', 'mjgutierrez', '2017-03-20 13:51:48', '2017-03-20 13:51:48');
INSERT INTO `app_historico` VALUES ('33442', 'autenticacion', 'autenticacion', 'Clave:Mg7857172', 'mjgutierrez', '2017-03-20 13:51:53', '2017-03-20 13:51:53');
INSERT INTO `app_historico` VALUES ('33443', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-20 13:51:59', '2017-03-20 13:51:59');
INSERT INTO `app_historico` VALUES ('33444', 'app_usuario', 'actualizado', '19', 'mjgutierrez', '2017-03-20 13:52:51', '2017-03-20 13:52:51');
INSERT INTO `app_historico` VALUES ('33445', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-20 14:26:10', '2017-03-20 14:26:10');
INSERT INTO `app_historico` VALUES ('33446', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-20 14:30:33', '2017-03-20 14:30:33');
INSERT INTO `app_historico` VALUES ('33447', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-20 15:41:55', '2017-03-20 15:41:55');
INSERT INTO `app_historico` VALUES ('33448', 'autenticacion', 'autenticacion', '', 'rquintana', '2017-03-20 16:09:35', '2017-03-20 16:09:35');
INSERT INTO `app_historico` VALUES ('33449', 'autenticacion', 'autenticacion', '', 'lmata', '2017-03-20 16:09:56', '2017-03-20 16:09:56');
INSERT INTO `app_historico` VALUES ('33450', 'resumen', 'creado', '22', 'rquintana', '2017-03-20 16:50:56', '2017-03-20 16:50:56');
INSERT INTO `app_historico` VALUES ('33451', 'resumen_distribucion', 'creado', '339', 'rquintana', '2017-03-20 16:50:56', '2017-03-20 16:50:56');
INSERT INTO `app_historico` VALUES ('33452', 'resumen_distribucion', 'creado', '340', 'rquintana', '2017-03-20 16:50:56', '2017-03-20 16:50:56');
INSERT INTO `app_historico` VALUES ('33453', 'resumen_distribucion', 'creado', '341', 'rquintana', '2017-03-20 16:50:56', '2017-03-20 16:50:56');
INSERT INTO `app_historico` VALUES ('33454', 'resumen_distribucion', 'creado', '342', 'rquintana', '2017-03-20 16:50:57', '2017-03-20 16:50:57');
INSERT INTO `app_historico` VALUES ('33455', 'resumen_distribucion_empresas', 'creado', '5', 'rquintana', '2017-03-20 16:50:57', '2017-03-20 16:50:57');
INSERT INTO `app_historico` VALUES ('33456', 'resumen_distribucion_empresas', 'creado', '6', 'rquintana', '2017-03-20 16:50:57', '2017-03-20 16:50:57');
INSERT INTO `app_historico` VALUES ('33457', 'categorias', 'creado', '9', 'mjgutierrez', '2017-03-20 16:54:02', '2017-03-20 16:54:02');
INSERT INTO `app_historico` VALUES ('33458', 'autenticacion', 'autenticacion', '', 'rquintana', '2017-03-21 08:03:00', '2017-03-21 08:03:00');
INSERT INTO `app_historico` VALUES ('33459', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-21 10:42:27', '2017-03-21 10:42:27');
INSERT INTO `app_historico` VALUES ('33460', 'autenticacion', 'autenticacion', '', 'ftorres', '2017-03-21 14:55:38', '2017-03-21 14:55:38');
INSERT INTO `app_historico` VALUES ('33461', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-21 15:22:22', '2017-03-21 15:22:22');
INSERT INTO `app_historico` VALUES ('33462', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-22 08:31:53', '2017-03-22 08:31:53');
INSERT INTO `app_historico` VALUES ('33463', 'autenticacion', 'autenticacion', '', 'amendez', '2017-03-22 09:04:01', '2017-03-22 09:04:01');
INSERT INTO `app_historico` VALUES ('33464', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-22 09:25:53', '2017-03-22 09:25:53');
INSERT INTO `app_historico` VALUES ('33465', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-22 15:23:16', '2017-03-22 15:23:16');
INSERT INTO `app_historico` VALUES ('33466', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-23 19:51:28', '2017-03-23 19:51:28');
INSERT INTO `app_historico` VALUES ('33467', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-24 07:41:44', '2017-03-24 07:41:44');
INSERT INTO `app_historico` VALUES ('33468', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-24 08:14:48', '2017-03-24 08:14:48');
INSERT INTO `app_historico` VALUES ('33469', 'autenticacion', 'autenticacion', 'Clave:Mg12345678', 'mjgutierrez', '2017-03-24 10:05:02', '2017-03-24 10:05:02');
INSERT INTO `app_historico` VALUES ('33470', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-24 10:05:07', '2017-03-24 10:05:07');
INSERT INTO `app_historico` VALUES ('33471', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-24 11:53:06', '2017-03-24 11:53:06');
INSERT INTO `app_historico` VALUES ('33472', 'autenticacion', 'autenticacion', 'Clave:Mg7857172', 'mjgutierrez', '2017-03-28 13:43:49', '2017-03-28 13:43:49');
INSERT INTO `app_historico` VALUES ('33473', 'autenticacion', 'autenticacion', 'Clave:Mg7857172', 'mjgutierrez', '2017-03-28 13:43:53', '2017-03-28 13:43:53');
INSERT INTO `app_historico` VALUES ('33474', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-03-28 13:43:58', '2017-03-28 13:43:58');
INSERT INTO `app_historico` VALUES ('33475', 'app_usuario', 'actualizado', '19', 'mjgutierrez', '2017-03-28 13:44:20', '2017-03-28 13:44:20');
INSERT INTO `app_historico` VALUES ('33476', 'autenticacion', 'autenticacion', '', 'ftorres', '2017-03-29 10:48:10', '2017-03-29 10:48:10');
INSERT INTO `app_historico` VALUES ('33477', 'app_usuario', 'actualizado', '3', 'ftorres', '2017-03-29 10:56:24', '2017-03-29 10:56:24');
INSERT INTO `app_historico` VALUES ('33478', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-05-05 09:26:29', '2017-05-05 09:26:29');
INSERT INTO `app_historico` VALUES ('33479', 'app_usuario', 'actualizado', '19', 'mjgutierrez', '2017-05-05 09:26:36', '2017-05-05 09:26:36');
INSERT INTO `app_historico` VALUES ('33480', 'autenticacion', 'autenticacion', 'Clave:admin', 'admin', '2017-09-07 11:24:16', '2017-09-07 11:24:16');
INSERT INTO `app_historico` VALUES ('33481', 'autenticacion', 'autenticacion', 'Clave:Mg7857172', 'admin', '2017-09-07 11:24:20', '2017-09-07 11:24:20');
INSERT INTO `app_historico` VALUES ('33482', 'autenticacion', 'autenticacion', '', 'mjgutierrez', '2017-09-07 11:24:25', '2017-09-07 11:24:25');
INSERT INTO `app_historico` VALUES ('33483', 'app_usuario', 'actualizado', '19', 'mjgutierrez', '2017-09-07 11:27:16', '2017-09-07 11:27:16');

-- ----------------------------
-- Table structure for app_menu
-- ----------------------------
DROP TABLE IF EXISTS `app_menu`;
CREATE TABLE `app_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `padre` int(10) unsigned NOT NULL,
  `posicion` int(10) unsigned NOT NULL,
  `direccion` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `icono` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_menu_direccion_unique` (`direccion`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of app_menu
-- ----------------------------
INSERT INTO `app_menu` VALUES ('1', 'Administrador', '0', '0', '#Administrador', 'fa fa-gear', '2016-07-02 06:06:37', '2016-07-02 06:06:37', null);
INSERT INTO `app_menu` VALUES ('2', 'Menu', '1', '0', 'menu', 'fa fa-sitemap', '2016-07-02 06:06:37', '2016-07-02 06:06:37', null);
INSERT INTO `app_menu` VALUES ('3', 'Perfiles', '1', '1', 'perfiles', 'fa fa-users', '2016-07-02 06:06:37', '2016-07-02 06:06:37', null);
INSERT INTO `app_menu` VALUES ('4', 'Usuarios', '1', '2', 'usuarios', 'fa fa-user', '2016-07-02 06:06:37', '2016-07-02 06:06:37', null);
INSERT INTO `app_menu` VALUES ('5', 'Inventario', '0', '2', '#Inventario', 'fa fa-book', '2016-07-02 06:20:34', '2016-07-02 06:20:34', null);
INSERT INTO `app_menu` VALUES ('6', 'Definiciones', '5', '0', '#Definiciones', 'fa fa-hashtag', '2016-07-02 06:20:34', '2016-07-02 06:20:34', null);
INSERT INTO `app_menu` VALUES ('7', 'Empresas', '6', '1', 'definiciones/empresas', 'fa fa-industry', '2016-07-02 06:20:34', '2016-07-02 06:20:34', null);
INSERT INTO `app_menu` VALUES ('8', 'Rubros', '6', '2', 'definiciones/rubros', 'fa fa-cutlery', '2016-07-02 06:20:34', '2016-07-02 06:20:34', null);
INSERT INTO `app_menu` VALUES ('9', 'Categoria', '6', '0', 'definiciones/categorias', 'fa fa-list', '2016-07-02 06:20:34', '2016-07-02 06:29:23', null);
INSERT INTO `app_menu` VALUES ('10', 'Carga', '5', '2', 'inventario', 'fa fa-list-alt', '2016-07-02 06:20:34', '2016-07-02 06:29:23', null);
INSERT INTO `app_menu` VALUES ('11', 'Inventario', '5', '1', 'reporte/inventario', 'fa fa-print', '2016-07-02 06:20:34', '2016-07-02 06:20:34', null);
INSERT INTO `app_menu` VALUES ('12', 'Graficas', '5', '3', 'graficas/inventario', 'fa fa-bar-chart', '2016-07-02 06:20:34', '2016-07-02 06:29:23', null);
INSERT INTO `app_menu` VALUES ('13', 'Comportamiento', '5', '4', 'comportamiento', 'fa fa-line-chart', '2016-07-02 06:20:34', '2016-07-02 06:29:23', null);
INSERT INTO `app_menu` VALUES ('14', 'Clap', '0', '1', '#Clap', 'fa fa-bullseye', '2016-07-02 06:22:00', '2016-07-02 06:22:00', null);
INSERT INTO `app_menu` VALUES ('15', 'Definiciones', '14', '0', '#DefinicionesClap', 'fa fa-hashtag', '2016-07-02 06:22:00', '2016-07-02 06:22:00', null);
INSERT INTO `app_menu` VALUES ('16', 'Municipio', '15', '0', 'definiciones/municipio', 'fa fa-map', '2016-07-02 06:22:00', '2016-07-02 06:22:00', null);
INSERT INTO `app_menu` VALUES ('17', 'Parroquia', '15', '1', 'definiciones/parroquia', 'fa fa-map-marker', '2016-07-02 06:22:00', '2016-07-02 06:22:00', null);
INSERT INTO `app_menu` VALUES ('18', 'Sector', '15', '2', 'definiciones/sector', 'fa fa-home', '2016-07-02 06:22:00', '2016-07-02 06:22:00', null);
INSERT INTO `app_menu` VALUES ('19', 'Clap', '14', '1', 'clap', 'fa fa-truck', '2016-07-02 06:22:00', '2016-07-02 06:29:23', null);
INSERT INTO `app_menu` VALUES ('20', 'Jornadas', '14', '2', 'jornadas', 'fa fa-cart-arrow-down', '2016-07-02 06:22:00', '2016-07-02 06:29:23', null);
INSERT INTO `app_menu` VALUES ('21', 'Reportes', '14', '3', 'reportes/clap', 'fa fa-file-pdf-o', '2016-07-02 06:22:00', '2016-07-02 06:29:23', null);
INSERT INTO `app_menu` VALUES ('23', 'Ingresos', '0', '3', '#Ingresos', 'fa fa-level-down', '2016-07-08 01:09:08', '2016-07-08 01:21:48', null);
INSERT INTO `app_menu` VALUES ('30', 'Definiciones', '23', '0', '#', 'fa fa-hashtag', '2016-07-08 01:21:48', '2016-07-08 01:21:48', null);
INSERT INTO `app_menu` VALUES ('31', 'Rubros', '30', '0', 'ingresos/definiciones/rubros', 'fa fa-cutlery', '2016-07-08 01:21:48', '2016-07-08 01:21:48', null);
INSERT INTO `app_menu` VALUES ('32', 'Carga', '23', '1', 'ingresos/inventario', 'fa fa-list-alt', '2016-07-08 02:05:26', '2016-07-08 02:05:26', null);
INSERT INTO `app_menu` VALUES ('33', 'Graficas', '23', '2', 'ingresos/graficas/inventario', 'fa fa-bar-chart', '2016-07-08 18:34:44', '2016-07-08 18:34:44', null);
INSERT INTO `app_menu` VALUES ('34', 'Comportamiento', '23', '3', 'ingresos/graficas/comportamiento', 'fa fa-line-chart', '2016-07-11 18:59:44', '2016-07-11 18:59:44', null);
INSERT INTO `app_menu` VALUES ('35', 'Rubros Requerimiento', '30', '2', 'ingresos/definiciones/rubros_requerimiento', 'fa fa-asterisk', '2016-08-03 23:37:26', '2016-08-09 18:02:53', null);
INSERT INTO `app_menu` VALUES ('36', 'Categorias', '30', '1', 'ingresos/definiciones/categorias', 'fa fa-list', '2016-08-09 18:02:52', '2016-08-09 18:02:52', null);
INSERT INTO `app_menu` VALUES ('37', 'Resumen', '0', '4', '#Resumen', 'fa fa-info', '2016-08-18 00:22:46', '2016-08-18 00:22:46', null);
INSERT INTO `app_menu` VALUES ('38', 'Carga resumen', '37', '0', 'resumen/carga', 'fa fa-upload', '2016-08-18 00:22:46', '2016-08-18 00:22:46', null);
INSERT INTO `app_menu` VALUES ('39', 'Reporte', '37', '1', 'resumen/reporte', 'fa fa-file-pdf-o', '2016-08-18 00:22:46', '2016-08-18 00:22:46', null);
INSERT INTO `app_menu` VALUES ('40', 'Creditos', '0', '5', '#Creditos', 'fa fa-usd', '2016-12-20 22:36:58', '2016-12-20 22:36:58', null);
INSERT INTO `app_menu` VALUES ('41', 'Definiciones', '40', '0', '#Definiciones_Creditos', 'fa fa-hashtag', '2016-12-20 22:36:59', '2016-12-20 22:36:59', null);
INSERT INTO `app_menu` VALUES ('42', 'Bancos', '41', '0', 'definiciones/bancos', 'fa fa-university', '2016-12-20 22:36:59', '2016-12-20 22:36:59', null);
INSERT INTO `app_menu` VALUES ('43', 'Carga', '40', '1', 'creditos/empresas', 'fa fa-upload', '2016-12-20 23:03:22', '2016-12-20 23:03:22', null);
INSERT INTO `app_menu` VALUES ('44', 'Graficos', '40', '2', 'creditos/graficos', 'fa fa-bar-chart', '2016-12-20 20:59:34', '2016-12-20 20:59:34', null);
INSERT INTO `app_menu` VALUES ('45', 'Reporte', '40', '3', 'creditos/reportes', 'fa fa-file-pdf-o', '2016-12-20 21:58:35', '2016-12-20 21:58:35', null);

-- ----------------------------
-- Table structure for app_perfil
-- ----------------------------
DROP TABLE IF EXISTS `app_perfil`;
CREATE TABLE `app_perfil` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_perfil_nombre_unique` (`nombre`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of app_perfil
-- ----------------------------
INSERT INTO `app_perfil` VALUES ('1', 'Administrador', '2016-07-01 22:06:36', '2017-03-19 21:27:05', null);
INSERT INTO `app_perfil` VALUES ('2', 'Reportes', '2016-07-01 22:06:36', '2017-03-19 21:26:46', null);
INSERT INTO `app_perfil` VALUES ('3', 'Administrador Inventario Alimento', '2016-07-21 10:17:24', '2017-03-19 21:26:52', null);
INSERT INTO `app_perfil` VALUES ('4', 'Gobernador', '2016-12-20 21:56:25', '2017-03-19 21:26:36', null);
INSERT INTO `app_perfil` VALUES ('5', 'Reportes creditos', '2016-12-22 09:02:30', '2016-12-22 09:02:30', '2016-12-22 10:12:59');
INSERT INTO `app_perfil` VALUES ('6', 'Administrador Proyecto', '2017-01-16 18:08:40', '2017-01-20 11:09:25', null);
INSERT INTO `app_perfil` VALUES ('7', 'Administrador Restringido', '2017-01-16 18:08:55', '2017-03-19 21:26:58', null);
INSERT INTO `app_perfil` VALUES ('8', 'Carnet', '2017-02-02 10:51:00', '2017-02-02 10:51:00', null);
INSERT INTO `app_perfil` VALUES ('9', 'Proyecto', '2017-02-28 13:20:28', '2017-02-28 13:20:28', null);

-- ----------------------------
-- Table structure for app_perfiles_permisos
-- ----------------------------
DROP TABLE IF EXISTS `app_perfiles_permisos`;
CREATE TABLE `app_perfiles_permisos` (
  `perfil_id` int(10) unsigned NOT NULL,
  `ruta` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `app_perfiles_permisos_app_perfil_id_foreign` (`perfil_id`) USING BTREE,
  CONSTRAINT `app_perfiles_permisos_ibfk_1` FOREIGN KEY (`perfil_id`) REFERENCES `app_perfil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of app_perfiles_permisos
-- ----------------------------
INSERT INTO `app_perfiles_permisos` VALUES ('6', '#Proyectos', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', '#Definicionesprotecto', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'ente_ejecutor', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'ente_ejecutor/nuevo', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'ente_ejecutor/cambiar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'ente_ejecutor/buscar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'ente_ejecutor/guardar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'ente_ejecutor/eliminar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'ente_ejecutor/restaurar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'ente_ejecutor/destruir', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'ente_ejecutor/datatable', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'estatus', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'estatus/nuevo', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'estatus/cambiar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'estatus/buscar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'estatus/guardar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'estatus/eliminar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'estatus/restaurar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'estatus/destruir', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'estatus/datatable', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'fuente_financiamiento', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'fuente_financiamiento/nuevo', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'fuente_financiamiento/cambiar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'fuente_financiamiento/buscar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'fuente_financiamiento/guardar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'fuente_financiamiento/eliminar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'fuente_financiamiento/restaurar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'fuente_financiamiento/destruir', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'fuente_financiamiento/datatable', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'sector_primario', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'sector_primario/nuevo', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'sector_primario/cambiar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'sector_primario/buscar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'sector_primario/guardar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'sector_primario/eliminar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'sector_primario/restaurar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'sector_primario/destruir', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'sector_primario/datatable', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'proyectos', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'proyectos/escritorio', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'proyectos/nuevo', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'proyectos/cambiar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'proyectos/tareas', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'proyectos/tareasdata_gantt', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'proyectos/buscar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'proyectos/carga', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'proyectos/guardar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'proyectos/eliminar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'proyectos/restaurar', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'proyectos/destruir', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'proyectos/datatable', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'reporte', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'reporte/imprimir', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'reporte/parroquias', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'grafica', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'grafica/grafica', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'grafica/grafica-torta', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('6', 'grafica/parroquias', '2017-01-20 11:09:25', '2017-01-20 11:09:25');
INSERT INTO `app_perfiles_permisos` VALUES ('8', '#Carnetizacion', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/puntos', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/fuerza_trabajo', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/puntos/nuevo', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/puntos/cambiar', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/puntos/buscar', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/puntos/guardar', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/puntos/eliminar', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/puntos/restaurar', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/puntos/destruir', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/puntos/datatable', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/formalizada', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/formalizada/nuevo', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/formalizada/cambiar', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/formalizada/buscar', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/formalizada/guardar', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/formalizada/eliminar', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/formalizada/restaurar', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/formalizada/destruir', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/formalizada/parroquias', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/formalizada/puntos', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/formalizada/estadistica', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/formalizada/datatable', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/fuerza_trabajo/nuevo', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/fuerza_trabajo/cambiar', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/fuerza_trabajo/buscar', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/fuerza_trabajo/guardar', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/fuerza_trabajo/eliminar', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/fuerza_trabajo/restaurar', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/fuerza_trabajo/destruir', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('8', 'carnetizacion/fuerza_trabajo/datatable', '2017-02-02 10:51:00', '2017-02-02 10:51:00');
INSERT INTO `app_perfiles_permisos` VALUES ('9', '#Proyectos', '2017-02-28 13:20:28', '2017-02-28 13:20:28');
INSERT INTO `app_perfiles_permisos` VALUES ('9', 'proyectos/escritorio', '2017-02-28 13:20:28', '2017-02-28 13:20:28');
INSERT INTO `app_perfiles_permisos` VALUES ('9', 'proyectos', '2017-02-28 13:20:28', '2017-02-28 13:20:28');
INSERT INTO `app_perfiles_permisos` VALUES ('9', 'proyectos/escritorio/data', '2017-02-28 13:20:28', '2017-02-28 13:20:28');
INSERT INTO `app_perfiles_permisos` VALUES ('9', 'proyectos/nuevo', '2017-02-28 13:20:28', '2017-02-28 13:20:28');
INSERT INTO `app_perfiles_permisos` VALUES ('9', 'proyectos/cambiar', '2017-02-28 13:20:28', '2017-02-28 13:20:28');
INSERT INTO `app_perfiles_permisos` VALUES ('9', 'proyectos/tareas', '2017-02-28 13:20:28', '2017-02-28 13:20:28');
INSERT INTO `app_perfiles_permisos` VALUES ('9', 'proyectos/tareasdata_gantt', '2017-02-28 13:20:28', '2017-02-28 13:20:28');
INSERT INTO `app_perfiles_permisos` VALUES ('9', 'proyectos/buscar', '2017-02-28 13:20:28', '2017-02-28 13:20:28');
INSERT INTO `app_perfiles_permisos` VALUES ('9', 'proyectos/carga', '2017-02-28 13:20:28', '2017-02-28 13:20:28');
INSERT INTO `app_perfiles_permisos` VALUES ('9', 'proyectos/guardar', '2017-02-28 13:20:28', '2017-02-28 13:20:28');
INSERT INTO `app_perfiles_permisos` VALUES ('9', 'proyectos/eliminar', '2017-02-28 13:20:28', '2017-02-28 13:20:28');
INSERT INTO `app_perfiles_permisos` VALUES ('9', 'proyectos/restaurar', '2017-02-28 13:20:28', '2017-02-28 13:20:28');
INSERT INTO `app_perfiles_permisos` VALUES ('9', 'proyectos/destruir', '2017-02-28 13:20:28', '2017-02-28 13:20:28');
INSERT INTO `app_perfiles_permisos` VALUES ('9', 'proyectos/datatable', '2017-02-28 13:20:28', '2017-02-28 13:20:28');
INSERT INTO `app_perfiles_permisos` VALUES ('9', 'reporte', '2017-02-28 13:20:28', '2017-02-28 13:20:28');
INSERT INTO `app_perfiles_permisos` VALUES ('9', 'reporte/imprimir', '2017-02-28 13:20:28', '2017-02-28 13:20:28');
INSERT INTO `app_perfiles_permisos` VALUES ('9', 'reporte/imprimir-escritorio', '2017-02-28 13:20:28', '2017-02-28 13:20:28');
INSERT INTO `app_perfiles_permisos` VALUES ('9', 'reporte/parroquias', '2017-02-28 13:20:28', '2017-02-28 13:20:28');
INSERT INTO `app_perfiles_permisos` VALUES ('9', 'grafica', '2017-02-28 13:20:28', '2017-02-28 13:20:28');
INSERT INTO `app_perfiles_permisos` VALUES ('9', 'grafica/grafica', '2017-02-28 13:20:28', '2017-02-28 13:20:28');
INSERT INTO `app_perfiles_permisos` VALUES ('9', 'grafica/grafica-torta', '2017-02-28 13:20:28', '2017-02-28 13:20:28');
INSERT INTO `app_perfiles_permisos` VALUES ('9', 'grafica/parroquias', '2017-02-28 13:20:28', '2017-02-28 13:20:28');
INSERT INTO `app_perfiles_permisos` VALUES ('4', '#Clap', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', 'clap/reportes', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', 'clap/reportes/imprimir', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', 'clap/reportes/imprimir-parroquia', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', 'clap/reportes/imprimir-clap', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', '#Ingresos', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', 'ingresos/graficas/inventario', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', 'ingresos/graficas/inventario/graficames', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', 'ingresos/graficas/inventario/grafica', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', 'ingresos/graficas/inventario/graficarubros', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', 'ingresos/comportamiento', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', 'ingresos/comportamiento/graficaAnos', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', 'ingresos/comportamiento/graficaRango', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', '#Inventario', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', 'inventario/reporte', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', 'inventario/reporte/imprimir', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', 'inventario/graficos', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', 'inventario/graficos/graficaEmpresas', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', 'inventario/graficos/graficaCategoria', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', 'inventario/comportamiento', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', 'inventario/comportamiento/graficaAnos', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', 'inventario/comportamiento/graficaRango', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', '#Resumen', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', 'resumen/reporte', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('4', 'resumen/reporte/imprimir', '2017-03-19 21:26:36', '2017-03-19 21:26:36');
INSERT INTO `app_perfiles_permisos` VALUES ('2', '#Ingresos', '2017-03-19 21:26:46', '2017-03-19 21:26:46');
INSERT INTO `app_perfiles_permisos` VALUES ('2', 'ingresos/graficas/inventario', '2017-03-19 21:26:46', '2017-03-19 21:26:46');
INSERT INTO `app_perfiles_permisos` VALUES ('2', 'ingresos/graficas/inventario/graficames', '2017-03-19 21:26:46', '2017-03-19 21:26:46');
INSERT INTO `app_perfiles_permisos` VALUES ('2', 'ingresos/graficas/inventario/grafica', '2017-03-19 21:26:46', '2017-03-19 21:26:46');
INSERT INTO `app_perfiles_permisos` VALUES ('2', 'ingresos/graficas/inventario/graficarubros', '2017-03-19 21:26:46', '2017-03-19 21:26:46');
INSERT INTO `app_perfiles_permisos` VALUES ('2', 'ingresos/comportamiento', '2017-03-19 21:26:46', '2017-03-19 21:26:46');
INSERT INTO `app_perfiles_permisos` VALUES ('2', 'ingresos/comportamiento/graficaAnos', '2017-03-19 21:26:46', '2017-03-19 21:26:46');
INSERT INTO `app_perfiles_permisos` VALUES ('2', 'ingresos/comportamiento/graficaRango', '2017-03-19 21:26:46', '2017-03-19 21:26:46');
INSERT INTO `app_perfiles_permisos` VALUES ('2', '#Inventario', '2017-03-19 21:26:46', '2017-03-19 21:26:46');
INSERT INTO `app_perfiles_permisos` VALUES ('2', 'inventario/reporte', '2017-03-19 21:26:46', '2017-03-19 21:26:46');
INSERT INTO `app_perfiles_permisos` VALUES ('2', 'inventario/reporte/imprimir', '2017-03-19 21:26:46', '2017-03-19 21:26:46');
INSERT INTO `app_perfiles_permisos` VALUES ('2', 'inventario/graficos', '2017-03-19 21:26:46', '2017-03-19 21:26:46');
INSERT INTO `app_perfiles_permisos` VALUES ('2', 'inventario/graficos/graficaEmpresas', '2017-03-19 21:26:46', '2017-03-19 21:26:46');
INSERT INTO `app_perfiles_permisos` VALUES ('2', 'inventario/graficos/graficaCategoria', '2017-03-19 21:26:46', '2017-03-19 21:26:46');
INSERT INTO `app_perfiles_permisos` VALUES ('2', 'inventario/comportamiento', '2017-03-19 21:26:46', '2017-03-19 21:26:46');
INSERT INTO `app_perfiles_permisos` VALUES ('2', 'inventario/comportamiento/graficaAnos', '2017-03-19 21:26:46', '2017-03-19 21:26:46');
INSERT INTO `app_perfiles_permisos` VALUES ('2', 'inventario/comportamiento/graficaRango', '2017-03-19 21:26:46', '2017-03-19 21:26:46');
INSERT INTO `app_perfiles_permisos` VALUES ('2', '#Resumen', '2017-03-19 21:26:46', '2017-03-19 21:26:46');
INSERT INTO `app_perfiles_permisos` VALUES ('2', 'resumen/reporte', '2017-03-19 21:26:46', '2017-03-19 21:26:46');
INSERT INTO `app_perfiles_permisos` VALUES ('2', 'resumen/reporte/imprimir', '2017-03-19 21:26:46', '2017-03-19 21:26:46');
INSERT INTO `app_perfiles_permisos` VALUES ('3', '#Clap', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'clap/carga', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'clap/carga/guardar', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'clap/carga/fecha', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'clap/carga/parroquias', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'clap/carga/sectores', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'clap/carga/buscar', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'clap/carga/archivo', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'clap/carga/datatable', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'clap/jornadas', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'clap/jornadas/fecha', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'clap/jornadas/guardar', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'clap/jornadas/parroquias', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'clap/jornadas/sectores', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'clap/jornadas/buscar', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'clap/jornadas/archivo', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'clap/jornadas/datatable', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'clap/reportes', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'clap/reportes/imprimir', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'clap/reportes/imprimir-parroquia', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'clap/reportes/imprimir-clap', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', '#Ingresos', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'ingresos/carga', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'ingresos/carga/guardar', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'ingresos/carga/fecha', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'ingresos/carga/archivo', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'ingresos/graficas/inventario', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'ingresos/graficas/inventario/graficames', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'ingresos/graficas/inventario/grafica', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'ingresos/graficas/inventario/graficarubros', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'ingresos/comportamiento', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'ingresos/comportamiento/graficaAnos', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'ingresos/comportamiento/graficaRango', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', '#Inventario', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'inventario/reporte', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'inventario/reporte/imprimir', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'inventario/carga', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'inventario/carga/guardar', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'inventario/carga/fecha', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'inventario/carga/archivo', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'inventario/graficos', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'inventario/graficos/graficaEmpresas', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'inventario/graficos/graficaCategoria', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'inventario/comportamiento', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'inventario/comportamiento/graficaAnos', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'inventario/comportamiento/graficaRango', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', '#Resumen', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'resumen/carga', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'resumen/carga/guardar', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'resumen/carga/buscar', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'resumen/carga/datatable', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'resumen/carga/eliminar', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'resumen/carga/archivo', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'resumen/carga/carga-empresa', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'resumen/reporte', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('3', 'resumen/reporte/imprimir', '2017-03-19 21:26:52', '2017-03-19 21:26:52');
INSERT INTO `app_perfiles_permisos` VALUES ('7', '#Clap', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'clap/carga', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'clap/carga/guardar', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'clap/carga/fecha', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'clap/carga/parroquias', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'clap/carga/sectores', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'clap/carga/buscar', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'clap/carga/archivo', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'clap/carga/datatable', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'clap/jornadas', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'clap/jornadas/fecha', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'clap/jornadas/guardar', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'clap/jornadas/parroquias', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'clap/jornadas/sectores', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'clap/jornadas/buscar', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'clap/jornadas/archivo', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'clap/jornadas/datatable', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'clap/reportes', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'clap/reportes/imprimir', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'clap/reportes/imprimir-parroquia', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'clap/reportes/imprimir-clap', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', '#Ingresos', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'ingresos/carga', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'ingresos/carga/guardar', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'ingresos/carga/fecha', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'ingresos/carga/archivo', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'ingresos/graficas/inventario', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'ingresos/graficas/inventario/graficames', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'ingresos/graficas/inventario/grafica', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'ingresos/graficas/inventario/graficarubros', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'ingresos/comportamiento', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'ingresos/comportamiento/graficaAnos', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'ingresos/comportamiento/graficaRango', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', '#Inventario', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'inventario/reporte', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'inventario/reporte/imprimir', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'inventario/carga', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'inventario/carga/guardar', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'inventario/carga/fecha', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'inventario/carga/archivo', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'inventario/graficos', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'inventario/graficos/graficaEmpresas', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'inventario/graficos/graficaCategoria', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'inventario/comportamiento', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'inventario/comportamiento/graficaAnos', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'inventario/comportamiento/graficaRango', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', '#Resumen', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'resumen/carga', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'resumen/carga/guardar', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'resumen/carga/buscar', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'resumen/carga/datatable', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'resumen/carga/eliminar', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'resumen/carga/archivo', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'resumen/carga/carga-empresa', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'resumen/reporte', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('7', 'resumen/reporte/imprimir', '2017-03-19 21:26:58', '2017-03-19 21:26:58');
INSERT INTO `app_perfiles_permisos` VALUES ('1', '#Administrador', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'usuarios', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'usuarios/buscar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'usuarios/guardar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'usuarios/eliminar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'usuarios/restaurar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'usuarios/destruir', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'usuarios/cambio', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'usuarios/arbol', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'usuarios/datatable', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'perfiles', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'perfiles/buscar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'perfiles/guardar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'perfiles/eliminar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'perfiles/restaurar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'perfiles/destruir', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'perfiles/arbol', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'perfiles/datatable', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', '#Clap', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', '#Definicionesclap', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/estados', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/estados/nuevo', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/estados/cambiar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/estados/buscar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/estados/guardar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/estados/eliminar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/estados/restaurar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/estados/destruir', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/estados/datatable', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/municipio', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/municipio/nuevo', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/municipio/cambiar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/municipio/buscar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/municipio/guardar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/municipio/eliminar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/municipio/restaurar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/municipio/destruir', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/municipio/datatable', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/parroquia', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/parroquia/nuevo', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/parroquia/cambiar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/parroquia/buscar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/parroquia/municipios', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/parroquia/guardar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/parroquia/eliminar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/parroquia/restaurar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/parroquia/destruir', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/parroquia/datatable', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/sector', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/sector/nuevo', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/sector/cambiar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/sector/buscar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/sector/municipios', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/sector/parroquia', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/sector/guardar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/sector/eliminar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/sector/restaurar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/sector/destruir', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/definiciones/sector/datatable', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/carga', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/carga/guardar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/carga/fecha', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/carga/parroquias', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/carga/sectores', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/carga/buscar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/carga/archivo', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/carga/datatable', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/jornadas', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/jornadas/fecha', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/jornadas/guardar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/jornadas/parroquias', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/jornadas/sectores', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/jornadas/buscar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/jornadas/archivo', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/jornadas/datatable', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/reportes', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/reportes/imprimir', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/reportes/imprimir-parroquia', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'clap/reportes/imprimir-clap', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', '#Ingresos', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', '#Definicionesingresos', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/rubros', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/rubros/guardar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/rubros/buscar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/rubros/datatable', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/rubros/eliminar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/categoria', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/categoria/nuevo', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/categoria/cambiar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/categoria/buscar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/categoria/guardar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/categoria/eliminar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/categoria/restaurar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/categoria/destruir', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/categoria/datatable', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/Rubors_Requerimientos', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/Rubors_Requerimientos/guardar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/Rubors_Requerimientos/buscar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/Rubors_Requerimientos/datatable', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/Rubors_Requerimientos/eliminar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/redesalimentacion', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/redesalimentacion/guardar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/redesalimentacion/buscar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/redesalimentacion/datatable', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/definiciones/redesalimentacion/eliminar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/carga', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/carga/guardar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/carga/fecha', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/carga/archivo', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/graficas/inventario', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/graficas/inventario/graficames', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/graficas/inventario/grafica', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/graficas/inventario/graficarubros', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/comportamiento', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/comportamiento/graficaAnos', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'ingresos/comportamiento/graficaRango', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', '#Inventario', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', '#Definicionesinventario', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/definiciones/categoria', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/definiciones/categoria/guardar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/definiciones/categoria/buscar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/definiciones/categoria/datatable', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/definiciones/categoria/eliminar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/definiciones/empresas', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/definiciones/empresas/guardar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/definiciones/empresas/buscar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/definiciones/empresas/datatable', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/definiciones/empresas/eliminar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/definiciones/rubros', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/definiciones/rubros/guardar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/definiciones/rubros/buscar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/definiciones/rubros/datatable', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/definiciones/rubros/eliminar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/reporte', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/reporte/imprimir', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/carga', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/carga/guardar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/carga/fecha', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/carga/archivo', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/graficos', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/graficos/graficaEmpresas', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/graficos/graficaCategoria', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/comportamiento', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/comportamiento/graficaAnos', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'inventario/comportamiento/graficaRango', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', '#Resumen', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'resumen/carga', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'resumen/carga/guardar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'resumen/carga/buscar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'resumen/carga/datatable', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'resumen/carga/eliminar', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'resumen/carga/archivo', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'resumen/carga/carga-empresa', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'resumen/reporte', '2017-03-19 21:27:05', '2017-03-19 21:27:05');
INSERT INTO `app_perfiles_permisos` VALUES ('1', 'resumen/reporte/imprimir', '2017-03-19 21:27:05', '2017-03-19 21:27:05');

-- ----------------------------
-- Table structure for app_usuario
-- ----------------------------
DROP TABLE IF EXISTS `app_usuario`;
CREATE TABLE `app_usuario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `dni` int(10) unsigned NOT NULL,
  `correo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user.png',
  `perfil_id` int(10) unsigned DEFAULT NULL,
  `super` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `autenticacion` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'l',
  `sexo` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edo_civil` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_usuario_usuario_unique` (`usuario`) USING BTREE,
  UNIQUE KEY `app_usuario_cedula_unique` (`dni`) USING BTREE,
  UNIQUE KEY `app_usuario_correo_unique` (`correo`) USING BTREE,
  KEY `app_usuario_app_perfil_id_foreign` (`perfil_id`) USING BTREE,
  CONSTRAINT `app_usuario_ibfk_1` FOREIGN KEY (`perfil_id`) REFERENCES `app_perfil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of app_usuario
-- ----------------------------
INSERT INTO `app_usuario` VALUES ('1', 'admin', 'Administrador', '', '$2y$10$6JztlJ3rM/QX8Gr7eO0i8e3OlXMAO8QI9475V2OSSdp/aIDqvXume', '12345678', 'admin@gmail.com', '0414-123-1234', 'user.png', '1', 's', 'B', '', '', '', '', '', '', '07ZvTgeIk4R3j9WGmkGV9U5pZXQut4nhe1Tg7gzrxxf2VQZYqHgwQz7MAQq4', '2016-07-01 22:06:37', '2016-08-22 18:01:16', '2016-08-24 11:25:15');
INSERT INTO `app_usuario` VALUES ('2', 'frangel', 'Francisco Rangel', 'Rangel', '$2y$10$Nm5YSTkF32bERhtoGd75BeKK1tUxyTbYI8/J4b5.p8f0BSIdZWl1e', '122254887', '', '', 'frangel.jpg', '4', 'n', 'l', 'm', '', '', '', '', '', 'Aba5OQ0kCJ99Qp13ZYAdjmH00o6AlMW1SbdDC86tNbB6Zozv9kLBNAGVrAMe', '2016-07-11 15:27:56', '2017-03-17 19:52:36', null);
INSERT INTO `app_usuario` VALUES ('3', 'ftorres', 'Francisco', 'Torres', '$2y$10$MU1aBVkqzr81MdCK4N16T.A9d0SP3HkA4VqAengogPKUHYgT4ZXKS', '10510309', 'ftorres@e-bolivar.gob.ve', '', 'user.png', '3', 'n', 'l', 'm', null, '', '', '', '', 'KnPhCyMM2qjyAVHGq09EtjjP7SLCw0pAAVn9UCrCgyAneXchGYu8y8QFd333', '2016-07-21 10:24:02', '2017-03-29 10:56:24', null);
INSERT INTO `app_usuario` VALUES ('5', 'alimentosb', 'Alimentos', 'Bol├¡var', '$2y$10$bAidYq6pARq.kFhx49tdi.nPpjitvTicLYpYgnwIZo1xAM1AtsIK.', '10578475', 'alimentosb@gmail.com', '', 'user.png', '2', 'n', 'l', '', null, '', '', '', '', null, '2016-07-21 10:26:10', '2016-07-21 10:26:10', null);
INSERT INTO `app_usuario` VALUES ('6', 'friosa', 'Friosa', '', '$2y$10$XLKcuXsvIy/wEhlzOtjA6.Fih6hPAg4PCnoxL5vcdLI2sag87Nk9m', '10658975', 'friosa@gmail.com', '', 'user.png', '2', 'n', 'l', '', null, '', '', '', '', null, '2016-07-21 10:49:58', '2016-07-21 10:49:58', null);
INSERT INTO `app_usuario` VALUES ('7', 'mercal', 'Mercal', '', '$2y$10$0nin4Dw35wtPMzNU1CaMNuva2XRQzuey1ged/Em8CLJ.9VqGlCBvu', '10264735', 'mercal@gmail.com', '', 'user.png', '2', 'n', 'l', '', null, '', '', '', '', null, '2016-07-21 10:50:39', '2016-07-21 10:50:39', null);
INSERT INTO `app_usuario` VALUES ('8', 'pdval', 'Pdval', '', '$2y$10$qPBxhid7JWNdEGefZSa0nuYVXIQkPPO2ugrttJO3EGr28gaOaNkim', '10398546', 'pdval@gmail.com', '', 'user.png', '2', 'n', 'l', '', null, '', '', '', '', null, '2016-07-21 10:51:42', '2016-07-21 10:51:42', null);
INSERT INTO `app_usuario` VALUES ('17', 'zodibolivar', 'Montilla', '', '$2y$10$Mf3djnOEvOpQtWkwZz3H9.xueGwPoGJ18CSL1KjhlP9KSHrdq8yj2', '1231242', 'Montilla@gmail.com', '', 'user.png', '2', 'n', 'l', '', null, '', '', '', '', '8eIjekXlggDMPUmCWETwTBqcJ5OJnnBljd5wYBGbxRdcB6XiioTfokZO2c5J', '2016-07-30 11:58:16', '2016-10-28 14:47:33', null);
INSERT INTO `app_usuario` VALUES ('18', 'acanonicco', 'Alberto Canonicco', '', '$2y$10$NEPlurcG16a1/vRtcVc02uNOCXow4RuYmEW69GO4pEhd5li3A075e', '3124124', 'acanonicco@gmail.com', '', 'user.png', '2', 'n', 'l', '', '', '', '', '', '', '8Et734ItrSrolwJzGij0QaCJDWyVH3cZzI6Jq2YK2MVFin1FmYpYEa7zoGwl', '2016-07-30 11:59:24', '2017-02-01 13:55:21', null);
INSERT INTO `app_usuario` VALUES ('19', 'mjgutierrez', 'miguelangel', 'gutierrez', '$2y$10$TrtNIEibtqF27AlNYqQQl.qz73coqVy5SWGlKt7Zzxg8Z2d5UaE/i', '19729038', 'mjgutierrez@e-bolivar.gob.ve', '0416-986-2346', 'mjgutierrez.jpg', '1', 's', 'l', 'm', '', '', '', '', '', '2uPoI2FHWlM2gvn64AKLbnWOsrMbK5SJVx6yuoCJtOX0a5SAVjmelj17Rxwd', '2016-08-18 09:43:59', '2017-09-07 11:27:16', null);
INSERT INTO `app_usuario` VALUES ('20', 'rquintana', 'Roosevelt', 'Quintana', '$2y$10$eJa5tFSVHatjjb/fWi1Mhu3DIaui0MCJJ2.VEmJrqkGAJ8pbRzQo2', '19530830', 'rquintana@e-bolivar.gob.ve', '', 'rquintana.jpg', '1', 's', 'l', '', '', '', '', '', '', 'tB8ZWZy7507tqA2xPVSGgjQToIXM5JT1DZD9X9zp6xHYsPSdJBLs73qhBVuj', '2016-08-18 17:41:42', '2017-03-20 11:47:03', null);
INSERT INTO `app_usuario` VALUES ('21', 'ajbetancourt', 'Antonio', 'Betancourt', '$2y$10$MLf74kyk3Ime2vnYHe1mIewIywm9PHdNBwRUhM2lCDteG/0AZbLJi', '12193480', 'ajbetancourt@e-bolivar.gob.ve', '', 'user.png', '1', 's', 'l', '', '', '', '', '', '', 'bO15QW164rGtBmIwKl18FtD3JbO9G0vJoKfVagq4EL9k0rLCg8gGNS6sZcCy', '2016-08-18 17:43:28', '2017-03-17 14:48:23', null);
INSERT INTO `app_usuario` VALUES ('22', 'amendez', 'Alejandro', 'M├®ndez', '$2y$10$/EnHwIcGEakmQ9vmr6pbP.kodM8g8dUXAr7t8nxXIQKL.Ti7xSE8m', '18476160', 'amendez@e-bolivar.gob.ve', '', 'user.png', '1', 's', 'l', '', null, '', '', '', '', 'o5yL0hCCwTThogQxnk75uSPRd74z1TU4MGwKG7fKg6iEU0VXowtyfCgL7N2k', '2016-08-18 17:45:42', '2017-02-02 15:43:48', null);
INSERT INTO `app_usuario` VALUES ('23', 'lmata', 'Luis', 'Mata', '$2y$10$8Jvt/QN4ujKauSYhm9ZpdOgYUH.KA9ybGJy.r0M0urs6o3.HPKpwS', '21578041', 'lmata@e-bolivar.gob.ve', '', 'user.png', '1', 's', 'l', '', null, '', '', '', '', 'NfaQu8A8T1dmtctKE0QfFosyCu3UkAf7IYwvgaHkApwm1c74cRgaOxEWQA9y', '2016-08-18 17:46:17', '2017-03-17 15:30:48', null);
INSERT INTO `app_usuario` VALUES ('24', 'vsalicetti', 'Victor', 'Salicetti', '$2y$10$lBw2rdZSvg72JtxLiAjHAeq5/YYK0OJfsOj8pKC8ae0HdNLn6k6bO', '10566143', 'vsalicetti@e-bolivar.gob.ve', '', 'user.png', '2', 'n', 'l', 'm', null, '', '', '', '', 'RNr83iizezvhJkH4pXgN1DGkRPb21biTGyPymwgDdOHPkaZLgQrHU8d1lclY', '2016-08-26 12:12:28', '2016-08-26 12:33:52', null);
INSERT INTO `app_usuario` VALUES ('30', 'tporras', 'Teodardo', 'Porras', '$2y$10$2eHK/x8/045efpvr5h3E0OW0ZLfJVLYmTHcTWDTCvv8j5V4iy3rd.', '5986748', 'tporras@e-bolivar.gob.ve', '', 'user.png', '2', 'n', 'l', 'm', null, '', '', '', '', 'bvpHel0pQdFVMUZYTnwGtTPTnA0I9lkksJLz5qqKUV53zUurlxJnbawCBpzB', '2016-09-09 13:31:20', '2016-09-09 13:32:37', null);
INSERT INTO `app_usuario` VALUES ('32', 'mtorres', 'Marcos', 'Torres', '$2y$10$sIhjXEZhjiS9gIOdvIYuH.KZwefcFq8/srnWFw2sh7JoB86DvFng6', '19875478', 'n/a@gmail.com', '', 'user.png', '2', 'n', 'l', '', null, '', '', '', '', 'DIiuVVWehUToruUlMWI6y5yKG5e4HiGaqRgjSTvmmnWQB5vS3RcP776UNSwt', '2016-10-06 17:43:24', '2016-10-06 17:43:49', null);
INSERT INTO `app_usuario` VALUES ('33', 'sunagro', 'Sunagro', 'Sunagro', '$2y$10$zus/3YU8rWhtOjjmSd8wEOdzbAeIz33K27bGVsR9p88ulxyLTQ2ui', '12587458', 'sunagro@noaplica.gob.ve', '', 'user.png', '2', 'n', 'l', '', null, '', '', '', '', 'kQPR7oqlAX5l1k5uvohqvyp1rxyhZGzJxLIWKUCp94XirrIrxmht60XrcdXn', '2016-11-07 15:08:10', '2016-12-11 19:08:23', null);
INSERT INTO `app_usuario` VALUES ('37', 'yaynires', 'Yaynires', '', '$2y$10$kSheTnJX6anjE1pOdVe5qeLkkTjG/Pp3hIxXFzTk2gmHnotdHrmjS', '19885748', 'yaynires@hotmail.com', '', 'user.png', '2', 'n', 'l', '', null, '', '', '', '', 'LeGhBLNExIkrpJf49CCRX36CP6dobfp8myGtY4x3t01Xz4OIbq14SCG4Z9rd', '2016-11-30 14:11:32', '2016-11-30 14:12:25', '2016-11-30 14:16:14');
INSERT INTO `app_usuario` VALUES ('38', 'jaynire', 'Jaynire', '', '$2y$10$VW7JAEfot8puTqKh2JRI4evW5yRoxBmxVJlFjhmuZd66TfsWD6oTO', '19885747', 'jaynire@hotmail.com', '', 'user.png', '2', 'n', 'l', '', '', '', '', '', '', 'ZLTWE0cYFT9oMqnNU8YCSvkAKhfuoifNWHaw8NxkYe9mDS2cnN6IX2EolugV', '2016-11-30 14:17:43', '2017-02-18 09:06:51', null);
INSERT INTO `app_usuario` VALUES ('45', 'toviedo', 'Tito', 'Oviedo', '$2y$10$oTj9TZhB.dSVQChChqliduEmaBCSIHNrx/gyyv6Le/TmV7dFUyF1K', '16548788', 'tovideo@e-bolivar.gob.ve', '', 'toviedo.jpg', '2', 'n', 'l', 'm', null, '', '', '', '', 'w6gctNYv8IP188iYYRw2Rn5xf5GqUZsCkknzydjRMDtideM8rSVgzrBgx3ZS', '2016-12-01 11:04:53', '2016-12-01 17:35:29', null);
INSERT INTO `app_usuario` VALUES ('46', 'salapalacio', 'sala', 'Palacio', '$2y$10$5KbT.sVz3E2lz8kcIXQRD.jbX/FdfG5mmvOrH9yUGZrN1b.a1mGiq', '246784512', 'salapalacio@gmail.com', '', 'user.png', '2', 'n', 'l', '', null, '', '', '', '', 'ayo0YmQAYzCxNj0kPRf11s9fj7FCfAFya2vxeM2ZX46UVCSRkJ6XJ7rnSkIO', '2016-12-22 09:03:53', '2016-12-22 09:15:48', null);
INSERT INTO `app_usuario` VALUES ('61', 'atroncoso', 'Albert', 'Troncoso', '$2y$10$wnCQhhqFGrb/13y/uwNCreuAjj./g6qi3CziOK9GSoLo7baA3WT6O', '14567890', 'atroncoso@e-bolivar.gob.ve', '', 'user.png', '8', 'n', 'l', 'm', '', '', '', '', '', 'C8bJ2PxF19XL0fozTOrBhBpD9XEnyJgehMvkeRmmBkjg3rtFmITRNxPOvuGW', '2017-02-02 15:57:45', '2017-02-02 16:57:13', null);
INSERT INTO `app_usuario` VALUES ('66', 'proyecto', 'Proyecto', 'Geb', '$2y$10$B3d7je6ra4vPRXksivcQOuauMspsXEQSNxuMtmO8LUt0J1lDpH4W6', '20172702', 'no@gmail.com', '0000-000-0000', 'user.png', '9', 'n', 'l', 'm', 'so', '', '', '', '', 'Mf5CYFAzpC8GqHi6K4k0LyKFjO6UQYpmXituJVwHF5pdoHUtwzXbaIe7cdbX', '2017-02-28 13:21:34', '2017-03-08 16:40:16', null);

-- ----------------------------
-- Table structure for app_usuario_info
-- ----------------------------
DROP TABLE IF EXISTS `app_usuario_info`;
CREATE TABLE `app_usuario_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `app_usuario_id` int(10) unsigned NOT NULL,
  `apellido` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sexo` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edo_civil` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_usuario_info_app_usuario_id_unique` (`app_usuario_id`) USING BTREE,
  CONSTRAINT `app_usuario_info_ibfk_1` FOREIGN KEY (`app_usuario_id`) REFERENCES `app_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of app_usuario_info
-- ----------------------------
INSERT INTO `app_usuario_info` VALUES ('1', '1', '', '', '', '', '', '', '', '2016-07-01 22:06:37', '2016-07-01 22:06:37', null);
INSERT INTO `app_usuario_info` VALUES ('2', '2', 'Rangel', 'm', null, '', '', '', '', '2016-07-11 15:27:56', '2016-07-11 15:27:56', null);
INSERT INTO `app_usuario_info` VALUES ('3', '3', 'Torres', 'm', null, '', '', '', '', '2016-07-21 10:24:03', '2016-07-21 10:24:03', null);
INSERT INTO `app_usuario_info` VALUES ('4', '5', 'Bol├¡var', '', null, '', '', '', '', '2016-07-21 10:26:10', '2016-07-21 10:26:10', null);
INSERT INTO `app_usuario_info` VALUES ('5', '6', '', '', null, '', '', '', '', '2016-07-21 10:49:58', '2016-07-21 10:49:58', null);
INSERT INTO `app_usuario_info` VALUES ('6', '7', '', '', null, '', '', '', '', '2016-07-21 10:50:39', '2016-07-21 10:50:39', null);
INSERT INTO `app_usuario_info` VALUES ('7', '8', '', '', null, '', '', '', '', '2016-07-21 10:51:42', '2016-07-21 10:51:42', null);
INSERT INTO `app_usuario_info` VALUES ('8', '17', '', '', null, '', '', '', '', '2016-07-30 11:58:16', '2016-07-30 11:58:16', null);
INSERT INTO `app_usuario_info` VALUES ('9', '18', '', '', null, '', '', '', '', '2016-07-30 11:59:24', '2016-07-30 11:59:24', null);
INSERT INTO `app_usuario_info` VALUES ('10', '19', 'gutierrez', 'm', null, '', '', '', '', '2016-08-18 09:43:59', '2016-08-18 09:43:59', null);
INSERT INTO `app_usuario_info` VALUES ('11', '20', 'Quintana', '', null, '', '', '', '', '2016-08-18 17:41:42', '2016-08-18 17:41:42', null);
INSERT INTO `app_usuario_info` VALUES ('12', '21', 'Betancourt', '', null, '', '', '', '', '2016-08-18 17:43:28', '2016-08-18 17:43:28', null);
INSERT INTO `app_usuario_info` VALUES ('13', '22', 'M├®ndez', '', null, '', '', '', '', '2016-08-18 17:45:42', '2016-08-18 17:45:42', null);
INSERT INTO `app_usuario_info` VALUES ('14', '23', 'Mata', '', null, '', '', '', '', '2016-08-18 17:46:17', '2016-08-18 17:46:17', null);
INSERT INTO `app_usuario_info` VALUES ('15', '24', 'Salicetti', 'm', null, '', '', '', '', '2016-08-26 12:12:28', '2016-08-26 12:12:28', null);
INSERT INTO `app_usuario_info` VALUES ('16', '30', 'Porras', 'm', null, '', '', '', '', '2016-09-09 13:31:20', '2016-09-09 13:31:20', null);
INSERT INTO `app_usuario_info` VALUES ('17', '32', 'Torres', '', null, '', '', '', '', '2016-10-06 17:43:24', '2016-10-06 17:43:24', null);
INSERT INTO `app_usuario_info` VALUES ('18', '33', 'Sunagro', '', null, '', '', '', '', '2016-11-07 15:08:10', '2016-11-07 15:08:10', null);
INSERT INTO `app_usuario_info` VALUES ('19', '37', '', '', null, '', '', '', '', '2016-11-30 14:11:32', '2016-11-30 14:11:32', null);
INSERT INTO `app_usuario_info` VALUES ('20', '38', '', '', null, '', '', '', '', '2016-11-30 14:17:43', '2016-11-30 14:17:43', null);
INSERT INTO `app_usuario_info` VALUES ('21', '45', 'Oviedo', 'm', null, '', '', '', '', '2016-12-01 11:04:53', '2016-12-01 11:04:53', null);
INSERT INTO `app_usuario_info` VALUES ('22', '46', 'Palacio', '', null, '', '', '', '', '2016-12-22 09:03:53', '2016-12-22 09:03:53', null);

-- ----------------------------
-- Table structure for app_usuario_permisos
-- ----------------------------
DROP TABLE IF EXISTS `app_usuario_permisos`;
CREATE TABLE `app_usuario_permisos` (
  `usuario_id` int(10) unsigned NOT NULL,
  `ruta` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `app_usuario_permisos_app_usuario_id_foreign` (`usuario_id`) USING BTREE,
  CONSTRAINT `app_usuario_permisos_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `app_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of app_usuario_permisos
-- ----------------------------
INSERT INTO `app_usuario_permisos` VALUES ('18', '#Clap', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'clap/reportes', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'clap/reportes/imprimir', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', '#Ingresos', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'ingresos/carga', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'ingresos/carga/guardar', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'ingresos/carga/fecha', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'ingresos/carga/archivo', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'ingresos/graficas/inventario', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'ingresos/graficas/inventario/graficames', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'ingresos/graficas/inventario/grafica', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'ingresos/graficas/inventario/graficarubros', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'ingresos/comportamiento', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'ingresos/comportamiento/graficaAnos', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'ingresos/comportamiento/graficaRango', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', '#Inventario', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'inventario/graficos', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'inventario/graficos/graficaEmpresas', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'inventario/graficos/graficaCategoria', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'inventario/comportamiento', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'inventario/comportamiento/graficaAnos', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'inventario/comportamiento/graficaRango', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', '#Resumen', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'resumen/reporte', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'resumen/reporte/imprimir', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', '#Creditos', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'creditos/graficas', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'creditos/graficas/grafica', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'creditos/graficas/grafica-torta', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'creditos/reporte', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'creditos/reporte/imprimir', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'creditos/reporte/municipios', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('18', 'creditos/reporte/parroquias', '2017-01-24 12:02:30', '2017-01-24 12:02:30');
INSERT INTO `app_usuario_permisos` VALUES ('19', '', '2017-01-24 18:29:26', '2017-01-24 18:29:26');
INSERT INTO `app_usuario_permisos` VALUES ('2', '#Clap', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'clap/jornadas', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'clap/jornadas/fecha', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'clap/jornadas/guardar', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'clap/jornadas/parroquias', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'clap/jornadas/sectores', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'clap/jornadas/buscar', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'clap/jornadas/archivo', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'clap/jornadas/datatable', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'clap/reportes', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'clap/reportes/imprimir', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', '#Ingresos', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'ingresos/graficas/inventario', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'ingresos/graficas/inventario/graficames', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'ingresos/graficas/inventario/grafica', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'ingresos/graficas/inventario/graficarubros', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'ingresos/comportamiento', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'ingresos/comportamiento/graficaAnos', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'ingresos/comportamiento/graficaRango', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', '#Inventario', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'inventario/graficos', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'inventario/graficos/graficaEmpresas', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'inventario/graficos/graficaCategoria', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'inventario/comportamiento', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'inventario/comportamiento/graficaAnos', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'inventario/comportamiento/graficaRango', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', '#Resumen', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'resumen/reporte', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'resumen/reporte/imprimir', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', '#Creditos', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'creditos/graficas', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'creditos/graficas/grafica', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'creditos/graficas/grafica-torta', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'creditos/reporte', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'creditos/reporte/imprimir', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'creditos/reporte/municipios', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'creditos/reporte/parroquias', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', '#Proyectos', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'reporte', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'reporte/imprimir', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'reporte/imprimir-escritorio', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'reporte/parroquias', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'grafica', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'grafica/grafica', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'grafica/grafica-torta', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'grafica/parroquias', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'empleados', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', '#Carnetizacion', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/puntos', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/fuerza_trabajo', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/puntos/nuevo', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/puntos/cambiar', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/puntos/buscar', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/puntos/guardar', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/puntos/eliminar', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/puntos/restaurar', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/puntos/destruir', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/puntos/datatable', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/formalizada', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/formalizada/nuevo', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/formalizada/cambiar', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/formalizada/buscar', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/formalizada/guardar', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/formalizada/eliminar', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/formalizada/restaurar', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/formalizada/destruir', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/formalizada/parroquias', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/formalizada/puntos', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/formalizada/estadistica', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/formalizada/datatable', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/fuerza_trabajo/nuevo', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/fuerza_trabajo/cambiar', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/fuerza_trabajo/buscar', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/fuerza_trabajo/guardar', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/fuerza_trabajo/eliminar', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/fuerza_trabajo/restaurar', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/fuerza_trabajo/destruir', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('2', 'carnetizacion/fuerza_trabajo/datatable', '2017-02-02 10:34:19', '2017-02-02 10:34:19');
INSERT INTO `app_usuario_permisos` VALUES ('38', '#Clap', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'clap/reportes', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'clap/reportes/imprimir', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'clap/reportes/imprimir-parroquia', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'clap/reportes/imprimir-clap', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', '#Ingresos', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'ingresos/graficas/inventario', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'ingresos/graficas/inventario/graficames', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'ingresos/graficas/inventario/grafica', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'ingresos/graficas/inventario/graficarubros', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'ingresos/comportamiento', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'ingresos/comportamiento/graficaAnos', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'ingresos/comportamiento/graficaRango', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', '#Inventario', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'inventario/graficos', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'inventario/graficos/graficaEmpresas', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'inventario/graficos/graficaCategoria', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'inventario/comportamiento', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'inventario/comportamiento/graficaAnos', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'inventario/comportamiento/graficaRango', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', '#Resumen', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'resumen/reporte', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'resumen/reporte/imprimir', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', '#Proyectos', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'proyectos/escritorio', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'reporte', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'reporte/imprimir', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'reporte/imprimir-escritorio', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'reporte/parroquias', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'grafica', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'grafica/grafica', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'grafica/grafica-torta', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'grafica/parroquias', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', '#Carnetizacion', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/puntos', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/fuerza_trabajo', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/puntos/nuevo', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/puntos/cambiar', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/puntos/buscar', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/puntos/guardar', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/puntos/eliminar', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/puntos/restaurar', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/puntos/destruir', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/puntos/datatable', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/formalizada', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/formalizada/nuevo', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/formalizada/cambiar', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/formalizada/buscar', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/formalizada/guardar', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/formalizada/eliminar', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/formalizada/restaurar', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/formalizada/destruir', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/formalizada/parroquias', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/formalizada/puntos', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/formalizada/estadistica', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/formalizada/datatable', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/fuerza_trabajo/nuevo', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/fuerza_trabajo/cambiar', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/fuerza_trabajo/buscar', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/fuerza_trabajo/guardar', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/fuerza_trabajo/eliminar', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/fuerza_trabajo/restaurar', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/fuerza_trabajo/destruir', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('38', 'carnetizacion/fuerza_trabajo/datatable', '2017-02-17 21:06:24', '2017-02-17 21:06:24');
INSERT INTO `app_usuario_permisos` VALUES ('66', '#Proyectos', '2017-02-28 19:25:05', '2017-02-28 19:25:05');
INSERT INTO `app_usuario_permisos` VALUES ('66', 'proyectos/escritorio', '2017-02-28 19:25:05', '2017-02-28 19:25:05');
INSERT INTO `app_usuario_permisos` VALUES ('66', 'proyectos', '2017-02-28 19:25:05', '2017-02-28 19:25:05');
INSERT INTO `app_usuario_permisos` VALUES ('66', 'proyectos/escritorio/data', '2017-02-28 19:25:05', '2017-02-28 19:25:05');
INSERT INTO `app_usuario_permisos` VALUES ('66', 'proyectos/nuevo', '2017-02-28 19:25:05', '2017-02-28 19:25:05');
INSERT INTO `app_usuario_permisos` VALUES ('66', 'proyectos/cambiar', '2017-02-28 19:25:05', '2017-02-28 19:25:05');
INSERT INTO `app_usuario_permisos` VALUES ('66', 'proyectos/tareas', '2017-02-28 19:25:05', '2017-02-28 19:25:05');
INSERT INTO `app_usuario_permisos` VALUES ('66', 'proyectos/tareasdata_gantt', '2017-02-28 19:25:05', '2017-02-28 19:25:05');
INSERT INTO `app_usuario_permisos` VALUES ('66', 'proyectos/buscar', '2017-02-28 19:25:05', '2017-02-28 19:25:05');
INSERT INTO `app_usuario_permisos` VALUES ('66', 'proyectos/carga', '2017-02-28 19:25:05', '2017-02-28 19:25:05');
INSERT INTO `app_usuario_permisos` VALUES ('66', 'proyectos/guardar', '2017-02-28 19:25:05', '2017-02-28 19:25:05');
INSERT INTO `app_usuario_permisos` VALUES ('66', 'proyectos/eliminar', '2017-02-28 19:25:05', '2017-02-28 19:25:05');
INSERT INTO `app_usuario_permisos` VALUES ('66', 'proyectos/restaurar', '2017-02-28 19:25:05', '2017-02-28 19:25:05');
INSERT INTO `app_usuario_permisos` VALUES ('66', 'proyectos/destruir', '2017-02-28 19:25:05', '2017-02-28 19:25:05');
INSERT INTO `app_usuario_permisos` VALUES ('66', 'proyectos/datatable', '2017-02-28 19:25:05', '2017-02-28 19:25:05');
INSERT INTO `app_usuario_permisos` VALUES ('66', 'reporte', '2017-02-28 19:25:05', '2017-02-28 19:25:05');
INSERT INTO `app_usuario_permisos` VALUES ('66', 'reporte/imprimir', '2017-02-28 19:25:05', '2017-02-28 19:25:05');
INSERT INTO `app_usuario_permisos` VALUES ('66', 'reporte/imprimir-escritorio', '2017-02-28 19:25:05', '2017-02-28 19:25:05');
INSERT INTO `app_usuario_permisos` VALUES ('66', 'reporte/parroquias', '2017-02-28 19:25:05', '2017-02-28 19:25:05');
INSERT INTO `app_usuario_permisos` VALUES ('66', 'grafica', '2017-02-28 19:25:05', '2017-02-28 19:25:05');
INSERT INTO `app_usuario_permisos` VALUES ('66', 'grafica/grafica', '2017-02-28 19:25:05', '2017-02-28 19:25:05');
INSERT INTO `app_usuario_permisos` VALUES ('66', 'grafica/grafica-torta', '2017-02-28 19:25:05', '2017-02-28 19:25:05');
INSERT INTO `app_usuario_permisos` VALUES ('66', 'grafica/parroquias', '2017-02-28 19:25:05', '2017-02-28 19:25:05');

-- ----------------------------
-- Table structure for bancos
-- ----------------------------
DROP TABLE IF EXISTS `bancos`;
CREATE TABLE `bancos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of bancos
-- ----------------------------
INSERT INTO `bancos` VALUES ('1', 'Banco Del Caribe', '2016-12-20 18:50:44', '2016-12-20 18:50:44', '2016-12-22 08:35:00');
INSERT INTO `bancos` VALUES ('2', 'Banesco Banco Universal', '2016-12-20 18:50:44', '2016-12-20 18:50:44', '2016-12-22 08:37:08');
INSERT INTO `bancos` VALUES ('3', 'Banco Caroni', '2016-12-20 18:50:44', '2016-12-20 18:50:44', '2016-12-22 08:33:44');
INSERT INTO `bancos` VALUES ('4', 'Finalven Banco De Inversion, Sa', '2016-12-20 18:50:44', '2016-12-20 18:50:44', '2016-12-22 08:37:43');
INSERT INTO `bancos` VALUES ('5', 'Sociedad Financiera Banco Orinoco, Ca', '2016-12-20 18:50:44', '2016-12-20 18:50:44', '2016-12-22 08:37:59');
INSERT INTO `bancos` VALUES ('6', 'BBVA Banco Provincial', '2016-12-20 18:50:44', '2016-12-20 18:50:44', '2016-12-22 08:37:16');
INSERT INTO `bancos` VALUES ('7', 'Banco Exterior', '2016-12-20 18:50:44', '2016-12-20 18:50:44', '2016-12-22 08:36:01');
INSERT INTO `bancos` VALUES ('8', 'Banco Hipotecario del Zulia', '2016-12-20 18:50:44', '2016-12-20 18:50:44', '2016-12-22 08:36:14');
INSERT INTO `bancos` VALUES ('9', 'Banco Industrial de Venezuela', '2016-12-20 18:50:44', '2016-12-20 18:50:44', '2016-12-22 08:36:33');
INSERT INTO `bancos` VALUES ('10', 'Banco Mercantil', '2016-12-20 18:50:44', '2016-12-20 18:50:44', '2016-12-22 08:36:42');
INSERT INTO `bancos` VALUES ('11', 'Banco Nacional de Credito, CA Banco Universal', '2016-12-20 18:50:44', '2016-12-20 18:50:44', '2016-12-22 08:36:53');
INSERT INTO `bancos` VALUES ('12', 'Banco Occidental de Descuento', '2016-12-20 18:50:44', '2016-12-20 18:50:44', '2016-12-22 08:37:01');
INSERT INTO `bancos` VALUES ('13', 'Banco de Venezuela', '2016-12-20 18:50:44', '2016-12-20 18:50:44', null);
INSERT INTO `bancos` VALUES ('14', 'Bicentenario Banco Universal', '2016-12-20 18:50:44', '2016-12-20 18:50:44', null);
INSERT INTO `bancos` VALUES ('15', 'DELSUR Banco Universal', '2016-12-20 18:50:44', '2016-12-20 18:50:44', '2016-12-22 08:37:35');
INSERT INTO `bancos` VALUES ('16', 'Corp Banca', '2016-12-20 18:50:44', '2016-12-20 18:50:44', '2016-12-22 08:37:26');
INSERT INTO `bancos` VALUES ('17', 'Fondo Comun', '2016-12-20 18:50:44', '2016-12-20 18:50:44', '2016-12-22 08:37:51');
INSERT INTO `bancos` VALUES ('18', 'Banco del Tesoro', '2016-12-20 18:50:44', '2016-12-20 18:50:44', null);

-- ----------------------------
-- Table structure for categorias
-- ----------------------------
DROP TABLE IF EXISTS `categorias`;
CREATE TABLE `categorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categorias_nombre_unique` (`nombre`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of categorias
-- ----------------------------
INSERT INTO `categorias` VALUES ('1', 'Granos', '2016-07-01 16:40:15', '2016-07-01 16:40:15', null);
INSERT INTO `categorias` VALUES ('2', 'Harinas', '2016-07-01 16:45:41', '2016-07-01 16:45:41', null);
INSERT INTO `categorias` VALUES ('3', 'Carnes', '2016-07-01 16:46:22', '2016-07-01 16:46:22', null);
INSERT INTO `categorias` VALUES ('4', 'Leche', '2016-07-01 16:46:59', '2016-07-01 16:46:59', null);
INSERT INTO `categorias` VALUES ('5', 'Aceites', '2016-07-01 16:47:23', '2016-07-01 16:47:23', null);
INSERT INTO `categorias` VALUES ('6', 'Arroz / Pasta', '2016-07-01 16:48:29', '2016-07-01 16:49:02', null);
INSERT INTO `categorias` VALUES ('7', 'Azucar', '2016-07-01 17:26:03', '2016-07-01 17:26:03', null);
INSERT INTO `categorias` VALUES ('8', 'prueba', '2017-03-17 08:51:54', '2017-03-17 08:51:54', '2017-03-17 12:36:21');
INSERT INTO `categorias` VALUES ('9', 'sgsd', '2017-03-20 16:54:02', '2017-03-20 16:54:02', null);

-- ----------------------------
-- Table structure for clap
-- ----------------------------
DROP TABLE IF EXISTS `clap`;
CREATE TABLE `clap` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `parroquia_id` int(10) unsigned DEFAULT NULL,
  `sector_id` int(10) unsigned DEFAULT NULL,
  `familia` int(10) unsigned NOT NULL,
  `consejo_comunal` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `clap_codigo_unique` (`codigo`) USING BTREE,
  KEY `clap_parroquia_id_foreign` (`parroquia_id`) USING BTREE,
  KEY `clap_sector_id_foreign` (`sector_id`) USING BTREE,
  CONSTRAINT `clap_ibfk_1` FOREIGN KEY (`parroquia_id`) REFERENCES `parroquia` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `clap_ibfk_2` FOREIGN KEY (`sector_id`) REFERENCES `sector` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of clap
-- ----------------------------
INSERT INTO `clap` VALUES ('1', 'nombre', 'CLAP-DA-464646-64646', 'nombre', '310', '2', '1022', 'nobre', '2017-03-20 12:49:37', '2017-03-20 12:49:37', null);

-- ----------------------------
-- Table structure for clap_contacto
-- ----------------------------
DROP TABLE IF EXISTS `clap_contacto`;
CREATE TABLE `clap_contacto` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `cedula` int(11) DEFAULT NULL,
  `clap_id` int(10) unsigned DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  KEY `clap_contacto_clap_id_foreign` (`clap_id`) USING BTREE,
  CONSTRAINT `clap_contacto_ibfk_1` FOREIGN KEY (`clap_id`) REFERENCES `clap` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of clap_contacto
-- ----------------------------
INSERT INTO `clap_contacto` VALUES ('0', null, '1', 'nombre', '4574-546-6367', '2017-03-20 12:49:37', '2017-03-20 12:49:37', null);

-- ----------------------------
-- Table structure for configuracion
-- ----------------------------
DROP TABLE IF EXISTS `configuracion`;
CREATE TABLE `configuracion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `propiedad` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `valor` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of configuracion
-- ----------------------------

-- ----------------------------
-- Table structure for empresa_credito
-- ----------------------------
DROP TABLE IF EXISTS `empresa_credito`;
CREATE TABLE `empresa_credito` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rif` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `nombres` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `estado_id` int(10) unsigned NOT NULL,
  `municipio_id` int(10) unsigned NOT NULL,
  `parroquia_id` int(10) unsigned NOT NULL,
  `direccion_domicilio` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `sitio_de_referencia` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `telefono_contacto` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `correo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `codigo_sica` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `representante_legal` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `rif_representante_legal` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `domicilio_rep_legal` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `telefono_rep_legal` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_importador` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `credito` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `banco_id` int(10) unsigned NOT NULL,
  `observacion` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empresa_credito_banco_id_foreign` (`banco_id`) USING BTREE,
  KEY `empresa_credito_estado_id_foreign` (`estado_id`) USING BTREE,
  KEY `empresa_credito_municipio_id_foreign` (`municipio_id`) USING BTREE,
  KEY `empresa_credito_parroquia_id_foreign` (`parroquia_id`) USING BTREE,
  CONSTRAINT `empresa_credito_ibfk_1` FOREIGN KEY (`banco_id`) REFERENCES `bancos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `empresa_credito_ibfk_2` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `empresa_credito_ibfk_3` FOREIGN KEY (`municipio_id`) REFERENCES `municipio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `empresa_credito_ibfk_4` FOREIGN KEY (`parroquia_id`) REFERENCES `parroquia` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of empresa_credito
-- ----------------------------

-- ----------------------------
-- Table structure for empresas
-- ----------------------------
DROP TABLE IF EXISTS `empresas`;
CREATE TABLE `empresas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `redes_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `empresas_nombre_unique` (`nombre`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of empresas
-- ----------------------------
INSERT INTO `empresas` VALUES ('1', 'MERCAL', '1', '2017-03-17 14:36:15', '2017-03-17 14:36:15', null);
INSERT INTO `empresas` VALUES ('2', 'PDVAL', '1', '2017-03-17 14:36:30', '2017-03-17 14:36:30', null);

-- ----------------------------
-- Table structure for ente_ejecutor
-- ----------------------------
DROP TABLE IF EXISTS `ente_ejecutor`;
CREATE TABLE `ente_ejecutor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ente_ejecutor
-- ----------------------------

-- ----------------------------
-- Table structure for estados
-- ----------------------------
DROP TABLE IF EXISTS `estados`;
CREATE TABLE `estados` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `iso_3166-2` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of estados
-- ----------------------------
INSERT INTO `estados` VALUES ('1', 'Amazonas', 'VE-X', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('2', 'Anzoátegui', 'VE-B', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('3', 'Apure', 'VE-C', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('4', 'Aragua', 'VE-D', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('5', 'Barinas', 'VE-E', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('6', 'Bolívar', 'VE-F', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('7', 'Carabobo', 'VE-G', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('8', 'Cojedes', 'VE-H', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('9', 'Delta Amacuro', 'VE-Y', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('10', 'Falcón', 'VE-I', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('11', 'Guárico', 'VE-J', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('12', 'Lara', 'VE-K', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('13', 'Mérida', 'VE-L', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('14', 'Miranda', 'VE-M', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('15', 'Monagas', 'VE-N', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('16', 'Nueva Esparta', 'VE-O', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('17', 'Portuguesa', 'VE-P', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('18', 'Sucre', 'VE-R', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('19', 'Táchira', 'VE-S', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('20', 'Trujillo', 'VE-T', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('21', 'Vargas', 'VE-W', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('22', 'Yaracuy', 'VE-U', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('23', 'Zulia', 'VE-V', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('24', 'Distrito Capital', 'VE-A', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);
INSERT INTO `estados` VALUES ('25', 'Dependencias Federales', 'VE-Z', '2017-03-16 20:14:46', '2017-03-16 20:14:46', null);

-- ----------------------------
-- Table structure for estatus
-- ----------------------------
DROP TABLE IF EXISTS `estatus`;
CREATE TABLE `estatus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of estatus
-- ----------------------------
INSERT INTO `estatus` VALUES ('1', 'Concluida', '2017-01-09 01:00:00', '2017-01-09 01:00:00', null);
INSERT INTO `estatus` VALUES ('2', 'En Ejecución', '2017-01-09 01:00:00', '2017-01-09 01:00:00', null);
INSERT INTO `estatus` VALUES ('3', 'Por Iniciar', '2017-01-09 01:00:00', '2017-01-09 01:00:00', null);

-- ----------------------------
-- Table structure for formalizada
-- ----------------------------
DROP TABLE IF EXISTS `formalizada`;
CREATE TABLE `formalizada` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `puntos_id` int(10) unsigned NOT NULL,
  `cantidad` int(11) NOT NULL,
  `cola` int(11) DEFAULT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `formalizada_puntos_id_foreign` (`puntos_id`),
  CONSTRAINT `formalizada_puntos_id_foreign` FOREIGN KEY (`puntos_id`) REFERENCES `puntos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of formalizada
-- ----------------------------

-- ----------------------------
-- Table structure for fuente_financiamiento
-- ----------------------------
DROP TABLE IF EXISTS `fuente_financiamiento`;
CREATE TABLE `fuente_financiamiento` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of fuente_financiamiento
-- ----------------------------

-- ----------------------------
-- Table structure for fuerza_trabajo
-- ----------------------------
DROP TABLE IF EXISTS `fuerza_trabajo`;
CREATE TABLE `fuerza_trabajo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of fuerza_trabajo
-- ----------------------------
INSERT INTO `fuerza_trabajo` VALUES ('1', 'SISTEMA DE MISIONES', '2017-01-26 11:48:48', '2017-01-26 11:48:48', null);
INSERT INTO `fuerza_trabajo` VALUES ('2', 'FFM', '2017-01-26 11:48:55', '2017-01-26 11:48:55', null);
INSERT INTO `fuerza_trabajo` VALUES ('3', 'SUCRE', '2017-01-26 11:48:58', '2017-01-26 11:48:58', null);
INSERT INTO `fuerza_trabajo` VALUES ('4', 'JPSUV', '2017-01-26 11:49:04', '2017-01-26 11:49:04', null);
INSERT INTO `fuerza_trabajo` VALUES ('5', 'MISIONES', '2017-01-26 11:49:12', '2017-01-26 11:49:12', null);
INSERT INTO `fuerza_trabajo` VALUES ('6', 'ROBERT SERRA', '2017-01-26 11:49:18', '2017-01-26 11:49:18', null);

-- ----------------------------
-- Table structure for ingresos_categoria
-- ----------------------------
DROP TABLE IF EXISTS `ingresos_categoria`;
CREATE TABLE `ingresos_categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ingresos_categoria
-- ----------------------------
INSERT INTO `ingresos_categoria` VALUES ('1', 'Alimentos', '2016-08-08 10:43:18', '2016-08-08 10:43:18', null);
INSERT INTO `ingresos_categoria` VALUES ('2', 'Cosmeticos', '2016-09-09 12:13:55', '2016-08-08 10:43:41', '2016-09-09 10:56:28');
INSERT INTO `ingresos_categoria` VALUES ('3', 'No alimenticios', '2016-08-09 10:10:32', '2016-08-09 10:10:32', null);
INSERT INTO `ingresos_categoria` VALUES ('4', 'prueba', '2017-03-17 13:47:30', '2017-03-17 12:43:38', '2017-03-17 13:47:30');
INSERT INTO `ingresos_categoria` VALUES ('5', 'pruebas', '2017-03-17 13:48:21', '2017-03-17 13:48:21', null);

-- ----------------------------
-- Table structure for ingresos_inventario
-- ----------------------------
DROP TABLE IF EXISTS `ingresos_inventario`;
CREATE TABLE `ingresos_inventario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `empresa_id` int(10) unsigned NOT NULL,
  `ingresorubros_id` int(10) unsigned NOT NULL,
  `cantidad` decimal(8,2) NOT NULL DEFAULT '0.00',
  `mes` int(11) NOT NULL,
  `mes_str` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ano` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ingresos_inventario
-- ----------------------------
INSERT INTO `ingresos_inventario` VALUES ('1', '1', '6', '200.00', '3', 'marzo', '2017', '2017-03-17 14:43:09', '2017-03-17 14:43:09', null);
INSERT INTO `ingresos_inventario` VALUES ('2', '1', '5', '0.00', '3', 'marzo', '2017', '2017-03-17 14:43:09', '2017-03-17 14:43:09', null);
INSERT INTO `ingresos_inventario` VALUES ('3', '2', '6', '50.00', '3', 'marzo', '2017', '2017-03-17 14:43:09', '2017-03-17 14:43:09', null);
INSERT INTO `ingresos_inventario` VALUES ('4', '2', '5', '0.00', '3', 'marzo', '2017', '2017-03-17 14:43:09', '2017-03-17 14:43:09', null);
INSERT INTO `ingresos_inventario` VALUES ('5', '1', '6', '230.00', '2', 'febrero', '2017', '2017-03-17 14:44:39', '2017-03-17 14:44:39', null);
INSERT INTO `ingresos_inventario` VALUES ('6', '1', '5', '0.00', '2', 'febrero', '2017', '2017-03-17 14:44:39', '2017-03-17 14:44:39', null);
INSERT INTO `ingresos_inventario` VALUES ('7', '2', '6', '25.00', '2', 'febrero', '2017', '2017-03-17 14:44:39', '2017-03-17 14:44:39', null);
INSERT INTO `ingresos_inventario` VALUES ('8', '2', '5', '0.00', '2', 'febrero', '2017', '2017-03-17 14:44:39', '2017-03-17 14:44:39', null);
INSERT INTO `ingresos_inventario` VALUES ('9', '1', '6', '344.00', '1', 'enero', '2017', '2017-03-17 14:44:50', '2017-03-17 14:44:50', null);
INSERT INTO `ingresos_inventario` VALUES ('10', '1', '5', '0.00', '1', 'enero', '2017', '2017-03-17 14:44:50', '2017-03-17 14:44:50', null);
INSERT INTO `ingresos_inventario` VALUES ('11', '2', '6', '89.00', '1', 'enero', '2017', '2017-03-17 14:44:50', '2017-03-17 14:44:50', null);
INSERT INTO `ingresos_inventario` VALUES ('12', '2', '5', '0.00', '1', 'enero', '2017', '2017-03-17 14:44:50', '2017-03-17 14:44:50', null);

-- ----------------------------
-- Table structure for ingresos_requerimiento
-- ----------------------------
DROP TABLE IF EXISTS `ingresos_requerimiento`;
CREATE TABLE `ingresos_requerimiento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `municipio_id` int(11) NOT NULL,
  `rubro_id` int(11) NOT NULL,
  `cantidad` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1343 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ingresos_requerimiento
-- ----------------------------
INSERT INTO `ingresos_requerimiento` VALUES ('1', '1', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('2', '2', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('3', '3', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('4', '4', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('5', '5', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('6', '6', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('7', '7', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('8', '8', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('9', '9', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('10', '10', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('11', '11', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('12', '12', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('13', '13', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('14', '14', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('15', '15', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('16', '16', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('17', '17', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('18', '18', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('19', '19', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('20', '20', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('21', '21', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('22', '22', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('23', '23', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('24', '24', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('25', '25', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('26', '26', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('27', '27', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('28', '28', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('29', '29', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('30', '30', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('31', '31', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('32', '32', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('33', '33', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('34', '34', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('35', '35', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('36', '36', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('37', '37', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('38', '38', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('39', '39', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('40', '40', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('41', '41', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('42', '42', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('43', '43', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('44', '44', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('45', '45', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('46', '46', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('47', '47', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('48', '48', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('49', '49', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('50', '50', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('51', '51', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('52', '52', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('53', '53', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('54', '54', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('55', '55', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('56', '56', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('57', '57', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('58', '58', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('59', '59', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('60', '60', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('61', '61', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('62', '62', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('63', '63', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('64', '64', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('65', '65', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('66', '66', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('67', '67', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('68', '68', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('69', '69', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('70', '70', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('71', '71', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('72', '72', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('73', '73', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('74', '74', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('75', '75', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('76', '76', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('77', '77', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('78', '78', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('79', '79', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('80', '80', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('81', '81', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('82', '82', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('83', '83', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('84', '84', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('85', '85', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('86', '86', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('87', '87', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('88', '88', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('89', '89', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('90', '90', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('91', '91', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('92', '92', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('93', '93', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('94', '94', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('95', '95', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('96', '96', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('97', '97', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('98', '98', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('99', '99', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('100', '100', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('101', '101', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('102', '102', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('103', '103', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('104', '104', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('105', '105', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('106', '106', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('107', '107', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('108', '108', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('109', '109', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('110', '110', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('111', '111', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('112', '112', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('113', '113', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('114', '114', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('115', '115', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('116', '116', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('117', '117', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('118', '118', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('119', '119', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('120', '120', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('121', '121', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('122', '122', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('123', '123', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('124', '124', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('125', '125', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('126', '126', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('127', '127', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('128', '128', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('129', '129', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('130', '130', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('131', '131', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('132', '132', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('133', '133', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('134', '134', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('135', '135', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('136', '136', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('137', '137', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('138', '138', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('139', '139', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('140', '140', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('141', '141', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('142', '142', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('143', '143', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('144', '144', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('145', '145', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('146', '146', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('147', '147', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('148', '148', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('149', '149', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('150', '150', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('151', '151', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('152', '152', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('153', '153', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('154', '154', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('155', '155', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('156', '156', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('157', '157', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('158', '158', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('159', '159', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('160', '160', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('161', '161', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('162', '162', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('163', '163', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('164', '164', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('165', '165', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('166', '166', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('167', '167', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('168', '168', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('169', '169', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('170', '170', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('171', '171', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('172', '172', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('173', '173', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('174', '174', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('175', '175', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('176', '176', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('177', '177', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('178', '178', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('179', '179', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('180', '180', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('181', '181', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('182', '182', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('183', '183', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('184', '184', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('185', '185', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('186', '186', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('187', '187', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('188', '188', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('189', '189', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('190', '190', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('191', '191', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('192', '192', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('193', '193', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('194', '194', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('195', '195', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('196', '196', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('197', '197', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('198', '198', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('199', '199', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('200', '200', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('201', '201', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('202', '202', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('203', '203', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('204', '204', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('205', '205', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('206', '206', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('207', '207', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('208', '208', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('209', '209', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('210', '210', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('211', '211', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('212', '212', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('213', '213', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('214', '214', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('215', '215', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('216', '216', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('217', '217', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('218', '218', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('219', '219', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('220', '220', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('221', '221', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('222', '222', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('223', '223', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('224', '224', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('225', '225', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('226', '226', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('227', '227', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('228', '228', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('229', '229', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('230', '230', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('231', '231', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('232', '232', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('233', '233', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('234', '234', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('235', '235', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('236', '236', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('237', '237', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('238', '238', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('239', '239', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('240', '240', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('241', '241', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('242', '242', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('243', '243', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('244', '244', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('245', '245', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('246', '246', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('247', '247', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('248', '248', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('249', '249', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('250', '250', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('251', '251', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('252', '252', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('253', '253', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('254', '254', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('255', '255', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('256', '256', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('257', '257', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('258', '258', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('259', '259', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('260', '260', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('261', '261', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('262', '262', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('263', '263', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('264', '264', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('265', '265', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('266', '266', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('267', '267', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('268', '268', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('269', '269', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('270', '270', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('271', '271', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('272', '272', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('273', '273', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('274', '274', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('275', '275', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('276', '276', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('277', '277', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('278', '278', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('279', '279', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('280', '280', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('281', '281', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('282', '282', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('283', '283', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('284', '284', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('285', '285', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('286', '286', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('287', '287', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('288', '288', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('289', '289', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('290', '290', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('291', '291', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('292', '292', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('293', '293', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('294', '294', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('295', '295', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('296', '296', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('297', '297', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('298', '298', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('299', '299', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('300', '300', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('301', '301', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('302', '302', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('303', '303', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('304', '304', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('305', '305', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('306', '306', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('307', '307', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('308', '308', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('309', '309', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('310', '310', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('311', '311', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('312', '312', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('313', '313', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('314', '314', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('315', '315', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('316', '316', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('317', '317', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('318', '318', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('319', '319', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('320', '320', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('321', '321', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('322', '322', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('323', '323', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('324', '324', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('325', '325', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('326', '326', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('327', '327', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('328', '328', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('329', '329', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('330', '330', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('331', '331', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('332', '332', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('333', '333', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('334', '334', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('335', '335', '3', '0.00', '2017-03-17 12:43:02', '2017-03-17 12:43:02', null);
INSERT INTO `ingresos_requerimiento` VALUES ('336', '1', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('337', '2', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('338', '3', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('339', '4', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('340', '5', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('341', '6', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('342', '7', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('343', '8', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('344', '9', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('345', '10', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('346', '11', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('347', '12', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('348', '13', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('349', '14', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('350', '15', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('351', '16', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('352', '17', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('353', '18', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('354', '19', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('355', '20', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('356', '21', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('357', '22', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('358', '23', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('359', '24', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('360', '25', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('361', '26', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('362', '27', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('363', '28', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('364', '29', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('365', '30', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('366', '31', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('367', '32', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('368', '33', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('369', '34', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('370', '35', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('371', '36', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('372', '37', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('373', '38', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('374', '39', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('375', '40', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('376', '41', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('377', '42', '4', '0.00', '2017-03-17 13:21:49', '2017-03-17 13:21:49', null);
INSERT INTO `ingresos_requerimiento` VALUES ('378', '43', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('379', '44', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('380', '45', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('381', '46', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('382', '47', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('383', '48', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('384', '49', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('385', '50', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('386', '51', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('387', '52', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('388', '53', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('389', '54', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('390', '55', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('391', '56', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('392', '57', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('393', '58', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('394', '59', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('395', '60', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('396', '61', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('397', '62', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('398', '63', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('399', '64', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('400', '65', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('401', '66', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('402', '67', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('403', '68', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('404', '69', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('405', '70', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('406', '71', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('407', '72', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('408', '73', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('409', '74', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('410', '75', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('411', '76', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('412', '77', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('413', '78', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('414', '79', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('415', '80', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('416', '81', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('417', '82', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('418', '83', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('419', '84', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('420', '85', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('421', '86', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('422', '87', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('423', '88', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('424', '89', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('425', '90', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('426', '91', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('427', '92', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('428', '93', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('429', '94', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('430', '95', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('431', '96', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('432', '97', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('433', '98', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('434', '99', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('435', '100', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('436', '101', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('437', '102', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('438', '103', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('439', '104', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('440', '105', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('441', '106', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('442', '107', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('443', '108', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('444', '109', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('445', '110', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('446', '111', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('447', '112', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('448', '113', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('449', '114', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('450', '115', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('451', '116', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('452', '117', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('453', '118', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('454', '119', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('455', '120', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('456', '121', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('457', '122', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('458', '123', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('459', '124', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('460', '125', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('461', '126', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('462', '127', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('463', '128', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('464', '129', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('465', '130', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('466', '131', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('467', '132', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('468', '133', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('469', '134', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('470', '135', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('471', '136', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('472', '137', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('473', '138', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('474', '139', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('475', '140', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('476', '141', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('477', '142', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('478', '143', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('479', '144', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('480', '145', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('481', '146', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('482', '147', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('483', '148', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('484', '149', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('485', '150', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('486', '151', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('487', '152', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('488', '153', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('489', '154', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('490', '155', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('491', '156', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('492', '157', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('493', '158', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('494', '159', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('495', '160', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('496', '161', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('497', '162', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('498', '163', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('499', '164', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('500', '165', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('501', '166', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('502', '167', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('503', '168', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('504', '169', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('505', '170', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('506', '171', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('507', '172', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('508', '173', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('509', '174', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('510', '175', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('511', '176', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('512', '177', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('513', '178', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('514', '179', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('515', '180', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('516', '181', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('517', '182', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('518', '183', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('519', '184', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('520', '185', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('521', '186', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('522', '187', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('523', '188', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('524', '189', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('525', '190', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('526', '191', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('527', '192', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('528', '193', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('529', '194', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('530', '195', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('531', '196', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('532', '197', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('533', '198', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('534', '199', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('535', '200', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('536', '201', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('537', '202', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('538', '203', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('539', '204', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('540', '205', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('541', '206', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('542', '207', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('543', '208', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('544', '209', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('545', '210', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('546', '211', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('547', '212', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('548', '213', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('549', '214', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('550', '215', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('551', '216', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('552', '217', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('553', '218', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('554', '219', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('555', '220', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('556', '221', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('557', '222', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('558', '223', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('559', '224', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('560', '225', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('561', '226', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('562', '227', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('563', '228', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('564', '229', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('565', '230', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('566', '231', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('567', '232', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('568', '233', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('569', '234', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('570', '235', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('571', '236', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('572', '237', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('573', '238', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('574', '239', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('575', '240', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('576', '241', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('577', '242', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('578', '243', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('579', '244', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('580', '245', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('581', '246', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('582', '247', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('583', '248', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('584', '249', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('585', '250', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('586', '251', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('587', '252', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('588', '253', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('589', '254', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('590', '255', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('591', '256', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('592', '257', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('593', '258', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('594', '259', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('595', '260', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('596', '261', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('597', '262', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('598', '263', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('599', '264', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('600', '265', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('601', '266', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('602', '267', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('603', '268', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('604', '269', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('605', '270', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('606', '271', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('607', '272', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('608', '273', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('609', '274', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('610', '275', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('611', '276', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('612', '277', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('613', '278', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('614', '279', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('615', '280', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('616', '281', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('617', '282', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('618', '283', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('619', '284', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('620', '285', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('621', '286', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('622', '287', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('623', '288', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('624', '289', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('625', '290', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('626', '291', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('627', '292', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('628', '293', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('629', '294', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('630', '295', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('631', '296', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('632', '297', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('633', '298', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('634', '299', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('635', '300', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('636', '301', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('637', '302', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('638', '303', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('639', '304', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('640', '305', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('641', '306', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('642', '307', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('643', '308', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('644', '309', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('645', '310', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('646', '311', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('647', '312', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('648', '313', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('649', '314', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('650', '315', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('651', '316', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('652', '317', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('653', '318', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('654', '319', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('655', '320', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('656', '321', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('657', '322', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('658', '323', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('659', '324', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('660', '325', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('661', '326', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('662', '327', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('663', '328', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('664', '329', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('665', '330', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('666', '331', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('667', '332', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('668', '333', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('669', '334', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('670', '335', '4', '0.00', '2017-03-17 13:21:50', '2017-03-17 13:21:50', null);
INSERT INTO `ingresos_requerimiento` VALUES ('671', '1', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('672', '2', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('673', '3', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('674', '4', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('675', '5', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('676', '6', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('677', '7', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('678', '8', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('679', '9', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('680', '10', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('681', '11', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('682', '12', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('683', '13', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('684', '14', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('685', '15', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('686', '16', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('687', '17', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('688', '18', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('689', '19', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('690', '20', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('691', '21', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('692', '22', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('693', '23', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('694', '24', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('695', '25', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('696', '26', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('697', '27', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('698', '28', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('699', '29', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('700', '30', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('701', '31', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('702', '32', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('703', '33', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('704', '34', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('705', '35', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('706', '36', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('707', '37', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('708', '38', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('709', '39', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('710', '40', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('711', '41', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('712', '42', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('713', '43', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('714', '44', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('715', '45', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('716', '46', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('717', '47', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('718', '48', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('719', '49', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('720', '50', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('721', '51', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('722', '52', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('723', '53', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('724', '54', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('725', '55', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('726', '56', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('727', '57', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('728', '58', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('729', '59', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('730', '60', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('731', '61', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('732', '62', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('733', '63', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('734', '64', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('735', '65', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('736', '66', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('737', '67', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('738', '68', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('739', '69', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('740', '70', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('741', '71', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('742', '72', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('743', '73', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('744', '74', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('745', '75', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('746', '76', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('747', '77', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('748', '78', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('749', '79', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('750', '80', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('751', '81', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('752', '82', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('753', '83', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('754', '84', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('755', '85', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('756', '86', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('757', '87', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('758', '88', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('759', '89', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('760', '90', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('761', '91', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('762', '92', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('763', '93', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('764', '94', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('765', '95', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('766', '96', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('767', '97', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('768', '98', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('769', '99', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('770', '100', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('771', '101', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('772', '102', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('773', '103', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('774', '104', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('775', '105', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('776', '106', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('777', '107', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('778', '108', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('779', '109', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('780', '110', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('781', '111', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('782', '112', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('783', '113', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('784', '114', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('785', '115', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('786', '116', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('787', '117', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('788', '118', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('789', '119', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('790', '120', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('791', '121', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('792', '122', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('793', '123', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('794', '124', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('795', '125', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('796', '126', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('797', '127', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('798', '128', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('799', '129', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('800', '130', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('801', '131', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('802', '132', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('803', '133', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('804', '134', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('805', '135', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('806', '136', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('807', '137', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('808', '138', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('809', '139', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('810', '140', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('811', '141', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('812', '142', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('813', '143', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('814', '144', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('815', '145', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('816', '146', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('817', '147', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('818', '148', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('819', '149', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('820', '150', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('821', '151', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('822', '152', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('823', '153', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('824', '154', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('825', '155', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('826', '156', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('827', '157', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('828', '158', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('829', '159', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('830', '160', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('831', '161', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('832', '162', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('833', '163', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('834', '164', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('835', '165', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('836', '166', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('837', '167', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('838', '168', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('839', '169', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('840', '170', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('841', '171', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('842', '172', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('843', '173', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('844', '174', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('845', '175', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('846', '176', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('847', '177', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('848', '178', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('849', '179', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('850', '180', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('851', '181', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('852', '182', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('853', '183', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('854', '184', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('855', '185', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('856', '186', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('857', '187', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('858', '188', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('859', '189', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('860', '190', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('861', '191', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('862', '192', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('863', '193', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('864', '194', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('865', '195', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('866', '196', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('867', '197', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('868', '198', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('869', '199', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('870', '200', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('871', '201', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('872', '202', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('873', '203', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('874', '204', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('875', '205', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('876', '206', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('877', '207', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('878', '208', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('879', '209', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('880', '210', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('881', '211', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('882', '212', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('883', '213', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('884', '214', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('885', '215', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('886', '216', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('887', '217', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('888', '218', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('889', '219', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('890', '220', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('891', '221', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('892', '222', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('893', '223', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('894', '224', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('895', '225', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('896', '226', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('897', '227', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('898', '228', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('899', '229', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('900', '230', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('901', '231', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('902', '232', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('903', '233', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('904', '234', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('905', '235', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('906', '236', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('907', '237', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('908', '238', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('909', '239', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('910', '240', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('911', '241', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('912', '242', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('913', '243', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('914', '244', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('915', '245', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('916', '246', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('917', '247', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('918', '248', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('919', '249', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('920', '250', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('921', '251', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('922', '252', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('923', '253', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('924', '254', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('925', '255', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('926', '256', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('927', '257', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('928', '258', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('929', '259', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('930', '260', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('931', '261', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('932', '262', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('933', '263', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('934', '264', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('935', '265', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('936', '266', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('937', '267', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('938', '268', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('939', '269', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('940', '270', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('941', '271', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('942', '272', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('943', '273', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('944', '274', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('945', '275', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('946', '276', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('947', '277', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('948', '278', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('949', '279', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('950', '280', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('951', '281', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('952', '282', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('953', '283', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('954', '284', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('955', '285', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('956', '286', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('957', '287', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('958', '288', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('959', '289', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('960', '290', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('961', '291', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('962', '292', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('963', '293', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('964', '294', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('965', '295', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('966', '296', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('967', '297', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('968', '298', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('969', '299', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('970', '300', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('971', '301', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('972', '302', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('973', '303', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('974', '304', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('975', '305', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('976', '306', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('977', '307', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('978', '308', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('979', '309', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('980', '310', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('981', '311', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('982', '312', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('983', '313', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('984', '314', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('985', '315', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('986', '316', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('987', '317', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('988', '318', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('989', '319', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('990', '320', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('991', '321', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('992', '322', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('993', '323', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('994', '324', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('995', '325', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('996', '326', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('997', '327', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('998', '328', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('999', '329', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1000', '330', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1001', '331', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1002', '332', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1003', '333', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1004', '334', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1005', '335', '5', '0.00', '2017-03-17 13:41:51', '2017-03-17 13:41:51', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1006', '14', '5', '1423.00', '2017-03-17 13:42:12', '2017-03-17 13:45:40', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1007', '1', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1008', '2', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1009', '3', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1010', '4', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1011', '5', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1012', '6', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1013', '7', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1014', '8', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1015', '9', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1016', '10', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1017', '11', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1018', '12', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1019', '13', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1020', '14', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1021', '15', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1022', '16', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1023', '17', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1024', '18', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1025', '19', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1026', '20', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1027', '21', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1028', '22', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1029', '23', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1030', '24', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1031', '25', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1032', '26', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1033', '27', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1034', '28', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1035', '29', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1036', '30', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1037', '31', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1038', '32', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1039', '33', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1040', '34', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1041', '35', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1042', '36', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1043', '37', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1044', '38', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1045', '39', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1046', '40', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1047', '41', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1048', '42', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1049', '43', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1050', '44', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1051', '45', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1052', '46', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1053', '47', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1054', '48', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1055', '49', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1056', '50', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1057', '51', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1058', '52', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1059', '53', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1060', '54', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1061', '55', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1062', '56', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1063', '57', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1064', '58', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1065', '59', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1066', '60', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1067', '61', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1068', '62', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1069', '63', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1070', '64', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1071', '65', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1072', '66', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1073', '67', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1074', '68', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1075', '69', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1076', '70', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1077', '71', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1078', '72', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1079', '73', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1080', '74', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1081', '75', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1082', '76', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1083', '77', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1084', '78', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1085', '79', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1086', '80', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1087', '81', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1088', '82', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1089', '83', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1090', '84', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1091', '85', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1092', '86', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1093', '87', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1094', '88', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1095', '89', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1096', '90', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1097', '91', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1098', '92', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1099', '93', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1100', '94', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1101', '95', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1102', '96', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1103', '97', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1104', '98', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1105', '99', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1106', '100', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1107', '101', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1108', '102', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1109', '103', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1110', '104', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1111', '105', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1112', '106', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1113', '107', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1114', '108', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1115', '109', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1116', '110', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1117', '111', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1118', '112', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1119', '113', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1120', '114', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1121', '115', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1122', '116', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1123', '117', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1124', '118', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1125', '119', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1126', '120', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1127', '121', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1128', '122', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1129', '123', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1130', '124', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1131', '125', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1132', '126', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1133', '127', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1134', '128', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1135', '129', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1136', '130', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1137', '131', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1138', '132', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1139', '133', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1140', '134', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1141', '135', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1142', '136', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1143', '137', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1144', '138', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1145', '139', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1146', '140', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1147', '141', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1148', '142', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1149', '143', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1150', '144', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1151', '145', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1152', '146', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1153', '147', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1154', '148', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1155', '149', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1156', '150', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1157', '151', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1158', '152', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1159', '153', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1160', '154', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1161', '155', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1162', '156', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1163', '157', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1164', '158', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1165', '159', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1166', '160', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1167', '161', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1168', '162', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1169', '163', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1170', '164', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1171', '165', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1172', '166', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1173', '167', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1174', '168', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1175', '169', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1176', '170', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1177', '171', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1178', '172', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1179', '173', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1180', '174', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1181', '175', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1182', '176', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1183', '177', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1184', '178', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1185', '179', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1186', '180', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1187', '181', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1188', '182', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1189', '183', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1190', '184', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1191', '185', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1192', '186', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1193', '187', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1194', '188', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1195', '189', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1196', '190', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1197', '191', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1198', '192', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1199', '193', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1200', '194', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1201', '195', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1202', '196', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1203', '197', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1204', '198', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1205', '199', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1206', '200', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1207', '201', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1208', '202', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1209', '203', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1210', '204', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1211', '205', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1212', '206', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1213', '207', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1214', '208', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1215', '209', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1216', '210', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1217', '211', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1218', '212', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1219', '213', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1220', '214', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1221', '215', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1222', '216', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1223', '217', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1224', '218', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1225', '219', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1226', '220', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1227', '221', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1228', '222', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1229', '223', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1230', '224', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1231', '225', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1232', '226', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1233', '227', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1234', '228', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1235', '229', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1236', '230', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1237', '231', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1238', '232', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1239', '233', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1240', '234', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1241', '235', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1242', '236', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1243', '237', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1244', '238', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1245', '239', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1246', '240', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1247', '241', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1248', '242', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1249', '243', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1250', '244', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1251', '245', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1252', '246', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1253', '247', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1254', '248', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1255', '249', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1256', '250', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1257', '251', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1258', '252', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1259', '253', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1260', '254', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1261', '255', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1262', '256', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1263', '257', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1264', '258', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1265', '259', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1266', '260', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1267', '261', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1268', '262', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1269', '263', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1270', '264', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1271', '265', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1272', '266', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1273', '267', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1274', '268', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1275', '269', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1276', '270', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1277', '271', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1278', '272', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1279', '273', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1280', '274', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1281', '275', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1282', '276', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1283', '277', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1284', '278', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1285', '279', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1286', '280', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1287', '281', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1288', '282', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1289', '283', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1290', '284', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1291', '285', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1292', '286', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1293', '287', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1294', '288', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1295', '289', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1296', '290', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1297', '291', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1298', '292', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1299', '293', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1300', '294', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1301', '295', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1302', '296', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1303', '297', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1304', '298', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1305', '299', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1306', '300', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1307', '301', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1308', '302', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1309', '303', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1310', '304', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1311', '305', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1312', '306', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1313', '307', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1314', '308', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1315', '309', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1316', '310', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1317', '311', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1318', '312', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1319', '313', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1320', '314', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1321', '315', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1322', '316', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1323', '317', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1324', '318', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1325', '319', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1326', '320', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1327', '321', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1328', '322', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1329', '323', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1330', '324', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1331', '325', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1332', '326', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1333', '327', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1334', '328', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1335', '329', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1336', '330', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1337', '331', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1338', '332', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1339', '333', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1340', '334', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1341', '335', '6', '0.00', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);
INSERT INTO `ingresos_requerimiento` VALUES ('1342', '1', '6', '100.00', '2017-03-17 14:44:00', '2017-03-17 14:44:00', null);

-- ----------------------------
-- Table structure for ingresos_rubros
-- ----------------------------
DROP TABLE IF EXISTS `ingresos_rubros`;
CREATE TABLE `ingresos_rubros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `estatus` int(11) DEFAULT '1',
  `categoria_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingreso_rubros_nombre_unique` (`nombre`) USING BTREE,
  KEY `ingreso_rubro_categoria_id` (`categoria_id`) USING BTREE,
  CONSTRAINT `ingresos_rubros_ibfk_1` FOREIGN KEY (`categoria_id`) REFERENCES `ingresos_categoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ingresos_rubros
-- ----------------------------
INSERT INTO `ingresos_rubros` VALUES ('3', 'prueba 2', '1', '1', '2017-03-17 12:43:02', '2017-03-17 12:43:15', '2017-03-17 12:43:22');
INSERT INTO `ingresos_rubros` VALUES ('4', 'pruebas', '0', '1', '2017-03-17 13:21:49', '2017-03-17 13:21:54', '2017-03-17 13:22:02');
INSERT INTO `ingresos_rubros` VALUES ('5', 'prueba adfad', '0', '1', '2017-03-17 13:41:51', '2017-03-17 13:41:57', null);
INSERT INTO `ingresos_rubros` VALUES ('6', 'AZUCAR', '1', '1', '2017-03-17 14:41:04', '2017-03-17 14:41:04', null);

-- ----------------------------
-- Table structure for inventario
-- ----------------------------
DROP TABLE IF EXISTS `inventario`;
CREATE TABLE `inventario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `empresa_id` int(10) unsigned NOT NULL,
  `rubros_id` int(10) unsigned NOT NULL,
  `cantidad` decimal(8,2) NOT NULL,
  `fecha` date NOT NULL,
  `ano` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `inventario_index_unique` (`empresa_id`,`rubros_id`,`fecha`,`ano`) USING BTREE,
  KEY `inventario_rubros_id_foreign` (`rubros_id`) USING BTREE,
  CONSTRAINT `inventario_ibfk_1` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `inventario_ibfk_2` FOREIGN KEY (`rubros_id`) REFERENCES `rubros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of inventario
-- ----------------------------
INSERT INTO `inventario` VALUES ('1', '1', '1', '45.83', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('2', '1', '2', '481.60', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('3', '1', '3', '0.00', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('4', '1', '4', '590.80', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('5', '1', '5', '0.00', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('6', '1', '6', '403.40', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('7', '1', '7', '0.00', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('8', '1', '8', '0.00', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('9', '1', '23', '0.00', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('10', '1', '9', '975.50', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('11', '1', '11', '36.10', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('12', '1', '10', '0.00', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('13', '1', '12', '0.00', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('14', '1', '13', '425.30', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('15', '1', '14', '1.00', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('16', '1', '15', '77.40', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('17', '1', '21', '30.10', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('18', '1', '16', '0.00', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('19', '1', '17', '14.40', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('20', '1', '18', '0.00', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('21', '1', '22', '651.10', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('22', '1', '20', '106.70', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('23', '2', '1', '9.90', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('24', '2', '2', '0.00', '2017-03-20', '2017', '2017-03-20 11:02:31', '2017-03-20 11:02:31', null);
INSERT INTO `inventario` VALUES ('25', '2', '3', '0.76', '2017-03-20', '2017', '2017-03-20 11:02:32', '2017-03-20 11:02:32', null);
INSERT INTO `inventario` VALUES ('26', '2', '4', '1.95', '2017-03-20', '2017', '2017-03-20 11:02:32', '2017-03-20 11:02:32', null);
INSERT INTO `inventario` VALUES ('27', '2', '5', '0.00', '2017-03-20', '2017', '2017-03-20 11:02:32', '2017-03-20 11:02:32', null);
INSERT INTO `inventario` VALUES ('28', '2', '6', '4.53', '2017-03-20', '2017', '2017-03-20 11:02:32', '2017-03-20 11:02:32', null);
INSERT INTO `inventario` VALUES ('29', '2', '7', '0.34', '2017-03-20', '2017', '2017-03-20 11:02:32', '2017-03-20 11:02:32', null);
INSERT INTO `inventario` VALUES ('30', '2', '8', '0.00', '2017-03-20', '2017', '2017-03-20 11:02:32', '2017-03-20 11:02:32', null);
INSERT INTO `inventario` VALUES ('31', '2', '23', '0.00', '2017-03-20', '2017', '2017-03-20 11:02:32', '2017-03-20 11:02:32', null);
INSERT INTO `inventario` VALUES ('32', '2', '9', '0.00', '2017-03-20', '2017', '2017-03-20 11:02:32', '2017-03-20 11:02:32', null);
INSERT INTO `inventario` VALUES ('33', '2', '11', '0.00', '2017-03-20', '2017', '2017-03-20 11:02:32', '2017-03-20 11:02:32', null);
INSERT INTO `inventario` VALUES ('34', '2', '10', '0.00', '2017-03-20', '2017', '2017-03-20 11:02:32', '2017-03-20 11:02:32', null);
INSERT INTO `inventario` VALUES ('35', '2', '12', '0.00', '2017-03-20', '2017', '2017-03-20 11:02:32', '2017-03-20 11:02:32', null);
INSERT INTO `inventario` VALUES ('36', '2', '13', '0.00', '2017-03-20', '2017', '2017-03-20 11:02:32', '2017-03-20 11:02:32', null);
INSERT INTO `inventario` VALUES ('37', '2', '14', '0.00', '2017-03-20', '2017', '2017-03-20 11:02:32', '2017-03-20 11:02:32', null);
INSERT INTO `inventario` VALUES ('38', '2', '15', '0.00', '2017-03-20', '2017', '2017-03-20 11:02:32', '2017-03-20 11:02:32', null);
INSERT INTO `inventario` VALUES ('39', '2', '21', '0.86', '2017-03-20', '2017', '2017-03-20 11:02:32', '2017-03-20 11:02:32', null);
INSERT INTO `inventario` VALUES ('40', '2', '16', '0.00', '2017-03-20', '2017', '2017-03-20 11:02:32', '2017-03-20 11:02:32', null);
INSERT INTO `inventario` VALUES ('41', '2', '17', '3.30', '2017-03-20', '2017', '2017-03-20 11:02:32', '2017-03-20 11:02:32', null);
INSERT INTO `inventario` VALUES ('42', '2', '18', '11.67', '2017-03-20', '2017', '2017-03-20 11:02:32', '2017-03-20 11:02:32', null);
INSERT INTO `inventario` VALUES ('43', '2', '22', '0.23', '2017-03-20', '2017', '2017-03-20 11:02:32', '2017-03-20 11:02:32', null);
INSERT INTO `inventario` VALUES ('44', '2', '20', '0.00', '2017-03-20', '2017', '2017-03-20 11:02:32', '2017-03-20 11:02:32', null);
INSERT INTO `inventario` VALUES ('45', '1', '1', '45.83', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('46', '1', '2', '497.00', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('47', '1', '3', '0.00', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('48', '1', '4', '654.30', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('49', '1', '5', '0.00', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('50', '1', '6', '427.60', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('51', '1', '7', '0.00', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('52', '1', '8', '0.00', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('53', '1', '23', '0.00', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('54', '1', '9', '97.50', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('55', '1', '11', '32.90', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('56', '1', '10', '0.00', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('57', '1', '12', '0.00', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('58', '1', '13', '425.80', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('59', '1', '14', '3.40', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('60', '1', '15', '131.10', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('61', '1', '21', '31.80', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('62', '1', '16', '0.00', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('63', '1', '17', '14.90', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('64', '1', '18', '0.00', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('65', '1', '22', '1003.40', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('66', '1', '20', '105.50', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('67', '2', '1', '10.24', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('68', '2', '2', '0.00', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('69', '2', '3', '1.03', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('70', '2', '4', '2.36', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('71', '2', '5', '0.00', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('72', '2', '6', '5.04', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('73', '2', '7', '0.48', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('74', '2', '8', '0.00', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('75', '2', '23', '0.00', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('76', '2', '9', '0.00', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('77', '2', '11', '0.00', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('78', '2', '10', '0.00', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('79', '2', '12', '0.00', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('80', '2', '13', '0.00', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('81', '2', '14', '0.00', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('82', '2', '15', '0.00', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('83', '2', '21', '2.23', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('84', '2', '16', '0.00', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('85', '2', '17', '3.44', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('86', '2', '18', '13.82', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('87', '2', '22', '0.58', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('88', '2', '20', '0.00', '2017-03-17', '2017', '2017-03-20 11:03:52', '2017-03-20 11:03:52', null);
INSERT INTO `inventario` VALUES ('89', '1', '1', '45.83', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('90', '1', '2', '499.40', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('91', '1', '3', '0.00', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('92', '1', '4', '637.00', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('93', '1', '5', '0.00', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('94', '1', '6', '429.30', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('95', '1', '7', '0.00', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('96', '1', '8', '0.00', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('97', '1', '23', '0.00', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('98', '1', '9', '83.50', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('99', '1', '11', '55.20', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('100', '1', '10', '0.00', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('101', '1', '12', '0.00', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('102', '1', '13', '432.60', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('103', '1', '14', '17.74', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('104', '1', '15', '151.20', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('105', '1', '21', '32.40', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('106', '1', '16', '0.00', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('107', '1', '17', '14.60', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('108', '1', '18', '0.00', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('109', '1', '22', '647.10', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('110', '1', '20', '146.90', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('111', '2', '1', '10.24', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('112', '2', '2', '0.00', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('113', '2', '3', '1.03', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('114', '2', '4', '2.36', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('115', '2', '5', '0.00', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('116', '2', '6', '5.04', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('117', '2', '7', '0.48', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('118', '2', '8', '0.00', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('119', '2', '23', '0.00', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('120', '2', '9', '0.00', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('121', '2', '11', '0.00', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('122', '2', '10', '0.00', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('123', '2', '12', '0.00', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('124', '2', '13', '0.00', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('125', '2', '14', '0.00', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('126', '2', '15', '0.00', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('127', '2', '21', '2.77', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('128', '2', '16', '0.00', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('129', '2', '17', '3.44', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('130', '2', '18', '14.16', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('131', '2', '22', '0.58', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);
INSERT INTO `inventario` VALUES ('132', '2', '20', '0.00', '2017-03-16', '2017', '2017-03-20 11:04:14', '2017-03-20 11:04:14', null);

-- ----------------------------
-- Table structure for jornada
-- ----------------------------
DROP TABLE IF EXISTS `jornada`;
CREATE TABLE `jornada` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `inicio` date NOT NULL,
  `fin` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `jornada_inicio_unique` (`inicio`) USING BTREE,
  UNIQUE KEY `jornada_fin_unique` (`fin`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of jornada
-- ----------------------------

-- ----------------------------
-- Table structure for jornada_proceso
-- ----------------------------
DROP TABLE IF EXISTS `jornada_proceso`;
CREATE TABLE `jornada_proceso` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `clap_id` int(10) unsigned NOT NULL,
  `cantidad` decimal(8,2) NOT NULL,
  `familia` int(10) unsigned NOT NULL,
  `fecha` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `jornada_proceso_index_unique` (`clap_id`,`fecha`) USING BTREE,
  CONSTRAINT `jornada_proceso_ibfk_1` FOREIGN KEY (`clap_id`) REFERENCES `clap` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of jornada_proceso
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('2016_04_21_181148_tablas_iniciales', '1');
INSERT INTO `migrations` VALUES ('2016_06_30_191048_tablas_iniciales', '2');

-- ----------------------------
-- Table structure for movilizacion_punto
-- ----------------------------
DROP TABLE IF EXISTS `movilizacion_punto`;
CREATE TABLE `movilizacion_punto` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `puntos_id` int(10) unsigned NOT NULL,
  `obervacion` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `movilizacion_punto_puntos_id_foreign` (`puntos_id`),
  CONSTRAINT `movilizacion_punto_puntos_id_foreign` FOREIGN KEY (`puntos_id`) REFERENCES `puntos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of movilizacion_punto
-- ----------------------------

-- ----------------------------
-- Table structure for municipio
-- ----------------------------
DROP TABLE IF EXISTS `municipio`;
CREATE TABLE `municipio` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `estados_id` int(11) unsigned DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `poblacion` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `municipio_estados_fk` (`estados_id`) USING BTREE,
  CONSTRAINT `municipio_ibfk_1` FOREIGN KEY (`estados_id`) REFERENCES `estados` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=336 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of municipio
-- ----------------------------
INSERT INTO `municipio` VALUES ('1', '1', 'Alto Orinoco', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('2', '1', 'Atabapo', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('3', '1', 'Atures', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('4', '1', 'Autana', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('5', '1', 'Manapiare', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('6', '1', 'Maroa', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('7', '1', 'Río Negro', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('8', '2', 'Anaco', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('9', '2', 'Aragua', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('10', '2', 'Manuel Ezequiel Bruzual', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('11', '2', 'Diego Bautista Urbaneja', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('12', '2', 'Fernando Peñalver', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('13', '2', 'Francisco Del Carmen Carvajal', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('14', '2', 'General Sir Arthur McGregor', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('15', '2', 'Guanta', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('16', '2', 'Independencia', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('17', '2', 'José Gregorio Monagas', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('18', '2', 'Juan Antonio Sotillo', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('19', '2', 'Juan Manuel Cajigal', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('20', '2', 'Libertad', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('21', '2', 'Francisco de Miranda', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('22', '2', 'Pedro María Freites', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('23', '2', 'Píritu', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('24', '2', 'San José de Guanipa', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('25', '2', 'San Juan de Capistrano', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('26', '2', 'Santa Ana', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('27', '2', 'Simón Bolívar', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('28', '2', 'Simón Rodríguez', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('29', '3', 'Achaguas', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('30', '3', 'Biruaca', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('31', '3', 'Muñóz', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('32', '3', 'Páez', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('33', '3', 'Pedro Camejo', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('34', '3', 'Rómulo Gallegos', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('35', '3', 'San Fernando', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('36', '4', 'Atanasio Girardot', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('37', '4', 'Bolívar', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('38', '4', 'Camatagua', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('39', '4', 'Francisco Linares Alcántara', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('40', '4', 'José Ángel Lamas', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('41', '4', 'José Félix Ribas', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('42', '4', 'José Rafael Revenga', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('43', '4', 'Libertador', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('44', '4', 'Mario Briceño Iragorry', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('45', '4', 'Ocumare de la Costa de Oro', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('46', '4', 'San Casimiro', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('47', '4', 'San Sebastián', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('48', '4', 'Santiago Mariño', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('49', '4', 'Santos Michelena', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('50', '4', 'Sucre', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('51', '4', 'Tovar', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('52', '4', 'Urdaneta', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('53', '4', 'Zamora', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('54', '5', 'Alberto Arvelo Torrealba', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('55', '5', 'Andrés Eloy Blanco', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('56', '5', 'Antonio José de Sucre', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('57', '5', 'Arismendi', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('58', '5', 'Barinas', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('59', '5', 'Bolívar', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('60', '5', 'Cruz Paredes', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('61', '5', 'Ezequiel Zamora', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('62', '5', 'Obispos', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('63', '5', 'Pedraza', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('64', '5', 'Rojas', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('65', '5', 'Sosa', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('66', '6', 'Caroní', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('67', '6', 'Cedeño', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('68', '6', 'El Callao', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('69', '6', 'Gran Sabana', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('70', '6', 'Heres', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('71', '6', 'Piar', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('72', '6', 'Angostura (Raúl Leoni)', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('73', '6', 'Roscio', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('74', '6', 'Sifontes', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('75', '6', 'Sucre', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('76', '6', 'Padre Pedro Chien', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('77', '7', 'Bejuma', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('78', '7', 'Carlos Arvelo', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('79', '7', 'Diego Ibarra', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('80', '7', 'Guacara', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('81', '7', 'Juan José Mora', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('82', '7', 'Libertador', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('83', '7', 'Los Guayos', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('84', '7', 'Miranda', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('85', '7', 'Montalbán', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('86', '7', 'Naguanagua', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('87', '7', 'Puerto Cabello', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('88', '7', 'San Diego', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('89', '7', 'San Joaquín', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('90', '7', 'Valencia', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('91', '8', 'Anzoátegui', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('92', '8', 'Tinaquillo', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('93', '8', 'Girardot', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('94', '8', 'Lima Blanco', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('95', '8', 'Pao de San Juan Bautista', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('96', '8', 'Ricaurte', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('97', '8', 'Rómulo Gallegos', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('98', '8', 'San Carlos', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('99', '8', 'Tinaco', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('100', '9', 'Antonio Díaz', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('101', '9', 'Casacoima', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('102', '9', 'Pedernales', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('103', '9', 'Tucupita', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('104', '10', 'Acosta', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('105', '10', 'Bolívar', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('106', '10', 'Buchivacoa', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('107', '10', 'Cacique Manaure', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('108', '10', 'Carirubana', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('109', '10', 'Colina', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('110', '10', 'Dabajuro', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('111', '10', 'Democracia', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('112', '10', 'Falcón', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('113', '10', 'Federación', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('114', '10', 'Jacura', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('115', '10', 'José Laurencio Silva', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('116', '10', 'Los Taques', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('117', '10', 'Mauroa', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('118', '10', 'Miranda', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('119', '10', 'Monseñor Iturriza', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('120', '10', 'Palmasola', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('121', '10', 'Petit', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('122', '10', 'Píritu', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('123', '10', 'San Francisco', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('124', '10', 'Sucre', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('125', '10', 'Tocópero', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('126', '10', 'Unión', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('127', '10', 'Urumaco', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('128', '10', 'Zamora', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('129', '11', 'Camaguán', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('130', '11', 'Chaguaramas', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('131', '11', 'El Socorro', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('132', '11', 'José Félix Ribas', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('133', '11', 'José Tadeo Monagas', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('134', '11', 'Juan Germán Roscio', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('135', '11', 'Julián Mellado', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('136', '11', 'Las Mercedes', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('137', '11', 'Leonardo Infante', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('138', '11', 'Pedro Zaraza', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('139', '11', 'Ortíz', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('140', '11', 'San Gerónimo de Guayabal', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('141', '11', 'San José de Guaribe', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('142', '11', 'Santa María de Ipire', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('143', '11', 'Sebastián Francisco de Miranda', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('144', '12', 'Andrés Eloy Blanco', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('145', '12', 'Crespo', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('146', '12', 'Iribarren', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('147', '12', 'Jiménez', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('148', '12', 'Morán', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('149', '12', 'Palavecino', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('150', '12', 'Simón Planas', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('151', '12', 'Torres', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('152', '12', 'Urdaneta', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('153', '13', 'Alberto Adriani', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('154', '13', 'Andrés Bello', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('155', '13', 'Antonio Pinto Salinas', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('156', '13', 'Aricagua', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('157', '13', 'Arzobispo Chacón', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('158', '13', 'Campo Elías', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('159', '13', 'Caracciolo Parra Olmedo', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('160', '13', 'Cardenal Quintero', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('161', '13', 'Guaraque', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('162', '13', 'Julio César Salas', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('163', '13', 'Justo Briceño', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('164', '13', 'Libertador', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('165', '13', 'Miranda', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('166', '13', 'Obispo Ramos de Lora', '0', '2017-03-16 20:16:53', '2017-03-16 20:16:53', null);
INSERT INTO `municipio` VALUES ('167', '13', 'Padre Noguera', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('168', '13', 'Pueblo Llano', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('169', '13', 'Rangel', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('170', '13', 'Rivas Dávila', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('171', '13', 'Santos Marquina', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('172', '13', 'Sucre', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('173', '13', 'Tovar', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('174', '13', 'Tulio Febres Cordero', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('175', '13', 'Zea', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('176', '14', 'Acevedo', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('177', '14', 'Andrés Bello', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('178', '14', 'Baruta', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('179', '14', 'Brión', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('180', '14', 'Buroz', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('181', '14', 'Carrizal', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('182', '14', 'Chacao', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('183', '14', 'Cristóbal Rojas', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('184', '14', 'El Hatillo', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('185', '14', 'Guaicaipuro', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('186', '14', 'Independencia', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('187', '14', 'Lander', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('188', '14', 'Los Salias', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('189', '14', 'Páez', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('190', '14', 'Paz Castillo', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('191', '14', 'Pedro Gual', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('192', '14', 'Plaza', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('193', '14', 'Simón Bolívar', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('194', '14', 'Sucre', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('195', '14', 'Urdaneta', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('196', '14', 'Zamora', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('197', '15', 'Acosta', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('198', '15', 'Aguasay', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('199', '15', 'Bolívar', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('200', '15', 'Caripe', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('201', '15', 'Cedeño', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('202', '15', 'Ezequiel Zamora', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('203', '15', 'Libertador', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('204', '15', 'Maturín', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('205', '15', 'Piar', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('206', '15', 'Punceres', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('207', '15', 'Santa Bárbara', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('208', '15', 'Sotillo', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('209', '15', 'Uracoa', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('210', '16', 'Antolín del Campo', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('211', '16', 'Arismendi', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('212', '16', 'García', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('213', '16', 'Gómez', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('214', '16', 'Maneiro', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('215', '16', 'Marcano', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('216', '16', 'Mariño', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('217', '16', 'Península de Macanao', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('218', '16', 'Tubores', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('219', '16', 'Villalba', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('220', '16', 'Díaz', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('221', '17', 'Agua Blanca', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('222', '17', 'Araure', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('223', '17', 'Esteller', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('224', '17', 'Guanare', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('225', '17', 'Guanarito', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('226', '17', 'Monseñor José Vicente de Unda', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('227', '17', 'Ospino', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('228', '17', 'Páez', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('229', '17', 'Papelón', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('230', '17', 'San Genaro de Boconoíto', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('231', '17', 'San Rafael de Onoto', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('232', '17', 'Santa Rosalía', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('233', '17', 'Sucre', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('234', '17', 'Turén', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('235', '18', 'Andrés Eloy Blanco', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('236', '18', 'Andrés Mata', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('237', '18', 'Arismendi', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('238', '18', 'Benítez', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('239', '18', 'Bermúdez', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('240', '18', 'Bolívar', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('241', '18', 'Cajigal', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('242', '18', 'Cruz Salmerón Acosta', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('243', '18', 'Libertador', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('244', '18', 'Mariño', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('245', '18', 'Mejía', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('246', '18', 'Montes', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('247', '18', 'Ribero', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('248', '18', 'Sucre', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('249', '18', 'Valdéz', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('250', '19', 'Andrés Bello', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('251', '19', 'Antonio Rómulo Costa', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('252', '19', 'Ayacucho', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('253', '19', 'Bolívar', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('254', '19', 'Cárdenas', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('255', '19', 'Córdoba', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('256', '19', 'Fernández Feo', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('257', '19', 'Francisco de Miranda', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('258', '19', 'García de Hevia', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('259', '19', 'Guásimos', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('260', '19', 'Independencia', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('261', '19', 'Jáuregui', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('262', '19', 'José María Vargas', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('263', '19', 'Junín', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('264', '19', 'Libertad', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('265', '19', 'Libertador', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('266', '19', 'Lobatera', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('267', '19', 'Michelena', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('268', '19', 'Panamericano', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('269', '19', 'Pedro María Ureña', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('270', '19', 'Rafael Urdaneta', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('271', '19', 'Samuel Darío Maldonado', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('272', '19', 'San Cristóbal', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('273', '19', 'Seboruco', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('274', '19', 'Simón Rodríguez', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('275', '19', 'Sucre', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('276', '19', 'Torbes', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('277', '19', 'Uribante', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('278', '19', 'San Judas Tadeo', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('279', '20', 'Andrés Bello', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('280', '20', 'Boconó', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('281', '20', 'Bolívar', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('282', '20', 'Candelaria', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('283', '20', 'Carache', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('284', '20', 'Escuque', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('285', '20', 'José Felipe Márquez Cañizalez', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('286', '20', 'Juan Vicente Campos Elías', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('287', '20', 'La Ceiba', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('288', '20', 'Miranda', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('289', '20', 'Monte Carmelo', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('290', '20', 'Motatán', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('291', '20', 'Pampán', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('292', '20', 'Pampanito', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('293', '20', 'Rafael Rangel', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('294', '20', 'San Rafael de Carvajal', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('295', '20', 'Sucre', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('296', '20', 'Trujillo', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('297', '20', 'Urdaneta', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('298', '20', 'Valera', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('299', '21', 'Vargas', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('300', '22', 'Arístides Bastidas', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('301', '22', 'Bolívar', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('302', '22', 'Bruzual', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('303', '22', 'Cocorote', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('304', '22', 'Independencia', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('305', '22', 'José Antonio Páez', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('306', '22', 'La Trinidad', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('307', '22', 'Manuel Monge', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('308', '22', 'Nirgua', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('309', '22', 'Peña', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('310', '22', 'San Felipe', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('311', '22', 'Sucre', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('312', '22', 'Urachiche', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('313', '22', 'José Joaquín Veroes', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('314', '23', 'Almirante Padilla', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('315', '23', 'Baralt', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('316', '23', 'Cabimas', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('317', '23', 'Catatumbo', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('318', '23', 'Colón', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('319', '23', 'Francisco Javier Pulgar', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('320', '23', 'Páez', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('321', '23', 'Jesús Enrique Losada', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('322', '23', 'Jesús María Semprún', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('323', '23', 'La Cañada de Urdaneta', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('324', '23', 'Lagunillas', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('325', '23', 'Machiques de Perijá', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('326', '23', 'Mara', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('327', '23', 'Maracaibo', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('328', '23', 'Miranda', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('329', '23', 'Rosario de Perijá', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('330', '23', 'San Francisco', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('331', '23', 'Santa Rita', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('332', '23', 'Simón Bolívar', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('333', '23', 'Sucre', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('334', '23', 'Valmore Rodríguez', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `municipio` VALUES ('335', '24', 'Libertador', '0', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);

-- ----------------------------
-- Table structure for parroquia
-- ----------------------------
DROP TABLE IF EXISTS `parroquia`;
CREATE TABLE `parroquia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `municipio_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parroquia_municipio_id_foreign` (`municipio_id`) USING BTREE,
  CONSTRAINT `parroquia_ibfk_1` FOREIGN KEY (`municipio_id`) REFERENCES `municipio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1140 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of parroquia
-- ----------------------------
INSERT INTO `parroquia` VALUES ('1', 'Alto Orinoco', '1', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('2', 'Huachamacare Acanaña', '1', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('3', 'Marawaka Toky Shamanaña', '1', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('4', 'Mavaka Mavaka', '1', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('5', 'Sierra Parima Parimabé', '1', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('6', 'Ucata Laja Lisa', '2', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('7', 'Yapacana Macuruco', '2', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('8', 'Caname Guarinuma', '2', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('9', 'Fernando Girón Tovar', '3', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('10', 'Luis Alberto Gómez', '3', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('11', 'Pahueña Limón de Parhueña', '3', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('12', 'Platanillal Platanillal', '3', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('13', 'Samariapo', '4', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('14', 'Sipapo', '4', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('15', 'Munduapo', '4', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('16', 'Guayapo', '4', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('17', 'Alto Ventuari', '5', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('18', 'Medio Ventuari', '5', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('19', 'Bajo Ventuari', '5', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('20', 'Victorino', '6', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('21', 'Comunidad', '6', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('22', 'Casiquiare', '7', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('23', 'Cocuy', '7', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('24', 'San Carlos de Río Negro', '7', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('25', 'Solano', '7', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('26', 'Anaco', '8', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('27', 'San Joaquín', '8', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('28', 'Cachipo', '9', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('29', 'Aragua de Barcelona', '9', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('30', 'Lechería', '11', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('31', 'El Morro', '11', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('32', 'Puerto Píritu', '12', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('33', 'San Miguel', '12', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('34', 'Sucre', '12', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('35', 'Valle de Guanape', '13', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('36', 'Santa Bárbara', '13', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('37', 'El Chaparro', '14', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('38', 'Tomás Alfaro', '14', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('39', 'Calatrava', '14', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('40', 'Guanta', '15', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('41', 'Chorrerón', '15', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('42', 'Mamo', '16', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('43', 'Soledad', '16', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('44', 'Mapire', '17', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('45', 'Piar', '17', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('46', 'Santa Clara', '17', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('47', 'San Diego de Cabrutica', '17', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('48', 'Uverito', '17', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('49', 'Zuata', '17', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('50', 'Puerto La Cruz', '18', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('51', 'Pozuelos', '18', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('52', 'Onoto', '19', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('53', 'San Pablo', '19', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('54', 'San Mateo', '20', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('55', 'El Carito', '20', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('56', 'Santa Inés', '20', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('57', 'La Romereña', '20', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('58', 'Atapirire', '21', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('59', 'Boca del Pao', '21', '2017-03-16 20:16:54', '2017-03-16 20:16:54', null);
INSERT INTO `parroquia` VALUES ('60', 'El Pao', '21', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('61', 'Pariaguán', '21', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('62', 'Cantaura', '22', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('63', 'Libertador', '22', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('64', 'Santa Rosa', '22', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('65', 'Urica', '22', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('66', 'Píritu', '23', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('67', 'San Francisco', '23', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('68', 'San José de Guanipa', '24', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('69', 'Boca de Uchire', '25', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('70', 'Boca de Chávez', '25', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('71', 'Pueblo Nuevo', '26', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('72', 'Santa Ana', '26', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('73', 'Bergantín', '27', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('74', 'Caigua', '27', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('75', 'El Carmen', '27', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('76', 'El Pilar', '27', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('77', 'Naricual', '27', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('78', 'San Crsitóbal', '27', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('79', 'Edmundo Barrios', '28', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('80', 'Miguel Otero Silva', '28', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('81', 'Achaguas', '29', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('82', 'Apurito', '29', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('83', 'El Yagual', '29', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('84', 'Guachara', '29', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('85', 'Mucuritas', '29', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('86', 'Queseras del medio', '29', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('87', 'Biruaca', '30', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('88', 'Bruzual', '31', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('89', 'Mantecal', '31', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('90', 'Quintero', '31', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('91', 'Rincón Hondo', '31', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('92', 'San Vicente', '31', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('93', 'Guasdualito', '32', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('94', 'Aramendi', '32', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('95', 'El Amparo', '32', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('96', 'San Camilo', '32', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('97', 'Urdaneta', '32', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('98', 'San Juan de Payara', '33', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('99', 'Codazzi', '33', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('100', 'Cunaviche', '33', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('101', 'Elorza', '34', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('102', 'La Trinidad', '34', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('103', 'San Fernando', '35', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('104', 'El Recreo', '35', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('105', 'Peñalver', '35', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('106', 'San Rafael de Atamaica', '35', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('107', 'Pedro José Ovalles', '36', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('108', 'Joaquín Crespo', '36', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('109', 'José Casanova Godoy', '36', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('110', 'Madre María de San José', '36', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('111', 'Andrés Eloy Blanco', '36', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('112', 'Los Tacarigua', '36', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('113', 'Las Delicias', '36', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('114', 'Choroní', '36', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('115', 'Bolívar', '37', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('116', 'Camatagua', '38', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('117', 'Carmen de Cura', '38', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('118', 'Santa Rita', '39', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('119', 'Francisco de Miranda', '39', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('120', 'Moseñor Feliciano González', '39', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('121', 'Santa Cruz', '40', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('122', 'José Félix Ribas', '41', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('123', 'Castor Nieves Ríos', '41', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('124', 'Las Guacamayas', '41', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('125', 'Pao de Zárate', '41', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('126', 'Zuata', '41', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('127', 'José Rafael Revenga', '42', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('128', 'Palo Negro', '43', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('129', 'San Martín de Porres', '43', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('130', 'El Limón', '44', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('131', 'Caña de Azúcar', '44', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('132', 'Ocumare de la Costa', '45', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('133', 'San Casimiro', '46', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('134', 'Güiripa', '46', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('135', 'Ollas de Caramacate', '46', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('136', 'Valle Morín', '46', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('137', 'San Sebastían', '47', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('138', 'Turmero', '48', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('139', 'Arevalo Aponte', '48', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('140', 'Chuao', '48', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('141', 'Samán de Güere', '48', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('142', 'Alfredo Pacheco Miranda', '48', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('143', 'Santos Michelena', '49', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('144', 'Tiara', '49', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('145', 'Cagua', '50', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('146', 'Bella Vista', '50', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('147', 'Tovar', '51', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('148', 'Urdaneta', '52', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('149', 'Las Peñitas', '52', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('150', 'San Francisco de Cara', '52', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('151', 'Taguay', '52', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('152', 'Zamora', '53', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('153', 'Magdaleno', '53', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('154', 'San Francisco de Asís', '53', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('155', 'Valles de Tucutunemo', '53', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('156', 'Augusto Mijares', '53', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('157', 'Sabaneta', '54', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('158', 'Juan Antonio Rodríguez Domínguez', '54', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('159', 'El Cantón', '55', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('160', 'Santa Cruz de Guacas', '55', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('161', 'Puerto Vivas', '55', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('162', 'Ticoporo', '56', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('163', 'Nicolás Pulido', '56', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('164', 'Andrés Bello', '56', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('165', 'Arismendi', '57', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('166', 'Guadarrama', '57', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('167', 'La Unión', '57', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('168', 'San Antonio', '57', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('169', 'Barinas', '58', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('170', 'Alberto Arvelo Larriva', '58', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('171', 'San Silvestre', '58', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('172', 'Santa Inés', '58', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('173', 'Santa Lucía', '58', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('174', 'Torumos', '58', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('175', 'El Carmen', '58', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('176', 'Rómulo Betancourt', '58', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('177', 'Corazón de Jesús', '58', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('178', 'Ramón Ignacio Méndez', '58', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('179', 'Alto Barinas', '58', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('180', 'Manuel Palacio Fajardo', '58', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('181', 'Juan Antonio Rodríguez Domínguez', '58', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('182', 'Dominga Ortiz de Páez', '58', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('183', 'Barinitas', '59', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('184', 'Altamira de Cáceres', '59', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('185', 'Calderas', '59', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('186', 'Barrancas', '60', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('187', 'El Socorro', '60', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('188', 'Mazparrito', '60', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('189', 'Santa Bárbara', '61', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('190', 'Pedro Briceño Méndez', '61', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('191', 'Ramón Ignacio Méndez', '61', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('192', 'José Ignacio del Pumar', '61', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('193', 'Obispos', '62', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('194', 'Guasimitos', '62', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('195', 'El Real', '62', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('196', 'La Luz', '62', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('197', 'Ciudad Bolívia', '63', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('198', 'José Ignacio Briceño', '63', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('199', 'José Félix Ribas', '63', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('200', 'Páez', '63', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('201', 'Libertad', '64', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('202', 'Dolores', '64', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('203', 'Santa Rosa', '64', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('204', 'Palacio Fajardo', '64', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('205', 'Ciudad de Nutrias', '65', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('206', 'El Regalo', '65', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('207', 'Puerto Nutrias', '65', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('208', 'Santa Catalina', '65', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('209', 'Cachamay', '66', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('210', 'Chirica', '66', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('211', 'Dalla Costa', '66', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('212', 'Once de Abril', '66', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('213', 'Simón Bolívar', '66', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('214', 'Unare', '66', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('215', 'Universidad', '66', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('216', 'Vista al Sol', '66', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('217', 'Pozo Verde', '66', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('218', 'Yocoima', '66', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('219', '5 de Julio', '66', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('220', 'Cedeño', '67', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('221', 'Altagracia', '67', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('222', 'Ascensión Farreras', '67', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('223', 'Guaniamo', '67', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('224', 'La Urbana', '67', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('225', 'Pijiguaos', '67', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('226', 'El Callao', '68', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('227', 'Gran Sabana', '69', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('228', 'Ikabarú', '69', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('229', 'Catedral', '70', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('230', 'Zea', '70', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('231', 'Orinoco', '70', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('232', 'José Antonio Páez', '70', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('233', 'Marhuanta', '70', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('234', 'Agua Salada', '70', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('235', 'Vista Hermosa', '70', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('236', 'La Sabanita', '70', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('237', 'Panapana', '70', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('238', 'Andrés Eloy Blanco', '71', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('239', 'Pedro Cova', '71', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('240', 'Raúl Leoni', '72', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('241', 'Barceloneta', '72', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('242', 'Santa Bárbara', '72', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('243', 'San Francisco', '72', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('244', 'Roscio', '73', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('245', 'Salóm', '73', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('246', 'Tumeremo', '74', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('247', 'El Dorado', '74', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('248', 'Dalla Costa', '74', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('249', 'San Isidro', '74', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('250', 'Sucre', '75', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('251', 'Aripao', '75', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('252', 'Guarataro', '75', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('253', 'Las Majadas', '75', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('254', 'Moitaco', '75', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('255', 'Padre Pedro Chien', '76', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('256', 'Río Grande', '76', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('257', 'Bejuma', '77', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('258', 'Canoabo', '77', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('259', 'Simón Bolívar', '77', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('260', 'Güigüe', '78', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('261', 'Carabobo', '78', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('262', 'Tacarigua', '78', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('263', 'Mariara', '79', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('264', 'Aguas Calientes', '79', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('265', 'Ciudad Alianza', '80', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('266', 'Guacara', '80', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('267', 'Yagua', '80', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('268', 'Morón', '81', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('269', 'Yagua', '81', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('270', 'Tocuyito', '82', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('271', 'Independencia', '82', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('272', 'Los Guayos', '83', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('273', 'Miranda', '84', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('274', 'Montalbán', '85', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('275', 'Naguanagua', '86', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('276', 'Bartolomé Salóm', '87', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('277', 'Democracia', '87', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('278', 'Fraternidad', '87', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('279', 'Goaigoaza', '87', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('280', 'Juan José Flores', '87', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('281', 'Unión', '87', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('282', 'Borburata', '87', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('283', 'Patanemo', '87', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('284', 'San Diego', '88', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('285', 'San Joaquín', '89', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('286', 'Candelaria', '90', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('287', 'Catedral', '90', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('288', 'El Socorro', '90', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('289', 'Miguel Peña', '90', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('290', 'Rafael Urdaneta', '90', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('291', 'San Blas', '90', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('292', 'San José', '90', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('293', 'Santa Rosa', '90', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('294', 'Negro Primero', '90', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('295', 'Cojedes', '91', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('296', 'Juan de Mata Suárez', '91', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('297', 'Tinaquillo', '92', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('298', 'El Baúl', '93', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('299', 'Sucre', '93', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('300', 'La Aguadita', '94', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('301', 'Macapo', '94', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('302', 'El Pao', '95', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('303', 'El Amparo', '96', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('304', 'Libertad de Cojedes', '96', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('305', 'Rómulo Gallegos', '97', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('306', 'San Carlos de Austria', '98', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('307', 'Juan Ángel Bravo', '98', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('308', 'Manuel Manrique', '98', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('309', 'General en Jefe José Laurencio Silva', '99', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('310', 'Curiapo', '100', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('311', 'Almirante Luis Brión', '100', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('312', 'Francisco Aniceto Lugo', '100', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('313', 'Manuel Renaud', '100', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('314', 'Padre Barral', '100', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('315', 'Santos de Abelgas', '100', '2017-03-16 20:16:55', '2017-03-16 20:16:55', null);
INSERT INTO `parroquia` VALUES ('316', 'Imataca', '101', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('317', 'Cinco de Julio', '101', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('318', 'Juan Bautista Arismendi', '101', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('319', 'Manuel Piar', '101', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('320', 'Rómulo Gallegos', '101', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('321', 'Pedernales', '102', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('322', 'Luis Beltrán Prieto Figueroa', '102', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('323', 'San José (Delta Amacuro)', '103', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('324', 'José Vidal Marcano', '103', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('325', 'Juan Millán', '103', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('326', 'Leonardo Ruíz Pineda', '103', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('327', 'Mariscal Antonio José de Sucre', '103', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('328', 'Monseñor Argimiro García', '103', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('329', 'San Rafael (Delta Amacuro)', '103', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('330', 'Virgen del Valle', '103', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('331', 'Clarines', '10', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('332', 'Guanape', '10', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('333', 'Sabana de Uchire', '10', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('334', 'Capadare', '104', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('335', 'La Pastora', '104', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('336', 'Libertador', '104', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('337', 'San Juan de los Cayos', '104', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('338', 'Aracua', '105', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('339', 'La Peña', '105', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('340', 'San Luis', '105', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('341', 'Bariro', '106', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('342', 'Borojó', '106', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('343', 'Capatárida', '106', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('344', 'Guajiro', '106', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('345', 'Seque', '106', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('346', 'Zazárida', '106', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('347', 'Valle de Eroa', '106', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('348', 'Cacique Manaure', '107', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('349', 'Norte', '108', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('350', 'Carirubana', '108', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('351', 'Santa Ana', '108', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('352', 'Urbana Punta Cardón', '108', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('353', 'La Vela de Coro', '109', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('354', 'Acurigua', '109', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('355', 'Guaibacoa', '109', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('356', 'Las Calderas', '109', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('357', 'Macoruca', '109', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('358', 'Dabajuro', '110', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('359', 'Agua Clara', '111', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('360', 'Avaria', '111', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('361', 'Pedregal', '111', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('362', 'Piedra Grande', '111', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('363', 'Purureche', '111', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('364', 'Adaure', '112', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('365', 'Adícora', '112', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('366', 'Baraived', '112', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('367', 'Buena Vista', '112', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('368', 'Jadacaquiva', '112', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('369', 'El Vínculo', '112', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('370', 'El Hato', '112', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('371', 'Moruy', '112', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('372', 'Pueblo Nuevo', '112', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('373', 'Agua Larga', '113', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('374', 'El Paují', '113', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('375', 'Independencia', '113', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('376', 'Mapararí', '113', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('377', 'Agua Linda', '114', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('378', 'Araurima', '114', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('379', 'Jacura', '114', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('380', 'Tucacas', '115', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('381', 'Boca de Aroa', '115', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('382', 'Los Taques', '116', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('383', 'Judibana', '116', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('384', 'Mene de Mauroa', '117', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('385', 'San Félix', '117', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('386', 'Casigua', '117', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('387', 'Guzmán Guillermo', '118', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('388', 'Mitare', '118', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('389', 'Río Seco', '118', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('390', 'Sabaneta', '118', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('391', 'San Antonio', '118', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('392', 'San Gabriel', '118', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('393', 'Santa Ana', '118', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('394', 'Boca del Tocuyo', '119', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('395', 'Chichiriviche', '119', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('396', 'Tocuyo de la Costa', '119', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('397', 'Palmasola', '120', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('398', 'Cabure', '121', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('399', 'Colina', '121', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('400', 'Curimagua', '121', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('401', 'San José de la Costa', '122', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('402', 'Píritu', '122', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('403', 'San Francisco', '123', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('404', 'Sucre', '124', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('405', 'Pecaya', '124', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('406', 'Tocópero', '125', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('407', 'El Charal', '126', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('408', 'Las Vegas del Tuy', '126', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('409', 'Santa Cruz de Bucaral', '126', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('410', 'Bruzual', '127', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('411', 'Urumaco', '127', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('412', 'Puerto Cumarebo', '128', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('413', 'La Ciénaga', '128', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('414', 'La Soledad', '128', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('415', 'Pueblo Cumarebo', '128', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('416', 'Zazárida', '128', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('417', 'Churuguara', '113', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('418', 'Camaguán', '129', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('419', 'Puerto Miranda', '129', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('420', 'Uverito', '129', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('421', 'Chaguaramas', '130', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('422', 'El Socorro', '131', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('423', 'Tucupido', '132', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('424', 'San Rafael de Laya', '132', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('425', 'Altagracia de Orituco', '133', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('426', 'San Rafael de Orituco', '133', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('427', 'San Francisco Javier de Lezama', '133', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('428', 'Paso Real de Macaira', '133', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('429', 'Carlos Soublette', '133', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('430', 'San Francisco de Macaira', '133', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('431', 'Libertad de Orituco', '133', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('432', 'Cantaclaro', '134', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('433', 'San Juan de los Morros', '134', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('434', 'Parapara', '134', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('435', 'El Sombrero', '135', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('436', 'Sosa', '135', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('437', 'Las Mercedes', '136', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('438', 'Cabruta', '136', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('439', 'Santa Rita de Manapire', '136', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('440', 'Valle de la Pascua', '137', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('441', 'Espino', '137', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('442', 'San José de Unare', '138', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('443', 'Zaraza', '138', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('444', 'San José de Tiznados', '139', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('445', 'San Francisco de Tiznados', '139', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('446', 'San Lorenzo de Tiznados', '139', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('447', 'Ortiz', '139', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('448', 'Guayabal', '140', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('449', 'Cazorla', '140', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('450', 'San José de Guaribe', '141', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('451', 'Uveral', '141', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('452', 'Santa María de Ipire', '142', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('453', 'Altamira', '142', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('454', 'El Calvario', '143', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('455', 'El Rastro', '143', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('456', 'Guardatinajas', '143', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('457', 'Capital Urbana Calabozo', '143', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('458', 'Quebrada Honda de Guache', '144', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('459', 'Pío Tamayo', '144', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('460', 'Yacambú', '144', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('461', 'Fréitez', '145', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('462', 'José María Blanco', '145', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('463', 'Catedral', '146', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('464', 'Concepción', '146', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('465', 'El Cují', '146', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('466', 'Juan de Villegas', '146', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('467', 'Santa Rosa', '146', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('468', 'Tamaca', '146', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('469', 'Unión', '146', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('470', 'Aguedo Felipe Alvarado', '146', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('471', 'Buena Vista', '146', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('472', 'Juárez', '146', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('473', 'Juan Bautista Rodríguez', '147', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('474', 'Cuara', '147', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('475', 'Diego de Lozada', '147', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('476', 'Paraíso de San José', '147', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('477', 'San Miguel', '147', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('478', 'Tintorero', '147', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('479', 'José Bernardo Dorante', '147', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('480', 'Coronel Mariano Peraza ', '147', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('481', 'Bolívar', '148', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('482', 'Anzoátegui', '148', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('483', 'Guarico', '148', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('484', 'Hilario Luna y Luna', '148', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('485', 'Humocaro Alto', '148', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('486', 'Humocaro Bajo', '148', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('487', 'La Candelaria', '148', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('488', 'Morán', '148', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('489', 'Cabudare', '149', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('490', 'José Gregorio Bastidas', '149', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('491', 'Agua Viva', '149', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('492', 'Sarare', '150', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('493', 'Buría', '150', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('494', 'Gustavo Vegas León', '150', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('495', 'Trinidad Samuel', '151', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('496', 'Antonio Díaz', '151', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('497', 'Camacaro', '151', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('498', 'Castañeda', '151', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('499', 'Cecilio Zubillaga', '151', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('500', 'Chiquinquirá', '151', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('501', 'El Blanco', '151', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('502', 'Espinoza de los Monteros', '151', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('503', 'Lara', '151', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('504', 'Las Mercedes', '151', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('505', 'Manuel Morillo', '151', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('506', 'Montaña Verde', '151', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('507', 'Montes de Oca', '151', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('508', 'Torres', '151', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('509', 'Heriberto Arroyo', '151', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('510', 'Reyes Vargas', '151', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('511', 'Altagracia', '151', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('512', 'Siquisique', '152', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('513', 'Moroturo', '152', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('514', 'San Miguel', '152', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('515', 'Xaguas', '152', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('516', 'Presidente Betancourt', '153', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('517', 'Presidente Páez', '153', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('518', 'Presidente Rómulo Gallegos', '153', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('519', 'Gabriel Picón González', '153', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('520', 'Héctor Amable Mora', '153', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('521', 'José Nucete Sardi', '153', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('522', 'Pulido Méndez', '153', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('523', 'La Azulita', '154', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('524', 'Santa Cruz de Mora', '155', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('525', 'Mesa Bolívar', '155', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('526', 'Mesa de Las Palmas', '155', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('527', 'Aricagua', '156', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('528', 'San Antonio', '156', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('529', 'Canagua', '157', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('530', 'Capurí', '157', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('531', 'Chacantá', '157', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('532', 'El Molino', '157', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('533', 'Guaimaral', '157', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('534', 'Mucutuy', '157', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('535', 'Mucuchachí', '157', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('536', 'Fernández Peña', '158', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('537', 'Matriz', '158', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('538', 'Montalbán', '158', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('539', 'Acequias', '158', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('540', 'Jají', '158', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('541', 'La Mesa', '158', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('542', 'San José del Sur', '158', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('543', 'Tucaní', '159', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('544', 'Florencio Ramírez', '159', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('545', 'Santo Domingo', '160', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('546', 'Las Piedras', '160', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('547', 'Guaraque', '161', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('548', 'Mesa de Quintero', '161', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('549', 'Río Negro', '161', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('550', 'Arapuey', '162', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('551', 'Palmira', '162', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('552', 'San Cristóbal de Torondoy', '163', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('553', 'Torondoy', '163', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('554', 'Antonio Spinetti Dini', '164', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('555', 'Arias', '164', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('556', 'Caracciolo Parra Pérez', '164', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('557', 'Domingo Peña', '164', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('558', 'El Llano', '164', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('559', 'Gonzalo Picón Febres', '164', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('560', 'Jacinto Plaza', '164', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('561', 'Juan Rodríguez Suárez', '164', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('562', 'Lasso de la Vega', '164', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('563', 'Mariano Picón Salas', '164', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('564', 'Milla', '164', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('565', 'Osuna Rodríguez', '164', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('566', 'Sagrario', '164', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('567', 'El Morro', '164', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('568', 'Los Nevados', '164', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('569', 'Andrés Eloy Blanco', '165', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('570', 'La Venta', '165', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('571', 'Piñango', '165', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('572', 'Timotes', '165', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('573', 'Eloy Paredes', '166', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('574', 'San Rafael de Alcázar', '166', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('575', 'Santa Elena de Arenales', '166', '2017-03-16 20:16:56', '2017-03-16 20:16:56', null);
INSERT INTO `parroquia` VALUES ('576', 'Santa María de Caparo', '167', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('577', 'Pueblo Llano', '168', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('578', 'Cacute', '169', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('579', 'La Toma', '169', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('580', 'Mucuchíes', '169', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('581', 'Mucurubá', '169', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('582', 'San Rafael', '169', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('583', 'Gerónimo Maldonado', '170', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('584', 'Bailadores', '170', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('585', 'Tabay', '171', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('586', 'Chiguará', '172', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('587', 'Estánquez', '172', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('588', 'Lagunillas', '172', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('589', 'La Trampa', '172', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('590', 'Pueblo Nuevo del Sur', '172', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('591', 'San Juan', '172', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('592', 'El Amparo', '173', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('593', 'El Llano', '173', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('594', 'San Francisco', '173', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('595', 'Tovar', '173', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('596', 'Independencia', '174', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('597', 'María de la Concepción Palacios Blanco', '174', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('598', 'Nueva Bolivia', '174', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('599', 'Santa Apolonia', '174', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('600', 'Caño El Tigre', '175', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('601', 'Zea', '175', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('602', 'Aragüita', '176', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('603', 'Arévalo González', '176', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('604', 'Capaya', '176', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('605', 'Caucagua', '176', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('606', 'Panaquire', '176', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('607', 'Ribas', '176', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('608', 'El Café', '176', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('609', 'Marizapa', '176', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('610', 'Cumbo', '177', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('611', 'San José de Barlovento', '177', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('612', 'El Cafetal', '178', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('613', 'Las Minas', '178', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('614', 'Nuestra Señora del Rosario', '178', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('615', 'Higuerote', '179', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('616', 'Curiepe', '179', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('617', 'Tacarigua de Brión', '179', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('618', 'Mamporal', '180', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('619', 'Carrizal', '181', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('620', 'Chacao', '182', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('621', 'Charallave', '183', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('622', 'Las Brisas', '183', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('623', 'El Hatillo', '184', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('624', 'Altagracia de la Montaña', '185', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('625', 'Cecilio Acosta', '185', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('626', 'Los Teques', '185', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('627', 'El Jarillo', '185', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('628', 'San Pedro', '185', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('629', 'Tácata', '185', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('630', 'Paracotos', '185', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('631', 'Cartanal', '186', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('632', 'Santa Teresa del Tuy', '186', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('633', 'La Democracia', '187', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('634', 'Ocumare del Tuy', '187', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('635', 'Santa Bárbara', '187', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('636', 'San Antonio de los Altos', '188', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('637', 'Río Chico', '189', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('638', 'El Guapo', '189', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('639', 'Tacarigua de la Laguna', '189', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('640', 'Paparo', '189', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('641', 'San Fernando del Guapo', '189', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('642', 'Santa Lucía del Tuy', '190', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('643', 'Cúpira', '191', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('644', 'Machurucuto', '191', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('645', 'Guarenas', '192', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('646', 'San Antonio de Yare', '193', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('647', 'San Francisco de Yare', '193', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('648', 'Leoncio Martínez', '194', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('649', 'Petare', '194', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('650', 'Caucagüita', '194', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('651', 'Filas de Mariche', '194', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('652', 'La Dolorita', '194', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('653', 'Cúa', '195', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('654', 'Nueva Cúa', '195', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('655', 'Guatire', '196', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('656', 'Bolívar', '196', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('657', 'San Antonio de Maturín', '197', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('658', 'San Francisco de Maturín', '197', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('659', 'Aguasay', '198', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('660', 'Caripito', '199', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('661', 'El Guácharo', '200', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('662', 'La Guanota', '200', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('663', 'Sabana de Piedra', '200', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('664', 'San Agustín', '200', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('665', 'Teresen', '200', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('666', 'Caripe', '200', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('667', 'Areo', '201', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('668', 'Capital Cedeño', '201', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('669', 'San Félix de Cantalicio', '201', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('670', 'Viento Fresco', '201', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('671', 'El Tejero', '202', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('672', 'Punta de Mata', '202', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('673', 'Chaguaramas', '203', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('674', 'Las Alhuacas', '203', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('675', 'Tabasca', '203', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('676', 'Temblador', '203', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('677', 'Alto de los Godos', '204', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('678', 'Boquerón', '204', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('679', 'Las Cocuizas', '204', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('680', 'La Cruz', '204', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('681', 'San Simón', '204', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('682', 'El Corozo', '204', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('683', 'El Furrial', '204', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('684', 'Jusepín', '204', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('685', 'La Pica', '204', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('686', 'San Vicente', '204', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('687', 'Aparicio', '205', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('688', 'Aragua de Maturín', '205', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('689', 'Chaguamal', '205', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('690', 'El Pinto', '205', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('691', 'Guanaguana', '205', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('692', 'La Toscana', '205', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('693', 'Taguaya', '205', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('694', 'Cachipo', '206', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('695', 'Quiriquire', '206', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('696', 'Santa Bárbara', '207', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('697', 'Barrancas', '208', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('698', 'Los Barrancos de Fajardo', '208', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('699', 'Uracoa', '209', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('700', 'Antolín del Campo', '210', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('701', 'Arismendi', '211', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('702', 'García', '212', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('703', 'Francisco Fajardo', '212', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('704', 'Bolívar', '213', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('705', 'Guevara', '213', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('706', 'Matasiete', '213', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('707', 'Santa Ana', '213', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('708', 'Sucre', '213', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('709', 'Aguirre', '214', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('710', 'Maneiro', '214', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('711', 'Adrián', '215', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('712', 'Juan Griego', '215', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('713', 'Yaguaraparo', '215', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('714', 'Porlamar', '216', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('715', 'San Francisco de Macanao', '217', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('716', 'Boca de Río', '217', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('717', 'Tubores', '218', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('718', 'Los Baleales', '218', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('719', 'Vicente Fuentes', '219', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('720', 'Villalba', '219', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('721', 'San Juan Bautista', '220', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('722', 'Zabala', '220', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('723', 'Capital Araure', '222', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('724', 'Río Acarigua', '222', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('725', 'Capital Esteller', '223', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('726', 'Uveral', '223', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('727', 'Guanare', '224', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('728', 'Córdoba', '224', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('729', 'San José de la Montaña', '224', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('730', 'San Juan de Guanaguanare', '224', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('731', 'Virgen de la Coromoto', '224', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('732', 'Guanarito', '225', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('733', 'Trinidad de la Capilla', '225', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('734', 'Divina Pastora', '225', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('735', 'Monseñor José Vicente de Unda', '226', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('736', 'Peña Blanca', '226', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('737', 'Capital Ospino', '227', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('738', 'Aparición', '227', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('739', 'La Estación', '227', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('740', 'Páez', '228', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('741', 'Payara', '228', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('742', 'Pimpinela', '228', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('743', 'Ramón Peraza', '228', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('744', 'Papelón', '229', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('745', 'Caño Delgadito', '229', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('746', 'San Genaro de Boconoito', '230', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('747', 'Antolín Tovar', '230', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('748', 'San Rafael de Onoto', '231', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('749', 'Santa Fe', '231', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('750', 'Thermo Morles', '231', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('751', 'Santa Rosalía', '232', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('752', 'Florida', '232', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('753', 'Sucre', '233', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('754', 'Concepción', '233', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('755', 'San Rafael de Palo Alzado', '233', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('756', 'Uvencio Antonio Velásquez', '233', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('757', 'San José de Saguaz', '233', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('758', 'Villa Rosa', '233', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('759', 'Turén', '234', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('760', 'Canelones', '234', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('761', 'Santa Cruz', '234', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('762', 'San Isidro Labrador', '234', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('763', 'Mariño', '235', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('764', 'Rómulo Gallegos', '235', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('765', 'San José de Aerocuar', '236', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('766', 'Tavera Acosta', '236', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('767', 'Río Caribe', '237', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('768', 'Antonio José de Sucre', '237', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('769', 'El Morro de Puerto Santo', '237', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('770', 'Puerto Santo', '237', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('771', 'San Juan de las Galdonas', '237', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('772', 'El Pilar', '238', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('773', 'El Rincón', '238', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('774', 'General Francisco Antonio Váquez', '238', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('775', 'Guaraúnos', '238', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('776', 'Tunapuicito', '238', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('777', 'Unión', '238', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('778', 'Santa Catalina', '239', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('779', 'Santa Rosa', '239', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('780', 'Santa Teresa', '239', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('781', 'Bolívar', '239', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('782', 'Maracapana', '239', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('783', 'Libertad', '241', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('784', 'El Paujil', '241', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('785', 'Yaguaraparo', '241', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('786', 'Cruz Salmerón Acosta', '242', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('787', 'Chacopata', '242', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('788', 'Manicuare', '242', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('789', 'Tunapuy', '243', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('790', 'Campo Elías', '243', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('791', 'Irapa', '244', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('792', 'Campo Claro', '244', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('793', 'Maraval', '244', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('794', 'San Antonio de Irapa', '244', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('795', 'Soro', '244', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('796', 'Mejía', '245', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('797', 'Cumanacoa', '246', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('798', 'Arenas', '246', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('799', 'Aricagua', '246', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('800', 'Cogollar', '246', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('801', 'San Fernando', '246', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('802', 'San Lorenzo', '246', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('803', 'Villa Frontado (Muelle de Cariaco)', '247', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('804', 'Catuaro', '247', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('805', 'Rendón', '247', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('806', 'San Cruz', '247', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('807', 'Santa María', '247', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('808', 'Altagracia', '248', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('809', 'Santa Inés', '248', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('810', 'Valentín Valiente', '248', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('811', 'Ayacucho', '248', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('812', 'San Juan', '248', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('813', 'Raúl Leoni', '248', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('814', 'Gran Mariscal', '248', '2017-03-16 20:16:57', '2017-03-16 20:16:57', null);
INSERT INTO `parroquia` VALUES ('815', 'Cristóbal Colón', '249', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('816', 'Bideau', '249', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('817', 'Punta de Piedras', '249', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('818', 'Güiria', '249', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('819', 'Andrés Bello', '250', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('820', 'Antonio Rómulo Costa', '251', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('821', 'Ayacucho', '252', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('822', 'Rivas Berti', '252', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('823', 'San Pedro del Río', '252', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('824', 'Bolívar', '253', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('825', 'Palotal', '253', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('826', 'General Juan Vicente Gómez', '253', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('827', 'Isaías Medina Angarita', '253', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('828', 'Cárdenas', '254', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('829', 'Amenodoro Ángel Lamus', '254', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('830', 'La Florida', '254', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('831', 'Córdoba', '255', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('832', 'Fernández Feo', '256', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('833', 'Alberto Adriani', '256', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('834', 'Santo Domingo', '256', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('835', 'Francisco de Miranda', '257', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('836', 'García de Hevia', '258', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('837', 'Boca de Grita', '258', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('838', 'José Antonio Páez', '258', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('839', 'Guásimos', '259', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('840', 'Independencia', '260', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('841', 'Juan Germán Roscio', '260', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('842', 'Román Cárdenas', '260', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('843', 'Jáuregui', '261', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('844', 'Emilio Constantino Guerrero', '261', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('845', 'Monseñor Miguel Antonio Salas', '261', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('846', 'José María Vargas', '262', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('847', 'Junín', '263', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('848', 'La Petrólea', '263', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('849', 'Quinimarí', '263', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('850', 'Bramón', '263', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('851', 'Libertad', '264', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('852', 'Cipriano Castro', '264', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('853', 'Manuel Felipe Rugeles', '264', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('854', 'Libertador', '265', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('855', 'Doradas', '265', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('856', 'Emeterio Ochoa', '265', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('857', 'San Joaquín de Navay', '265', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('858', 'Lobatera', '266', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('859', 'Constitución', '266', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('860', 'Michelena', '267', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('861', 'Panamericano', '268', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('862', 'La Palmita', '268', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('863', 'Pedro María Ureña', '269', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('864', 'Nueva Arcadia', '269', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('865', 'Delicias', '270', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('866', 'Pecaya', '270', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('867', 'Samuel Darío Maldonado', '271', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('868', 'Boconó', '271', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('869', 'Hernández', '271', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('870', 'La Concordia', '272', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('871', 'San Juan Bautista', '272', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('872', 'Pedro María Morantes', '272', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('873', 'San Sebastián', '272', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('874', 'Dr. Francisco Romero Lobo', '272', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('875', 'Seboruco', '273', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('876', 'Simón Rodríguez', '274', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('877', 'Sucre', '275', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('878', 'Eleazar López Contreras', '275', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('879', 'San Pablo', '275', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('880', 'Torbes', '276', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('881', 'Uribante', '277', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('882', 'Cárdenas', '277', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('883', 'Juan Pablo Peñalosa', '277', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('884', 'Potosí', '277', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('885', 'San Judas Tadeo', '278', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('886', 'Araguaney', '279', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('887', 'El Jaguito', '279', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('888', 'La Esperanza', '279', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('889', 'Santa Isabel', '279', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('890', 'Boconó', '280', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('891', 'El Carmen', '280', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('892', 'Mosquey', '280', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('893', 'Ayacucho', '280', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('894', 'Burbusay', '280', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('895', 'General Ribas', '280', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('896', 'Guaramacal', '280', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('897', 'Vega de Guaramacal', '280', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('898', 'Monseñor Jáuregui', '280', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('899', 'Rafael Rangel', '280', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('900', 'San Miguel', '280', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('901', 'San José', '280', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('902', 'Sabana Grande', '281', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('903', 'Cheregüé', '281', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('904', 'Granados', '281', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('905', 'Arnoldo Gabaldón', '282', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('906', 'Bolivia', '282', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('907', 'Carrillo', '282', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('908', 'Cegarra', '282', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('909', 'Chejendé', '282', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('910', 'Manuel Salvador Ulloa', '282', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('911', 'San José', '282', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('912', 'Carache', '283', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('913', 'La Concepción', '283', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('914', 'Cuicas', '283', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('915', 'Panamericana', '283', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('916', 'Santa Cruz', '283', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('917', 'Escuque', '284', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('918', 'La Unión', '284', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('919', 'Santa Rita', '284', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('920', 'Sabana Libre', '284', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('921', 'El Socorro', '285', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('922', 'Los Caprichos', '285', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('923', 'Antonio José de Sucre', '285', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('924', 'Campo Elías', '286', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('925', 'Arnoldo Gabaldón', '286', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('926', 'Santa Apolonia', '287', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('927', 'El Progreso', '287', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('928', 'La Ceiba', '287', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('929', 'Tres de Febrero', '287', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('930', 'El Dividive', '288', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('931', 'Agua Santa', '288', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('932', 'Agua Caliente', '288', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('933', 'El Cenizo', '288', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('934', 'Valerita', '288', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('935', 'Monte Carmelo', '289', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('936', 'Buena Vista', '289', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('937', 'Santa María del Horcón', '289', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('938', 'Motatán', '290', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('939', 'El Baño', '290', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('940', 'Jalisco', '290', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('941', 'Pampán', '291', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('942', 'Flor de Patria', '291', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('943', 'La Paz', '291', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('944', 'Santa Ana', '291', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('945', 'Pampanito', '292', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('946', 'La Concepción', '292', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('947', 'Pampanito II', '292', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('948', 'Betijoque', '293', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('949', 'José Gregorio Hernández', '293', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('950', 'La Pueblita', '293', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('951', 'Los Cedros', '293', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('952', 'Carvajal', '294', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('953', 'Campo Alegre', '294', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('954', 'Antonio Nicolás Briceño', '294', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('955', 'José Leonardo Suárez', '294', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('956', 'Sabana de Mendoza', '295', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('957', 'Junín', '295', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('958', 'Valmore Rodríguez', '295', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('959', 'El Paraíso', '295', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('960', 'Andrés Linares', '296', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('961', 'Chiquinquirá', '296', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('962', 'Cristóbal Mendoza', '296', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('963', 'Cruz Carrillo', '296', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('964', 'Matriz', '296', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('965', 'Monseñor Carrillo', '296', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('966', 'Tres Esquinas', '296', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('967', 'Cabimbú', '297', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('968', 'Jajó', '297', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('969', 'La Mesa de Esnujaque', '297', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('970', 'Santiago', '297', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('971', 'Tuñame', '297', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('972', 'La Quebrada', '297', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('973', 'Juan Ignacio Montilla', '298', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('974', 'La Beatriz', '298', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('975', 'La Puerta', '298', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('976', 'Mendoza del Valle de Momboy', '298', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('977', 'Mercedes Díaz', '298', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('978', 'San Luis', '298', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('979', 'Caraballeda', '299', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('980', 'Carayaca', '299', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('981', 'Carlos Soublette', '299', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('982', 'Caruao Chuspa', '299', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('983', 'Catia La Mar', '299', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('984', 'El Junko', '299', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('985', 'La Guaira', '299', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('986', 'Macuto', '299', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('987', 'Maiquetía', '299', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('988', 'Naiguatá', '299', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('989', 'Urimare', '299', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('990', 'Arístides Bastidas', '300', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('991', 'Bolívar', '301', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('992', 'Chivacoa', '302', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('993', 'Campo Elías', '302', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('994', 'Cocorote', '303', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('995', 'Independencia', '304', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('996', 'José Antonio Páez', '305', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('997', 'La Trinidad', '306', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('998', 'Manuel Monge', '307', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('999', 'Salóm', '308', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1000', 'Temerla', '308', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1001', 'Nirgua', '308', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1002', 'San Andrés', '309', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1003', 'Yaritagua', '309', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1004', 'San Javier', '310', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1005', 'Albarico', '310', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1006', 'San Felipe', '310', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1007', 'Sucre', '311', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1008', 'Urachiche', '312', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1009', 'El Guayabo', '313', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1010', 'Farriar', '313', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1011', 'Isla de Toas', '314', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1012', 'Monagas', '314', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1013', 'San Timoteo', '315', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1014', 'General Urdaneta', '315', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1015', 'Libertador', '315', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1016', 'Marcelino Briceño', '315', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1017', 'Pueblo Nuevo', '315', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1018', 'Manuel Guanipa Matos', '315', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1019', 'Ambrosio', '316', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1020', 'Carmen Herrera', '316', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1021', 'La Rosa', '316', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1022', 'Germán Ríos Linares', '316', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1023', 'San Benito', '316', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1024', 'Rómulo Betancourt', '316', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1025', 'Jorge Hernández', '316', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1026', 'Punta Gorda', '316', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1027', 'Arístides Calvani', '316', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1028', 'Encontrados', '317', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1029', 'Udón Pérez', '317', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1030', 'Moralito', '318', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1031', 'San Carlos del Zulia', '318', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1032', 'Santa Cruz del Zulia', '318', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1033', 'Santa Bárbara', '318', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1034', 'Urribarrí', '318', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1035', 'Carlos Quevedo', '319', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1036', 'Francisco Javier Pulgar', '319', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1037', 'Simón Rodríguez', '319', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1038', 'Guamo-Gavilanes', '319', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1039', 'La Concepción', '321', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1040', 'San José', '321', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1041', 'Mariano Parra León', '321', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1042', 'José Ramón Yépez', '321', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1043', 'Jesús María Semprún', '322', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1044', 'Barí', '322', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1045', 'Concepción', '323', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1046', 'Andrés Bello', '323', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1047', 'Chiquinquirá', '323', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1048', 'El Carmelo', '323', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1049', 'Potreritos', '323', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1050', 'Libertad', '324', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1051', 'Alonso de Ojeda', '324', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1052', 'Venezuela', '324', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1053', 'Eleazar López Contreras', '324', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1054', 'Campo Lara', '324', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1055', 'Bartolomé de las Casas', '325', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1056', 'Libertad', '325', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1057', 'Río Negro', '325', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1058', 'San José de Perijá', '325', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1059', 'San Rafael', '326', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1060', 'La Sierrita', '326', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1061', 'Las Parcelas', '326', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1062', 'Luis de Vicente', '326', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1063', 'Monseñor Marcos Sergio Godoy', '326', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1064', 'Ricaurte', '326', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1065', 'Tamare', '326', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1066', 'Antonio Borjas Romero', '327', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1067', 'Bolívar', '327', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1068', 'Cacique Mara', '327', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1069', 'Carracciolo Parra Pérez', '327', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1070', 'Cecilio Acosta', '327', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1071', 'Cristo de Aranza', '327', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1072', 'Coquivacoa', '327', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1073', 'Chiquinquirá', '327', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1074', 'Francisco Eugenio Bustamante', '327', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1075', 'Idelfonzo Vásquez', '327', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1076', 'Juana de Ávila', '327', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1077', 'Luis Hurtado Higuera', '327', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1078', 'Manuel Dagnino', '327', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1079', 'Olegario Villalobos', '327', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1080', 'Raúl Leoni', '327', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1081', 'Santa Lucía', '327', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1082', 'Venancio Pulgar', '327', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1083', 'San Isidro', '327', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1084', 'Altagracia', '328', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1085', 'Faría', '328', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1086', 'Ana María Campos', '328', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1087', 'San Antonio', '328', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1088', 'San José', '328', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1089', 'Donaldo García', '329', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1090', 'El Rosario', '329', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1091', 'Sixto Zambrano', '329', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1092', 'San Francisco', '330', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1093', 'El Bajo', '330', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1094', 'Domitila Flores', '330', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1095', 'Francisco Ochoa', '330', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1096', 'Los Cortijos', '330', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1097', 'Marcial Hernández', '330', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1098', 'Santa Rita', '331', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1099', 'El Mene', '331', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1100', 'Pedro Lucas Urribarrí', '331', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1101', 'José Cenobio Urribarrí', '331', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1102', 'Rafael Maria Baralt', '332', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1103', 'Manuel Manrique', '332', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1104', 'Rafael Urdaneta', '332', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1105', 'Bobures', '333', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1106', 'Gibraltar', '333', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1107', 'Heras', '333', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1108', 'Monseñor Arturo Álvarez', '333', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1109', 'Rómulo Gallegos', '333', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1110', 'El Batey', '333', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1111', 'Rafael Urdaneta', '334', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1112', 'La Victoria', '334', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1113', 'Raúl Cuenca', '334', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1114', 'Sinamaica', '320', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1115', 'Alta Guajira', '320', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1116', 'Elías Sánchez Rubio', '320', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1117', 'Guajira', '320', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1118', 'Altagracia', '335', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1119', 'Antímano', '335', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1120', 'Caricuao', '335', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1121', 'Catedral', '335', '2017-03-16 20:16:58', '2017-03-16 20:16:58', null);
INSERT INTO `parroquia` VALUES ('1122', 'Coche', '335', '2017-03-16 20:16:59', '2017-03-16 20:16:59', null);
INSERT INTO `parroquia` VALUES ('1123', 'El Junquito', '335', '2017-03-16 20:16:59', '2017-03-16 20:16:59', null);
INSERT INTO `parroquia` VALUES ('1124', 'El Paraíso', '335', '2017-03-16 20:16:59', '2017-03-16 20:16:59', null);
INSERT INTO `parroquia` VALUES ('1125', 'El Recreo', '335', '2017-03-16 20:16:59', '2017-03-16 20:16:59', null);
INSERT INTO `parroquia` VALUES ('1126', 'El Valle', '335', '2017-03-16 20:16:59', '2017-03-16 20:16:59', null);
INSERT INTO `parroquia` VALUES ('1127', 'La Candelaria', '335', '2017-03-16 20:16:59', '2017-03-16 20:16:59', null);
INSERT INTO `parroquia` VALUES ('1128', 'La Pastora', '335', '2017-03-16 20:16:59', '2017-03-16 20:16:59', null);
INSERT INTO `parroquia` VALUES ('1129', 'La Vega', '335', '2017-03-16 20:16:59', '2017-03-16 20:16:59', null);
INSERT INTO `parroquia` VALUES ('1130', 'Macarao', '335', '2017-03-16 20:16:59', '2017-03-16 20:16:59', null);
INSERT INTO `parroquia` VALUES ('1131', 'San Agustín', '335', '2017-03-16 20:16:59', '2017-03-16 20:16:59', null);
INSERT INTO `parroquia` VALUES ('1132', 'San Bernardino', '335', '2017-03-16 20:16:59', '2017-03-16 20:16:59', null);
INSERT INTO `parroquia` VALUES ('1133', 'San José', '335', '2017-03-16 20:16:59', '2017-03-16 20:16:59', null);
INSERT INTO `parroquia` VALUES ('1134', 'San Juan', '335', '2017-03-16 20:16:59', '2017-03-16 20:16:59', null);
INSERT INTO `parroquia` VALUES ('1135', 'San Pedro', '335', '2017-03-16 20:16:59', '2017-03-16 20:16:59', null);
INSERT INTO `parroquia` VALUES ('1136', 'Santa Rosalía', '335', '2017-03-16 20:16:59', '2017-03-16 20:16:59', null);
INSERT INTO `parroquia` VALUES ('1137', 'Santa Teresa', '335', '2017-03-16 20:16:59', '2017-03-16 20:16:59', null);
INSERT INTO `parroquia` VALUES ('1138', 'Sucre (Catia)', '335', '2017-03-16 20:16:59', '2017-03-16 20:16:59', null);
INSERT INTO `parroquia` VALUES ('1139', '23 de enero', '335', '2017-03-16 20:16:59', '2017-03-16 20:16:59', null);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`) USING BTREE,
  KEY `password_resets_token_index` (`token`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for proyectos
-- ----------------------------
DROP TABLE IF EXISTS `proyectos`;
CREATE TABLE `proyectos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `estados_id` int(10) unsigned NOT NULL,
  `municipio_id` int(10) unsigned NOT NULL,
  `parroquia_id` int(10) unsigned NOT NULL,
  `inversion` decimal(15,2) NOT NULL,
  `sector_primario_id` int(10) unsigned NOT NULL,
  `estatus_id` int(10) unsigned NOT NULL,
  `fuente_financiamiento_id` int(10) unsigned NOT NULL,
  `avance` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `ente_ejecutor_id` int(10) unsigned NOT NULL,
  `ano_ejecucion` smallint(3) unsigned NOT NULL,
  `numero_certificacion` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `proyectos_municipio_id_foreign` (`municipio_id`) USING BTREE,
  KEY `proyectos_parroquia_id_foreign` (`parroquia_id`) USING BTREE,
  KEY `proyectos_sector_primario_id_foreign` (`sector_primario_id`) USING BTREE,
  KEY `proyectos_estatus_id_foreign` (`estatus_id`) USING BTREE,
  KEY `proyectos_fuente_financiamiento_id_foreign` (`fuente_financiamiento_id`) USING BTREE,
  KEY `proyectos_ente_ejecutor_id_foreign` (`ente_ejecutor_id`) USING BTREE,
  KEY `proyectos_estados_id_foreign` (`estados_id`) USING BTREE,
  CONSTRAINT `proyectos_ibfk_1` FOREIGN KEY (`ente_ejecutor_id`) REFERENCES `ente_ejecutor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `proyectos_ibfk_2` FOREIGN KEY (`estados_id`) REFERENCES `estados` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `proyectos_ibfk_3` FOREIGN KEY (`estatus_id`) REFERENCES `estatus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `proyectos_ibfk_4` FOREIGN KEY (`fuente_financiamiento_id`) REFERENCES `fuente_financiamiento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `proyectos_ibfk_5` FOREIGN KEY (`municipio_id`) REFERENCES `municipio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `proyectos_ibfk_6` FOREIGN KEY (`parroquia_id`) REFERENCES `parroquia` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `proyectos_ibfk_7` FOREIGN KEY (`sector_primario_id`) REFERENCES `sector_primario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of proyectos
-- ----------------------------

-- ----------------------------
-- Table structure for proyectos_links
-- ----------------------------
DROP TABLE IF EXISTS `proyectos_links`;
CREATE TABLE `proyectos_links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source` int(10) unsigned NOT NULL,
  `target` int(10) unsigned NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of proyectos_links
-- ----------------------------

-- ----------------------------
-- Table structure for proyectos_tareas
-- ----------------------------
DROP TABLE IF EXISTS `proyectos_tareas`;
CREATE TABLE `proyectos_tareas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `proyectos_id` int(10) unsigned NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `duration` int(10) unsigned NOT NULL,
  `progress` double(8,2) NOT NULL,
  `sortorder` double NOT NULL,
  `parent` int(10) unsigned NOT NULL,
  `deadline` timestamp NULL DEFAULT NULL,
  `planned_start` timestamp NULL DEFAULT NULL,
  `planned_end` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `proyectos_tareas_proyectos_id_foreign` (`proyectos_id`) USING BTREE,
  CONSTRAINT `proyectos_tareas_ibfk_1` FOREIGN KEY (`proyectos_id`) REFERENCES `proyectos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of proyectos_tareas
-- ----------------------------

-- ----------------------------
-- Table structure for puntos
-- ----------------------------
DROP TABLE IF EXISTS `puntos`;
CREATE TABLE `puntos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `municipio_id` int(10) unsigned NOT NULL,
  `parroquia_id` int(10) unsigned NOT NULL,
  `ubicacion` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `puntos_municipio_id_foreign` (`municipio_id`),
  KEY `puntos_parroquia_id_foreign` (`parroquia_id`),
  CONSTRAINT `puntos_municipio_id_foreign` FOREIGN KEY (`municipio_id`) REFERENCES `municipio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `puntos_parroquia_id_foreign` FOREIGN KEY (`parroquia_id`) REFERENCES `parroquia` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of puntos
-- ----------------------------

-- ----------------------------
-- Table structure for redes_alimentacion
-- ----------------------------
DROP TABLE IF EXISTS `redes_alimentacion`;
CREATE TABLE `redes_alimentacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of redes_alimentacion
-- ----------------------------
INSERT INTO `redes_alimentacion` VALUES ('1', 'Publicas', '2016-08-11 00:57:35', '2016-08-11 01:18:55', null);
INSERT INTO `redes_alimentacion` VALUES ('2', 'Privadas', '2016-08-11 00:58:10', '2016-08-11 01:28:10', null);
INSERT INTO `redes_alimentacion` VALUES ('3', 'Extranjeras', '2017-03-17 13:47:00', '2017-03-17 13:47:00', null);
INSERT INTO `redes_alimentacion` VALUES ('4', 'prueba', '2017-03-17 13:47:09', '2017-03-17 13:47:04', '2017-03-17 13:47:09');

-- ----------------------------
-- Table structure for responsable_punto
-- ----------------------------
DROP TABLE IF EXISTS `responsable_punto`;
CREATE TABLE `responsable_punto` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `puntos_id` int(10) unsigned NOT NULL,
  `cedula` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fuerza_trabajo_id` int(10) unsigned NOT NULL,
  `mision_especifica` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `responsable_punto_puntos_id_foreign` (`puntos_id`),
  KEY `responsable_punto_fuerza_trabajo_id_foreign` (`fuerza_trabajo_id`),
  CONSTRAINT `responsable_punto_fuerza_trabajo_id_foreign` FOREIGN KEY (`fuerza_trabajo_id`) REFERENCES `fuerza_trabajo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `responsable_punto_puntos_id_foreign` FOREIGN KEY (`puntos_id`) REFERENCES `puntos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of responsable_punto
-- ----------------------------

-- ----------------------------
-- Table structure for resumen
-- ----------------------------
DROP TABLE IF EXISTS `resumen`;
CREATE TABLE `resumen` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fecha_inicio` date NOT NULL,
  `fecha_final` date NOT NULL,
  `jornadas` int(10) unsigned NOT NULL COMMENT 'Cantidad de Jornadas Realizadas',
  `familias` int(10) unsigned NOT NULL COMMENT 'Cantidad de Familias Atendidas',
  `claps` int(10) unsigned NOT NULL COMMENT 'Cantidad de Claps Atendidos',
  `bases` int(10) unsigned NOT NULL COMMENT 'Cantidad de Bases de Misiones Atendidas',
  `instituciones` int(10) unsigned NOT NULL COMMENT 'Cantidad de Instituciones Atendidas',
  `mision_vivienda` int(11) NOT NULL,
  `punto_circulo` int(10) unsigned NOT NULL COMMENT 'Cantidad de Punto y Circulo Atendidas',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `resumen_fecha_inicio_unique` (`fecha_inicio`) USING BTREE,
  UNIQUE KEY `resumen_fecha_final_unique` (`fecha_final`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of resumen
-- ----------------------------
INSERT INTO `resumen` VALUES ('20', '2017-03-06', '2017-03-20', '544', '44', '54', '4', '4', '545', '45', '2017-03-20 13:24:07', '2017-03-20 13:24:07', null);
INSERT INTO `resumen` VALUES ('21', '2017-03-01', '2017-03-13', '5345', '46', '465', '4654', '65', '465', '46', '2017-03-20 13:24:56', '2017-03-20 13:24:56', null);
INSERT INTO `resumen` VALUES ('22', '2017-03-20', '2017-03-26', '25', '400', '50', '10', '10', '30', '20', '2017-03-20 16:50:56', '2017-03-20 16:50:56', null);

-- ----------------------------
-- Table structure for resumen_distribucion
-- ----------------------------
DROP TABLE IF EXISTS `resumen_distribucion`;
CREATE TABLE `resumen_distribucion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `municipio_id` int(10) unsigned NOT NULL,
  `resumen_id` int(10) unsigned NOT NULL,
  `jornadas` int(10) unsigned NOT NULL,
  `jornadas_acumulado` int(10) unsigned NOT NULL,
  `familias` int(10) unsigned NOT NULL,
  `familias_acumulado` int(10) unsigned NOT NULL,
  `cantidad` decimal(8,2) unsigned NOT NULL,
  `cantidad_acumulado` decimal(8,2) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `resumen_distribucion_municipio_id_foreign` (`municipio_id`) USING BTREE,
  KEY `resumen_distribucion_resumen_id_foreign` (`resumen_id`) USING BTREE,
  CONSTRAINT `resumen_distribucion_ibfk_1` FOREIGN KEY (`municipio_id`) REFERENCES `municipio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `resumen_distribucion_ibfk_2` FOREIGN KEY (`resumen_id`) REFERENCES `resumen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=343 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of resumen_distribucion
-- ----------------------------
INSERT INTO `resumen_distribucion` VALUES ('331', '100', '20', '0', '55', '45', '5', '54.00', '545.00', '2017-03-20 13:24:07', '2017-03-20 13:24:07', null);
INSERT INTO `resumen_distribucion` VALUES ('332', '101', '20', '45', '45', '5', '5', '5.00', '5.00', '2017-03-20 13:24:07', '2017-03-20 13:24:07', null);
INSERT INTO `resumen_distribucion` VALUES ('333', '102', '20', '44', '55', '5', '5', '5.00', '545.00', '2017-03-20 13:24:07', '2017-03-20 13:24:07', null);
INSERT INTO `resumen_distribucion` VALUES ('334', '103', '20', '45', '45', '45', '45', '55.00', '5.00', '2017-03-20 13:24:07', '2017-03-20 13:24:07', null);
INSERT INTO `resumen_distribucion` VALUES ('335', '100', '21', '54', '6546', '54', '654', '654.00', '64.00', '2017-03-20 13:24:56', '2017-03-20 13:24:56', null);
INSERT INTO `resumen_distribucion` VALUES ('336', '101', '21', '65', '46', '54', '654', '654.00', '654.00', '2017-03-20 13:24:56', '2017-03-20 13:24:56', null);
INSERT INTO `resumen_distribucion` VALUES ('337', '102', '21', '654', '6', '54', '654', '65.00', '46.00', '2017-03-20 13:24:56', '2017-03-20 13:24:56', null);
INSERT INTO `resumen_distribucion` VALUES ('338', '103', '21', '54', '654', '654', '654', '654.00', '654.00', '2017-03-20 13:24:56', '2017-03-20 13:24:56', null);
INSERT INTO `resumen_distribucion` VALUES ('339', '100', '22', '30', '30', '30', '30', '30.00', '30.00', '2017-03-20 16:50:56', '2017-03-20 16:50:56', null);
INSERT INTO `resumen_distribucion` VALUES ('340', '101', '22', '60', '60', '60', '60', '60.00', '60.00', '2017-03-20 16:50:56', '2017-03-20 16:50:56', null);
INSERT INTO `resumen_distribucion` VALUES ('341', '102', '22', '90', '90', '90', '90', '90.00', '90.00', '2017-03-20 16:50:56', '2017-03-20 16:50:56', null);
INSERT INTO `resumen_distribucion` VALUES ('342', '103', '22', '120', '120', '120', '120', '120.00', '120.00', '2017-03-20 16:50:57', '2017-03-20 16:50:57', null);

-- ----------------------------
-- Table structure for resumen_distribucion_empresas
-- ----------------------------
DROP TABLE IF EXISTS `resumen_distribucion_empresas`;
CREATE TABLE `resumen_distribucion_empresas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `resumen_id` int(10) unsigned NOT NULL,
  `empresas_id` int(10) unsigned NOT NULL,
  `claps` int(11) DEFAULT NULL,
  `familias` int(11) DEFAULT NULL,
  `jornadas` int(11) DEFAULT NULL,
  `cantidad` decimal(8,2) NOT NULL COMMENT 'Cantidad de Alimentos Distribuidos en Toneladas',
  `cantidad_acumulado` decimal(8,2) NOT NULL COMMENT 'Cantidad de Alimentos Distribuidos Acumulados en Toneladas',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `resumen_distribucion_empresas_resumen_id_foreign` (`resumen_id`) USING BTREE,
  KEY `resumen_distribucion_empresas_empresas_id_foreign` (`empresas_id`) USING BTREE,
  CONSTRAINT `resumen_distribucion_empresas_ibfk_1` FOREIGN KEY (`empresas_id`) REFERENCES `empresas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `resumen_distribucion_empresas_ibfk_2` FOREIGN KEY (`resumen_id`) REFERENCES `resumen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of resumen_distribucion_empresas
-- ----------------------------
INSERT INTO `resumen_distribucion_empresas` VALUES ('1', '20', '1', '5', '5', '55', '5.00', '5.00', '2017-03-20 13:24:07', '2017-03-20 13:24:07', null);
INSERT INTO `resumen_distribucion_empresas` VALUES ('2', '20', '2', '45', '54', '54', '45.00', '5.00', '2017-03-20 13:24:07', '2017-03-20 13:24:07', null);
INSERT INTO `resumen_distribucion_empresas` VALUES ('3', '21', '1', '654', '654', '654', '54.00', '654.00', '2017-03-20 13:24:56', '2017-03-20 13:24:56', null);
INSERT INTO `resumen_distribucion_empresas` VALUES ('4', '21', '2', '46', '54', '654', '65.00', '465.00', '2017-03-20 13:24:56', '2017-03-20 13:24:56', null);
INSERT INTO `resumen_distribucion_empresas` VALUES ('5', '22', '1', '150', '150', '150', '150.00', '150.00', '2017-03-20 16:50:57', '2017-03-20 16:50:57', null);
INSERT INTO `resumen_distribucion_empresas` VALUES ('6', '22', '2', '180', '180', '180', '180.00', '180.00', '2017-03-20 16:50:57', '2017-03-20 16:50:57', null);

-- ----------------------------
-- Table structure for rubros
-- ----------------------------
DROP TABLE IF EXISTS `rubros`;
CREATE TABLE `rubros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `estatus` int(11) DEFAULT '1',
  `categoria_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rubros_nombre_unique` (`nombre`) USING BTREE,
  KEY `categoria_id` (`categoria_id`) USING BTREE,
  CONSTRAINT `rubros_ibfk_1` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of rubros
-- ----------------------------
INSERT INTO `rubros` VALUES ('1', 'Aceite no regulado', '1', '5', '2016-06-25 17:35:57', '2016-07-01 17:23:39', null);
INSERT INTO `rubros` VALUES ('2', 'Aceite regulado', '1', '5', '2016-06-25 17:35:58', '2016-07-01 17:24:17', null);
INSERT INTO `rubros` VALUES ('3', 'Arroz blanco presentacion no regulada', '1', '6', '2016-06-25 17:35:58', '2016-07-01 17:24:26', null);
INSERT INTO `rubros` VALUES ('4', 'Arroz blanco presentacion regulada', '1', '6', '2016-06-25 17:35:58', '2016-07-01 17:24:35', null);
INSERT INTO `rubros` VALUES ('5', 'Arvejas', '1', '1', '2016-06-25 17:35:58', '2016-07-01 17:24:48', null);
INSERT INTO `rubros` VALUES ('6', 'Azucar domestica', '1', '7', '2016-06-25 17:35:58', '2016-07-01 17:26:17', null);
INSERT INTO `rubros` VALUES ('7', 'Café molido presentacion no regulada', '1', '1', '2016-06-25 17:35:58', '2016-07-01 17:26:29', null);
INSERT INTO `rubros` VALUES ('8', 'Café molido presentacion regulada', '1', '1', '2016-06-25 17:35:58', '2016-07-01 17:26:37', null);
INSERT INTO `rubros` VALUES ('9', 'Caraota negra', '1', '1', '2016-06-25 17:35:58', '2016-07-01 17:26:46', null);
INSERT INTO `rubros` VALUES ('10', 'Carne de bovino en canal', '1', '3', '2016-06-25 17:35:58', '2016-07-01 17:27:10', null);
INSERT INTO `rubros` VALUES ('11', 'Carne de bovino depostada', '1', '3', '2016-06-25 17:35:58', '2016-07-01 17:26:59', null);
INSERT INTO `rubros` VALUES ('12', 'Harina de maiz precocida no regulada', '1', '2', '2016-06-25 17:35:58', '2016-07-01 17:27:24', null);
INSERT INTO `rubros` VALUES ('13', 'Harina de maiz precocida regulada', '1', '2', '2016-06-25 17:35:58', '2016-07-01 17:27:35', null);
INSERT INTO `rubros` VALUES ('14', 'Harina de trigo familiar', '1', '2', '2016-06-25 17:35:58', '2016-07-01 17:28:00', null);
INSERT INTO `rubros` VALUES ('15', 'Leche en polvo completa - uso domestico', '1', '4', '2016-06-25 17:35:59', '2016-07-01 17:28:13', null);
INSERT INTO `rubros` VALUES ('16', 'Lentejas', '1', '1', '2016-06-25 17:35:59', '2016-07-01 17:29:06', null);
INSERT INTO `rubros` VALUES ('17', 'Margarina', '1', '5', '2016-06-25 17:35:59', '2016-07-01 17:29:46', null);
INSERT INTO `rubros` VALUES ('18', 'Pasta alimenticia no regulada', '1', '6', '2016-06-25 17:35:59', '2016-07-01 17:29:55', null);
INSERT INTO `rubros` VALUES ('20', 'Pollo beneficiado entero', '1', '3', '2016-06-25 17:37:04', '2016-07-01 17:30:36', null);
INSERT INTO `rubros` VALUES ('21', 'Leche UHT', '1', '4', '2016-06-30 20:46:29', '2016-07-01 17:28:39', null);
INSERT INTO `rubros` VALUES ('22', 'Pasta alimenticia regulada', '1', '6', '2016-06-30 21:12:16', '2016-07-01 17:30:05', null);
INSERT INTO `rubros` VALUES ('23', 'Caja de alimento mexicana', '2', '1', '2016-12-22 17:16:26', '2016-12-22 17:16:26', null);

-- ----------------------------
-- Table structure for sector
-- ----------------------------
DROP TABLE IF EXISTS `sector`;
CREATE TABLE `sector` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `parroquia_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sector_index_unique` (`slug`,`parroquia_id`) USING BTREE,
  KEY `sector_parroquia_id_foreign` (`parroquia_id`) USING BTREE,
  CONSTRAINT `sector_ibfk_1` FOREIGN KEY (`parroquia_id`) REFERENCES `parroquia` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sector
-- ----------------------------
INSERT INTO `sector` VALUES ('1', 'banco Obrero', 'banco-obrero', '229', '2017-03-17 16:36:11', '2017-03-17 16:36:11', null);
INSERT INTO `sector` VALUES ('2', 'Gutiapo', 'gutiapo', '310', '2017-03-20 12:44:53', '2017-03-20 12:44:53', null);

-- ----------------------------
-- Table structure for sector_primario
-- ----------------------------
DROP TABLE IF EXISTS `sector_primario`;
CREATE TABLE `sector_primario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sector_primario
-- ----------------------------
INSERT INTO `sector_primario` VALUES ('1', 'Agua', '2017-01-09 01:00:00', '2017-01-09 01:00:00', null);
INSERT INTO `sector_primario` VALUES ('2', 'Infraestructura (Vial)', '2017-01-09 01:00:00', '2017-01-09 01:00:00', null);
INSERT INTO `sector_primario` VALUES ('3', 'Educación', '2017-01-09 01:00:00', '2017-01-09 01:00:00', null);
INSERT INTO `sector_primario` VALUES ('4', 'Salud', '2017-01-09 01:00:00', '2017-01-09 01:00:00', null);
INSERT INTO `sector_primario` VALUES ('5', 'Infraestructura', '2017-01-09 01:00:00', '2017-01-09 01:00:00', null);
INSERT INTO `sector_primario` VALUES ('6', 'Social', '2017-01-09 01:00:00', '2017-01-09 01:00:00', null);
INSERT INTO `sector_primario` VALUES ('7', 'Agroindustria', '2017-01-09 01:00:00', '2017-01-09 01:00:00', null);
INSERT INTO `sector_primario` VALUES ('8', 'Fortalecimiento Institucional', '2017-01-09 01:00:00', '2017-01-09 01:00:00', null);
INSERT INTO `sector_primario` VALUES ('9', 'Seguridad', '2017-01-09 01:00:00', '2017-01-09 01:00:00', null);
INSERT INTO `sector_primario` VALUES ('10', 'Minería', '2017-01-09 01:00:00', '2017-01-09 01:00:00', null);
INSERT INTO `sector_primario` VALUES ('11', 'Turismo', '2017-01-09 01:00:00', '2017-01-09 01:00:00', null);

-- ----------------------------
-- Table structure for sessions
-- ----------------------------
DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8_unicode_ci,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sessions
-- ----------------------------
INSERT INTO `sessions` VALUES ('345lqRcwx5eyOlstA7HHIwFugcynhnnczqpfYGR9', null, '66.102.7.250', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko; Google Web Preview) Chrome/41.0.2272.118 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoiR0ppaDdDcmFodldCRWZGUktYMmNnWVhWTTVuazlCOHR4R0t5RDhydCI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czozOToiaHR0cDovL2ludmVudGFyaW9kZWx0YS5lLWJvbGl2YXIuZ29iLnZlIjt9czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDU6Imh0dHA6Ly9pbnZlbnRhcmlvZGVsdGEuZS1ib2xpdmFyLmdvYi52ZS9sb2dpbiI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNTA0ODI5NDYyO3M6MToiYyI7aToxNTA0ODI5NDYxO3M6MToibCI7czoxOiIwIjt9fQ==', '1504829462');
INSERT INTO `sessions` VALUES ('c1mtjqMlbKNe2b3aDpuKsPCyazQDEW8xzC3A3MF7', null, '172.20.92.158', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoiUW9tNGhDa1VtdGRvWE1OWHlIQkNEU1BlTXNBRnd5cUk2MkZ2Mk13QSI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czozOToiaHR0cDovL2ludmVudGFyaW9kZWx0YS5lLWJvbGl2YXIuZ29iLnZlIjt9czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDU6Imh0dHA6Ly9pbnZlbnRhcmlvZGVsdGEuZS1ib2xpdmFyLmdvYi52ZS9sb2dpbiI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNTA0Nzk4MDU2O3M6MToiYyI7aToxNTA0Nzk3ODUwO3M6MToibCI7czoxOiIwIjt9fQ==', '1504798056');
INSERT INTO `sessions` VALUES ('eK8lOmk1yCyc9n9US5P3aqWEDlOFkF7FuKuO6D1v', null, '66.249.93.25', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiVUlnRURER2JoaWo3aERFYTRNeHNyYTNEakZOWlRiQ3NqcEdRbXFacyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDU6Imh0dHA6Ly9pbnZlbnRhcmlvZGVsdGEuZS1ib2xpdmFyLmdvYi52ZS9sb2dpbiI7fXM6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNTA1MDA3NDg2O3M6MToiYyI7aToxNTA1MDA3NDg2O3M6MToibCI7czoxOiIwIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', '1505007486');
INSERT INTO `sessions` VALUES ('eLuqZ8MfkcD6O8rQs3TxWwFRD7fIhTMyWvgsz7id', null, '66.102.7.250', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko; Google Web Preview) Chrome/41.0.2272.118 Safari/537.36', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiOUN3SjltRmU4NjlGT2RydjY2TFpodTJpcmY3Tk9JR2dxVGdKWG10ViI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDU6Imh0dHA6Ly9pbnZlbnRhcmlvZGVsdGEuZS1ib2xpdmFyLmdvYi52ZS9sb2dpbiI7fXM6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNTA0OTE3NDU0O3M6MToiYyI7aToxNTA0OTE3NDU0O3M6MToibCI7czoxOiIwIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', '1504917454');
INSERT INTO `sessions` VALUES ('ji6DsdLyp4nN15s0Hq86SL6RMC0wEGLue2dlYeJT', null, '66.102.7.249', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko; Google Web Preview) Chrome/41.0.2272.118 Safari/537.36', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiVVBvbmF2WllySUdCbWNwdkhhb3pWZW5DM0lYT1dVSWxNMkJXY1FlTCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDU6Imh0dHA6Ly9pbnZlbnRhcmlvZGVsdGEuZS1ib2xpdmFyLmdvYi52ZS9sb2dpbiI7fXM6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNTA0OTIxMzQzO3M6MToiYyI7aToxNTA0OTIxMzQzO3M6MToibCI7czoxOiIwIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', '1504921343');
INSERT INTO `sessions` VALUES ('jRMLbDuMhlc3A6L08hI2JczqVSQg2dSYCKjXGCnq', null, '66.249.88.8', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoibGNodWNBMHM5bUJqdDVleTdSQ0M3aGUyaWJsWG9vQUNXNkRBUkl5TyI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czozOToiaHR0cDovL2ludmVudGFyaW9kZWx0YS5lLWJvbGl2YXIuZ29iLnZlIjt9czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mzk6Imh0dHA6Ly9pbnZlbnRhcmlvZGVsdGEuZS1ib2xpdmFyLmdvYi52ZSI7fXM6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNTA0ODk5MDI1O3M6MToiYyI7aToxNTA0ODk5MDI1O3M6MToibCI7czoxOiIwIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', '1504899025');
INSERT INTO `sessions` VALUES ('JYPKyNbCrdJGYLPMXuamoALawZ22SK2hCq53RFFM', null, '66.249.88.5', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiWGhCUGVQQVdtUkFVQkF4enVLcmgwYnhSeGxoRXVxQVdxSWVlMEM1YiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDU6Imh0dHA6Ly9pbnZlbnRhcmlvZGVsdGEuZS1ib2xpdmFyLmdvYi52ZS9sb2dpbiI7fXM6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNTA0ODk5MDI3O3M6MToiYyI7aToxNTA0ODk5MDI3O3M6MToibCI7czoxOiIwIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', '1504899027');
INSERT INTO `sessions` VALUES ('kVv0ZKiIeQoCmfdy8tDLkgxxl5v5yOSjb4XISGIC', null, '66.249.88.5', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoiVXJwb1BQMWVTdGFyU1h2VDJjbzBXclNEVUhxSVNvMDFlSjN6STJFcyI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czozOToiaHR0cDovL2ludmVudGFyaW9kZWx0YS5lLWJvbGl2YXIuZ29iLnZlIjt9czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mzk6Imh0dHA6Ly9pbnZlbnRhcmlvZGVsdGEuZS1ib2xpdmFyLmdvYi52ZSI7fXM6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNTA0ODAwODU2O3M6MToiYyI7aToxNTA0ODAwODU2O3M6MToibCI7czoxOiIwIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', '1504800856');
INSERT INTO `sessions` VALUES ('MEOFyC34wFBjFYygdl06rDwWBiL7OTZ4bkbRn8gy', null, '66.102.7.249', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko; Google Web Preview) Chrome/41.0.2272.118 Safari/537.36', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiSXlPZXBFalpLR1BaeHlzSWREOHRIVzZjQUppemIzMFAxTmhaZXhwYyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDU6Imh0dHA6Ly9pbnZlbnRhcmlvZGVsdGEuZS1ib2xpdmFyLmdvYi52ZS9sb2dpbiI7fXM6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNTA0OTA2NzA5O3M6MToiYyI7aToxNTA0OTA2NzA5O3M6MToibCI7czoxOiIwIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', '1504906709');
INSERT INTO `sessions` VALUES ('oKY92JvDRqzjdMNGdSsoyp9dyuE2aOSekIIji1vY', null, '66.102.7.248', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko; Google Web Preview) Chrome/41.0.2272.118 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoiVzVNcXdYSXkzd2V4MXQ2Sjh2OUtvc3ZqRmp1OG1OQzZhdXp3ZE9xZiI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czozOToiaHR0cDovL2ludmVudGFyaW9kZWx0YS5lLWJvbGl2YXIuZ29iLnZlIjt9czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDU6Imh0dHA6Ly9pbnZlbnRhcmlvZGVsdGEuZS1ib2xpdmFyLmdvYi52ZS9sb2dpbiI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNTA0ODE3NTkwO3M6MToiYyI7aToxNTA0ODE3NTg5O3M6MToibCI7czoxOiIwIjt9fQ==', '1504817590');
INSERT INTO `sessions` VALUES ('qZUzVqeqxt8H7FyIZ9aX3RoBrxHtA94z5qYir9J2', null, '66.249.88.11', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiVjRINUpmSEdDMnJOa0pZMWJuWG55NW1EamNjYmNSNzFhRjMyRFk1eiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDU6Imh0dHA6Ly9pbnZlbnRhcmlvZGVsdGEuZS1ib2xpdmFyLmdvYi52ZS9sb2dpbiI7fXM6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNTA0ODAwODU4O3M6MToiYyI7aToxNTA0ODAwODU4O3M6MToibCI7czoxOiIwIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', '1504800858');
INSERT INTO `sessions` VALUES ('TEKsDiXYdSeMbNXlyYaun9Y8uuAg9L1OSXlXcnCs', null, '66.102.7.249', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko; Google Web Preview) Chrome/41.0.2272.118 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoicUNpRUVLY243T2RLOTBqcGNIaEpkZTFHa1JOM1Rvb0JFVmowOUw0aiI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czozOToiaHR0cDovL2ludmVudGFyaW9kZWx0YS5lLWJvbGl2YXIuZ29iLnZlIjt9czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDU6Imh0dHA6Ly9pbnZlbnRhcmlvZGVsdGEuZS1ib2xpdmFyLmdvYi52ZS9sb2dpbiI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNTA0ODIxMjc5O3M6MToiYyI7aToxNTA0ODIxMjc4O3M6MToibCI7czoxOiIwIjt9fQ==', '1504821279');
INSERT INTO `sessions` VALUES ('tQvWRud9PmZUFhuEljeGISUoDtwR92pO6StVo2H5', null, '172.20.92.158', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoiSzNHclgzbm9rbzg5MFBQcFM0dnM4eXBlOXZ6b1RpTTY0clNOU2VZVCI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czozOToiaHR0cDovL2ludmVudGFyaW9kZWx0YS5lLWJvbGl2YXIuZ29iLnZlIjt9czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDU6Imh0dHA6Ly9pbnZlbnRhcmlvZGVsdGEuZS1ib2xpdmFyLmdvYi52ZS9sb2dpbiI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNTA0OTAzNDQ3O3M6MToiYyI7aToxNTA0OTAzNDQ0O3M6MToibCI7czoxOiIwIjt9fQ==', '1504903447');
INSERT INTO `sessions` VALUES ('VHQHOufQclGJqEO0I8RarMi2tqVHmas7ZFIVgi4b', null, '66.102.9.11', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko; Google Web Preview) Chrome/41.0.2272.118 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoic21xSDRiV3JXQVZqS1dmZGd1ajM0UXdGTGVtVndBNGduMUlDOXQwNSI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czozOToiaHR0cDovL2ludmVudGFyaW9kZWx0YS5lLWJvbGl2YXIuZ29iLnZlIjt9czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDU6Imh0dHA6Ly9pbnZlbnRhcmlvZGVsdGEuZS1ib2xpdmFyLmdvYi52ZS9sb2dpbiI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNTA0ODEyMzEyO3M6MToiYyI7aToxNTA0ODEyMzA4O3M6MToibCI7czoxOiIwIjt9fQ==', '1504812312');
INSERT INTO `sessions` VALUES ('xtAPD7FSX3R9GGeiYheP7skOkP394nYrYw0rKPdu', null, '66.102.7.250', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko; Google Web Preview) Chrome/41.0.2272.118 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoiZFJmU1k3aUtKZnVoanRQVUpPcnVhS3RibjVaVzJBYkF0MFpYVnZiaSI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czozOToiaHR0cDovL2ludmVudGFyaW9kZWx0YS5lLWJvbGl2YXIuZ29iLnZlIjt9czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDU6Imh0dHA6Ly9pbnZlbnRhcmlvZGVsdGEuZS1ib2xpdmFyLmdvYi52ZS9sb2dpbiI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNTA0ODAwODU3O3M6MToiYyI7aToxNTA0ODAwODU2O3M6MToibCI7czoxOiIwIjt9fQ==', '1504800857');
