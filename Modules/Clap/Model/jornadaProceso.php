<?php 

namespace Modules\Clap\Model;

use Modules\Admin\Model\modelo;
use Carbon\Carbon;

class jornadaProceso extends modelo{
	protected $table = 'jornada_proceso';

	protected $fillable = ['clap_id', 'cantidad','fecha','familia'];

	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

	public function setFechaAttribute($value)
	{
        $this->attributes['fecha'] = Carbon::createFromFormat('d/m/Y', $value);
    }
    
    public function getFechaAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y');
    }
}
   