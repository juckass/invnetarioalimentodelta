<?php 

namespace Modules\Clap\Model;

use Modules\Admin\Model\modelo;
use Carbon\Carbon;

class jornada extends modelo{
	protected $table = 'jornada';

	protected $fillable = ['inicio','fin'];

	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];


	public function setInicioAttribute($value)
    {
        $this->attributes['inicio'] = Carbon::createFromFormat('d/m/Y', $value);
    }
    
    public function getInicioAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y');
    }

    public function setFinAttribute($value)
    {
        $this->attributes['fin'] = Carbon::createFromFormat('d/m/Y', $value);
    }
    
    public function getFinAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y');
    }

}
   