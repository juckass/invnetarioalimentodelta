<?php

namespace Modules\Clap\Model;

use Modules\Admin\Model\Modelo;

use Modules\Clap\Model\Estados;

class Sector extends modelo
{
    protected $table = 'sector';
    protected $fillable = ["estados_id","municipio_id","parroquia_id","nombre","slug"];
    protected $campos = [
        'estados_id' => [
            'type' => 'select',
            'label' => 'Estado',
            'placeholder' => '- Seleccione un Estado',
            'url' => 'clap/definiciones/estados'
        ],
        'municipio_id' => [
            'type' => 'select',
            'label' => 'Municipio',
            'placeholder' => '- Seleccione un Municipio',
            'url' => 'clap/definiciones/municipio'
        ],
        'parroquia_id' => [
            'type' => 'select',
            'label' => 'Parroquia',
            'placeholder' => '- Seleccione un Parroquia',
            'url' => 'clap/definiciones/parroquia'
        ],
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Sector'
        ]
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['estados_id']['options'] = Estados::pluck('nombre', 'id');
        $this->campos['municipio_id']['options'] = Municipio::pluck('nombre', 'id');
        $this->campos['parroquia_id']['options'] = Parroquia::pluck('nombre', 'id');
    }

    public function clap()
	{
		return $this->hasMany('Modules\Clap\Model\clap');
	}

	public function parroquia()
	{
		return $this->belongsTo('Modules\Proyecto\Model\Parroquia');
	}
}