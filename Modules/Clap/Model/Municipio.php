<?php

namespace Modules\Clap\Model;

use Modules\Admin\Model\Modelo;

use Modules\Clap\Model\Estados;

class Municipio extends modelo
{
    protected $table = 'municipio';
    protected $fillable = ["estados_id","nombre","poblacion"];
    protected $campos = [
        'estados_id' => [
            'type' => 'select',
            'label' => 'Estado',
            'placeholder' => '- Seleccione un Estado',
            'url' => 'clap/definiciones/estados'
        ],
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Municipio'
        ],
        'poblacion' => [
            'type' => 'number',
            'label' => 'Poblaci\u00f3n',
            'placeholder' => 'Poblaci\u00f3n del Municipio'
        ]
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['estados_id']['options'] = Estados::pluck('nombre', 'id');
    }

    public function estados()
	{
		return $this->belongsTo('Modules\Clap\Model\estados');
	}

	public function parroquia()
	{
		return $this->hasMany('Modules\Proyecto\Model\Parroquia');
	}

	public function proyectos()
	{
		return $this->hasMany('Modules\Proyecto\Model\Proyectos');
	}

	public function resumen_distribucion()
	{
		return $this->hasMany('Modules\Resumen\Model\resumen_distribucion');
	}

	
}