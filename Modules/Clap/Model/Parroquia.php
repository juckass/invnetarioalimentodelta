<?php

namespace Modules\Clap\Model;

use Modules\Admin\Model\Modelo;

use Modules\Clap\Model\Estados;

class Parroquia extends modelo
{
    protected $table = 'parroquia';
    protected $fillable = ["nombre","municipio_id"];
    protected $campos = [
        'estados_id' => [
            'type' => 'select',
            'label' => 'Estado',
            'placeholder' => '- Seleccione un Estado',
            'url' => 'clap/definiciones/estados'
        ],
        'municipio_id' => [
            'type' => 'select',
            'label' => 'Municipio',
            'placeholder' => '- Seleccione un Municipio',
            'url' => 'clap/definiciones/municipio'
        ],
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Parroquia'
        ]
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['estados_id']['options'] = Estados::pluck('nombre', 'id');
    }

    public function clap()
	{
		return $this->hasMany('Modules\Clap\Model\clap');
	}

	public function municipio()
	{
		return $this->belongsTo('Modules\Proyecto\Model\Municipio');
	}

	public function proyectos()
	{
		return $this->hasMany('Modules\Proyecto\Model\Proyectos');
	}

	public function sector()
	{
		return $this->hasMany('Modules\Clap\Model\sector');
	}

	
}