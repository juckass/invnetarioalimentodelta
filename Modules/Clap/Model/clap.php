<?php 



namespace Modules\Clap\Model;



use Modules\Admin\Model\modelo;



class clap extends modelo{

	protected $table = 'clap';

	protected $fillable = ['nombre','codigo','parroquia_id','sector_id','consejo_comunal','familia','slug'];

	public function clap_contacto(){

		return $this->hasOne('Modules\Clap\Model\clap_contacto', 'clap_id');

	}

}

   