<?php 

namespace Modules\Clap\Model;

use Modules\Admin\Model\modelo;

class clap_contacto extends modelo{
	protected $table = 'clap_contacto';
	protected $fillable = ['cedula','nombre','telefono','clap_id'];

	public function clap(){

		return $this->belongsTo('Modules\Clap\Model\clap', 'clap_id');

	}
}
   