<?php

namespace Modules\Clap\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'clap');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'clap');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'clap');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
