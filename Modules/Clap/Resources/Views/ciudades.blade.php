@extends('admin::layouts.default')
@section('content')
	@include('admin::partials.botonera')

	{{ Form::bsModalBusqueda([
		'Estados Id' => '33.333333333333',
		'Nombre' => '33.333333333333',
		'Capital' => '33.333333333333'
	]) }}

	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url('/') }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Ciudades</span>
		</li>
	</ul>
	
	<div class="row">
		{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
		
		
			{{ Form::bsNumber('estados_id', '', [
				'label' => 'Estados Id',
				'placeholder' => 'Estados Id',
				'required' => 'required'
			]) }} 
			{{ Form::bsText('nombre', '', [
				'label' => 'Nombre',
				'placeholder' => 'Nombre',
				'required' => 'required'
			]) }} 
			{{ Form::bsText('capital', '', [
				'label' => 'Capital',
				'placeholder' => 'Capital',
				'required' => 'required'
			]) }}
		
		{!! Form::close() !!}
	</div>
@endsection