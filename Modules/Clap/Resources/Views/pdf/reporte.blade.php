@if (!$html)
@include('clap::pdf.estilo_reporte')
@endif
<div id="reporteHtml">
	<div class="header clearfix">
		@if (!$html)
		<div id="logo">
			<img src="{{ url('public/img/logos/logo.png') }}" style="width: 4cm;">
		</div>
		@endif
		<h1>REPORTE DE DISTRIBUCION DE CLAP POR {{ strtoupper($region) }}</h1>
		<div id="project">
			<div><span>Reporte Realizado Desde:</span> {{ $fechaInicio }} &nbsp;&nbsp;&nbsp;
			<span>Hasta:</span> {{ $fechaFin }}</div>
			</div>
		<div id="company" class="clearfix">
			<div><span>Fecha de impresi&oacute;n</span> {{ date('d/m/Y') }}</div>
		</div>
	</header>
	<div class="main">  
		<table>
			<thead>
				<tr>
					<th>{{ ucwords($region) }}</th>

					<th>Clap Registrados</th>
					<th>Clap Conformados</th>
				</tr>
			</thead>
			<tbody>
				<?php

				$totales = [0, 0, 0, 0, 0, 0];
				?>

				@foreach($jornadas as $jornada)
					<?php
					if (!isset($totalClapRegistrados[$jornada->id])){
						$totalClapRegistrados[$jornada->id] = 0;
					}

					if (!isset($totalClapConformados[$jornada->id])){
						$totalClapConformados[$jornada->id] = 0;
					}

					if (!isset($totalAtendidosClapRegistrados[$jornada->id])){
						$totalAtendidosClapRegistrados[$jornada->id] = 0;
					}

					if (!isset($totalAtendidosClapConformados[$jornada->id])){
						$totalAtendidosClapConformados[$jornada->id] = 0;
					}
					
					if (!isset($totalAtendidos[$jornada->id])){
						$totalAtendidos[$jornada->id] = 0;
					}

					//$totalClaps = $totalClapRegistrados[$jornada->id] + $totalClapConformados[$jornada->id];
					$totalPorAtender = $totalClapRegistrados[$jornada->id] + $totalClapConformados[$jornada->id] - $totalAtendidos[$jornada->id];

					if ($totalPorAtender < 0){
						$totalPorAtender = 0;
					}

					$totales[0] += $totalClapRegistrados[$jornada->id];
					$totales[1] += $totalClapConformados[$jornada->id];
					$totales[2] += $totalAtendidos[$jornada->id];
					$totales[3] += $totalPorAtender;
					$totales[4] += $jornada->familia;
					?>
					<tr data-{{ $region }}="{{ $jornada->id }}">
						<td class="izq">{{ $jornada->nombre }}</td>

						<td>{{ number_format($totalClapRegistrados[$jornada->id], 0, ',', '.') }}</td>
						<td>{{ number_format($totalClapConformados[$jornada->id], 0, ',', '.') }}</td>
					</tr>
				@endforeach

				<tr style="font-weight: bold;">
					<td class="izq">Total</td>

					<td>{{ number_format($totales[0], 0, ',', '.') }}</td>
					<td>{{ number_format($totales[1], 0, ',', '.') }}</td>
				</tr>
			</tbody>
		</table>	
	</div>
	<div class="footer">
		Gobernaci&oacute;n del estado Bol&iacute;var
	</div>
</div>