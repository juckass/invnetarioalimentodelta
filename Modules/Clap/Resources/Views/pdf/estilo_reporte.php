<style type="text/css">
#reporteHtml {
	position: relative;
	margin: 0 auto; 
	color: #001028;
	background: #FFFFFF;
	font-size: 12px; 
	font-family: Arial;
}

#reporteHtml .clearfix:after {
	content: "";
	display: table;
	clear: both;
}

#reporteHtml a {
	color: #5D6975;
	text-decoration: underline;
}

#reporteHtml .header {
	padding: 10px 0;
	margin-bottom: 30px;
}

#reporteHtml #logo {
	text-align: center;
	margin-bottom: 10px;
}

#reporteHtml #logo img {
	width: 90px;
}

#reporteHtml h1 {
	border-top: 1px solid  #5D6975;
	border-bottom: 1px solid  #5D6975;
	color: #5D6975;
	font-size: 2.4em;
	line-height: 1.4em;
	font-weight: normal;
	text-align: center;
	margin: 0 0 20px 0;
}

#reporteHtml #project {
	float: left;
}

#reporteHtml #project span {
	color: #5D6975;
	text-align: right;
	margin-right: 10px;
	display: inline-block;
}

#reporteHtml #company {
	float: right;
	text-align: right;
}

#reporteHtml #project div,
#reporteHtml #company div {
	white-space: nowrap;
}

#reporteHtml table {
	width: 100%;
	border-collapse: collapse;
	border-spacing: 0;
	margin-bottom: 20px;
	font-family: Arial;
}

#reporteHtml table tr:nth-child(2n-1) {
	background: #F5F5F5;
}

#reporteHtml table th{
	background: #FFFFFF;
}

#reporteHtml table th,
#reporteHtml table td {
	text-align: center;
}

#reporteHtml table th {
	padding: 5px 20px;
	color: #5D6975;
	border-bottom: 1px solid #C1CED9;
	white-space: nowrap;
	font-weight: normal;
}

#reporteHtml table td {
	cursor: pointer;
}

#reporteHtml table td {
	padding: 10px 20px;
	vertical-align: top;
}

#reporteHtml table td.der{
	text-align: right;
}

#reporteHtml table td.izq{
	text-align: left;
}

#reporteHtml .footer {
	color: #5D6975;
	width: 100%;
	height: 30px;
	position: absolute;
	bottom: 0;
	border-top: 1px solid #C1CED9;
	padding: 8px 0;
	text-align: center;
}

#reporteHtml table tr:hover{ 
	background-color: #ccfafc !important;
}
</style>