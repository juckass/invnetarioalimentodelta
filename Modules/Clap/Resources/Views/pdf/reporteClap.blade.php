@if (!$html)
@include('clap::pdf.estilo_reporte')
@endif

<div id="reporteHtml">
	<div class="header clearfix">
		<h1>Detalle del Clap {{ strtoupper($clap->nombre) }}</h1>
		
		<div id="company" class="clearfix">
			<div><span>Fecha de impresi&oacute;n</span> {{ date('d/m/Y') }}</div>
		</div>
	</header>
	<div class="main">  
		<table>
			<caption>Datos del Clap</caption>
			<tbody>
				<tr>
					<td class="izq" style="font-weight: bold; width: 30%;">Nombre: </td>
					<td class="izq" style="width: 70%;">{{ $clap->nombre }}</td>
				</tr>
				<tr>
					<td class="izq" style="font-weight: bold;">Codigo: </td>
					<td class="izq">{{ $clap->codigo }}</td>
				</tr>
				<tr>
					<td class="izq" style="font-weight: bold;">Municipio: </td>
					<td class="izq">{{ $municipio }}</td>
				</tr>
				<tr>
					<td class="izq" style="font-weight: bold;">Parroquia: </td>
					<td class="izq">{{ $parroquia }}</td>
				</tr>
				<tr>
					<td class="izq" style="font-weight: bold;">Nro de Personas: </td>
					<td class="izq">{{ $clap->familia * 4 }}</td>
				</tr>
			</tbody>
		</table>	
	</div>
	<div class="main">  
		<table>
			<caption>Personas de Contacto</caption>
			<thead>
				<tr>
					<th style="width: 50%;">Nombre</th>
					<th style="width: 50%;">Telefono</th>
				</tr>
			</thead>
			<tbody>
				@foreach($personas as $persona)
				<tr>
					<td class="izq">{{ $persona->nombre }}</td>
					<td class="izq">{{ $persona->telefono }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>	
	</div>

	<br /><br />
	<div class="footer">
		Gobernaci&oacute;n del estado Bol&iacute;var
	</div>
</div>