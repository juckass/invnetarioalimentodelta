@if (!$html)
@include('clap::pdf.estilo_reporte')
@endif

<div id="reporteHtml">
	<div class="header clearfix">
		<h1>REPORTE DE DISTRIBUCION DE CLAP POR {{ strtoupper($region) }}</h1>
		<div id="project">
			<div><span>Reporte Realizado Desde:</span> {{ $fechaInicio }} &nbsp;&nbsp;&nbsp;
			<span>Hasta:</span> {{ $fechaFin }}</div>
		</div>
		<div id="company" class="clearfix">
			<div><span>Fecha de impresi&oacute;n</span> {{ date('d/m/Y') }}</div>
		</div>

		<div>
			<br /><br />
			<div><span>Municipio:</span> {{ $municipio }} <br />
			<div><span>Parroquia:</span> {{ $parroquia }} <br /><br />
		</div>
	</header>
	<div class="main">  
		<table>
			<thead>
				<tr>
					<th>{{ ucwords($region) }}</th>
					<th>Codigo</th>
					<th>Cedula Responsable</th>
					
					<th>Responsable</th>
					<th>Telefono</th>
				</tr>
			</thead>
			<tbody>
				@foreach($claps as $clap)
					@if(isset($clap->clap_contacto->cedula))
						<tr data-clap="{{ base64_encode($clap->clap_contacto->cedula) }}">
					@else
						<tr data-clap="0">
					@endif
					
						<td class="izq">{{ $clap->nombre }}</td>
						<td class="izq">{{ $clap->codigo }}</td>
						
						@if(isset($clap->clap_contacto->cedula))
						<td class="izq">{{ $clap->clap_contacto->cedula }}</td>
						@else
						<td class="izq"></td>
						@endif
						@if(isset($clap->clap_contacto->nombre))
						<td class="izq">{{ $clap->clap_contacto->nombre }}</td>
						@else
						<td class="izq"></td>
						@endif
						@if(isset($clap->clap_contacto->telefono))
						<td class="izq">{{ $clap->clap_contacto->telefono }}</td>
						@else
						<td class="izq"></td>
						@endif
					</tr> 
				@endforeach
			</tbody>
		</table>	
	</div>
	<div class="footer">
		Gobernaci&oacute;n del estado Bol&iacute;var
	</div>
</div>