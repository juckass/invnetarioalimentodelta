@extends(isset($layouts) ? $layouts : 'admin::layouts.default')

@section('content-top')
    @include('admin::partials.botonera')
    
    @include('admin::partials.ubicacion', ['ubicacion' => ['Municipio']])
    
    @include('admin::partials.modal-busqueda', [
        'titulo' => 'Buscar Municipio.',
        'columnas' => [
            'Estados' => '33.333333333333',
		'Nombre' => '33.333333333333'
		
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Municipio->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection