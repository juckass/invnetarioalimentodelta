@extends('admin::layouts.default')
@section('content')
	<div id="botonera">
		<button id="buscar" class="btn green tooltips" data-container="body" data-placement="top" data-original-title="Inicio">
			<i class="fa fa-home"></i>
			<span class="visible-lg-inline visible-md-inline">Inicio</span>
		</button>

		<button id="atras" class="btn blue tooltips" data-container="body" data-placement="top" data-original-title="Atrás">
			<i class="fa fa-arrow-left"></i>
			<span class="visible-lg-inline visible-md-inline">Atr&aacute;s</span>
		</button>

		<button id="imprimir" class="btn btn-info tooltips" data-container="body" data-placement="top" data-original-title="{{ Lang::get('backend.btn_group.print.title') }}">
			<i class="fa fa-print"></i>
			<span class="visible-lg-inline visible-md-inline">{{ Lang::get('backend.btn_group.print.btn') }}</span>
		</button>
	</div>

	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Reportes<i class="fa fa-circle"></i></span>
		</li>
		<li>
			<span>Inventario</span>
		</li>
	</ul>

	<div class="row">
		{!! Form::open([
			'id' => 'formulario',
			'name' => 'formulario',
			'method' => 'POST',
			'target' => '_blank',
			'url' => 'reportes/clap/imprimir'
		]) !!}
			<input id="tipoReporte" name="tipoReporte" type="hidden" value="" />
			<input id="municipio_id" name="municipio_id" type="hidden" value="" />
			<input id="parroquia_id" name="parroquia_id" type="hidden" value="" />
			<input id="clap_id" name="clap_id" type="hidden" value="" />

			<input id="tipo" name="tipo" type="hidden" value="rango" />
			<input id="fechadesde" name="fechadesde" type="hidden" value="{{ $fechadesde }}" />
			<input id="fechahasta" name="fechahasta" type="hidden" value="{{ $fechahasta }}" />

			{{ Form::bsSelect('tipo_clap', 
				[
					'r' => 'Registrados',
					'c' => 'Conformados',
				], '',[
				'label' => 'Tipos de Clap',
			]) }}


			{{ Form::bsSelect('estatus', 
				[
					's' => 'Atendido',
					'n' => 'No atendido',
				], '',[
				'label' => 'Estatus',
			]) }}
			<?php

			/*
			{{ Form::bsSelect('tipo', 
				[
					'rango' => 'Rango de fecha',
					'meses' => 'Meses',
				], '', [
					'label' => 'Tipo de Grafica',
					'required' => 'required'
			]) }}

			<div id="anos">
				{{ Form::bsSelect('mes', $meses, intval(date('m')), [
					'label' => 'Meses',
					'required' => 'required'
				]) }}

				{{ Form::bsSelect('ano', $anos, '', [
					'label' => 'Años',
					'required' => 'required'
				]) }}
			</div>

			<div id="rango">
				{{ Form::bsText('fechadesde', $fechadesde, [
					'label' => 'Desde',
					'required' => 'required'
				]) }}

				{{ Form::bsText('fechahasta', $fechahasta, [
					'label' => 'Hasta',
					'required' => 'required'
				]) }}
			</div>
			*/
			?> 
		{!! Form::close() !!}
	</div>

	<div id="reportehtml"></div>
@endsection