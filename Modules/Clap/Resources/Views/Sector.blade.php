@extends(isset($layouts) ? $layouts : 'admin::layouts.default')

@section('content-top')
    @include('admin::partials.botonera')
    
    @include('admin::partials.ubicacion', ['ubicacion' => ['Sector']])
    
    @include('admin::partials.modal-busqueda', [
        'titulo' => 'Buscar Sector.',
        'columnas' => [
        
		'Parroquia' => '20',
		'Nombre' => '20'

        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Sector->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection