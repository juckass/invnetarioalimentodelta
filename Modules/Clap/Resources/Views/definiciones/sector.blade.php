@extends('admin::layouts.default')
@section('content')
	@include('admin::partials.botonera')

	{{ Form::bsModalBusqueda([
		'Sector' => '50',
		'Municipio' => '25',
		'Parroquia' => '25'
	]) }}

	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Clap</span><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Definiciones</span><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Sectores</span>
		</li>
	</ul>

	<div class="row">
		
			{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST']) !!}
				
				{{ Form::bsSelect('municipio_id', $controller->municipios() ,'', [
					'label' 		=> 'Municipio',
					'required' 		=> 'required',
					'class_cont' 	=> 'col-md-4 col-sm-6'
				]) }}

				{{ Form::bsSelect('parroquia_id', [],'', [
					'label' 		=> 'Parroquia',
					'required' 		=> 'required',
					'class_cont' 	=> 'col-md-4 col-sm-6'
				]) }}

				{{ Form::bsText('nombre', '', [
					'label' 		=> 'Nombre',
					'placeholder' 	=> 'Nombre',
					'required' 		=> 'required',
					'class_cont' 	=> 'col-md-4 col-sm-6'
				]) }}
			{!! Form::close() !!}
		
	</div>
@endsection

@push('css')
<style type="text/css" media="screen">
	#arbol{
		min-height: 200px;
	}
</style>
@endpush