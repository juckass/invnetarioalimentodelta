@extends('admin::layouts.default')
@section('content')
	@include('admin::partials.botonera')

	{{ Form::bsModalBusqueda([
		'Nombre' => '50',
		'Municipio' => '50',
	]) }}

	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Clap</span><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Definiciones</span><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Parroquias</span>
		</li>
	</ul>

	<div class="row">
		<div class="col-sm-6 col-xs-12">
			{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST']) !!}
				
				{{ Form::bsSelect('municipio_id', $controller->municipios() ,'', [
					'label' 		=> 'Municipio',
					'required' 		=> 'required',
					'class_cont' 	=> ''
				]) }}

				{{ Form::bsText('nombre', '', [
					'label' 		=> 'Nombre',
					'placeholder' 	=> 'Nombre',
					'required' 		=> 'required',
					'class_cont' 	=> ''
				]) }}
			{!! Form::close() !!}
		</div>
	</div>
@endsection

@push('css')
<style type="text/css" media="screen">
	#arbol{
		min-height: 200px;
	}
</style>
@endpush