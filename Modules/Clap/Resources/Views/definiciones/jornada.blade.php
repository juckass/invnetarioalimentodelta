@extends('admin::layouts.default')
@section('content')
	@include('admin::partials.botonera')

	{{ Form::bsModalBusqueda([
		'Jornadas' => '50',
		'Fecha de Inicio' => '25',
		'Fecha de Finalización' => '25',

	]) }}

	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Clap</span><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Definiciones</span><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Jornadas</span>
		</li>
	</ul>

	<div class="row">

			{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST']) !!}
				
				{{ Form::bsText('inicio', '', [
					'label' 		=> 'Fecha de Inicio',
					'placeholder' 	=> 'DD/MM/YYY',
					'required' 		=> 'required',
					'class_cont' 	=> 'col-md-4'
				]) }}

				{{ Form::bsText('fin', '', [
					'label' 		=> 'Fecha de Finalización',
					'placeholder' 	=> 'DD/MM/YYY',
					'class_cont' 	=> 'col-md-4',
				]) }}

			{!! Form::close() !!}

	</div>
@endsection
