@extends('admin::layouts.default')
@section('content')
	@include('admin::partials.botonera')

	{{ Form::bsModalBusqueda([
		'Clap' => '30',
		'Código' => '25',
		'Municipio' => '10',
		'Parroquia' => '15',
		'Sector' => '20'	
	],[
		'titulo' => 'Buscar Claps.'
	]) }}

	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Clap</span>
		</li>
	</ul>

	<div class="row">
		{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST']) !!}
			<div class="col-xs-12 subtitulo">
				<h4>Ubicaci&oacute;n</h4>
			</div>

			{{ Form::bsSelect('municipio_id', $controller->municipios() ,'', [
				'label' 		=> 'Municipio',
				'required' 		=> 'required',
				'class_cont' 	=> 'col-md-4 col-sm-6'
			]) }}

			{{ Form::bsSelect('parroquia_id', [],'', [
				'label' 		=> 'Parroquia',
				'required' 		=> 'required',
				'class_cont' 	=> 'col-md-4 col-sm-6'
			]) }}

			{{ Form::bsSelect('sector_id', [],'', [
				'label' 		=> 'Sector',
				'class_cont' 	=> 'col-md-4 col-sm-6'
			]) }}
			
			<div class="col-xs-12 subtitulo">
				<h4>Datos del Clap </h4>
			</div>

			{{ Form::bsText('nombre', '', [
				'label' 		=> 'Nombre del Clap',
				'placeholder' 	=> 'Nombre',
				'required' 		=> 'required'
			]) }}

			{{ Form::bsText('codigo', '', [
				'label' 		=> 'Código del Clap',
				'placeholder' 	=> 'Ej: CLAP-BOL-000000-00000'
			]) }}

			{{ Form::bsText('consejo_comunal', '', [
				'label' 		=> 'Consejo Comunal',
				'placeholder' 	=> 'Nombre del Consejo Comunal'
			]) }}

			{{ Form::bsNumber('familia', '', [
				'label' 		=> 'Cantidad de familias',
				'placeholder'	=> '0',
				'required' 		=> 'required'
			]) }}

			<div class="col-xs-12 subtitulo">
				<h4>Informaci&oacute;n de las Personas de Contacto</h4>
			</div>

			<div class="col-xs-12">
				<table id="tablaResponsable" class="table table-bordered table-striped table-condensed flip-content">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Telefono</th>
							<th style="width: 80px;">
								<div class="btn-group btn-group-sm btn-group-solid">
									<button id="removerTodo" type="button" class="btn red"><i class="fa fa-remove"></i></button>
									<button id="agregar" type="button" class="btn blue"><i class="fa fa-plus"></i></button>
								</div>
							</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		{!! Form::close() !!}
		
		<div class="col-xs-12 subtitulo">
			<h4>Cargar Archivo <small>Excel (.csv, .xls, .xlsx, .ods)</small></h4>
			
			{!! Form::open(['id' => 'carga', 'name' => 'carga', 'method' => 'POST']) !!}
				<input id="subir" name="subir" type="file"/>
				<button id="subir_btn" type="button" class="btn btn-primary mt-ladda-btn ladda-button" data-style="expand-right">
					<span class="ladda-label">
						<i class="icon-arrow-right"></i> Carga archivo Excel
					</span>
				</button>
			{!! Form::close() !!}
		</div>
	</div>
@endsection
@push('css')
<style type="text/css">
.subtitulo h4 {
	border-bottom: 1px solid #707070;
	font-size: 18px;
	padding-bottom: 10px;
}

#subir {
	display: none;
}

#tablaResponsable{
    background-color: #fff;
}

#tablaResponsable .btn {
    margin-left: -1px;
    margin-right: 0;
}
</style>
@endpush

@push('js')
<script type="text/x-tmpl" id="tmpl-contacto">
{% for (var i = 0; i < o.responsable.length; i++) { %}
	<tr>
		<td><input name="nombre_responsable[]" class="form-control" placeholder="Nombre Completo" type="text" value="{%=o.responsable[i].nombre%}"></td>
		<td><input name="telefono_responsable[]" class="form-control telefono" placeholder="0###-###-####" type="text" value="{%=o.responsable[i].telefono%}"></td>
		<td>
			<button type="button" class="btn red"><i class="fa fa-remove"></i></button>
		</td>
	</tr>
{% } %}
</script>
@endpush