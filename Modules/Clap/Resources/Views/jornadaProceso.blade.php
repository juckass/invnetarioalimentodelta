@extends('admin::layouts.default')
@section('content')
	@include('admin::partials.botonera')

	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Clap</span>
		</li>
	</ul>

	<div class="row">
		{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST']) !!}
			{{ Form::bsText('fecha', '', [
				'label' 		=> 'Fecha',
				'placeholder' 	=> 'DD/MM/YYY',
				'required' 		=> 'required',
				'class_cont' 	=> 'col-md-4 col-sm-6'
			]) }}

			<div class="col-xs-12 subtitulo">
				<h4>Ubicaci&oacute;n del Clap</h4>
			</div>

			{{ Form::bsSelect('municipio_id', $controller->municipios() ,'', [
				'label' 		=> 'Municipio',
				'required' 		=> 'required',
				'class_cont' 	=> 'col-md-4 col-sm-6'
			]) }}

			{{ Form::bsSelect('parroquia_id', [],'', [
				'label' 		=> 'Parroquia',
				'required' 		=> 'required',
				'class_cont' 	=> 'col-md-4 col-sm-6'
			]) }}

			{{ Form::bsSelect('sector_id', [],'', [
				'label' 		=> 'Sector',
				'class_cont' 	=> 'col-md-4 col-sm-6'
			]) }}

			{{ Form::bsSelect('estatus', [
				's'=> 'Atendido',
				'n'=> 'No atendido',
				],'', [
				'label' 		=> 'Estatus',
				'class_cont' 	=> 'col-md-4 col-sm-6'
			]) }}
			
			<div class="col-xs-12 subtitulo">
				<h4>Datos del Clap </h4>
			</div>

			<div class="col-md-12 table-responsive">
				<table id="tabla" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th width="30%"><input type="checkbox" id="checkall" title="Seleccionar todo"> Nombre del Clap</th> 
							<th width="20%">Parroquia</th>
							<th width="25%">Familias a atender</th>
							<th width="25%">Toneladas de alimentos</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
			</div>
		{!! Form::close() !!}
			
	</div>
@endsection

@push('css')
<style type="text/css">
#tabla tbody tr:hover {
	cursor: pointer;
}

#tabla tbody tr.color-view:hover{
	cursor: not-allowed;
}
#tabla tbody tr input {
	width: 100%;
} 
.subtitulo h4 {
	border-bottom: 1px solid #707070;
	font-size: 18px;
	padding-bottom: 10px;
}

.check{
	display: none;
}

#subir {
	display: none;
}

</style>
@endpush

@push('js')
<script type="text/x-tmpl" id="tmpl-tabla">
	{% for (var i = 0; i < o.length; i++) { %}
	<tr class="
	{% if (o[i].atendido !== null) { %}
	color-view bg-green-seagreen bg-font-green-seagreen bold uppercase
	{% } %}
	">
		<td>
			{% if (o[i].atendido === null) { %}
			<input class="check" name="clap_id[{%=o[i].id%}]" value="{%=o[i].id%}" type="checkbox" />
			{% } %}
			{%=o[i].clap%}
		</td>
		<td>{%=o[i].parroquia%}</td>
		<td><input name="familia[{%=o[i].id%}]" class="form-control" value="0" type="text" disabled="disabled" readonly="readonly" /></td>
		<td><input name="cantidad[{%=o[i].id%}]" class="form-control" value="0" type="text" disabled="disabled" readonly="readonly" /></td>
	</tr>
	{% } %}
</script>
@endpush