<?php namespace Modules\Clap\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ClapDatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call(estadosSeeder::class);
		$this->call(ciudadesSeeder::class);
		$this->call(municipiosSeeder::class);
		$this->call(parroquiasSeeder::class);

		Model::reguard();
	}

}