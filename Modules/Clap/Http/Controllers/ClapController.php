<?php 

namespace Modules\Clap\Http\Controllers;

//Dependencias
use DB;
use Modules\Clap\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Excel;

//Request
use Modules\Clap\Http\Requests\clapRequest;
use Illuminate\Http\Request;
//modelo(s)
use Modules\Clap\Model\clap;
use Modules\Clap\Model\clap_contacto;
use Modules\Clap\Model\Municipio;
use Modules\Clap\Model\Parroquia;
use Modules\Clap\Model\sector;

class ClapController extends Controller{
	public $titulo = 'Clap';
	
	public $librerias = [
		'maskedinput',
		'datatables',
		'maskedinput',
		'ladda',
		'template'
	];

	public $js=[
		'clap'
	];
	public $css=[
		'clap'
	];

	public function index()
	{
		
		return $this->view('clap::clap');
	}

	public function buscar(Request $request, $id = 0){
		$clap = clap::find($id);

		if ($clap){
			$municipio = Parroquia::select('municipio_id')
				->where('id', $clap->parroquia_id)
				->first()
				->municipio_id;
			
			$parroquia_id = Parroquia::where('municipio_id', $municipio)
				->pluck('nombre','id')
				->put('_', $clap->parroquia_id);

			$sector_id = sector::where('parroquia_id', $clap->parroquia_id)
				->pluck('nombre','id')
				->put('_', $clap->sector_id);

			$responsable = clap_contacto::where('clap_id', $clap->id)->get();

			$respuesta = array_merge($clap->toArray(), [
				'responsable' => $responsable,
				'municipio_id' => $municipio,
				'parroquia_id' => $parroquia_id,
				'sector_id' => $sector_id,
				's' => 's',
				'msj' => trans('controller.buscar')
			]);

			return $respuesta;
		}
		
		return trans('controller.nobuscar');
	}

	protected function data($request){
		$data = $request->all();

		$data['slug'] = str_slug($data['nombre'], '-');

		if ($data['codigo'] == ''){
			$msj = 'Guardado Satisfactoriamente con el Codigo Vacio';
			$data['codigo'] = NULL;
		}

		return $data;
	}

	public function guardar(clapRequest $request, $id = 0){
		DB::beginTransaction();

		try{
			$data = $this->data($request);

			$clap = $id == 0 ? new clap() : clap::find($id);

            $clap->fill($data);
            $clap->save();

			$this->guardar_contacto($clap->id, $request);
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}

		DB::commit();
		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	protected function guardar_contacto($id, $request){
		foreach ($request->nombre_responsable as $key => $value) {

			$contacto = clap_contacto::firstOrNew([
				'clap_id' => $id,
				'telefono' => $request->telefono_responsable[$key]
			])->fill([
				'nombre' => $request->nombre_responsable[$key]
			])->save();
		}
	}

	public function eliminar(Request $request, $id = 0){
		DB::beginTransaction();
		try{
			$clap = clap::destroy($id);
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}

		DB::commit();
		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

	public function datatable(){
		$sql = clap::select(['clap.id as id','clap.nombre as clap','clap.codigo as codigo', 'parroquia.nombre as parroquia', 'municipio.nombre as municipio', 'sector.nombre as sector'])
			->leftJoin('sector', 'clap.sector_id', '=', 'sector.id')
			->leftJoin('parroquia', 'clap.parroquia_id', '=', 'parroquia.id')
			->leftJoin('municipio', 'parroquia.municipio_id', '=', 'municipio.id');

	    return Datatables::of($sql)->setRowId('id')->make(true);
	}

	public function municipios(){
		return Municipio::where('estados_id','=', 9)->get()->pluck('nombre', 'id');
	}

	public function parroquias(Request $request){
		$sql = Parroquia::where('municipio_id', $request->id)
					->pluck('nombre','id')
					->toArray();

		$salida = ['s' => 'n' , 'msj'=> 'El Municipio no contiene Parroquias'];
		
		if($sql){
			$salida = ['s' => 's' , 'msj'=> 'Parroquias encontradas', 'parroquia_id'=> $sql];
		}				
		
		return $salida;
	}

	public function sectores(Request $request){
		$sql = sector::where('parroquia_id', $request->id)
					->pluck('nombre','id')
					->toArray();

		$salida = ['s' => 'n' , 'msj'=> 'La Parroquia no Contiene Sectores'];
		
		if($sql){
			$salida = ['s' => 's' , 'msj'=> 'Sectores encontrados', 'sector_id'=> $sql];
		}				
		
		return $salida;
	}

	public function archivo2(Request $request){
		$mimes = [
			'text/csv',
			'application/vnd.ms-excel',
			'application/vnd.oasis.opendocument.spreadsheet',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
		];

		$ruta = public_path('archivos/');
		$archivo = $request->file('subir');

		$mime = $archivo->getClientMimeType();

		if (!in_array($mime, $mimes)) {
			return "Error de Validación, la extension el archivo cargado debe ser: xls,xlsx,csv";
		}

        do {
			$nombre_archivo = str_random(20) . '.' . $archivo->getClientOriginalExtension();
		} while (is_file($ruta . $nombre_archivo));

    
        $archivo->move($ruta, $nombre_archivo);
		
		chmod($ruta . $nombre_archivo, 0777);

		$xls = $ruta . $nombre_archivo;

		$datos = Excel::selectSheetsByIndex(0)->load($xls)->get();

		$municipios = [];
		foreach (Municipio::pluck('nombre', 'id') as $municipios_id => $municipio) {
			$slug = str_slug($municipio, '-');
			$municipios[$slug] = intval($municipios_id);
		}

		$parroquias = [];
		foreach (Parroquia::pluck('nombre', 'id') as $parroquias_id => $parroquia) {
			$slug = str_slug($parroquia, '-');
			$parroquias[$slug] = intval($parroquias_id);
		}

		try {
			DB::beginTransaction();
			foreach($datos as $dato){
				$municipio_slug = str_replace('nn', 'n', str_slug($dato->municipio, '-'));
				$parroquia_slug = str_replace('nn', 'n', str_slug($dato->parroquia, '-'));

				$dato->sector 	= trim($dato->sector);
				$sector_slug    = str_replace('nn', 'n', str_slug($dato->sector, '-'));

				if ($dato->sector == ''){
					$sector_slug = str_replace('nn', 'n', str_slug($dato->comunidad, '-'));
				}

				/*
				if (!isset($parroquias[$parroquia_slug])){
					$parro = Parroquia::create([
						'nombre' => ucwords(strtolower($dato->parroquia)),
						'municipio_id' => $municipios[$municipio_slug]
					]);

					$parroquias[$parroquia_slug] = $parro->id;
				}
				*/

				$sector = sector::firstOrNew([
					'slug' => $sector_slug,
					'parroquia_id' => $parroquias[$parroquia_slug]
				]);

				$sector->nombre = ucwords(strtolower($dato->comunidad));
				$sector->save();
				
				$dato->codigo = trim($dato->codigo);

				$dato->nombre = trim($dato->nombre);
				$dato->consejo_comunal = trim($dato->consejo_comunal);
				if ($dato->nombre == '' && $dato->consejo_comunal == ''){
					continue;
				}

				if ($dato->nombre == ''){
					$dato->nombre = $dato->consejo_comunal;
				}

				$clapData = [
					'codigo' => $dato->codigo,
					'parroquia_id' => $parroquias[$parroquia_slug],
					'sector_id' => $sector->id,
					'nombre' => $dato->nombre,
					'familia' => $dato->familias,
					'slug' => str_slug($dato->nombre, '-'),
					'consejo_comunal' => $dato->consejo_comunal
				];

				if ($clapData['familia'] <= 0){
					$clapData['familia'] = 0;
				}

				if ($clapData['codigo'] != ''){
					$clap = clap::firstOrNew([
						'codigo' => $clapData['codigo']
					]);
				}else{
					unset($clapData['codigo']);
					$clap = clap::firstOrNew([
						'parroquia_id' => $clapData['parroquia_id'],
						'slug' => $clapData['slug']
					]);
				}

				$clap->fill($clapData)->save();

				$dato->responsable = trim($dato->responsable);

				$numeros = trim($dato->telefono_responsable);
				

				if ($dato->responsable != '' || $numeros != ''){
					$numeros = str_replace(' ', '', $numeros);

					if (preg_match('/^\d{3,4}\-?\d{6,9}\-\d{3,4}\-?\d{6,9}/', $numeros)){
						$numeros = explode('-', $numeros);
					}else{
						$numeros = [$numeros];
					}

					foreach ($numeros as $numero) {
						$numero = trim($numero);
						$numero = ltrim($numero, '0');
						$numero = str_replace(['-', '_', '.', ' '], '', $numero);

						$numero = preg_replace('/^(\d{3})(\d{6,9})$/', '0$1-$2', $numero);

						clap_contacto::firstOrNew([
							'clap_id' => $clap->id,
							'telefono' => $numero
						])->fill([
							'nombre' => ucwords(strtolower($dato->responsable))
						])->save();
					}
				}
			}
			DB::commit();
		} catch (Exception $e) {
			dd($e);
			DB::rollback();
		}

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function archivo(Request $request){
		$mimes = [
			'text/csv',
			'application/vnd.ms-excel',
			'application/vnd.oasis.opendocument.spreadsheet',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
		];

		$ruta = public_path('archivos/');
		$archivo = $request->file('subir');

		$mime = $archivo->getClientMimeType();

		if (!in_array($mime, $mimes)) {
			return "Error de Validación, la extension el archivo cargado debe ser: xls,xlsx,csv";
		}

        do {
			$nombre_archivo = str_random(20) . '.' . $archivo->getClientOriginalExtension();
		} while (is_file($ruta . $nombre_archivo));

    
        $archivo->move($ruta, $nombre_archivo);
		
		chmod($ruta . $nombre_archivo, 0777);

		$xls = $ruta . $nombre_archivo;

		$datos = Excel::selectSheetsByIndex(0)->load($xls)->get();

		$municipios = [];
		foreach (Municipio::pluck('nombre', 'id') as $municipios_id => $municipio) {
			$slug = str_slug($municipio, '-');
			$municipios[$slug] = intval($municipios_id);
		}

		$parroquias = [];
		foreach (Parroquia::pluck('nombre', 'id') as $parroquias_id => $parroquia) {
			$slug = str_slug($parroquia, '-');
			$parroquias[$slug] = intval($parroquias_id);
		}

		try {
			DB::beginTransaction();
			$inf = [];
			foreach($datos as $dato){
				$codigo = $dato->codigo_clap;

			$clap_id = clap::select(['id'])->where('codigo', $codigo)->get()->toArray();
			
			if(count($clap_id)== 0){
				continue;
			}

			

			/*$inf[] = [
				'codigo' => $dato->codigo_clap,
				'id_clap' => $clap_id[0]['id'],
				'cedula' => intval($dato->cedula),
				'telefono' => strval($dato->telefono)
			];*/

			
			$contacto =  clap_contacto::firstOrNew([
				'clap_id' => $clap_id[0]['id']
			])
			->fill([
				'cedula' => intval($dato->cedula),
				'telefono' => strval($dato->telefono),
				'nombre' =>	''
			])
			->save();


			}
			//dd($inf);
			DB::commit();
		} catch (Exception $e) {
			dd($e);
			DB::rollback();
		}

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}
	public function buscarcedula($cedula){
		$buscar = new Cne();
		$respuesta = $buscar->buscar($cedula);
		
		if ($respuesta){
			return $respuesta;
		}

		return 'No existe esta cedula';
	}

}