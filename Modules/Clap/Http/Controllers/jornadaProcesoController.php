<?php 

namespace Modules\Clap\Http\Controllers;

//Dependencias
use Modules\Clap\Http\Controllers\Controller;
use DB;
use Yajra\Datatables\Datatables;
use carbon\Carbon;

//Request
use Modules\Clap\Http\Requests\jornadaProcesoRequest;
use Illuminate\Http\Request;
//modelo(s)
use Modules\Clap\Model\jornadaProceso;
use Modules\Clap\Model\jornada;
use Modules\Clap\Model\Municipio;
use Modules\Clap\Model\Parroquia;
use Modules\Clap\Model\sector;
use Modules\Clap\Model\clap;

class jornadaProcesoController extends Controller{

	public $titulo = 'Jornadas';
	
	public $librerias =[
		'datatables',
		'jquery-ui',
		'jquery-ui-timepicker',
		'template'
	];

	public $js = [
		'jornadaProceso'
	];


	public function index()
	{	
		return $this->view('clap::jornadaProceso');
	}

	public function buscar(Request $request, $id = 0){
		
		$ultJornada = jornada::select('inicio')->orderBy('inicio', 'desc')->first()->toArray();
		
		
		$query = clap::select([
			'clap.id as id',
			'clap.nombre as clap',
			'clap.codigo',
			'jornada_proceso.id as atendido',
			'municipio.nombre as municipio',
			'parroquia.nombre as parroquia',
			])
			->leftJoin('jornada_proceso','jornada_proceso.clap_id', '=', 'clap.id')
			->leftJoin('parroquia', 'clap.parroquia_id', '=', 'parroquia.id')
			->leftJoin('municipio', 'parroquia.municipio_id', '=', 'municipio.id')
			->where('municipio_id', '=', $request->municipio_id)
			->orderBy('clap.nombre', 'asc')
			->groupBy([ 'clap.id', 'clap.nombre', 'clap.codigo', 'municipio.nombre', 'parroquia.nombre' ]);

		if ($request->estatus == 's'){
			$query
			->whereNotNull('jornada_proceso.id')
			->whereBetween('jornada_proceso.fecha', [
				Carbon::createFromFormat('d/m/Y', $ultJornada['inicio'])->format('Y-m-d'),
				Carbon::now()->format('Y-m-d')
			]);
		}elseif ($request->estatus == 'n'){
			$query->whereNull('jornada_proceso.id');
		}

		if ($request->parroquia_id != ''){
			$query->where('parroquia.id', $request->parroquia_id);
		}
		if ($request->sector_id != ''){
			$query->where('clap.sector_id', $request->sector_id);
		}

		$data = $query->get()->toArray();
		return $data;
		
		if ($rs){
			$respuesta = array_merge($rs->toArray(), [
				's' => 's', 
				'msj' => trans('controller.buscar')
			]);

			return $respuesta;
		}
		
		return trans('controller.nobuscar');
	}

	public function guardar(jornadaProcesoRequest $request){
		try{
			$fecha 		= $request->fecha;
			$claps 		= $request->clap_id;
			$familias 	= $request->familia;
			$cantidades = $request->cantidad;

			foreach ($claps as $id => $clap) {
				
				jornadaProceso::create([
					'clap_id' => $clap,
					'cantidad' => $cantidades[$id],
					'familia' => $familias[$id],
					'fecha' => $fecha,
				]);
			}

		}catch(Exception $e){
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function actualizar(jornadaRequest $request, $id = 0){
		try{
			$rs = jornada::find($id)->update($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function eliminar(Request $request, $id = 0){
		try{
			$rs = jornada::destroy($id);
		}catch(Exception $e){
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

	public function datatable(){
		
		$sql = jornada::select(['jornada.id as id','jornada.nombre as jornada', 'parroquia.nombre as parroquia', 'municipio.nombre as municipio'])
			->leftJoin('parroquia', 'jornada.parroquia_id', '=', 'parroquia.id')
			->leftJoin('municipio', 'parroquia.municipio_id', '=', 'municipio.id');


	    return Datatables::of($sql)->setRowId('id')->make(true);
	}

	public function municipios(){

		return Municipio::where('estados_id', 9)->pluck('nombre', 'id');
	}

	public function parroquias(Request $request){
		$sql = Parroquia::select('id','nombre')
					->where('municipio_id', $request->id)
					->pluck('nombre','id');

		$salida =['s' => 'n' , 'msj'=> 'El Municipio no contiene Parroquias'];
		
		if($sql){

			$salida =['s' => 's' , 'msj'=> 'Parroquias encontradas', 'parroquia_id'=> $sql];
		}				
		
		return $salida;
	}

	public function sectores(Request $request){
		$sql = sector::select('id','nombre')
					->where('parroquia_id', $request->id)
					->pluck('nombre','id');

		$salida =['s' => 'n' , 'msj'=> 'La Parroquia no Contiene Sectores'];
		
		if($sql){

			$salida =['s' => 's' , 'msj'=> 'Sectores encontradas', 'sector_id'=> $sql];
		}				
		
		return $salida;
	}
}