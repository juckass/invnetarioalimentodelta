<?php 

namespace Modules\Clap\Http\Controllers;

//Dependencias
use Modules\Clap\Http\Controllers\Controller;
use DB;
use Yajra\Datatables\Datatables;

//Request
use Modules\Clap\Http\Requests\jornadaRequest;
use Illuminate\Http\Request;
//modelo(s)
use Modules\Clap\Model\jornada;

class jornadaController extends Controller{

	public $titulo = 'Definición de Jornadas';
	
	public $librerias =[
		'datatables',
		'jquery-ui',
		'jquery-ui-timepicker',
	];

	public $js = [
		'jornada'
	];

	public function index()
	{
		return $this->view('clap::definiciones.jornada', $this->_app());
	}

	public function buscar(Request $request, $id = 0){
		$rs = jornada::find($id);
		
		if ($rs){
			$respuesta = array_merge($rs->toArray(), [
				's' => 's', 
				'msj' => trans('controller.buscar')
			]);

			return $respuesta;
		}
		
		return trans('controller.nobuscar');
	}

	public function guardar(jornadaRequest $request){
		try{
			$rs = jornada::create($request->all());
		}catch(Exception $e){
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function actualizar(jornadaRequest $request, $id = 0){
		try{
			$rs = jornada::find($id)->update($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function eliminar(Request $request, $id = 0){
		try{
			$rs = jornada::destroy($id);
		}catch(Exception $e){
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

	public function datatable(){
		$sql = jornada::select([
			'id', 'inicio','fin'
		]);

	    return Datatables::of($sql)->setRowId('id')->make(true);
	}
}