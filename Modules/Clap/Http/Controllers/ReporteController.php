<?php 

namespace Modules\Clap\Http\Controllers;

use DB;
use Carbon\Carbon;
use Modules\Clap\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Modules\Clap\Model\clap;

class ReporteController extends Controller {
	protected $titulo = 'Reporte';

	public $librerias = [
		'jquery-ui',
		'jquery-ui-timepicker',
	];

	protected $meses = [
		'Enero',
		'Febrero',
		'Marzo',
		'Abril',
		'Mayo',
		'Junio',
		'Julio',
		'Agosto',
		'Septiembre',
		'Octubre',
		'Noviembre',
		'Diciembre'
	];

	public $js=[
		'reportes/clap'
	];
	public $css=[
		'reportes/clap'
	];

	public function index()
	{
		$anos = [2016 => 2016];
		$fechadesde = DB::table('jornada_proceso')->select(DB::raw('min(fecha) as fecha'))->first();
		$fechahasta = DB::table('jornada_proceso')->select(DB::raw('max(fecha) as fecha'))->first();

		return $this->view('clap::reporte', [
			'meses' => $this->meses,
			'anos' => $anos,
			'fechadesde' => Carbon::parse($fechadesde->fecha)->format('d/m/Y'),
			'fechahasta' => Carbon::parse($fechahasta->fecha)->format('d/m/Y')
		]);
	}
	
	public function imprimir(Request $request){
		if ($request->isMethod('get')) {
		    return $this->data_imprimir($request, true);
		}

		$pdf = new \mPDF('c', 'A4-L');
		$pdf->WriteHTML($this->data_imprimir($request, false));
		$pdf->Output();
	}

	protected function data_imprimir($request, $html){
		$tipoReporte = $request->tipoReporte;

		$meses = [
			'January', 'February', 'March', 'April', 'May', 'June', 'July', 
			'August', 'September', 'October', 'November', 'December'
		];

		$reporte = ['html' => $html];

		if ($tipoReporte == 'municipios'){
			$municipio_id = 0;
			$region = $reporte['region'] = 'municipio';
		}else{
			$municipio_id = $request->municipio_id;
			$region = $reporte['region'] = 'parroquia';
		}

		if ($request->tipo == 'meses'){
			$fecha = "01/" . ($request->mes + 1) . "/{$request->ano}";

			$fechaInicio = Carbon::createFromFormat('d/m/Y', $fecha)->format("Y-m-d"); 
			$fechaFin    = Carbon::parse('last day of ' . $meses[$request->mes] . ' ' . $request->ano)->format('Y-m-d');

			$reporte['fechaInicio'] = $fecha;
			$reporte['fechaFin'] = Carbon::parse('last day of ' . $meses[$request->mes] . ' ' . $request->ano)->format('d/m/Y');
		}else{
			$fechaInicio = Carbon::createFromFormat('d/m/Y', $request->fechadesde)->format("Y-m-d"); 
			$fechaFin    = Carbon::createFromFormat('d/m/Y', $request->fechahasta)->format("Y-m-d");

			$reporte['fechaInicio'] = $request->fechadesde;
			$reporte['fechaFin'] = $request->fechahasta;
		}

		/* Clap en total */
		$totalClapsSql = DB::table('clap')
			->leftJoin('parroquia', 'parroquia.id', '=', 'clap.parroquia_id')
			->leftJoin('municipio', 'municipio.id', '=', 'parroquia.municipio_id')
			->where('municipio.estados_id','=', 9)
			->groupBy([$region.'.id', $region.'.nombre']);

		/* filtros */

		if ($request->tipo_clap == 'r'){
			$totalClapsSql->whereNotNull('clap.codigo');
		}elseif ($request->tipo_clap == 'c'){
			$totalClapsSql->whereNull('clap.codigo');
		}
		/* Fin de filtros */

		if ($municipio_id != 0){
			$totalClapsSql->where('municipio.id', $municipio_id);
		}

		$_totalClapsSql = clone $totalClapsSql;
		$totalClapRegistrados = $_totalClapsSql
			->whereNotNull('codigo')
			->pluck(DB::raw('count(*) as cantidad'), $region.'.id');

		$_totalClapsSql = clone $totalClapsSql;
		$totalClapConformados = $_totalClapsSql
			//->whereNull('codigo')
			->pluck(DB::raw('count(*) as cantidad'), $region.'.id');
		
		$reporte['totalClapRegistrados'] = $totalClapRegistrados;
		$reporte['totalClapConformados'] = $totalClapConformados;
		/* Clap atendidos */
		$totalClapsSql = DB::table('jornada_proceso')
			->join('clap', 'clap.id', '=', 'jornada_proceso.clap_id')
			->join('parroquia', 'parroquia.id', '=', 'clap.parroquia_id')
			->join('municipio', 'municipio.id', '=', 'parroquia.municipio_id')
			->whereBetween('fecha', [$fechaInicio, $fechaFin])
			->groupBy(['clap.id', $region.'.id']);

		/* filtros */
		if ($request->tipo_clap == 'r'){
			$totalClapsSql->whereNotNull('clap.codigo');
		}elseif ($request->tipo_clap == 'c'){
			$totalClapsSql->whereNull('clap.codigo');
		}

		/* Fin de filtros */

		if ($municipio_id != 0){
			$totalClapsSql->where('municipio.id', $municipio_id);
		}

		$totalAtendidos = $totalClapsSql->pluck(DB::raw('count(*) as cantidad'), $region.'.id');

		$reporte['totalAtendidos'] = $totalAtendidos;
		

		/* datos de la jornada */
		
		$regiones = [];

		$regionesSql = DB::table($region)->whereNull('deleted_at');

		if ($municipio_id != 0){
			$regionesSql->where('municipio_id', $municipio_id);
		}
		if ($region == 'municipio'){
			
			$regiones = $regionesSql->where('estados_id',9)->pluck('nombre', 'id');
		}else{
			$regiones = $regionesSql->pluck('nombre', 'id');
		}
		
		foreach ($regiones as $id => $valor) {
			$regiones[$id] = new \stdClass();

			$regiones[$id]->id = $id;
			$regiones[$id]->nombre = $valor;
			$regiones[$id]->porcentaje = 0;
			$regiones[$id]->familia = 0;
			$regiones[$id]->cantidad = 0;
		}

		$filtros = [$region.'.id', $region.'.nombre'];
		if ($region == 'municipio'){
			$filtros[] = $region.'.poblacion';
		}

		$jornadas = DB::table('jornada_proceso')
			->select([
				$region.'.id',
				DB::raw('(((sum(jornada_proceso.familia) * 4) * 100) / municipio.poblacion) as porcentaje'),
				DB::raw('sum(jornada_proceso.familia) as familia'),
				DB::raw('sum(jornada_proceso.cantidad) as cantidad')
			])
			->join('clap', 'clap.id', '=', 'jornada_proceso.clap_id')
			->join('parroquia', 'parroquia.id', '=', 'clap.parroquia_id')
			->join('municipio', 'municipio.id', '=', 'parroquia.municipio_id')
			->whereBetween('jornada_proceso.fecha', [$fechaInicio, $fechaFin])
			->groupBy(array_merge($filtros, ['municipio.poblacion']))
			->orderBy($region.'.id');
		
		/* Filtros*/
		if ($request->tipo_clap == 'r'){
			$jornadas->whereNotNull('clap.codigo');
		}elseif ($request->tipo_clap == 'c'){
			$jornadas->whereNull('clap.codigo');
		}

		if ($request->estatus == 's'){
			$jornadas->whereNotNull('jornada_proceso.id');
		}elseif ($request->estatus == 'n'){
			$jornadas->whereNull('jornada_proceso.id');
		}
		/*Fin de filtros*/

		if ($municipio_id != 0){
			$jornadas->where('municipio.id', $municipio_id);
		}

		foreach ($jornadas->get() as $jornada) {
			$regiones[$jornada->id]->porcentaje = $jornada->porcentaje;
			$regiones[$jornada->id]->familia = $jornada->familia;
			$regiones[$jornada->id]->cantidad = $jornada->cantidad;
		}

		$reporte['jornadas'] = $regiones;

		return view('clap::pdf.reporte', $reporte)->render();
	}

	public function imprimirParroquia(Request $request){
		$tipoReporte = $request->tipoReporte;

		$meses = [
			'January', 'February', 'March', 'April', 'May', 'June', 'July', 
			'August', 'September', 'October', 'November', 'December'
		];

		$reporte = ['html' => true];
		$region = $reporte['region'] = 'clap';

		$municipio_id = $request->municipio_id;
		$parroquia_id = $request->parroquia_id;

		if ($request->tipo == 'meses'){
			$fecha = "01/" . ($request->mes + 1) . "/{$request->ano}";

			$fechaInicio = Carbon::createFromFormat('d/m/Y', $fecha)->format("Y-m-d"); 
			$fechaFin    = Carbon::parse('last day of ' . $meses[$request->mes] . ' ' . $request->ano)->format('Y-m-d');

			$reporte['fechaInicio'] = $fecha;
			$reporte['fechaFin'] = Carbon::parse('last day of ' . $meses[$request->mes] . ' ' . $request->ano)->format('d/m/Y');
		}else{
			$fechaInicio = Carbon::createFromFormat('d/m/Y', $request->fechadesde)->format("Y-m-d"); 
			$fechaFin    = Carbon::createFromFormat('d/m/Y', $request->fechahasta)->format("Y-m-d");

			$reporte['fechaInicio'] = $request->fechadesde;
			$reporte['fechaFin'] = $request->fechahasta;
		}

		/* Claps */
		$claps = clap::where('clap.parroquia_id', $parroquia_id);
			
			//->whereBetween('fecha', [$fechaInicio, $fechaFin])
			//->orderBy('clap.nombre');

		if ($request->tipo_clap == 'r'){
			$claps->whereNotNull('clap.codigo');
		}elseif ($request->tipo_clap == 'c'){
			$claps->whereNull('clap.codigo');
		}

		
		$reporte['claps'] = $claps->get();

		$municipio = DB::table('municipio')->select('nombre')->where('id', $municipio_id)->first();
		$parroquia = DB::table('parroquia')->select('nombre')->where('id', $parroquia_id)->first();
		
		$reporte['municipio'] = $municipio->nombre;
		$reporte['parroquia'] = $parroquia->nombre;

		/* jornadas */
		$jornadas = DB::table('jornada_proceso')
			/*->select([
				'clap.id',
				DB::raw('sum(jornada_proceso.familia) as familia')
			])*/
			->join('clap', 'clap.id', '=', 'jornada_proceso.clap_id')
			->where('clap.parroquia_id', $parroquia_id)
			->whereBetween('jornada_proceso.fecha', [$fechaInicio, $fechaFin])
			->groupBy('clap.id');
			

		/* Filtros*/
		if ($request->tipo_clap == 'r'){
			$jornadas->whereNotNull('clap.codigo');
		}elseif ($request->tipo_clap == 'c'){
			$jornadas->whereNull('clap.codigo');
		}

		if ($request->estatus == 's'){
			$jornadas->whereNotNull('jornada_proceso.id');
		}elseif ($request->estatus == 'n'){
			$jornadas->whereNull('jornada_proceso.id');
		}
		/*Fin de filtros*/
		
		$reporte['jornadas'] = $jornadas->pluck(DB::raw('sum(jornada_proceso.familia) as familia'), 'clap.id');

		return view('clap::pdf.reporteParroquia', $reporte)->render();
	}

	public function imprimirClap(Request $request){
		$municipio_id = $request->municipio_id;
		$parroquia_id = $request->parroquia_id;

		$municipio = DB::table('municipio')->select('nombre')->where('id', $municipio_id)->first()->nombre;
		$parroquia = DB::table('parroquia')->select('nombre')->where('id', $parroquia_id)->first()->nombre;

		$clap = DB::table('clap')
			->where('clap.id', $request->clap_id)
			->first();

		$personas = DB::table('clap_contacto')
			->where('clap_id', $request->clap_id)
			->get();


		 $html = view('clap::pdf.reporteClap', [
			'municipio' => $municipio,
			'parroquia' => $parroquia,
			'clap' => $clap,
			'personas' => $personas,
			'html' => true
		])->render();

        if ($request->has('html')) {
            return $html;
        }

        $pdf = new \mPDF('c', 'A4-L');
        //$pdf = PDF::loadHTML($html);
        $pdf->WriteHTML($html);
        return $pdf->download('reporte.pdf');
	}
}  