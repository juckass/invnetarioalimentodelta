<?php 

namespace Modules\Clap\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

abstract class Controller extends BaseController {
	protected $titulo = 'Clap';
	public $prefijo_ruta = '';

	public $app = 'alimentos';

	protected $patch_js = [
		'public/js',
		'public/plugins',
		'Modules/Clap/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'Modules/Clap/Assets/css',
	];
}