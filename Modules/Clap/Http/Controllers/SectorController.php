<?php

namespace Modules\Clap\Http\Controllers;

//Controlador Padre
use Modules\Clap\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Clap\Http\Requests\SectorRequest;

//Modelos
use Modules\Clap\Model\Sector;
use Modules\Clap\Model\Municipio;
use Modules\Clap\Model\Parroquia;

class SectorController extends Controller
{
    protected $titulo = 'Sector';

    public $js = [
        'sector'
    ];
    
    public $css = [
        'sector'
    ];

    public $librerias = [
        'alphanum',
        'maskedinput',
        'datatables',
    ];

    public function index()
    {
        return $this->view('clap::Sector', [
            'Sector' => new Sector()
        ]);
    }

    public function nuevo()
    {
        $Sector = new Sector();
        return $this->view('clap::Sector', [
            'layouts' => 'admin::layouts.popup',
            'Sector' => $Sector
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Sector = Sector::find($id);
        return $this->view('clap::Sector', [
            'layouts' => 'admin::layouts.popup',
            'Sector' => $Sector
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Sector = Sector::withTrashed()->find($id);
        } else {
            $Sector = Sector::find($id);
        }

        if ($Sector) {
            return array_merge($Sector->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(Request $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Sector = $id == 0 ? new Sector() : Sector::find($id);

            $datos = $request->all();
            $datos['slug'] = str_slug($request->nombre, '-');
            unset($datos['estados_id']);
            unset($datos['municipio_id']);
            $Sector->fill($datos);
            $Sector->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $Sector->id,
            'texto' => $Sector->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Sector::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Sector::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Sector::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Sector::select([
            'sector.id', 'parroquia.nombre as parroquia', 'sector.nombre','sector.deleted_at'
        ])->join('parroquia', 'parroquia.id','=','sector.parroquia_id');

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }

    public function municipios(Request $request){
        $sql = Municipio::where('estados_id', $request->id)
                    ->pluck('nombre','id')
                    ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'el estado no Contiene municipios'];
        
        if($sql){
            $salida = ['s' => 's' , 'msj'=> 'Municipios encontrados', 'municipio_id'=> $sql];
        }               
        
        return $salida;
    }  

    public function parroquia(Request $request){
        $sql = Parroquia::where('municipio_id', $request->id)
                    ->pluck('nombre','id')
                    ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'el Municipio no Contiene Parroquia'];
        
        if($sql){
            $salida = ['s' => 's' , 'msj'=> 'Parroquia encontrados', 'parroquia_id'=> $sql];
        }               
        
        return $salida;
    } 
}