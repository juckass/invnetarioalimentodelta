<?php

namespace Modules\Clap\Http\Requests;

use App\Http\Requests\Request;

class jornadaRequest extends Request {
	
	protected $reglasArr = [
		'inicio' => ['required'],
		'fin' => ['required'],
	];
}