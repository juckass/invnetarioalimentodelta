<?php

namespace Modules\Clap\Http\Requests;

use App\Http\Requests\Request;

class SectorRequest extends Request {
    protected $reglasArr = [
		'estados_id' => ['integer'], 
		'municipio_id' => ['integer'], 
		'parroquia_id' => ['integer', 'unique:sector,parroquia_id'], 
		'nombre' => ['required', 'min:3', 'max:100'], 
		'slug' => ['required', 'min:3', 'max:100', 'unique:sector,slug']
	];
}