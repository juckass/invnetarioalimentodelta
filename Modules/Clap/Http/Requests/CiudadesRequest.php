<?php

namespace Modules\Clap\Http\Requests;

use App\Http\Requests\Request;
 
class CiudadesRequest extends Request {
	protected $tabla = 'ciudades';
	protected $reglasArr = [
		'estados_id' => ['required', 'integer'], 
		'nombre' => ['required', 'min:3', 'max:100'], 
		'capital' => ['required']
	];

	public function rules(){
		return $this->reglas();
	}
}