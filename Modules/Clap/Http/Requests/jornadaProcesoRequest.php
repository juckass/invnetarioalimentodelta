<?php

namespace Modules\Clap\Http\Requests;

use App\Http\Requests\Request;

class jornadaProcesoRequest extends Request {
	
	protected $reglasArr = [
		'fecha' => ['required'],
		'clap_id.*' => ['required', 'int'],
		'cantidad.*' => ['required', 'numeric'],
		'familia.*' => ['required', 'int']	
	];
}