<?php

namespace Modules\Clap\Http\Requests;

use App\Http\Requests\Request;

class clapRequest extends Request {
	
	protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:80'],
		'codigo' => ['nombre', 'regex:/^CLAP\-DA\-\d{6}\-\d{5}$/'],
		'municipio_id' => ['required'],
		'parroquia_id' => ['required'],
		'familia' => ['required']
	];
}