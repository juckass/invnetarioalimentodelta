var aplicacion, subida ,$form, tabla = false;
$(function(){
	$("#eliminar", "#botonera").remove();
	$("#buscar", "#botonera").remove();

	aplicacion = new app("formulario", {
		'limpiar' : function(){
			
			aplicacion.rellenar({
				'parroquia_id' : [],
				'sector_id' : []
			});

			$("#tabla tbody").html('');
		},
		'buscar' : function(r){
			
		}
	});
	
	

	$form = aplicacion.form;

	//ya fue atendido, no se puede editar
	//color-view bg-green-seagreen bg-font-green-seagreen bold uppercase

	//Seleccionado para ser atendido
	//color-view bg-blue bg-font-blue

	$("#tabla tbody").on('click', 'tr', function(evento){
		if ($(evento.target).is('input')){
			return;
		}

		activarFila($(this));
	});

	
	$("#fecha").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        
        onClose: function( selectedDate ) {
            $( "#fecha" ).datepicker( "option", "minDate", selectedDate );
        }
    });

	$('#municipio_id').change(function(){
		aplicacion.rellenar({
				'parroquia_id' : [],
			});

		aplicacion.selectCascada($(this).val(), 'parroquia_id','parroquias');
		
		buscar();
	});

	$('#parroquia_id').change(function(){
		aplicacion.rellenar({
				'sector_id' : [],
			});
		aplicacion.selectCascada($(this).val(), 'sector_id','sectores');
		buscar();
	});

	$('#sector_id').change(function(){
		buscar();
	});

	$('#estatus').change(function(){
		buscar();
	});

	
	$('#checkall').change(function(){
		$check = $(this).is(':checked');

		$('#tabla tbody tr')
		.find(':checkbox')
		.each(function(){
			activarFila($(this).parents('tr'), $check);
		});
	   /*
		if($(this).is(':checked')) {
			checkboxes.prop('checked', true);
		} else {
			checkboxes.prop('checked', false);
		}*/
	});
});

function activarFila(ele, $check){
	var $checkbox = ele.find('input[type=\'checkbox\']');

	if (!$checkbox.length) return;
	console.log('ola k ase?');

	$check = $check || !$checkbox.prop('checked');

	$checkbox .prop('checked', $check);
	ele.find('input.form-control').prop('disabled', !$check).prop('readonly', !$check);
	ele.find('input.form-control').first().focus();

	if ($check){
		ele.addClass("color-view bg-blue bg-font-blue");
	}else{
		ele.removeClass("color-view bg-blue bg-font-blue");
	}
}

function buscar(){
	$.ajax({
		'url' : $url + 'buscar',
		'data' : {
			fecha : $("#fecha").val(),
			municipio_id : $("#municipio_id").val(),
			parroquia_id : $("#parroquia_id").val(),
			sector_id : $("#sector_id").val(),
			estatus : $("#estatus").val(),
		},
		method:"GET",
		'success' : function(r){
			$("#tabla tbody").html(tmpl("tmpl-tabla", r));
			$("#tabla tbody").on('focus', 'input', function(){
				if (this.value == '0'){
					$(this).select();
				}
			});
		}
	});
}