var aplicacion, subida ,$form, tabla = false;
$(function(){
	aplicacion = new app("formulario", {
		'buscar' : function(r){
			$("tbody", "#tablaResponsable").append(tmpl('tmpl-contacto', r));
			$(".telefono", "#tablaResponsable").mask("9999-999-9999");
		},
		'limpiar' : function(){
			tabla.fnDraw();

			aplicacion.rellenar({
				'parroquia_id' : [],
				'sector_id' : []
			});

			$("tbody", "#tablaResponsable").html('');
		}
	});

	/*eventos del formualrio de carga*/
	$('#carga').clearForm();

	$('#subir_btn').on('click', function(e){
		e.preventDefault();
		$("#subir:hidden").trigger('click');
	});


	$("#subir").on('change', function(){
		var l = Ladda.create($("#subir_btn").get(0));
	 	l.start();

		var options2 = { 
			url : $url + 'archivo',
			type : 'POST',
			success: function(r){
				aviso(r);
				$('#carga').clearForm();
			},
			complete : function(){
				l.stop();
				$("#carga").clearForm();
			}
		}; 
		
		$('#carga').ajaxSubmit(options2); 
	});

	/* Tabla responsable */

	$("#removerTodo").on('click', function(){
		$("tbody", "#tablaResponsable").html('');
	});

	$("#agregar").on('click', function(){
		$("tbody", "#tablaResponsable").append(tmpl('tmpl-contacto', {
			responsable : [{
				nombre : '',
				telefono : ''
			}]
		}));

		$(".telefono", "#tablaResponsable").mask("9999-999-9999");
	});

	$("tbody", "#tablaResponsable").on('click', '.btn', function(){
		$(this).parents('tr').remove();
	});


	/*fin eventos del formualrio de carga*/

	$form = aplicacion.form;

	tabla = $('#tabla')
	.on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	})
	.dataTable({
		ajax: $url + "datatable",
		columns: [
			{ data: 'clap', name: 'clap.nombre' },
			{ data: 'codigo', name: 'clap.codigo' },
			{ data: 'municipio', name: 'municipio.nombre' },
			{ data: 'parroquia', name: 'parroquia.nombre' },
			{ data: 'sector', name: 'sector.nombre' },
		]
	});

	$('#codigo').mask("CLAP-DA-999999-99999"); //ej:AAAA-AAA-999999-99999

	$('#municipio_id').change(function(){
		aplicacion.selectCascada($(this).val(), 'parroquia_id','parroquias');
	});

	$('#parroquia_id').change(function(){
		aplicacion.selectCascada($(this).val(), 'sector_id','sectores');
	});
});