var aplicacion, $form, tabla = false;
$(function(){
	aplicacion = new app("formulario", {
		'limpiar' : function(){
			tabla.fnDraw();
		},
	});

	$form = aplicacion.form;

	tabla = $('#tabla')
	.on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	})
	.dataTable({
		processing: true,
		serverSide: true,
		ajax: $url + "datatable",
		oLanguage: datatableEspanol,
		columns: [
			{ data: 'id', name: 'id' },
			{ data: 'inicio', name: 'inicio' },
			{ data: 'fin', name: 'fin' }
		]
	});

	$( "#inicio, #fin", $form ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        
        onClose: function( selectedDate ) {
                $( "#fecha" ).datepicker( "option", "minDate", selectedDate );
        }
    });

});