var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
		'limpiar' : function(){
			tabla.fnDraw();
		}
	});

	$form = aplicacion.form;

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [{"data":"parroquia","name":"parroquia.nombre"},{"data":"nombre","name":"sector.nombre"}]
	});
	
	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});

	$('#estados_id').change(function(){
		aplicacion.selectCascada($(this).val(), 'municipio_id','municipios');
	});
	$('#municipio_id').change(function(){
		aplicacion.selectCascada($(this).val(), 'parroquia_id','parroquia');
	});
});