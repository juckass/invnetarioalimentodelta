$(function() {
	$("option:first", "#tipo").remove();
	$("option:first", "#mes").remove();
	$("option:first", "#ano").remove();

	$("#fechadesde").datepicker({
		changeMonth: true,
		onSelect : function(){
			reporte();
		}
	}).change(function(){
		reporte();
	});

	$("#fechahasta").datepicker({
		changeMonth: true,
		onSelect : function(){
			reporte();
		}
	}).change(function(){
		reporte();
	});

	$("#tipo").on('change', function(){
		var tipo = $('#tipo').val();

		if(tipo == "meses"){
			$("#anos").css('display', 'block');
			$("#rango").css('display', 'none');
		}else if(tipo == "rango"){
			$("#rango").css('display', 'block');
			$("#anos").css('display', 'none');
		}

		reporte();
	}).change();
	
	$('#imprimir').click(function(){
		if ($("#tipoReporte").val() == 'municipios'){
			reporte(true);
		}else{
			reporteMunicipio(true);
		}
	});

	$("#buscar").click(function(){
		reporte();
	});

	$("#atras").click(function(){
		var tipo = $("#tipoReporte").val();

		switch(tipo) {
			case 'municipios':
			case 'parroquias':
				reporte();
				break;
			case 'clap':
				reporteMunicipio();
				break;
			case 'clap-detalle':
				reporteParroquia();
				break;
			default:
				reporte();
		}
	});

	$('#tipo_clap').change(function(){
		$('#estatus').val('');
		reporte();
	});

	$('#estatus').change(function(){
		reporte();
	});
});

function reporte($form){
	$form = $form || false;

	if ($("#tipo").val() == 'rango'){
		if($("#fechadesde").val() == '' || $("#fechahasta").val() == ''){
			//aviso('El campo Fecha es Requerido.', false);
			return false;
		}
	}
	
	$("#tipoReporte").val('municipios');
	$("#municipio_id").val(0);

	if ($form){
		$('#formulario').submit();
	}
	
	$.ajax($url + 'imprimir', {
		data : $('#formulario').serialize(),
		success : function(html){
			$("#tipoReporte").val();

			$("#reportehtml").html(html);

			$("#reportehtml table").on('click', 'tr', function(){
				var municipio_id = $(this).data('municipio');

				$("#municipio_id").val(municipio_id);

				reporteMunicipio();
			});
		}
	});
}

function reporteMunicipio($form){
	$form = $form || false;

	if ($("#tipo").val() == 'rango'){
		if($("#fechadesde").val() == '' || $("#fechahasta").val() == ''){
			//aviso('El campo Fecha es Requerido.', false);
			return false;
		}
	}

	$("#tipoReporte").val('parroquias');

	if ($form){
		$('#formulario').submit();
	}
	
	$.ajax($url + 'imprimir', {
		data : $('#formulario').serialize(),
		success : function(html){
			$("#reportehtml").html(html);

			$("#reportehtml table").on('click', 'tr', function(){
				var parroquia_id = $(this).data('parroquia');

				$("#parroquia_id").val(parroquia_id);

				reporteParroquia();
			});
		}
	});
}

function reporteParroquia($form){
	$form = $form || false;

	if ($("#tipo").val() == 'rango'){
		if($("#fechadesde").val() == '' || $("#fechahasta").val() == ''){
			//aviso('El campo Fecha es Requerido.', false);
			return false;
		}
	}

	$("#tipoReporte").val('clap');

	if ($form){
		$('#formulario').submit();
	}
	
	$.ajax($url + 'imprimir-parroquia', {
		data : $('#formulario').serialize(),
		success : function(html){
			$("#reportehtml").html(html);
			$("#reportehtml table").on('click', 'tr', function(){
				var clap_id = $(this).data('clap');
				$("#clap_id").val(clap_id);
				 if (clap_id == 0 ){
				 	aviso('Se requiere la Cedula del responsable del clap')
				 }else{
				 	var url = 'http://consultapsuv.e-bolivar.gob.ve/consulta.php?valor_enviado='+clap_id;
				    window.open(url, '_blank');
				 }
			});
		}
	});
}


function reporteClap(id){
       
	/*$form = $form || false;

	if ($("#tipo").val() == 'rango'){
		if($("#fechadesde").val() == '' || $("#fechahasta").val() == ''){
			//aviso('El campo Fecha es Requerido.', false);
			return false;
		}
	}
	$("#tipoReporte").val('clap-detalle');
	if ($form){
		//$('#formulario').submit();
	}
	$("#reportehtml table").on('click', 'tr', function(){
				alert();
			});
	$.ajax($url + 'imprimir-clap', {
		data : $('#formulario').serialize(),
		success : function(html){
			//$("#reportehtml").html(html);
			$("#reportehtml table").on('click', 'tr', function(){
				alert();
			});
		}
	});*/
}