<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => Config::get('admin.prefix')], function() {
	Route::group(['prefix' => 'clap'], function(){ 
		Route::group(['prefix' => 'definiciones'], function(){ 
			Route::group(['prefix' => 'estados'], function() {
				Route::get('/', 				'EstadosController@index');
				Route::get('nuevo', 			'EstadosController@nuevo');
				Route::get('cambiar/{id}', 		'EstadosController@cambiar');
				
				Route::get('buscar/{id}', 		'EstadosController@buscar');

				Route::post('guardar',			'EstadosController@guardar');
				Route::put('guardar/{id}', 		'EstadosController@guardar');

				Route::delete('eliminar/{id}', 	'EstadosController@eliminar');
				Route::post('restaurar/{id}', 	'EstadosController@restaurar');
				Route::delete('destruir/{id}', 	'EstadosController@destruir');

				Route::get('datatable', 		'EstadosController@datatable');
			});

			Route::group(['prefix' => 'municipio'], function() {
				Route::get('/', 				'MunicipioController@index');
				Route::get('nuevo', 			'MunicipioController@nuevo');
				Route::get('cambiar/{id}', 		'MunicipioController@cambiar');
				
				Route::get('buscar/{id}', 		'MunicipioController@buscar');

				Route::post('guardar',			'MunicipioController@guardar');
				Route::put('guardar/{id}', 		'MunicipioController@guardar');

				Route::delete('eliminar/{id}', 	'MunicipioController@eliminar');
				Route::post('restaurar/{id}', 	'MunicipioController@restaurar');
				Route::delete('destruir/{id}', 	'MunicipioController@destruir');

				Route::get('datatable', 		'MunicipioController@datatable');
			});

			Route::group(['prefix' => 'parroquia'], function() {
				Route::get('/', 				'ParroquiaController@index');
				Route::get('nuevo', 			'ParroquiaController@nuevo');
				Route::get('cambiar/{id}', 		'ParroquiaController@cambiar');
				
				Route::get('buscar/{id}', 		'ParroquiaController@buscar');
				Route::get('municipios/{id}',   'ParroquiaController@municipios');
				Route::post('guardar',			'ParroquiaController@guardar');
				Route::put('guardar/{id}', 		'ParroquiaController@guardar');

				Route::delete('eliminar/{id}', 	'ParroquiaController@eliminar');
				Route::post('restaurar/{id}', 	'ParroquiaController@restaurar');
				Route::delete('destruir/{id}', 	'ParroquiaController@destruir');

				Route::get('datatable', 		'ParroquiaController@datatable');
			});

			Route::group(['prefix' => 'sector'], function() {
				Route::get('/', 				'SectorController@index');
				Route::get('nuevo', 			'SectorController@nuevo');
				Route::get('cambiar/{id}', 		'SectorController@cambiar');
				
				Route::get('buscar/{id}', 		'SectorController@buscar');
				Route::get('municipios/{id}',   'SectorController@municipios');
				Route::get('parroquia/{id}',   'SectorController@parroquia');
				Route::post('guardar',			'SectorController@guardar');
				Route::put('guardar/{id}', 		'SectorController@guardar');

				Route::delete('eliminar/{id}', 	'SectorController@eliminar');
				Route::post('restaurar/{id}', 	'SectorController@restaurar');
				Route::delete('destruir/{id}', 	'SectorController@destruir');

				Route::get('datatable', 		'SectorController@datatable');
			});
		});
		
		Route::group(['prefix' => 'reportes'], function(){ 
			Route::get('/', 				'ReporteController@index');
			Route::get('imprimir',			'ReporteController@imprimir');
			Route::get('imprimir-parroquia','ReporteController@imprimirParroquia');
			Route::get('imprimir-clap',		'ReporteController@imprimirClap');
			Route::post('imprimir',			'ReporteController@imprimir');
		});	

		Route::group(['prefix' => 'carga'], function(){ 
			Route::get('/', 				'ClapController@index');
			Route::post('guardar',			'ClapController@guardar');
			Route::get('fecha', 			'ClapController@fecha');
			Route::put('guardar/{id}',		'ClapController@actualizar');
			Route::get('parroquias/{id}', 	'ClapController@parroquias');
			Route::get('sectores/{id}', 	'ClapController@sectores');
			Route::get('buscar/{id}', 		'ClapController@buscar');
			Route::post('archivo/', 		'ClapController@archivo');
			Route::get('datatable', 		'ClapController@datatable');	
		});
		
		Route::group(['prefix' => 'jornadas'], function(){ 
			Route::get('/', 				'jornadaProcesoController@index');
			Route::get('fecha', 			'jornadaProcesoController@fecha');
			Route::post('guardar',			'jornadaProcesoController@guardar');
			Route::put('guardar/{id}',		'jornadaProcesoController@actualizar');
			Route::get('parroquias/{id}', 	'jornadaProcesoController@parroquias');
			Route::get('sectores/{id}', 	'jornadaProcesoController@sectores');
			Route::get('buscar/{id}', 		'jornadaProcesoController@buscar');
			Route::get('buscar', 			'jornadaProcesoController@buscar');
			Route::post('archivo/', 		'jornadaProcesoController@archivo');
			Route::get('datatable', 		'jornadaProcesoController@datatable');	
		});	
	});

    //{{route}}
});