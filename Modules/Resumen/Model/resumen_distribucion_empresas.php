<?php
namespace Modules\Resumen\Model;

use Modules\Admin\Model\modelo;
use Carbon\Carbon;

class resumen_distribucion_empresas extends modelo
{
	protected $table = 'resumen_distribucion_empresas';
    protected $fillable = [
		'resumen_id',
		'empresas_id',
        'claps', 
		'cantidad', 
		'cantidad_acumulado'
    ];

    //protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

 	public function resumen()
    {
        return $this->belongTo('Modules\Resumen\Model\resumen');
    }
}
