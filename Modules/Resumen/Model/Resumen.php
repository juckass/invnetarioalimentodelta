<?php
namespace Modules\Resumen\Model;

use Modules\Admin\Model\modelo;
use Carbon\Carbon;
class Resumen extends modelo
{
	protected $table = 'resumen';
    protected $fillable = ['fecha_inicio', 'fecha_final', 'jornadas','mision_vivienda', 'familias', 'claps', 'bases', 'instituciones', 'punto_circulo'];

    //protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
	
    public function setFechaInicioAttribute($value)
	{
		// 2016-06-27
		$formato = 'd/m/Y';
		if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
			$formato = 'Y-m-d';
		}
		
		$this->attributes['fecha_inicio'] = Carbon::createFromFormat($formato, $value);
	}

	public function setFechaFinalAttribute($value)
	{
		// 2016-06-27
		$formato = 'd/m/Y';
		if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
			$formato = 'Y-m-d';
		}
		
		$this->attributes['fecha_final'] = Carbon::createFromFormat($formato, $value);
	}
	

	public function getFechaInicioAttribute($value){
		return Carbon::parse($value)->format('d/m/Y');
	}
	public function getFechaFinalAttribute($value){
		return Carbon::parse($value)->format('d/m/Y');
	}

	public function distribuciones()
    {
        return $this->hasMany('Modules\Resumen\Model\resumen_distribucion');
    }

    public function distribuciones_empresas()
    {
        return $this->hasMany('Modules\Resumen\Model\resumen_distribucion_empresas');
    }
}