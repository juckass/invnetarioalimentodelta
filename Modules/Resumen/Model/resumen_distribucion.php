<?php
namespace Modules\Resumen\Model;

use Modules\Admin\Model\modelo;
use Carbon\Carbon;

class resumen_distribucion extends modelo
{
	protected $table = 'resumen_distribucion';
    protected $fillable = [
		'municipio_id',
		'resumen_id',
		'jornadas',
		'jornadas_acumulado',
		'familias',
		'familias_acumulado',
		'cantidad',
		'cantidad_acumulado',
    ];

    //protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

 	public function resumen()
    {
        return $this->belongTo('Modules\Resumen\Model\resumen');
    }
}
