<?php namespace Modules\Resumen\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

abstract class Controller extends BaseController {
	protected $titulo = 'Backend';

	public $app = 'alimentos';

	protected $patch_js = [
		'public/js',
		'public/plugins',
		'Modules/Resumen/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'Modules/Resumen/Assets/css',
	];
}