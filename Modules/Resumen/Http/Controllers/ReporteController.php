<?php

namespace Modules\Resumen\Http\Controllers;

//Controlador Padre
use Modules\Resumen\Http\Controllers\Controller;

//Dependencias
use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

//Request
use Modules\Resumen\Http\Requests\ReporteRequest;

//Modelos
use Modules\Inventario\Model\definiciones\Empresas;
use Modules\Clap\Model\Municipio;

use Modules\Resumen\Model\Resumen;
use Modules\Resumen\Model\resumen_distribucion;
use Modules\Resumen\Model\resumen_distribucion_empresas;

class ReporteController extends Controller {
	protected $titulo = 'Reporte';

	public $librerias = [
		'highcharts',
		'highcharts-drilldown',
		'highmaps',
	];

	public $css = [
		'nouislider/nouislider.min.css',
		'nouislider/nouislider.pips.css',
		'resumen/reporte'
	];

	public $js = [
		'maps/venezuela/delta_amacuro.js',
		'nouislider/nouislider.min.js',
		'resumen/reporte'
	];

	public function __construct(){
		parent::__construct();
	}

	public function index() {
		return $this->view('resumen::Reporte');
	}
	public function registros(){
		$resumenes 	= Resumen::with('distribuciones', 'distribuciones_empresas')->orderBy('resumen.fecha_final')->get();
		//$municipios = municipio::where('estados_id', 9)->get();

		$_municipios = municipio::where('estados_id', 9)->orderBy('nombre', 'ASC')->get();

        $municipios = [];
        foreach ($_municipios as $_municipio) {
            $municipios[$_municipio->id] = $_municipio;
        }

		$empresas 	= empresas::where('redes_id', 1)->orWhere('redes_id', 3)->pluck("nombre", 'id');
		$requerimiento = DB::table('ingresos_requerimiento')
			->select(['municipio_id', DB::raw('SUM(cantidad) as cantidad')])
			->groupBy('municipio_id')
			->pluck("cantidad", 'municipio_id');
		if ($resumenes){
			return [
				's' => 's',
				'registros' 	=> $resumenes,
				'municipios' 	=> $municipios,
				'empresas' 		=> $empresas,
				'requerimiento' => $requerimiento
			];
		}
		return ['s' => 's', 'msj' => 'No se Encontraron Registros'];
	}
}