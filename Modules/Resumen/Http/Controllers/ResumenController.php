<?php
namespace Modules\Resumen\Http\Controllers;
//Controlador Padre
use Modules\Resumen\Http\Controllers\Controller;
//Dependencias
use DB;
use Excel;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
//Request
use Modules\Resumen\Http\Requests\ResumenRequest;
//Modelos
use Modules\Inventario\Model\definiciones\Empresas;
use Modules\Clap\Model\Municipio;
use Modules\Resumen\Model\Resumen;
use Modules\Resumen\Model\resumen_distribucion;
use Modules\Resumen\Model\resumen_distribucion_empresas;
class ResumenController extends Controller {
    protected $titulo = 'Resumen';
    public $librerias = [
        'alphanum',
        'maskedinput',
        'datatables',
        'jquery-ui',
        'jquery-ui-timepicker'
    ];
    public $js = [
        'resumen/carga'
    ];

    public $css = [
        'resumen/resumen'
    ];
    public function index() 
    {
        $municipios = municipio::where('estados_id', 9)->orderBy('nombre', 'ASC')->get();
        $empresas = empresas::orderBy('nombre', 'ASC')->get();
        return $this->view('resumen::Resumen', [
            'municipios' => $municipios,
            'empresas' => $empresas
        ]);
    }

    public function buscar(Request $request){
        $id = $request->id;
        $Resumen = Resumen::find($id);
        $resumen_distribuciones = resumen_distribucion::where('resumen_id', $id)
            //->groupBy('municipio_id')
        ->get();
        $resumen_distribucion_empresas = resumen_distribucion_empresas::where('resumen_id', $id)
        //->groupBy('empresas_id')
        ->get();
        $resumen_municipio = [];
        foreach ($resumen_distribuciones as $resumen_distribucion) {
            $resumen_municipio[$resumen_distribucion->municipio_id] = [ 
                'jornadas'              => $resumen_distribucion->jornadas,
                'jornadas_acumulado'    => $resumen_distribucion->jornadas_acumulado,
                'familias'              => $resumen_distribucion->familias,
                'familias_acumulado'    => $resumen_distribucion->familias_acumulado,
                'cantidad'              => $resumen_distribucion->cantidad,
                'cantidad_acumulado'    => $resumen_distribucion->cantidad_acumulado,
            ];
        }
        $resumen_empresas = [];
        foreach ($resumen_distribucion_empresas as $resumen_distribucion_empresa) {
            $resumen_empresas[$resumen_distribucion_empresa->empresas_id] = [ 
                'claps'                 => $resumen_distribucion_empresa->claps,
                'jornadas'              => $resumen_distribucion_empresa->jornadas,
                'familias'              => $resumen_distribucion_empresa->familias,
                'cantidad'              => $resumen_distribucion_empresa->cantidad,
                'cantidad_acumulado'    => $resumen_distribucion_empresa->cantidad_acumulado    
            ];
        }
        $datos = [
            '0' => $Resumen,
            '1' => $resumen_municipio,
            '2' => $resumen_empresas
        ];
        if ($Resumen) {
            return array_merge($datos, [
                's' => 's', 
                'msj' => trans('controller.buscar')
            ]);
        }
        
        return trans('controller.nobuscar');
    }
    public function guardar(ResumenRequest $request){
        DB::beginTransaction();
        try {
            $fecha_inicio = Carbon::createFromFormat('d/m/Y', $request->fecha_inicio)->format("Y-m-d");
            $fecha_final = Carbon::createFromFormat('d/m/Y',$request->fecha_final)->format("Y-m-d");
            if ($request->id == "") {
                $Resumen = Resumen::create([
                    'fecha_inicio'   => $fecha_inicio,
                    'fecha_final'    => $fecha_final,
                    'jornadas'       => $request->jornadas,
                    'familias'       => $request->familias,
                    'claps'          => $request->claps,
                    'bases'          => $request->bases,
                    'instituciones'  => $request->instituciones,
                    'punto_circulo'  => $request->punto_circulo,
                    'mision_vivienda'=> $request->mision_vivienda
                ]);
                $id_resumen = $Resumen['id'];
            } else {
                $id_resumen = $request->id;
                Resumen::find($id_resumen)->update([
                    'jornadas'       => $request->jornadas,
                    'familias'       => $request->familias,
                    'claps'          => $request->claps,
                    'bases'          => $request->bases,
                    'instituciones'  => $request->instituciones,
                    'punto_circulo'  => $request->punto_circulo,
                    'mision_vivienda'=> $request->mision_vivienda
                ]);
            }
            $municipios = municipio::where('estados_id', 9)->orderBy('nombre', 'ASC')->get();
            $distribucion = $request->input("muni");
            foreach ($municipios as $municipio) {
                $distribucion_2 = resumen_distribucion::firstOrNew([
                    'municipio_id'   => $municipio->id,
                    'resumen_id'     => $id_resumen
                ]);
                $distribucion_2->jornadas           = $distribucion[$municipio->id]['jornadas'];
                $distribucion_2->jornadas_acumulado = $distribucion[$municipio->id]['jornadas_acumulado'];
                $distribucion_2->familias           = $distribucion[$municipio->id]['familias'];
                $distribucion_2->familias_acumulado = $distribucion[$municipio->id]['familias_acumulado'];
                $distribucion_2->cantidad           = $distribucion[$municipio->id]['cantidad'];
                $distribucion_2->cantidad_acumulado = $distribucion[$municipio->id]['cantidad_acumulado'];
                $distribucion_2->save();
            }
            $empresas = empresas::orderBy('nombre', 'ASC')->get();
            $distribucion_empresas = $request->input("inv");
            foreach ($empresas as $empresa) {
                $resumen_distribucion = resumen_distribucion_empresas::firstOrNew([
                    'empresas_id'   =>  $empresa->id,
                    'resumen_id'     => $id_resumen
                ]);
                $resumen_distribucion->claps                = $distribucion_empresas[$empresa->id]['claps'];
                $resumen_distribucion->familias             = $distribucion_empresas[$empresa->id]['familias'];
                $resumen_distribucion->jornadas             = $distribucion_empresas[$empresa->id]['jornadas'];
                $resumen_distribucion->cantidad             = $distribucion_empresas[$empresa->id]['cantidad'];
                $resumen_distribucion->cantidad_acumulado   = $distribucion_empresas[$empresa->id]['cantidad_acumulado'];
                $resumen_distribucion->save();
            }
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();
        return ['s' => 's', 'msj' => trans('controller.incluir')];
    }
    public function eliminar(Request $request, $id = 0){
        try {
            $Resumen = Resumen::destroy($id);
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }
        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }
    public function datatable(){
        $sql = Resumen::select('id','fecha_inicio', 'fecha_final');
        return Datatables::of($sql)->setRowId('id')->make(true);
    }
    public function cargaEmpresa(){
        $empresas = [
            'friosa' => 1,
            'mercal' => 2,
            'pdval' => 3,
            'alimentos-bolivar' => 5,
        ];
        $empresas = empresas::all();
        /*
        \DB::listen(function($sql, $bindings, $time) {
            var_dump($sql);
            var_dump($bindings);
            var_dump($time);
        });
        */
        try {
            DB::beginTransaction();
            foreach ($empresas as $empresa) {
                $xls = 'public/archivos/claps_empresas/' . str_slug($empresa->nombre) . '.xlsx';
                if (!is_file($xls)) {
                    continue;
                }
                //echo "extrayendo $xls<br />";
                $datos = Excel::selectSheetsByIndex(0)->load($xls)->get();
                foreach ($datos as $dato) {
                    $_fecha = explode('-', $dato->fecha);
                    $fecha = Carbon::createFromDate("20" . $_fecha[2], $_fecha[0], $_fecha[1])->format('Y-m-d');
                    $resumen = resumen::where('fecha_inicio', '<=', $fecha)
                                      ->where('fecha_final', '>=', $fecha)
                                      ->first();
                    if (!$resumen) {
                        continue;
                    }
                    $resumen_empresa = resumen_distribucion_empresas::where('resumen_id', $resumen->id)
                        ->where('empresas_id', $empresa->id)
                        ->first();
                    $resumen_empresa->claps = $dato->cantidad;
                    $resumen_empresa->save();
                }
                DB::commit();
            }
        } catch (Exception $e) {
            dd($e);
            DB::rollback();
        }
        return ['s' => 's', 'msj' => trans('controller.incluir')];
    }
}