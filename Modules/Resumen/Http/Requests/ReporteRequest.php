<?php

namespace Modules\Resumen\Http\Requests;

use App\Http\Requests\Request;
 
class ReporteRequest extends Request {
	protected $tabla = 'reporte';
	protected $reglasArr = [
		
	];

	public function rules(){
		return $this->reglas();
	}
}