<?php

namespace Modules\Resumen\Http\Requests;

use App\Http\Requests\Request;
 
class ResumenRequest extends Request {
	protected $tabla = 'resumen';
	protected $reglasArr = [
		'fecha_inicio' => ['required'], 
		'fecha_final' => ['required'], 
		'jornadas' => ['required', 'integer'], 
		'familias' => ['required', 'integer'], 
		'claps' => ['required', 'integer'], 
		'bases' => ['required', 'integer'], 
		'instituciones' => ['required', 'integer'], 
		'punto_circulo' => ['required', 'integer'],
		'mision_vivienda'=>['required', 'integer'],
		'muni.*.*' =>  ['required', 'regex:/^\d{1,8}([\.\,]\d{1,3})?$/'],
		'inv.*.*' =>  ['required', 'regex:/^\d{1,8}([\.\,]\d{1,3})?$/']
	];

	public function rules(){
		return $this->reglas();
	}
}