var _mapa = [],
	municipios = [],
	empresas = [],
	registros = [],
	requerimientos = [],
	registro_actual = 0,
	noUi_tooltip,
	tipo_grafica,
	tipo_grafica_general = 'clap',
	descripcion_tipo_grafica = {
		'jornadas' : 'Jornadas Realizadas',
		'clap' : 'Claps Atendidos',
		'distribucion' : 'Toneladas Metricas',
		'familias' : 'Familias Atendidas',
		'personas' : 'Personas Atendidas',
		'densidad' : 'Densidad Poblacional',
		'requerimiento' : 'Requerimiento',
	};


$(function(){
	registro_actual = registros.length - 1;

	generarMapa();
	generarGraficaComportamiento();
	generarGraficaComportamientoGeneral();

	crearSlider();

	$("option:first", "#tipo_grafica").remove();
	$("#tipo_grafica").change(function(){
		var grafica = $('#comportamiento').highcharts();
		tipo_grafica = $("#tipo_grafica").val();
		reporte();

		var dataComportamiento = getDataComportamiento();
		
		/*
		for (var i = 0; i < dataComportamiento.length; i++) {
			
			grafica.series[i].setData(dataComportamiento[i].data);
		}*/
          
        for (var i in dataComportamiento ){
                
            //grafica.series[i].setData(dataComportamiento[i].data);
        }
		
	
		grafica.setTitle({text: "Comportamiento de " + descripcion_tipo_grafica[tipo_grafica]});

		for(i in grafica.series){
			if (i == 0) continue;
			grafica.series[i].update({ 
				visible: false
			}, false);
		}

		grafica.redraw();
	}).change();
	$("option[value='']", "#tipo_grafica_comportamiento").remove();
	$("#tipo_grafica_comportamiento").change(function(){
		var grafica = $('#comportamientoGeneral').highcharts();
		tipo_grafica_general = $("#tipo_grafica_comportamiento").val();

		var dataComportamiento = getDataComportamientoGeneral();


		for (var i = 0; i < dataComportamiento.length; i++) {
			grafica.series[i].setData(dataComportamiento[i].data);
		}
		
		grafica.setTitle({text: "Comportamiento de " + descripcion_tipo_grafica[tipo_grafica_general]});

		grafica.redraw();
	}).change();
});

function getDataComportamiento(){
	var d = [],
	dataComportamiento = {
		0 : {
			name : 'Estado Delta Amacuro2',
			data : []
		}
	};

	for (var j = 0; j < registros.length; j++) {
		dataComportamiento[0].data.push(0);
	}
       

	for(var i in municipios){
		var data = [];
		dataComportamiento[municipios[i]['id']] = {
			name : municipios[i]['nombre'],
			data : data
		};
	}
    
	/*for (var i = 0; i < municipios.length; i++) {
		var data = [];
		dataComportamiento[municipios[i].id] = {
			name : municipios[i].nombre,
			data : data
		};
	}*/

	for (var p = 0; p < registros.length; p++) {
		r = registros[p];
		
		for (var j = 0; j < r.distribuciones.length; j++) {
			var valor = 0;
			d = r.distribuciones[j];

			switch(tipo_grafica){
				case 'jornadas':
					valor = parseInt(d.jornadas);
					break;

				case 'distribucion':
					valor = parseFloat(d.cantidad);
					break;

				case 'familias':
					valor = parseInt(d.familias);
					break;

				case 'personas':
					valor = parseInt(d.familias * 4);
					break;
			}

			dataComportamiento[d.municipio_id].data.push(valor);

		}
	}
 
	
	var _dataComportamiento = [{
		name : 'Estado Delta Amacuro',
		data : dataComportamiento[0].data
	}];

	for(var i in municipios){
		var data = [];
		_dataComportamiento.push({
			name : municipios[i]['nombre'],
			data : dataComportamiento[municipios[i]['id']].data
		});
	}

	/*for (var i = 0; i < municipios.length; i++) {
		_dataComportamiento.push({
			name : municipios[i].nombre,
			data : dataComportamiento[municipios[i].id].data
		});
	}*/
	
	dataComportamiento = [];

	for (var i = 0; i < _dataComportamiento.length; i++) {
		for (var j = 0; j < _dataComportamiento[i].data.length; j++) {
			_dataComportamiento[0].data[j] += _dataComportamiento[i].data[j];
		}
	}

	for (var j = 0; j < _dataComportamiento[0].data.length; j++) {
		_dataComportamiento[0].data[j] = Math.round(_dataComportamiento[0].data[j] * 100) / 100;
	}
      console.log(_dataComportamiento);
	return _dataComportamiento;
}

function getDataComportamientoGeneral() {
	var dataComportamiento = [], empresa_comportamiento = {};

	dataComportamiento.push({
		name : 'Estado Delta Amacuro',
		data : []
	});
	
	for (j in empresas) {
		empresa_comportamiento[j] = dataComportamiento.length;

		dataComportamiento.push({
			name : empresas[j],
			data : []
		});
	}

	for (var j = 0; j < registros.length; j++) {
	
		var totalComportamiento = 0;
		for (var i in registros[j].distribuciones_empresas){
			var e = registros[j].distribuciones_empresas[i];
			if (empresas[e.empresas_id] === undefined){
				continue;
			}
			
			switch(tipo_grafica_general){
				case 'jornadas':
					valor = parseInt(e.jornadas);
					break;

				case 'distribucion':
					valor = parseFloat(e.cantidad);
					break;

				case 'familias':
					valor = parseInt(e.familias);
					break;

				case 'clap':
					valor = parseInt(e.claps);
					break;
			}

			if (isNaN(valor)) {
				valor = 0;
			}
			
			totalComportamiento += valor;

			dataComportamiento[empresa_comportamiento[e.empresas_id]].data.push(valor);
		}
		
		dataComportamiento[0].data.push(totalComportamiento);
	}

  
	return dataComportamiento;
}

function getDataMapa() {
	var r = registros[registro_actual], d = [], _mapa = [];

	for (var i = 0; i < r.distribuciones.length; i++) {
		var valor = 0;
		d = r.distribuciones[i];

		switch(tipo_grafica){
			case 'jornadas':
				valor = parseInt(d.jornadas);
				break;

			case 'distribucion':
				valor = parseFloat(d.cantidad);
				break;

			case 'familias':
				valor = parseInt(d.familias);
				break;

			case 'personas':
				valor = parseInt(d.familias * 4);
				break;

			case 'densidad':
				valor = parseInt(municipios[d.municipio_id].poblacion);
				break;

			case 'requerimiento':
				valor = parseFloat(requerimientos[d.municipio_id]);
				break;
		}

		var ele = {
			'slug' : slug(municipios[d.municipio_id].nombre)
		};
		
		if (valor > 0){
			ele.value = valor;
		}

		_mapa.push(ele);
	}

	return _mapa;
}

function dataResumen(){
	var r = registros[registro_actual];

	var $info = $("#info_resumen");
	$info.html("");

	$info.append("<div class='numero'>" + number_format(r.jornadas, 0, ',', '.') 		+ "</div> Jornadas Realizadas.<br />");
	$info.append("<div class='numero'>" + number_format(r.claps, 0, ',', '.') 			+ "</div> Claps Atendidos.<br />");
	$info.append("<div class='numero'>" + number_format(r.bases, 0, ',', '.') 			+ "</div> Base de Misiones Atendidas.<br />");
	$info.append("<div class='numero'>" + number_format(r.familias, 0, ',', '.') 		+ "</div> Familias Atendidas.<br />");
	$info.append("<div class='numero'>" + number_format(r.familias * 4, 0, ',', '.') 	+ "</div> Personas Beneficiadas.<br />");
	$info.append("<div class='numero'>" + number_format(r.instituciones, 0, ',', '.') 	+ "</div> Instituciones Atendidas.<br />");
	$info.append("<div class='numero'>" + number_format(r.mision_vivienda, 0, ',', '.') + "</div> Misi&oacute;n Vivienda Atendidas.<br />");
	$info.append("<div class='numero'>" + number_format(r.punto_circulo, 0, ',', '.') 	+ "</div> Punto y Circulos Atendidos<br />");

	$info.append("<br />");

	var totalDistribuido = 0, 
	empresas_html = "";

	for (var i in r.distribuciones_empresas){
		var e = r.distribuciones_empresas[i];
		if (empresas[e.empresas_id] === undefined){
			continue;
		}

		totalDistribuido += parseFloat(e.cantidad);
		
		empresas_html += "<div class='numero'>" + number_format(e.cantidad, 2, ',', '.') + "</div>" + empresas[e.empresas_id] + "<br />";
	}

	$info.append(empresas_html);
	$info.append("<div class='numero'>" + number_format(totalDistribuido, 2, ',', '.') + "</div> Toneladas Metricas Distribuidas<br />");
}

function reporte(){
	var r = registros[registro_actual], d = [];

	if (r == undefined){
		return;
	}
	
	var _mapa = getDataMapa();
	
	dataResumen();

	$('#mapa').highcharts().series[0].setData(_mapa);
	$('#mapa').highcharts().series[0].name = descripcion_tipo_grafica[tipo_grafica];
}

function crearSlider(){
	var e = document.getElementById("slider"), timeOutToolTip = false;

	var valores = [];
	for (var i = 0; i < registros.length; i++) {
		valores.push(i);
	}

	noUiSlider.create(e, {
		start: registro_actual,
		step: 1,
		range: {
			min: 0,
			max: registros.length - 1
		},
		pips: {
			mode: 'values',
			values: valores,
			density: 8
		}
	});

	
	for (var n = e.getElementsByClassName("noUi-handle"), t = [], i = 0; i < n.length; i++){
		t[i] = document.createElement("div");
		n[i].appendChild(t[i]);
	}

	t[0].className += "noUi-tooltip";
	t[0].innerHTML = "<strong>Fecha: </strong><span></span>";
	t[0] = t[0].getElementsByTagName("span")[0];

	var r = registros[registro_actual];
	$("#fecha_reporte").html('<b>Desde:</b> ' + r.fecha_inicio + '<br /><b>Hasta:</b> ' + r.fecha_final);

	noUi_tooltip = $(".noUi-tooltip");

	e.noUiSlider.on('update', function ( values, handle ) {
		registro_actual = parseInt(values[handle]);

		var r = registros[registro_actual];

		t[0].innerHTML = '<b>Desde:</b> ' + r.fecha_inicio + '<br /><b>Hasta:</b> ' + r.fecha_final;

		if (timeOutToolTip !== false){
			clearTimeout(timeOutToolTip);
		}

		registro_actual = parseInt(values[handle]);

		var r = registros[registro_actual];

		$("#fecha_reporte").html('<b>Desde:</b> ' + r.fecha_inicio + '<br /><b>Hasta:</b> ' + r.fecha_final);
		reporte();

		noUi_tooltip.stop().animate({
			opacity : 1
		}, 300);

		timeOutToolTip = setTimeout(function(){
			noUi_tooltip.stop().animate({
				opacity : 0
			}, 300);
		}, 3000);
	});
}

function generarMapa(){
	$('#mapa').highcharts('Map', {
		'chart' : {
			'borderWidth' : 1,
			'animation': false
		},
		'credits': {
			'enabled' : false
		},
		'title': {
			'align': 'center',
			'text': '',
			'useHTML': false,
		},
		legend: {
			'layout': 'vertical',
			'align': 'left',
			'verticalAlign': 'bottom'
		},
		'mapNavigation': {
			'enabled': true
		},
		'colorAxis': {},
		'series': [{
			'type' : 'map',
			'name': descripcion_tipo_grafica[tipo_grafica],
			'animation': {
				'duration': 1000
			},
			
			data : _mapa,
			mapData: Highcharts.maps['venezuela/delta_amacuro'],
			joinBy: ['slug', 'slug'],

			'dataLabels': {
				'enabled': true,
				'color': '#FFFFFF',
				'formatter': function () {
					if (this.point.value > 0) {
						//return this.key + " (" + this.point.value + ")";
						return this.key;
					}
				}
			},
			'states': {
				'hover': {
					'color': '#1A3C82'
				}
			}
		}]
	});
}

function generarGraficaComportamiento(){

    //var dataComportamiento2 = getDataComportamiento();
        //console.log(dataComportamiento2);
	var dataComportamiento = [{
		name : 'Estado Delta Amacuro',
		data : []
	}],
	categorias = [];

	for (var j = 0; j < registros.length; j++) {
		dataComportamiento[0].data.push(0);
	}
     
	for (var i = 0; i < municipios.length; i++) {
		var data = [];
		for (var j = 0; j < registros.length; j++) {
			data.push(0);
		}

		dataComportamiento.push({
			name : municipios[i].nombre,
			data : data
		});
	}

	for (var j = 0; j < registros.length; j++) {
		categorias.push(registros[j].fecha_inicio + '<br />' + registros[j].fecha_final)
	}

   
	$('#comportamiento').highcharts({
		chart: {
			type: 'line'
		},
		title: {
			text: 'Comportamiento'
		},
		xAxis: {
			categories: categorias,
			tickmarkPlacement: 'on',
			title: {
				enabled: false
			}
		},
		yAxis: {
			title: {
				text: ''
			}
		},
		tooltip: {
			shared: true
		},
		plotOptions: {
			area: {
				stacking: 'normal',
				lineColor: '#666666',
				lineWidth: 1,
				marker: {
					lineWidth: 1,
					lineColor: '#666666'
				}
			}
		},
		series: dataComportamiento
	});
}

function generarGraficaComportamientoGeneral(){
	var dataComportamiento = getDataComportamientoGeneral(),
	categorias = [];

	for (var j = 0; j < registros.length; j++) {
		categorias.push(registros[j].fecha_inicio + '<br />' + registros[j].fecha_final)
	}

	$('#comportamientoGeneral').highcharts({
		chart: {
			type: 'line'
		},
		title: {
			text: 'Comportamiento de la Distribución a los Clap'
		},
		xAxis: {
			categories: categorias,
			tickmarkPlacement: 'on',
			title: {
				enabled: false
			}
		},
		yAxis: {
			title: {
				text: ''
			}
		},
		tooltip: {
			shared: true
		},
		plotOptions: {
			area: {
				stacking: 'normal',
				lineColor: '#666666',
				lineWidth: 1,
				marker: {
					lineWidth: 1,
					lineColor: '#666666'
				}
			}
		},
		series: dataComportamiento
	});
}

function number_format (number, decimals, decPoint, thousandsSep) {
	number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
	var n = !isFinite(+number) ? 0 : +number;
	var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals);
	var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep;
	var dec = (typeof decPoint === 'undefined') ? '.' : decPoint;
	var s = '';

	var toFixedFix = function (n, prec) {
		var k = Math.pow(10, prec);
		return '' + (Math.round(n * k) / k).toFixed(prec);
	}

	// @todo: for IE parseFloat(0.55).toFixed(0) = 0;
	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
	if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	}
	if ((s[1] || '').length < prec) {
		s[1] = s[1] || '';
		s[1] += new Array(prec - s[1].length + 1).join('0');
	}

	return s.join(dec);
}