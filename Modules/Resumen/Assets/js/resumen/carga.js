var aplicacion, $form, tabla;

$(function() {

	aplicacion = new app('formulario', {

		'limpiar' : function(){
			$("input", ".tabla-inventario").val('0');
			$("input", ".tabla-empresas").val('0');
		},
	});

	$form = aplicacion.form;

	$( "#fecha_inicio" ).datepicker({
		defaultDate: "+1w",
		changeMonth: true,
		onClose: function( selectedDate ) {
			$( "#fecha_final" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	$( "#fecha_final" ).datepicker({

		defaultDate: "+1w",

		changeMonth: true,

		

		onClose: function( selectedDate ) {

			$( "#fecha_inicio" ).datepicker( "option", "maxDate", selectedDate );

		}

	});

	$("input", ".tabla-inventario").each(function(){

		var municipio = this.getAttribute('data-municipio');

		this.value = 0;

	});



	$("input", ".tabla-empresas").each(function(){

		var municipio = this.getAttribute('data-empresa');

		this.value = 0;

	});



	tabla = $('#tabla')

	.on("click", "tbody tr", function(){

		buscarregistros(this.id);

	})

	.dataTable({

		processing: true,

		serverSide: true,

		ajax: $url + "datatable",

		oLanguage: datatableEspanol,

		columns: [

			{ data: 'fecha_inicio', name: 'fecha_inicio' },

			{ data: 'fecha_final', name: 'fecha_final' }

		]

	});

});



function buscarregistros(id){



	$.ajax({

		url : $url + 'buscar',

		data:{

			'id':id

		},success: function(r){				

			console.log(r[0]['id']);

			$('#id').val(r[0]['id']);

			$('#fecha_inicio').val(r[0]['fecha_inicio']);

			$('#fecha_final').val(r[0]['fecha_inicio']);

			$('#jornadas').val(r[0]['jornadas']);

			$('#familias').val(r[0]['familias']);

			$('#claps').val(r[0]['claps']);

			$('#bases').val(r[0]['bases']);

			$('#instituciones').val(r[0]['instituciones']);

			$('#punto_circulo').val(r[0]['punto_circulo']);

			$('#mision_vivienda').val(r[0]['mision_vivienda']);



			$("input", ".tabla-inventario").each(function(){

				var municipio = this.getAttribute('data-municipio'),

				campo = this.getAttribute('data-campo');

				console.log(municipio, campo);

				

				this.value = r[1][municipio][campo] || 0;

			});



			$("input", ".tabla-empresas").each(function(){

				var empresa = this.getAttribute('data-empresa'),

				campo2 = this.getAttribute('data-campo2');

				this.value = r[2][empresa][campo2] || 0;

			});

		},

		complete : function(x,e,o){

			ajaxComplete(x,e,o);

			$('#modalTablaBusqueda').modal('hide');

		}

	});

}