<?php



/*

|--------------------------------------------------------------------------

| Web Routes

|--------------------------------------------------------------------------

|

| This file is where you may define all of the routes that are handled

| by your module. Just tell Laravel the URIs it should respond

| to using a Closure or controller method. Build something great!

|

*/



Route::group(['prefix' => Config::get('admin.prefix')], function() {



	

	Route::group(['prefix' => 'resumen'], function(){ 

		Route::group(['prefix' => 'reporte'], function(){ 
			Route::get('/', 				'ReporteController@index');
			Route::post('imprimir',			'ReporteController@imprimir');
		});	

		Route::group(['prefix' => 'carga'], function(){ 
			Route::get('/', 				'ResumenController@index');
			Route::post('guardar',			'ResumenController@guardar');

			Route::put('guardar/{id}',		'ResumenController@actualizar');

			Route::get('buscar/{id}', 		'ResumenController@buscar');

			Route::get('buscar', 			'ResumenController@buscar');

			Route::get('datatable', 		'ResumenController@datatable');	

			Route::delete('eliminar/{id}', 	'ResumenController@eliminar');

			Route::post('archivo', 			'ResumenController@archivo');



			Route::get('carga-empresa', 	'ResumenController@cargaEmpresa');

			//resumen/carga/carga-empresa

		});	

	});



    //{{route}}

});