@extends('admin::layouts.default')
@section('content')
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url('/') }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			Resumen <i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Reporte</span>
		</li>
	</ul>
	
	<div class="row">
		<div class="col-md-12">
			<div id="fecha_reporte"></div>
			<div id="slider" class="noUi-danger"></div>
		</div>
	</div>

	<div class="row" style="margin-top: 50px;">
		<div id="mapa" class="col-md-6"></div>
		{{ Form::bsSelect('tipo_grafica', [
			'jornadas' => 'Jornadas Realizadas',
			'distribucion' => 'Toneladas Metricas Distribuidas',
			'familias' => 'Familias Atendidas',
			'personas' => 'Personas Atendidas',
			'densidad' => 'Densidad Poblacional',
			'requerimiento' => 'Requerimiento'
		] ,'', [
			'label' 		=> 'Tipo de Dato',
			'class_cont' 	=> 'col-sm-6'
		]) }}
		<div class="col-md-6">
			<div id="info_resumen"></div>
		</div>

		<div id="comportamiento" class="col-md-12"></div>

		{{ Form::bsSelect('tipo_grafica_comportamiento', [
			'clap' => 'Claps Atendidos',
			'jornadas' => 'Jornadas Realizadas',
			'distribucion' => 'Toneladas Metricas Distribuidas',
			'familias' => 'Familias Atendidas'
		] ,'', [
			'label' 		=> false,
			'class_cont' 	=> 'col-md-4 col-sm-6'
		]) }}
		<div id="comportamientoGeneral" class="col-md-12"></div>
	</div>
@endsection


@push('js')
<script type="text/javascript">
<?php $data = $controller->registros(); ?>

municipios = {!! json_encode($data['municipios']) !!};
empresas = {!! json_encode($data['empresas']) !!};
registros = {!! json_encode($data['registros']) !!};


</script>
@endpush