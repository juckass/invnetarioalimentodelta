@extends('admin::layouts.default')
@section('content')
	@include('admin::partials.botonera')

	{{ Form::bsModalBusqueda([
		'Desde' => '50',
		'Hasta' => '50'
	]) }}
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url('/') }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			Resumen <i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Resumen</span>
		</li>
	</ul>
	
	<div class="row">
		{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
			<div class="col-lg-12" id="resumen">
				<div class="panel  borde">
					<div class="panel-heading bg-red-thunderbird bg-font-red-thunderbird">
						<h3 class="panel-title">Resumen</h3>
					</div>
					<div class="panel-body">
						
					{{ Form::bsText('fecha_inicio', '', [
						'label' => 'Desde',
						'required' => 'required'
					]) }}

					{{ Form::bsText('fecha_final','', [
						'label' => 'Hasta',
						'required' => 'required'
					]) }}

					{{ Form::bsNumber('jornadas', '', [
						'label' => 'Jornadas',
						'placeholder' => 'Jornadas Realizadas',
						'required' => 'required'
					]) }}	

					{{ Form::bsNumber('familias', '', [
						'label' => 'Familias',
						'placeholder' => 'Familias Atendidas',
						'required' => 'required'
					]) }}

					{{ Form::bsNumber('claps', '', [
						'label' => 'Claps',
						'placeholder' => 'Claps Atendidos',
						'required' => 'required'
					]) }}

					{{ Form::bsNumber('bases', '', [
						'label' => 'Bases de Misiones',
						'placeholder' => 'Bases de Misiones Atendidas',
						'required' => 'required'
					]) }}

					{{ Form::bsNumber('instituciones', '', [
						'label' => 'Instituciones',
						'placeholder' => 'Instituciones Atendidas',
						'required' => 'required'
					]) }}

					{{ Form::bsNumber('mision_vivienda', '', [
						'label' => 'Gran Misión Vivienda Venezuela',
						'placeholder' => 'Gran Misión Vivienda Venezuela',
						'required' => 'required'
					]) }}

					{{ Form::bsNumber('punto_circulo', '', [
						'label' => 'Punto y Circulo',
						'placeholder' => 'Punto y Circulo Atendidos',
						'required' => 'required'
					]) }}

					</div>
				</div>
			</div>
			<div class="col-lg-12" id="">
				<div class="panel borde">
					<div class="panel-heading bg-font-red-thunderbird bg-red-thunderbird ">
						<h3 class="panel-title">Resumen Distribucion</h3>
					</div>
					<div class="panel-body">
						<div class="col-lg-12">
							<table border="0" class="table table-striped table-hover tabla-inventario">
								<thead>
									<tr>
										<th class="text-center">Municipios</th>
										<th class="text-center">Jornadas</th>
										<th class="text-center">Jornadas Acumulado</th>
										<th class="text-center">Familias</th>
										<th class="text-center">Familias Acumulado</th>
										<th class="text-center">Cantidad</th>
										<th class="text-center">Cantidad Acumulado</th>
									</tr>
								</thead>
								<tbody>
									@foreach($municipios as $municipio)
										<tr>
											<td>{{$municipio->nombre}}</td>

											<td class="cont_input">
												<input value="0,00" name="muni[{{ $municipio->id }}][jornadas]" type="text" data-municipio="{{ $municipio->id }}" data-campo="jornadas" />
											</td>

											<td class="cont_input">
												<input value="0,00" name="muni[{{ $municipio->id }}][jornadas_acumulado]" type="text" data-municipio="{{ $municipio->id }}"  data-campo="jornadas_acumulado" />
											</td>

											<td class="cont_input">
												<input value="0,00" name="muni[{{ $municipio->id }}][familias]" type="text" data-municipio="{{ $municipio->id }}" data-campo="familias" />
											</td>

											<td class="cont_input">
												<input value="0,00" name="muni[{{ $municipio->id }}][familias_acumulado]" type="text" data-municipio="{{ $municipio->id }}" data-campo="familias_acumulado" />
											</td>

											<td class="cont_input">
												<input value="0,00" name="muni[{{ $municipio->id }}][cantidad]" type="text" data-municipio="{{ $municipio->id }}" data-campo="cantidad" />
											</td>

											<td class="cont_input">
												<input value="0,00" name="muni[{{ $municipio->id }}][cantidad_acumulado]" type="text" data-municipio="{{ $municipio->id }}" data-campo="cantidad_acumulado" />
											</td>
										</tr>
									@endforeach 
								</tbody>
							</table>
						</div>		
					</div>
				</div>
			</div>
			<div class="col-md-12" id="resumen">
				<div class="panel  borde">
					<div class="panel-heading bg-font-red-thunderbird bg-red-thunderbird">
						<h3 class="panel-title">Resumen Distribucion Empresas</h3>
					</div>
					<div class="panel-body">			
						<table border="0" class="table table-striped table-hover tabla-empresas">
							<thead>
								<tr>
									<th class="text-center">Empresas</th>
									<th class="text-center">Cantidad</th>
									<th class="text-center">Cantidad Acumulado</th>
									<th class="text-center">Claps Atendidos</th>
									<th class="text-center">Familias Atendidas</th>
									<th class="text-center">Jornadas Realizadas</th>
								</tr>
							</thead>
							<tbody>
								@foreach($empresas as $empresa)
									<tr>
										<td>{{$empresa->nombre}}</td>

										<td class="cont_input">
											<input value="0,00" name="inv[{{ $empresa->id }}][cantidad]" type="text" data-empresa="{{ $empresa->id }}" data-campo2="cantidad" />
										</td>

										<td class="cont_input">
											<input value="0,00" name="inv[{{ $empresa->id }}][cantidad_acumulado]" type="text" data-empresa="{{ $empresa->id }}" data-campo2="cantidad_acumulado" />
										</td>

										<td class="cont_input">
											<input value="0,00" name="inv[{{ $empresa->id }}][claps]" type="text" data-empresa="{{ $empresa->id }}" data-campo2="claps" />
										</td>

										<td class="cont_input">
											<input value="0,00" name="inv[{{ $empresa->id }}][familias]" type="text" data-empresa="{{ $empresa->id }}" data-campo2="familias" />
										</td>

										<td class="cont_input">
											<input value="0,00" name="inv[{{ $empresa->id }}][jornadas]" type="text" data-empresa="{{ $empresa->id }}" data-campo2="jornadas" />
										</td>
									</tr>
								@endforeach 
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<input type="hidden" name="id" id="id" value="" />		
		{!! Form::close() !!}
	</div>
@endsection
@push('css')
<style>
	.bg-font-red-thunderbird {
	    color: #fff!important;
	}
	
	.bg-red-thunderbird {
	    background: #D91E18!important;
	}
	.borde{
		border: 1px transparent!important; 
	}
</style>
@endpush