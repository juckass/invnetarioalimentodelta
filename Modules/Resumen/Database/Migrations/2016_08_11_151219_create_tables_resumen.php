<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesResumen extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('resumen', function(Blueprint $table)
		{
			$table->increments('id');

			$table->date('fecha_inicio')->unique();
			$table->date('fecha_final')->unique();

			$table->integer('jornadas')->unsigned()->comment('Cantidad de Jornadas Realizadas');
			$table->integer('familias')->unsigned()->comment('Cantidad de Familias Atendidas');
			$table->integer('claps')->unsigned()->comment('Cantidad de Claps Atendidos');
			$table->integer('mision_vivienda')->unsigned()->comment('Gran Misión Vivienda');
			$table->integer('bases')->unsigned()->comment('Cantidad de Bases de Misiones Atendidas');
			$table->integer('instituciones')->unsigned()->comment('Cantidad de Instituciones Atendidas');
			$table->integer('punto_circulo')->unsigned()->comment('Cantidad de Punto y Circulo Atendidas');
			 
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('resumen_distribucion', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('municipio_id')->unsigned();
			$table->integer('resumen_id')->unsigned();
			$table->integer('jornadas')->unsigned();
			$table->integer('jornadas_acumulado')->unsigned();
			
			$table->integer('familias')->unsigned();
			$table->integer('familias_acumulado')->unsigned();

			$table->decimal('cantidad', 8, 2)->unsigned();
			$table->decimal('cantidad_acumulado', 8, 2)->unsigned();
			
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('municipio_id')
				->references('id')->on('municipio')
				->onDelete('cascade')->onUpdate('cascade');

			$table->foreign('resumen_id')
				->references('id')->on('resumen')
				->onDelete('cascade')->onUpdate('cascade');	
		});

		Schema::create('resumen_distribucion_empresas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('resumen_id')->unsigned();
			$table->integer('empresas_id')->unsigned();

			$table->decimal('cantidad', 8, 2)->comment('Cantidad de Alimentos Distribuidos en Toneladas');
			$table->decimal('cantidad_acumulado', 8, 2)->comment('Cantidad de Alimentos Distribuidos Acumulados en Toneladas');
			
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('resumen_id')
				->references('id')->on('resumen')
				->onDelete('cascade')->onUpdate('cascade');

			$table->foreign('empresas_id')
				->references('id')->on('empresas')
				->onDelete('cascade')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('resumen_distribucion_empresas');
		Schema::dropIfExists('resumen_distribucion');
		Schema::dropIfExists('resumen');
	}

}
