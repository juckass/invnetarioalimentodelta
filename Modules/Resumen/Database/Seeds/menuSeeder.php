<?php namespace Modules\Resumen\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Admin\Model\app_menu as menu;

class menuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = menu::create([
			'nombre' => 'Resumen',
			'padre' => 0,
			'posicion' => 4,
			'direccion' => '#Resumen',
			'icono' => 'fa fa-info'
		]);

			menu::create([
				'nombre' => 'Carga',
				'padre' => $menu->id,
				'posicion' => 1,
				'direccion' => 'resumen/carga',
				'icono' => 'fa fa-upload'
			]);

			menu::create([
				'nombre' => 'Reporte',
				'padre' => $menu->id,
				'posicion' => 2,
				'direccion' => 'resumen/reporte',
				'icono' => 'fa fa-file-pdf-o'
			]);

    }
}



