<?php

$menu['resumen'] = [
	[
		'nombre' 	=> 'Resumen',
		'direccion' => '#Resumen',
		'icono' 	=> 'fa fa-info',
		'menu' 		=> [
			[
				'nombre' 	=> 'Carga',
				'direccion' => 'resumen/carga',
				'icono' 	=> 'fa fa-upload'
			],
			[
				'nombre' 	=> 'Reporte',
				'direccion' => 'resumen/reporte',
				'icono' 	=> 'fa fa-file-pdf-o'
			]	
		]	
	]	
];
