<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => Config::get('admin.prefix')], function() {
	Route::group(['prefix' => 'proyectos'], function() {
		Route::get('escritorio',				'ProyectosController@escritorio');
		Route::get('escritorio/data',			'ProyectosController@data');

		Route::get('/', 						'ProyectosController@index');
		Route::get('nuevo', 					'ProyectosController@nuevo');
		Route::get('cambiar/{id}', 				'ProyectosController@cambiar');
		Route::get('tareas/{id}', 				'ProyectosController@tareas');
		Route::get('tareas/{id}/data_gantt', 	'ProyectosController@data_gantt');
		
		Route::get('buscar/{id}', 				'ProyectosController@buscar');
		Route::get('carga', 					'ProyectosController@carga');

		Route::post('archivo',					'ProyectosController@carga');
		Route::post('guardar',					'ProyectosController@guardar');
		Route::put('guardar/{id}', 				'ProyectosController@guardar');

		Route::delete('eliminar/{id}', 			'ProyectosController@eliminar');
		Route::post('restaurar/{id}', 			'ProyectosController@restaurar');
		Route::delete('destruir/{id}', 			'ProyectosController@destruir');

		Route::get('datatable', 				'ProyectosController@datatable');
	});


	Route::group(['prefix' => 'fuente_financiamiento'], function() {
		Route::get('/', 				'FuenteFinanciamientoController@index');
		Route::get('nuevo', 			'FuenteFinanciamientoController@nuevo');
		Route::get('cambiar/{id}', 		'FuenteFinanciamientoController@cambiar');
		
		Route::get('buscar/{id}', 		'FuenteFinanciamientoController@buscar');

		Route::post('guardar',			'FuenteFinanciamientoController@guardar');
		Route::put('guardar/{id}', 		'FuenteFinanciamientoController@guardar');

		Route::delete('eliminar/{id}', 	'FuenteFinanciamientoController@eliminar');
		Route::post('restaurar/{id}', 	'FuenteFinanciamientoController@restaurar');
		Route::delete('destruir/{id}', 	'FuenteFinanciamientoController@destruir');

		Route::get('datatable', 		'FuenteFinanciamientoController@datatable');
	});


	Route::group(['prefix' => 'sector_primario'], function() {
		Route::get('/', 				'SectorPrimarioController@index');
		Route::get('nuevo', 			'SectorPrimarioController@nuevo');
		Route::get('cambiar/{id}', 		'SectorPrimarioController@cambiar');
		
		Route::get('buscar/{id}', 		'SectorPrimarioController@buscar');

		Route::post('guardar',			'SectorPrimarioController@guardar');
		Route::put('guardar/{id}', 		'SectorPrimarioController@guardar');

		Route::delete('eliminar/{id}', 	'SectorPrimarioController@eliminar');
		Route::post('restaurar/{id}', 	'SectorPrimarioController@restaurar');
		Route::delete('destruir/{id}', 	'SectorPrimarioController@destruir');

		Route::get('datatable', 		'SectorPrimarioController@datatable');
	});


	Route::group(['prefix' => 'ente_ejecutor'], function() {
		Route::get('/', 				'EnteEjecutorController@index');
		Route::get('nuevo', 			'EnteEjecutorController@nuevo');
		Route::get('cambiar/{id}', 		'EnteEjecutorController@cambiar');
		
		Route::get('buscar/{id}', 		'EnteEjecutorController@buscar');

		Route::post('guardar',			'EnteEjecutorController@guardar');
		Route::put('guardar/{id}', 		'EnteEjecutorController@guardar');

		Route::delete('eliminar/{id}', 	'EnteEjecutorController@eliminar');
		Route::post('restaurar/{id}', 	'EnteEjecutorController@restaurar');
		Route::delete('destruir/{id}', 	'EnteEjecutorController@destruir');

		Route::get('datatable', 		'EnteEjecutorController@datatable');
	});


	Route::group(['prefix' => 'estatus'], function() {
		Route::get('/', 				'EstatusController@index');
		Route::get('nuevo', 			'EstatusController@nuevo');
		Route::get('cambiar/{id}', 		'EstatusController@cambiar');
		
		Route::get('buscar/{id}', 		'EstatusController@buscar');

		Route::post('guardar',			'EstatusController@guardar');
		Route::put('guardar/{id}', 		'EstatusController@guardar');

		Route::delete('eliminar/{id}', 	'EstatusController@eliminar');
		Route::post('restaurar/{id}', 	'EstatusController@restaurar');
		Route::delete('destruir/{id}', 	'EstatusController@destruir');

		Route::get('datatable', 		'EstatusController@datatable');
	});

	Route::group(['prefix' => 'reporte'], function() {
		Route::get('/', 					'ReportesController@index');
		Route::get('imprimir', 				'ReportesController@imprimir');
		Route::post('imprimir', 			'ReportesController@imprimir');
		Route::get('imprimir-escritorio',	'ReportesController@imprimirEscritorio');
		Route::get('parroquias/{id}', 		'ReportesController@parroquias')->where('id', '[\d\,]+');
	});

	Route::group(['prefix' => 'fichas'], function() {
		Route::get('/', 					'FichasController@index');
		Route::get('imprimir', 				'FichasController@imprimir');
		Route::post('imprimir', 			'FichasController@imprimir');
		Route::get('imprimir-escritorio',	'FichasController@imprimirEscritorio');
		Route::get('parroquias/{id}', 		'FichasController@parroquias')->where('id', '[\d\,]+');
	});

	Route::group(['prefix' => 'grafica'], function() {
		Route::get('/', 				'GraficosController@index');
		Route::get('grafica', 			'GraficosController@grafica');
		Route::get('grafica-torta', 	'GraficosController@graficaTorta');
		Route::get('parroquias/{id}', 	'GraficosController@parroquias');
	});


    //{{route}}
});