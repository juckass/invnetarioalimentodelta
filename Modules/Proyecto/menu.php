<?php

$menu['clap'] = [
	[
		'nombre' 	=> 'Clap',
		'direccion' => '#Clap',
		'icono' 	=> 'fa fa-bullseye',
		'menu' 		=> [
			[
				'nombre' 	=> 'Definiciones',
				'direccion' => '#Definicionesclap',
				'icono' 	=> 'fa fa-list',
				'menu' 		=>[
					[
						'nombre' 	=> 'Estados',
						'direccion' => 'clap/definiciones/estados',
						'icono' 	=> 'fa fa-map'
					],	

					[
						'nombre' 	=> 'Ciudades',
						'direccion' => 'clap/definiciones/ciudades',
						'icono' 	=> 'fa fa-map'
					],
					[
						'nombre' 	=> 'Municipio',
						'direccion' => 'clap/definiciones/municipio',
						'icono' 	=> 'fa fa-map'
					],
					[ 
						'nombre' 	=> 'Parroquia',
						'direccion' => 'clap/definiciones/parroquia',
						'icono' 	=> 'fa fa-map-marker'
					],
					[ 
						'nombre' 	=> 'Sector',
						'direccion' => 'clap/definiciones/sector',
						'icono' 	=> 'fa fa-home'
					]
				]			
			],
			[
				'nombre' 	=> 'clap',
				'direccion' => 'clap/carga',
				'icono' 	=> 'fa fa-truck',
			],
			[
				'nombre' 	=> 'Jornadas',
				'direccion' => 'clap/jornadas',
				'icono' 	=> 'fa fa-cart-arrow-down',
			],
			[
				'nombre' 	=> 'Reportes',
				'direccion' => 'clap/reportes',
				'icono' 	=> 'fa fa-truck',
			]
		]
	]
];