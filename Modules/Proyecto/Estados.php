<?php

namespace Modules\Clap\Model;

use Modules\Admin\Model\Modelo;

class Estados extends modelo
{
    protected $table = 'estados';
    protected $fillable = ["nombre","iso_3166-2"];
    protected $campos = [
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Estados'
        ],
        'iso_3166-2' => [
            'type' => 'text',
            'label' => 'Iso 3166-2',
            'placeholder' => 'Iso 3166-2 del Estados'
        ]
    ];

    public function proyectos()
	{
		return $this->hasMany('Modules\Proyecto\Model\Proyectos');
	}
}