@extends(isset($layouts) ? $layouts : 'admin::layouts.default')

@section('content')
    <div class="row">
        <div id="diagrama_gantt" class="col-md-12" style='height:250px;'></div>
    </div>
@endsection

@push('js')
<script type="text/javascript">
    /* chart configuration and initialization */
    gantt.config.xml_date = "%Y-%m-%d %H:%i:%s";
    gantt.config.step = 1;
    gantt.config.scale_unit= "day";
    gantt.init("diagrama_gantt", new Date(2017,0,1), new Date(2017,12,31));
    /* refers to the 'data' action that we will create in the next substep */
    gantt.load($url + "data_gantt", "xml");
    /* refers to the 'data' action as well */
    var dp = new gantt.dataProcessor("./data_gantt");
    dp.init(gantt);
</script>
@endpush