@extends(isset($layouts) ? $layouts : 'admin::layouts.default')

@section('content-top')
    @include('admin::partials.botonera')
    
    @include('admin::partials.ubicacion', ['ubicacion' => ['Ente Ejecutor']])
    
    @include('admin::partials.modal-busqueda', [
        'titulo' => 'Buscar EnteEjecutor.',
        'columnas' => [
            'Nombre' => '100'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $EnteEjecutor->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection