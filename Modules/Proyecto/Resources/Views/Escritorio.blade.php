@extends('admin::layouts.default')
@section('content')
	@include('admin::partials.ubicacion', ['ubicacion' => ['Proyectos', 'Escritorio']])

    <div id="app_escritorio">
        <div class="row">
            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                <label>A&ntilde;o de Ejecuci&oacute;n</label>
                <select id="ano" class="form-control" v-model="ano">
                    <option value="0">Todos los A&ntilde;os</option>
                    <option v-for="anno in annos" v-bind:value="anno.value">
                        @{{ anno.text }}
                    </option>
                </select>
            </div>

            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                <label>Estatus</label>
                <select id="estatu" class="form-control" v-model="estatu">
                    <option value="0">- Seleccione</option>
                    <option v-for="est in estatus" v-bind:value="est.id">
                        @{{ est.nombre }}
                    </option>
                </select>
            </div>

            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                <label>Sector Primario</label>
                <select id="sector_primario" class="form-control" v-model="sector_primario">
                    <option value="0">- Seleccione</option>
                    <option v-for="sector in sectores" v-bind:value="sector.id">
                        @{{ sector.nombre }}
                    </option>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-md-offset-3 ">
                <div class="dashboard-stat red">
                    <div class="visual">
                        <i class="fa fa-money"></i>
                    </div>
                    <div class="details">
                        <div class="number"> @{{ total_inversion.substring(0, total_inversion.length - 7).replace(/(.+).(\d{3})/, "\$1,\$2") }} <small style="font-size: 20px;">M Bs</small> </div>
                        <div class="desc"> Total Inversi&oacute;n </div>
                    </div>
                    <a class="more" href="javascript:;"> Total Inversi&oacute;n
                        <i class="m-icon-swapright m-icon-white"></i>
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <div class="dashboard-stat yellow">
                            <div class="visual">
                                <i class="fa fa-folder-open-o"></i>
                            </div>
                            <div class="details">
                                <div class="number"> @{{ total_proyectos }} </div>
                                <div class="desc"> Proyectos </div>
                            </div>
                            <a class="more" @click="cambiarEstatus(0)" href="javascript:;"> Todos los Proyectos
                                <i class="m-icon-swapright m-icon-white"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="dashboard-stat purple">
                            <div class="visual">
                                <i class="fa fa-line-chart"></i>
                            </div>
                            <div class="details">
                                <div class="number"> @{{ total_proyectos_ejecucion }} </div>
                                <div class="desc"> En Ejecuci&oacute;n </div>
                            </div>
                            <a class="more" @click="cambiarEstatus(2)" href="javascript:;"> Proyectos en Ejecuci&oacute;n
                                <i class="m-icon-swapright m-icon-white"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="dashboard-stat green">
                            <div class="visual">
                                <i class="fa fa-thumbs-o-up"></i>
                            </div>
                            <div class="details">
                                <div class="number"> @{{ total_proyectos_concluidos }} </div>
                                <div class="desc"> Concluidos </div>
                            </div>
                            <a class="more" @click="cambiarEstatus(1)" href="javascript:;"> Proyectos Concluidos
                                <i class="m-icon-swapright m-icon-white"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="dashboard-stat green-dark">
                            <div class="visual">
                                <i class="fa fa-play"></i>
                            </div>
                            <div class="details">
                                <div class="number"> @{{ total_proyectos_iniciar }} </div>
                                <div class="desc"> Por Iniciar </div>
                            </div>
                            <a class="more" @click="cambiarEstatus(3)" href="javascript:;"> Proyectos por Iniciar
                                <i class="m-icon-swapright m-icon-white"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div id="grafica"></div>
            </div>
    	</div>

        <div class="row" style="margin-top: 20px;">
            <div class="col-md-4 col-sm-6">
                <div class="portlet light ">
                    <div class="portlet-title tabbable-line">
                        <div class="caption">
                            <i class="icon-globe font-dark hide"></i>
                            <span class="caption-subject font-green-steel bold uppercase">Distribuci&oacute;n por Sector</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <ul class="feeds">
                            <li v-for="sector in sectores">
                                <div class="col1">
                                    <div class="cont">
                                        <div class="cont-col1">
                                            <div class="label label-sm label-info">
                                                <i class="fa " v-bind:class="[sector.icono]"></i>
                                            </div>
                                        </div>
                                        <div class="cont-col2">
                                            <div class="desc">
                                                <a :href="'{{ url('reporte') }}?sector_primario_id=' + sector.id + '&ano=' + ano + '&estatus_id=' + estatu" target="_blank"> 
                                                    @{{ sector.nombre }}.
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col2">
                                    <div class="date"> @{{ sector.cantidades[estatu] }} </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-8 col-sm-6">
                <div class="portlet light ">
                    <div class="portlet-title tabbable-line">
                        <div class="caption">
                            <i class="icon-globe font-dark hide"></i>
                            <span class="caption-subject font-green-steel bold uppercase">Distribución por Municipio</span>
                        </div>
                    </div>
                    <div class="portlet-body" style="position: relative;">
                        <div style="position: absolute; top: 25px; left: 60px; z-index: 10000; font-weight: bold;">
                            Ámbito Estatal: @{{ total_proyectos_estatal }}
                        </div>
                        <div id="mapa"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="margin-top: 20px;">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet-title tabbable-line">
                        <div class="caption">
                            <i class="icon-globe font-dark hide"></i>
                            <span class="caption-subject font-green-steel bold uppercase">Grafica de Distribuci&oacute;n por Sector</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="graficaSectores"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
<style type="text/css">
.dashboard-stat.green .visual>i {
    color: #FFF;
    opacity: 0.2;
    filter: alpha(opacity=20);
}

.feeds li .col2>.date {
    font-style: normal;
    color: #616161;
}
</style>
@endpush

@push('js')
<script type="text/javascript">
var datos = {
    total_proyectos           : '{{ $total_proyectos }}',
    total_proyectos_ejecucion : '{{ $total_proyectos_ejecucion }}',
    total_proyectos_concluidos: '{{ $total_proyectos_concluidos }}',
    total_proyectos_iniciar   : '{{ $total_proyectos_iniciar }}',
    total_inversion           : '{{ $total_inversion }}',
    total_proyectos_estatal   : '{{ $total_proyectos_estatal }}',
    sectores                  : {!! json_encode($sectores) !!},
    municipios                : {!! json_encode($municipios) !!},
    annos                     : {!! json_encode($annos) !!},
    estatus                   : {!! json_encode($estatus) !!},
    ano                       : 0,
    estatu                    : 0,
    sector_primario           : 0
};
var dire = "{{ url('reporte')}}";   
</script>
@endpush