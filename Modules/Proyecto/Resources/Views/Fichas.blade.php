@extends('admin::layouts.default')
@section('content')
	<div id="botonera">
		<button id="imprimir" class="btn btn-info" title="{{ Lang::get('backend.btn_group.print.title') }}">
			<i class="fa fa-print"></i>
			<span class="visible-lg-inline visible-md-inline">{{ Lang::get('backend.btn_group.print.btn') }}</span>
		</button>
	</div>

	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Proyectos<i class="fa fa-circle"></i></span>
		</li>
		<li>
			<span>Reportes</span>
		</li>
	</ul>

	<div class="row">
		{!! Form::open([
			'id' => 'formulario',
			'method' => 'post',
			'target' => '_blank',
			'url' => 'fichas/imprimir'
		]) !!}
			{!! $Proyectos->generate(["numero_certificacion", "ano_ejecucion", "estatus_id", "fuente_financiamiento_id", "ente_ejecutor_id", "municipio_id", "parroquia_id", "sector_primario_id"]) !!}
		{!! Form::close() !!}
	</div>

	<div id="reportehtml"></div>
@endsection

@push('js')
<script type="text/javascript">
var sector_primario_id = '{{ $sector_primario_id }}',
    ano = '{{ $ano }}'
    estatus_id = '{{ $estatus_id }}'
    municipio_id = '{{$municipio_id}}';
</script>
@endpush