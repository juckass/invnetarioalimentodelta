@extends(isset($layouts) ? $layouts : 'admin::layouts.default')

@section('content-top')
    @include('admin::partials.botonera')
    
    @include('admin::partials.ubicacion', ['ubicacion' => ['Parroquia']])
    
    @include('admin::partials.modal-busqueda', [
        'titulo' => 'Buscar Parroquia.',
        'columnas' => [
            'Municipio' => '50',
            'Nombre' => '50'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Parroquia->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection