@extends('admin::layouts.default')
@section('content')
	<div id="botonera">
		<button id="buscar" class="btn green" title="{{ Lang::get('backend.btn_group.search.title') }}">
			<i class="fa fa-search"></i>
			<span class="visible-lg-inline visible-md-inline">{{ Lang::get('backend.btn_group.search.btn') }}</span>
		</button>

		<button id="exportar" class="btn purple-studio" title="Exportar a Excel">
			<i class="fa fa-file-excel-o"></i>
			<span class="visible-lg-inline visible-md-inline">Exportar</span>
		</button>

		<button id="imprimir" class="btn btn-info" title="{{ Lang::get('backend.btn_group.print.title') }}">
			<i class="fa fa-print"></i>
			<span class="visible-lg-inline visible-md-inline">{{ Lang::get('backend.btn_group.print.btn') }}</span>
		</button>
	</div>

	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Proyectos<i class="fa fa-circle"></i></span>
		</li>
		<li>
			<span>Reportes</span>
		</li>
	</ul>

	<div class="row">
		{!! Form::open([
			'id' => 'formulario',
			'method' => 'post',
			'target' => '_blank',
			'url' => 'reporte/imprimir'
		]) !!}
			<input type="hidden" name="excel" value="0" />
			<input type="hidden" name="html" value="0" />
			
			{!! $Proyectos->generate(["numero_certificacion"])	!!}


			{{ Form::bsSelect('ano_ejecucion', $controller->annos(), '', [
				'label' => 'Año de ejecución',
				'class' => 'bs-select',
				'multiple' => 'multiple',
				'name'    => 'ano_ejecucion[]'
			]) }}

			{{ Form::bsSelect('estatus_id', $controller->estatus(), '', [
				'label' => 'Estatus &nbsp;&nbsp;&nbsp;&nbsp;',
				'class' => 'bs-select',
				'multiple' => 'multiple',
				'name'    => 'estatus_id[]'
			]) }}

			{{ Form::bsSelect('fuente_financiamiento_id', $controller->fuenteFinanciamiento(), '', [
				'label' => 'Fuente Financiamiento',
				'class' => 'bs-select',
				'multiple' => 'multiple',
				'name'    => 'fuente_financiamiento_id[]'
			]) }}

			{{ Form::bsSelect('ente_ejecutor_id', $controller->enteEjecutor(), '', [
				'label' => 'Ente Ejecutor',
				'class' => 'bs-select',
				'multiple' => 'multiple',
				'name'    => 'ente_ejecutor_id[]'
			]) }}

			{{ Form::bsSelect('municipio_id', $controller->municipio(), '', [
				'label' => 'Municipio &nbsp;&nbsp;&nbsp;',
				'class' => 'bs-select',
				'multiple' => 'multiple',
				'name'    => 'municipio_id[]'
			]) }}

			{{ Form::bsSelect('parroquia_id', $controller->parroquia(), '', [
				'label' => 'Parroquia &nbsp;&nbsp;&nbsp;',
				'class' => 'bs-select',
				'multiple' => 'multiple',
				'name'    => 'parroquia_id[]'
			]) }}

			{{ Form::bsSelect('sector_primario_id', $controller->sectorPrimario(), '', [
				'label' => 'Sector Primario',
				'class' => 'bs-select',
				'multiple' => 'multiple',
				'name'    => 'sector_primario_id[]'
			]) }}
		{!! Form::close() !!}
	</div>

	<div id="reporteHtml" v-show="proyectos.length > 0">
		<div class="header clearfix">
			<div id="logo">
				<img src="{{ url('public/img/logos/logo.png') }}" style="width: 4cm;">
			</div>
			<h1>REPORTE</h1>
			<div id="project">
				<div></div>
			</div>
			<div id="company" class="clearfix">
				<div><span>Fecha de impresi&oacute;n</span> {{ date('d/m/Y') }}</div>
			</div>
		</div>
		<div class="main">  
			<table>
				<thead>
					<tr>
						<th class="service" style="width: 2%">#</th>
						<th class="service" style="width: 30%">NOMBRE</th>
						<th class="service" style="width: 9%">A&Ntilde;O</th>
						<th class="service" style="width: 9%">ESTATUS</th>
						<th class="service" style="width: 10%">ENTE EJECUTOR</th>
						<th class="service" style="width: 10%">MUNICIPIO</th>
						<th class="service" style="width: 10%">PARROQUIA</th>
						<th class="service" style="width: 10%">AVANCE</th>
						<th class="service" style="width: 10%">INVERSION</th>
					</tr>
				</thead>
				<tbody>
					<tr v-for="proyecto in proyectos">
						<td class="service" style="width: 2%">@{{ proyecto[0] }}</td>
						<td class="service" style="width: 30%">@{{ proyecto[1] }}</td>
						<td class="service" style="width: 9%">@{{ proyecto[2] }}</td>
						<td class="service" style="width: 9%">@{{ proyecto[3] }}</td>
						<td class="service" style="width: 10%">@{{ proyecto[4] }}</td>
						<td class="service" style="width: 10%">@{{ proyecto[5] }}</td>
						<td class="service" style="width: 10%">@{{ proyecto[6] }}</td>
						<td class="service" style="width: 10%">@{{ proyecto[7] }}%</td>
						<td class="service" style="width: 10%; text-align: right;">@{{ proyecto[8] }}</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<th class="service" colspan="7" style="text-align: right;">Total de Inversi&oacute;n: </th>
						<th class="service" style="text-align: right;">@{{ total }}</th>
					</tr>
				</tfoot>
			</table>	
		</div>
		<div class="footer">
			Gobernaci&oacute;n del estado Bol&iacute;var
		</div>
	</div>
@endsection

@push('css')
<style type="text/css">
	.bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
		width: 100% !important;
	}
</style>
@endpush

@push('js')
<script type="text/javascript">
var sector_primario_id = '{{ $sector_primario_id }}',
	ano = '{{ $ano }}'
	estatus_id = '{{ $estatus_id }}'
	municipio_id = '{{$municipio_id}}';
</script>
@endpush