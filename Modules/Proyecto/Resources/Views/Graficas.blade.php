@extends('admin::layouts.default')
@section('content')
	<div id="botonera">
		<button id="btn_graficar" class="btn btn-app btn-info" title="Graficar">
			<i class="fa fa-bar-chart"></i>
			<span class="visible-lg-inline visible-md-inline">Graficar</span>
		</button>
	</div>

	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Graficas</span>
		</li>
	</ul>

	<div class="row">
		{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST']) !!}
			
		{!! Form::close() !!}

		<div class="portlet box blue col-sm-12">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-bar-chart"></i>
					<span class="caption-subject bold uppercase"> Proyectos</span>
					<span class="caption-helper font-white">{{ $controller->cantidad() }}</span>
				</div>
			</div>
			<div class="portlet-body tabs-below">
				<div id="grafica"></div>
			</div>
		</div>

		
		<div class="portlet box blue col-sm-12">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-bar-chart"></i>
					<span class="caption-subject bold uppercase"> Cantidad de Proyectos por Municipio</span>
				</div>
			</div>
			<div class="portlet-body tabs-below">
				<div id="grafica_torta"></div>
			</div>
		</div>
	</div>
@endsection