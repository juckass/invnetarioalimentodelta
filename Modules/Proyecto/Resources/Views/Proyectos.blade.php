@extends(isset($layouts) ? $layouts : 'admin::layouts.default')

@push('botones')
<a id="tareas" href="#" target="_black" class="btn green-soft tooltips" data-container="body" data-placement="top" data-original-title="Tareas">
    <i class="fa fa-tasks" aria-hidden="true"></i>
    <span class="visible-lg-inline visible-md-inline">Tareas</span>
</a>
@endpush

@section('content-top')
    @include('admin::partials.botonera')
    
    @include('admin::partials.ubicacion', ['ubicacion' => ['Proyectos']])
    
    @include('admin::partials.modal-busqueda', [
        'titulo' => 'Buscar Proyectos.',
        'columnas' => [
            'Nombre' => 45,
            'Estatus' => 10,
            'Ente Ejecutor' => 10,
            'Municipio' => 10,
            'Parroquia' => 10,
            'Sector Primario' => 10,
            'Año' => 5
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Proyectos->generate() !!}
        {!! Form::close() !!}
        
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h4>Cargar Archivo <small>Excel (.csv, .xls, .xlsx, .ods)</small></h4>
            <hr>
            {!! Form::open(['id' => 'carga', 'name' => 'carga', 'method' => 'POST']) !!}
                <input id="upload" name="subir" type="file"/>

                <button id="subir" type="button" class="btn btn-primary mt-ladda-btn ladda-button" data-style="expand-right">
                    <span class="ladda-label">
                        <i class="icon-arrow-right"></i> Carga archivo Excel
                    </span>
                </button>
            {!! Form::close() !!}
        </div>
        
    </div>
@endsection