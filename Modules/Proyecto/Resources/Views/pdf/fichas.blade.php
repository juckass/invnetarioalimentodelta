<!DOCTYPE html>
<html>
	<head>
		<title>Reporte</title>
		@include('proyecto::pdf.css')

		<style type="text/css">
		#reporteHtml{
			height: 140mm;
			font-size: 10px;
			border-bottom: solid 2px #00cc66;
		}
		#titulo {
			width: 150mm;
			float: right;
			border-bottom: solid 2px #00cc66;
			font-size: 16pt;
			text-align: center; 
		}
		#reporteHtml thead th{
			background-color: #C00000;
			color: #fff !important;
			text-align: center;
		}
		#reporteHtml table td {
			padding: 5px 12px;
			text-align: right;
		}

		.header{
			margin-bottom: 1px;
		}
		</style>
	</head>
	<body>
		<?php $j = 0; ?>
		@foreach($data as $llave => $dato)
		<?php
		if (($municipio == '' && $llave == 'parroquias') || ($municipio != '' && $llave == 'municipios')) {
			continue;
		}
		?>

		<div id="reporteHtml" 
			@if ($j == 2)
			style="page-break-before:always;"
			@endif 
			>
			<header class="header clearfix" style="margin-bottom: 1px;">
				<div id="logo" style="text-align: left; float: left;">
					<img src="{{ url('public/img/logos/logo.png') }}" style="width: 35mm;">
				</div>
				<div id="titulo">
					<div style="border-bottom: solid 2px black; margin-bottom: 2mm; padding-bottom: 2mm;">
						{!! $titulo[$j] !!}
					</div>
				</div>
			</header>

			<div class="main">  
				<table>
					<thead> 
						<tr>
							<th class="service" style="width: 2%">ITEMS</th>
							<th class="service" style="width: 30%">{{ strtoupper($llave) }}</th>
							<th class="service" style="width: 9%; text-align: right;">NRO. DE<br />PROYECTOS</th>
							<th class="service" style="width: 9%; text-align: right;">INVERSI&Oacute;N</th>
							<th class="service" style="width: 10%; text-align: right;">%</th>
						</tr>
					</thead>
					<tbody>
						<?php $i = 0; ?>
						@foreach($dato as $fila)
							<tr>
								<td class="service" style="width: 2%">{{ ++$i }}</td>
								<td class="service" style="width: 30%">{!! htmlentities($fila->nombre) !!}</td>
								<td class="service" style="width: 9%; text-align: right;">{{ $fila->cantidad }}</td>
								<td class="service" style="width: 9%; text-align: right;">{{ number_format(floatval($fila->inversion), 2, ',', '.') }}</td>
								<td class="service" style="width: 10%; text-align: right;">{{ number_format(floatval($fila->inversion) * 100 / $totales[$j][1], 2, ',', '.') }} %</td>
							</tr>
						@endforeach
					</tbody>
					<tfoot>
						<tr>
							<td class="service" style="width: 2%" colspan="2"><b>Total:</b></td>
							<td class="service" style="width: 9%; text-align: right;"><b>{{ number_format($totales[$j][0], 0, ',', '.') }}</b></td>
							<td class="service" style="width: 9%; text-align: right;"><b>{{ number_format($totales[$j][1], 2, ',', '.') }}</b></td>
							<td class="service" style="width: 10%; text-align: right;"><b>100 %</b></td>
						</tr>
					</tfoot>
				</table>	
			</div>
		</div>
		<?php $j++; ?>
		@endforeach
	</body>
</html>



