@include('proyecto::pdf.css')

<div id="reporteHtml">
	<div class="header clearfix">
		<div id="logo">
			<img src="{{ url('public/img/logos/logo.png') }}" style="width: 4cm;">
		</div>
		<h1>REPORTE</h1>
		<div id="project">
	        <div></div>
		</div>
		<div id="company" class="clearfix">
			<div><span>Fecha de impresi&oacute;n</span> {{ date('d/m/Y') }}</div>
		</div>
	</div>
	<div class="main">  
		<table>
			<thead>
				<tr>
					<th class="service" style="width: 2%">#</th>
					<th class="service" style="width: 30%">NOMBRE</th>
					<th class="service" style="width: 9%">A&Ntilde;O</th>
					<th class="service" style="width: 9%">ESTATUS</th>
					<th class="service" style="width: 10%">ENTE EJECUTOR</th>
					<th class="service" style="width: 10%">MUNICIPIO</th>
					<th class="service" style="width: 10%">PARROQUIA</th>
					<th class="service" style="width: 10%">AVANCE</th>
					<th class="service" style="width: 10%">INVERSION</th>
				</tr>
			</thead>
			<tbody>
				@foreach($query as $proyecto)
					<?php $total += $proyecto->inversion; ?>
					<tr>
						<td class="service" style="width: 2%">{{ ++$i }}</td>
						<td class="service" style="width: 30%">{{ $proyecto->nombre }}</td>
						<td class="service" style="width: 9%">{{ $proyecto->ano }}</td>
						<td class="service" style="width: 9%">{{ $proyecto->estatus }}</td>
						<td class="service" style="width: 10%">{{ $proyecto->ente }}</td>
						<td class="service" style="width: 10%">{{ $proyecto->municipio }}</td>
						<td class="service" style="width: 10%">{{ $proyecto->parroquia }}</td>
						<td class="service" style="width: 10%">{{ $proyecto->avance }}%</td>
						<td class="service" style="width: 10%; text-align: right;">{{ number_format($proyecto->inversion, 2, ',', '.') }}</td>
					</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th class="service" colspan="7" style="text-align: right;">Total de Inversi&oacute;n: </th>
					<th class="service" style="text-align: right;">{{ number_format($total, 2, ',', '.') }}</th>
				</tr>
			</tfoot>
		</table>	
	</div>
	<div class="footer">
		Gobernaci&oacute;n del estado Bol&iacute;var
	</div>
</div>