<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Parroquia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parroquia', function(Blueprint $table){
            $table->increments('id');
            
			$table->integer('municipio_id')->unsigned();
			$table->string('nombre', 80);

			$table->timestamps();
			$table->softDeletes();

            $table->foreign('municipio_id')->references('id')->on('municipio')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parroquia');
    }
}
