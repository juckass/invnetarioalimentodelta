<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Proyectos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('proyectos', function(Blueprint $table){
            $table->increments('id');

			$table->string('nombre', 80);
			$table->integer('municipio_id')->unsigned();
			$table->integer('parroquia_id')->unsigned();
			$table->decimal('inversion', 10, 2);
			$table->integer('sector_primario_id')->unsigned();
			$table->integer('estatus_id')->unsigned();
			$table->decimal('avance', 3, 2);
			$table->integer('fuente_financiamiento_id')->unsigned();
			$table->integer('ente_ejecutor_id')->unsigned();
			$table->tinyInteger('ano_ejecucion')->unsigned();
			$table->string('numero_certificacion', 80);

            $table->foreign('municipio_id')->references('id')->on('municipio')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('parroquia_id')->references('id')->on('parroquia')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('sector_primario_id')->references('id')->on('sector_primario')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('estatus_id')->references('id')->on('estatus')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('fuente_financiamiento_id')->references('id')->on('fuente_financiamiento')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('ente_ejecutor_id')->references('id')->on('ente_ejecutor')->onDelete('cascade')->onUpdate('cascade');

			$table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyectos');
    }
}
