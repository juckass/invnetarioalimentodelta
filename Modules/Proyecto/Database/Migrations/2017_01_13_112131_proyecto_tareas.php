<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProyectoTareas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyectos_tareas', function(Blueprint $table){
            $table->increments('id');
            
            $table->integer('proyectos_id')->unsigned();

            $table->string('text', 255);
            $table->timestamp('start_date');
            $table->integer('duration')->unsigned();
            $table->float('progress');
            $table->double('sortorder');
            $table->integer('parent')->unsigned();
            $table->timestamp('deadline')->nullable();
            $table->timestamp('planned_start')->nullable();
            $table->timestamp('planned_end')->nullable();

            $table->softDeletes();

            $table->foreign('proyectos_id')->references('id')->on('proyectos')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyecto_tareas');
    }
}
