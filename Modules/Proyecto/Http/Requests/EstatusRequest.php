<?php namespace Modules\Proyecto\Http\Requests;

use App\Http\Requests\Request;

class EstatusRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:80']
	];
}