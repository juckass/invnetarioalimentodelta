<?php namespace Modules\Proyecto\Http\Requests;

use App\Http\Requests\Request;

class ParroquiaRequest extends Request {
    protected $reglasArr = [
		'municipio_id' => ['required', 'integer'], 
		'nombre' => ['required', 'min:3', 'max:80']
	];
}