<?php namespace Modules\Proyecto\Http\Requests;

use App\Http\Requests\Request;

class ProyectosLinksRequest extends Request {
    protected $reglasArr = [
		'source' => ['required', 'integer'], 
		'target' => ['required', 'integer'], 
		'type' => ['required', 'min:3', 'max:255']
	];
}