<?php namespace Modules\Proyecto\Http\Requests;

use App\Http\Requests\Request;

class ProyectosRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['min:3', 'max:80'], 
		'numero_certificacion' => ['required', 'integer'], 
		'estatus_id' => ['required', 'integer'], 
		'fuente_financiamiento_id' => ['required', 'integer'], 
		'inversion' => ['required'], 
		'ente_ejecutor_id' => ['required', 'integer'], 
		'municipio_id' => ['required', 'integer'], 
		'parroquia_id' => ['required', 'integer'], 
		'sector_primario_id' => ['required', 'integer'], 
		'avance' => ['required'], 
		'ano_ejecucion' => ['integer']
	];
}