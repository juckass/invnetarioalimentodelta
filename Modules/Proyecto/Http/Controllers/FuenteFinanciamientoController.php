<?php namespace Modules\Proyecto\Http\Controllers;

//Controlador Padre
use Modules\Proyecto\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Proyecto\Http\Requests\FuenteFinanciamientoRequest;

//Modelos
use Modules\Proyecto\Model\FuenteFinanciamiento;

class FuenteFinanciamientoController extends Controller
{
    protected $titulo = 'Fuente Financiamiento';

    public $js = [
        'FuenteFinanciamiento'
    ];
    
    public $css = [
        'FuenteFinanciamiento'
    ];

    public $librerias = [
        'alphanum',
        'maskedinput',
        'datatables',
    ];

    public function index()
    {
        return $this->view('proyecto::FuenteFinanciamiento', [
            'FuenteFinanciamiento' => new FuenteFinanciamiento()
        ]);
    }

    public function nuevo()
    {
        $FuenteFinanciamiento = new FuenteFinanciamiento();
        return $this->view('proyecto::FuenteFinanciamiento', [
            'layouts' => 'admin::layouts.popup',
            'FuenteFinanciamiento' => $FuenteFinanciamiento
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $FuenteFinanciamiento = FuenteFinanciamiento::find($id);
        return $this->view('proyecto::FuenteFinanciamiento', [
            'layouts' => 'admin::layouts.popup',
            'FuenteFinanciamiento' => $FuenteFinanciamiento
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')){
            $FuenteFinanciamiento = FuenteFinanciamiento::withTrashed()->find($id);
        } else {
            $FuenteFinanciamiento = FuenteFinanciamiento::find($id);
        }

        if ($FuenteFinanciamiento){
            return array_merge($FuenteFinanciamiento->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(FuenteFinanciamientoRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $FuenteFinanciamiento = $id == 0 ? new FuenteFinanciamiento() : FuenteFinanciamiento::find($id);

            $FuenteFinanciamiento->fill($request->all());
            $FuenteFinanciamiento->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $FuenteFinanciamiento->id,
            'texto' => $FuenteFinanciamiento->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            FuenteFinanciamiento::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            FuenteFinanciamiento::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            FuenteFinanciamiento::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = FuenteFinanciamiento::select([
            'id', 'nombre', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}