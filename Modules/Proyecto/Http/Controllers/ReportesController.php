<?php 

namespace Modules\Proyecto\Http\Controllers;

use DB;
use PDF;
use Carbon\Carbon;
use Modules\Proyecto\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Excel;

use Modules\Proyecto\Model\Proyectos;
use Modules\Clap\Model\Municipio;
use Modules\Clap\Model\Parroquia;
use Modules\Proyecto\Model\Estatus;
use Modules\Proyecto\Model\FuenteFinanciamiento;
use Modules\Proyecto\Model\EnteEjecutor;
use Modules\Proyecto\Model\SectorPrimario;

class ReportesController extends Controller
{
    protected $titulo = 'Reporte';

    public $css = ['Reporte'];
    public $js = ['Reporte'];

    public $librerias = [
        'vue',
        'bootstrap-select'
    ];

    public function index(Request $request)
    {
        $municipio = 0;

        if($request->municipio !=''){
           $municipio = Municipio::select('id')->where('nombre', $request->municipio)->first()->ToArray();
        }

        return $this->view('proyecto::Reporte', [
            'sector_primario_id' => $request->sector_primario_id,
            'ano'                => $request->ano == 0 ? '' : $request->ano,
            'estatus_id'         => $request->estatus_id == 0 ? '' : $request->estatus_id,
            'municipio_id'       => $municipio == 0 ? '' : $municipio['id'],   
            'Proyectos'          => new Proyectos()
        ]);
    }
    
    public function imprimirEscritorio(Request $request){
        
    }
    
    public function imprimir(Request $request)
    {
        $request->excel = intval($request->excel);

        if ($request->excel == 1) {
            return $this->excel($request);
        }

        $query = Proyectos::select([
                'proyectos.nombre', 
                'proyectos.ano_ejecucion as ano',
                'estatus.nombre as estatus',
                'ente_ejecutor.nombre as ente',
                'municipio.nombre as municipio',
                'parroquia.nombre as parroquia',
                'proyectos.avance',
                'proyectos.inversion'
            ])
            ->leftJoin('estatus', 'proyectos.estatus_id', '=', 'estatus.id')
            ->leftJoin('ente_ejecutor', 'proyectos.ente_ejecutor_id', '=', 'ente_ejecutor.id')
            ->leftJoin('municipio', 'proyectos.municipio_id', '=', 'municipio.id')
            ->leftJoin('parroquia', 'proyectos.parroquia_id', '=', 'parroquia.id');


        $campos = ["numero_certificacion", "ano_ejecucion", "estatus_id", "fuente_financiamiento_id", "ente_ejecutor_id", "municipio_id", "parroquia_id", "sector_primario_id"];

        foreach ($campos as $campo) {
            $filtro = $request->{$campo};
            
            if (empty($filtro) || (is_array($filtro) && $filtro[0] == '')) {
                continue;
            }

            if($campo == 'numero_certificacion') { 
                $query->where($campo, $filtro);
            } else {
                $query->whereIn($campo, explode(',', $filtro[0]));
            }
        }

        if ($request->has('html')) {
            $proyectos = [];
            $total = 0;
            $i = 0;
            foreach ($query->get() as $proyecto) {
                $total += $proyecto->inversion;
                $proyectos[] = [
                    ++$i,
                    $proyecto->nombre,
                    $proyecto->ano,
                    $proyecto->estatus,
                    $proyecto->ente,
                    $proyecto->municipio,
                    $proyecto->parroquia,
                    $proyecto->avance,
                    number_format($proyecto->inversion, 2, ',', '.')
                ];
            }
            return ['proyectos' => $proyectos, 'total' => number_format($total, 2, ',', '.')];
        }

        $html = view('proyecto::pdf.reporte', [
            'i'            => 0,
            'query'        => $query->get(),
            'total'        => 0
        ])->render();


        $pdf = PDF::loadHTML($html);
        return $pdf->download('reporte.pdf');
    }

    protected function excel($request)
    {
        return Excel::create('Reporte', function ($excel) {

            // Set the title
            $excel->setTitle('Reporte de Proyectos');

            // Chain the setters
            $excel->setCreator('Dirección de Informática y Sistemas')
                  ->setCompany('GEB');

            // Call them separately
            $excel->setDescription('Reporte de Proyectos');

            // Our first sheet
            $excel->sheet('First sheet', function ($sheet) {
                $sheet->setOrientation('landscape');
                $data = [];
                $proyectos = Proyectos::select('*')->get();

                foreach ($proyectos as $proyecto) {
                    $data[] = [
                        'Nombre'                   => $proyecto->nombre,
                        'Municipio'                => $proyecto->municipio->nombre,
                        'Parroquia'                => $proyecto->parroquia->nombre,
                        'Inversion'                => $proyecto->inversion,
                        'Sector Primario'          => $proyecto->sector_primario->nombre,
                        'Estatus'                  => $proyecto->estatus->nombre,
                        'Fuente de Financiamiento' => $proyecto->fuente_financiamiento->nombre,
                        'Avance'                   => $proyecto->avance,
                        'Ente Ejecutor'            => $proyecto->ente_ejecutor->nombre,
                        'Año de Ejecucion'         => $proyecto->ano_ejecucion,
                        'Numero de Certificacion'  => $proyecto->numero_certificacion
                    ];
                }
                $sheet->fromArray($data);
            });
        })->download('xlsx');
    }

    public function municipios(Request $request)
    {
        return municipio::pluck('nombre', 'id');
    }
    
    public function parroquias(Request $request)
    {
        $sql = Parroquia::select('id', 'nombre')
                    ->whereIn('municipio_id', explode(',', $request->id))
                    ->orderBy('municipio_id')
                    ->pluck('nombre', 'id');

        $salida = ['s' => 'n' , 'msj' => 'El Municipio no contiene Parroquias'];
        
        if ($sql) {
            $salida = ['s' => 's' , 'msj' => 'Parroquias encontrados', 'parroquia_id' => $sql];
        }
        
        return $salida;
    }

    public function annos(){

        return Proyectos::distinct()->select('ano_ejecucion')->orderBy('ano_ejecucion', 'desc')->pluck('ano_ejecucion', 'ano_ejecucion');

    }

    public function estatus(){
        return Estatus::pluck('nombre', 'id');
    }

    public function fuenteFinanciamiento(){
        return FuenteFinanciamiento::pluck('nombre', 'id');
    }

    public function enteEjecutor(){
        return EnteEjecutor::pluck('nombre', 'id');
    }

    public function municipio(){
        return Municipio::pluck('nombre', 'id');
    }
    public function parroquia(){
        return Parroquia::pluck('nombre', 'id');
    }
    public function sectorPrimario(){
        return SectorPrimario::pluck('nombre', 'id');
    }
}
