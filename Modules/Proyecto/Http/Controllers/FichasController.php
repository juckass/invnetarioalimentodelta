<?php 

namespace Modules\Proyecto\Http\Controllers;

use DB;
use PDF;
use Carbon\Carbon;
use Modules\Proyecto\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Excel;

use Modules\Proyecto\Model\Proyectos;
use Modules\Clap\Model\Municipio;
use Modules\Clap\Model\Parroquia;

class FichasController extends Controller
{
    protected $titulo = 'Fichas';

    public $js = ['Fichas'];

    public $librerias = [
        'jquery-ui',
        'jquery-ui-timepicker',
    ];

    public function index(Request $request)
    {
        $municipio = 0;
        if($request->municipio !=''){
           $municipio = Municipio::select('id')->where('nombre', $request->municipio)->first()->ToArray();
        }

        return $this->view('proyecto::Fichas', [
            'sector_primario_id' => $request->sector_primario_id,
            'ano'                => $request->ano == 0 ? '' : $request->ano,
            'estatus_id'         => $request->estatus_id == 0 ? '' : $request->estatus_id,
            'municipio_id'       => $municipio == 0 ? '' : $municipio['id'],   
            'Proyectos'          => new Proyectos()
        ]);
    }
    
    public function imprimir(Request $request)
    {
        $isHtml = $request->has('html');
        $isHtml = false;
        $dataSectores = Proyectos::select(
                'sector_primario.nombre as nombre',
                DB::raw('count(proyectos.id) as cantidad'),
                DB::raw('sum(proyectos.inversion) as inversion')
            )
            ->leftJoin('sector_primario', 'sector_primario.id', '=', 'proyectos.sector_primario_id')
            ->groupBy('sector_primario.nombre');

        $dataMunicipios = Proyectos::select(
                'municipio.nombre as nombre',
                DB::raw('count(proyectos.id) as cantidad'),
                DB::raw('sum(proyectos.inversion) as inversion')
            )
            ->leftJoin('municipio', 'municipio.id', '=', 'proyectos.municipio_id')
            ->groupBy('municipio.nombre');

        $dataParroquias = Proyectos::select(
                'parroquia.nombre as nombre',
                DB::raw('count(proyectos.id) as cantidad'),
                DB::raw('sum(proyectos.inversion) as inversion')
            )
            ->leftJoin('parroquia', 'parroquia.id', '=', 'proyectos.parroquia_id')
            ->groupBy('parroquia.nombre');

        $dataEstatus = Proyectos::select(
                'estatus.nombre as nombre',
                DB::raw('count(proyectos.id) as cantidad'),
                DB::raw('sum(proyectos.inversion) as inversion')
            )
            ->leftJoin('estatus', 'estatus.id', '=', 'proyectos.estatus_id')
            ->groupBy('estatus.nombre');

        $campos = ['numero_certificacion', 'ano_ejecucion', 'estatus_id', 'fuente_financiamiento_id', 'ente_ejecutor_id', 'municipio_id', 'parroquia_id', 'sector_primario_id'];

        $titulos = [
            ['Resumen por Sector'],
            ['Resumen por Municipios'],
            ['Resumen por Parroquias'],
            ['Resumen por Estatus']
        ];

        for ($i = 0, $c = count($titulos); $i < $c; $i++) { 
            if ($request->ano_ejecucion == ''){
                $titulos[$i][] = 'A&ntilde;os 2004-' . date('Y');
            }else{
                $titulos[$i][] = 'A&ntilde;o ' . $request->ano_ejecucion;
            }

            if ($request->municipio_id != ''){
                $titulos[$i][] = '<br />Municipio ' . Municipio::find($request->municipio_id)->nombre;
            }

            $titulos[$i] = implode(', ', $titulos[$i]);
        }

        foreach ($campos as $campo) {
            $filtro = $request->{$campo};

            if ($filtro == '') {
                continue;
            }

            $campo = 'proyectos.' . $campo;
            $dataSectores->where($campo, $filtro);
            $dataMunicipios->where($campo, $filtro);
            $dataParroquias->where($campo, $filtro);
            $dataEstatus->where($campo, $filtro);
        }

        $_dataSectores = $dataSectores->get();
        $_dataMunicipios = $dataMunicipios->get();
        $_dataParroquias = $dataParroquias->get();
        $_dataEstatus = $dataEstatus->get();

        $totales = [
            [0, 0],
            [0, 0],
            [0, 0],
            [0, 0]
        ];

        foreach ($_dataSectores as $fila) {
            $totales[0][0] += $fila->cantidad;
            $totales[0][1] += floatval($fila->inversion);
        }

        foreach ($_dataMunicipios as $fila) {
            $totales[1][0] += $fila->cantidad;
            $totales[1][1] += floatval($fila->inversion);
        }

        foreach ($_dataParroquias as $fila) {
            $totales[2][0] += $fila->cantidad;
            $totales[2][1] += floatval($fila->inversion);
        }

        foreach ($_dataEstatus as $fila) {
            $totales[3][0] += $fila->cantidad;
            $totales[3][1] += floatval($fila->inversion);
        }

        $html = view('proyecto::pdf.fichas', [
            'titulo'         => $titulos,
            'data'           => [
                'sectores'   => $_dataSectores,
                'municipios' => $_dataMunicipios,
                'parroquias' => $_dataParroquias,
                'estatus'    => $_dataEstatus,
            ],
            'totales'        => $totales,
            'municipio'      => $request->municipio_id,
            'html'           => $isHtml
        ])->render();
        
        //return $html;
        if ($isHtml) {
            return $html;
        }

        $pdf = PDF::loadHTML($html)
            ->setOption('page-width', '219.8')
            ->setOption('page-height', '280.1');
            //->setOrientation('landscape');
        return $pdf->stream();
    }

    public function municipios(Request $request)
    {
        return municipio::pluck('nombre', 'id');
    }
    
    public function parroquias(Request $request)
    {
        $sql = Parroquia::select('id', 'nombre')->where('municipio_id', $request->id)->pluck('nombre', 'id');

        if ($sql) {
            return ['s' => 's' , 'msj' => 'Parroquias encontrados', 'parroquia_id' => $sql];
        }
        
        return ['s' => 'n' , 'msj' => 'El Municipio no contiene Parroquias'];
    }
}
