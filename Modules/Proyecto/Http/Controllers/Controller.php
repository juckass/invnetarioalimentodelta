<?php namespace Modules\Proyecto\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{

    public $app = 'proyecto';

    protected $patch_js = [
        'public/js',
        'public/plugins',
        'Modules/Proyecto/Assets/js',
    ];

    protected $patch_css = [
        'public/css',
        'public/plugins',
        'Modules/Proyecto/Assets/css',
    ];
}
