<?php namespace Modules\Proyecto\Http\Controllers;

//Controlador Padre
use Modules\Proyecto\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Proyecto\Http\Requests\SectorPrimarioRequest;

//Modelos
use Modules\Proyecto\Model\SectorPrimario;

class SectorPrimarioController extends Controller
{
    protected $titulo = 'Sector Primario';

    public $js = [
        'SectorPrimario'
    ];
    
    public $css = [
        'SectorPrimario'
    ];

    public $librerias = [
        'alphanum',
        'maskedinput',
        'datatables',
    ];

    public function index()
    {
        return $this->view('proyecto::SectorPrimario', [
            'SectorPrimario' => new SectorPrimario()
        ]);
    }

    public function nuevo()
    {
        $SectorPrimario = new SectorPrimario();
        return $this->view('proyecto::SectorPrimario', [
            'layouts' => 'admin::layouts.popup',
            'SectorPrimario' => $SectorPrimario
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $SectorPrimario = SectorPrimario::find($id);
        return $this->view('proyecto::SectorPrimario', [
            'layouts' => 'admin::layouts.popup',
            'SectorPrimario' => $SectorPrimario
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')){
            $SectorPrimario = SectorPrimario::withTrashed()->find($id);
        } else {
            $SectorPrimario = SectorPrimario::find($id);
        }

        if ($SectorPrimario){
            return array_merge($SectorPrimario->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(SectorPrimarioRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $SectorPrimario = $id == 0 ? new SectorPrimario() : SectorPrimario::find($id);

            $SectorPrimario->fill($request->all());
            $SectorPrimario->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $SectorPrimario->id,
            'texto' => $SectorPrimario->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            SectorPrimario::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            SectorPrimario::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            SectorPrimario::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = SectorPrimario::select([
            'id', 'nombre', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}