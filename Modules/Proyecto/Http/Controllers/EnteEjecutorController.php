<?php namespace Modules\Proyecto\Http\Controllers;

//Controlador Padre
use Modules\Proyecto\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Proyecto\Http\Requests\EnteEjecutorRequest;

//Modelos
use Modules\Proyecto\Model\EnteEjecutor;

class EnteEjecutorController extends Controller
{
    protected $titulo = 'Ente Ejecutor';

    public $js = [
        'EnteEjecutor'
    ];
    
    public $css = [
        'EnteEjecutor'
    ];

    public $librerias = [
        'alphanum',
        'maskedinput',
        'datatables',
    ];

    public function index()
    {
        return $this->view('proyecto::EnteEjecutor', [
            'EnteEjecutor' => new EnteEjecutor()
        ]);
    }

    public function nuevo()
    {
        $EnteEjecutor = new EnteEjecutor();
        return $this->view('proyecto::EnteEjecutor', [
            'layouts' => 'admin::layouts.popup',
            'EnteEjecutor' => $EnteEjecutor
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $EnteEjecutor = EnteEjecutor::find($id);
        return $this->view('proyecto::EnteEjecutor', [
            'layouts' => 'admin::layouts.popup',
            'EnteEjecutor' => $EnteEjecutor
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')){
            $EnteEjecutor = EnteEjecutor::withTrashed()->find($id);
        } else {
            $EnteEjecutor = EnteEjecutor::find($id);
        }

        if ($EnteEjecutor){
            return array_merge($EnteEjecutor->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(EnteEjecutorRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $EnteEjecutor = $id == 0 ? new EnteEjecutor() : EnteEjecutor::find($id);

            $EnteEjecutor->fill($request->all());
            $EnteEjecutor->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $EnteEjecutor->id,
            'texto' => $EnteEjecutor->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            EnteEjecutor::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            EnteEjecutor::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            EnteEjecutor::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = EnteEjecutor::select([
            'id', 'nombre', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}