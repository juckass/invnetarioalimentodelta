<?php namespace Modules\Proyecto\Http\Controllers;

//Controlador Padre
use Modules\Proyecto\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;
use Excel;
use Dhtmlx\Connector\GanttConnector;

//Request
use Modules\Proyecto\Http\Requests\ProyectosRequest;

//Modelos
use Modules\Clap\Model\Estados;
use Modules\Clap\Model\Municipio;
use Modules\Clap\Model\Parroquia;

use Modules\Proyecto\Model\Proyectos;

use Modules\Proyecto\Model\SectorPrimario;
use Modules\Proyecto\Model\Estatus;
use Modules\Proyecto\Model\FuenteFinanciamiento;
use Modules\Proyecto\Model\EnteEjecutor;

use Modules\Proyecto\Model\ProyectosLinks;
use Modules\Proyecto\Model\ProyectosTareas;

class ProyectosController extends Controller
{
    protected $titulo = 'Proyectos';

    public $js = [
        'Proyectos'
    ];
    
    public $css = [
        'Proyectos'
    ];

    public $librerias = [
        'datatables',
        'ladda'
    ];

    public function index(Request $request)
    {
        return $this->view('proyecto::Proyectos', [
            'Proyectos' => new Proyectos()
        ]);
    }

    public function escritorio(Request $request)
    {
        $this->js = [
            'Escritorio', 
            'maps/venezuela/bolivar.js'
        ];

        $this->librerias = [
            'highcharts',
            'highcharts-drilldown',
            'highmaps',
            'vue',
        ];

        $data = $this->data($request);

        $annos =  [];

        foreach (Proyectos::distinct()->select('ano_ejecucion')->orderBy('ano_ejecucion', 'desc')->get() as $ano){
            $annos[] = [
                'value' => $ano->ano_ejecucion,
                'text' => $ano->ano_ejecucion
            ];
        }

        $data['Proyectos'] = new Proyectos();
        $data['annos']     = $annos;

        return $this->view('proyecto::Escritorio', $data);
    }

    public function data(Request $request){
        $ano             = intval($request->ano);
        $estatu          = intval($request->estatu);
        $sector_primario = intval($request->sector_primario);

        $registros = Proyectos::where('ano_ejecucion', $ano)->count();
        
        if ($registros == 0){
            $ano = 0;
        }

        $iconos = [
            'fa-leaf',
            'fa-tint',
            'fa-book',
            'fa-handshake-o',
            'fa-building',
            'fa-building',
            'fa-subway',
            'fa-ambulance',
            'fa-cab',
            'fa-users',
            'fa-plane',
        ];

        $estatus = [
            'Todos',
            'Concluida',
            'En Ejecución',
            'Por Iniciar'
        ];

        $i = 0;
        $sectores = [];

        foreach (SectorPrimario::all()->sortBy('nombre') as $sector) {
            $sector = $sector->toArray();

            $sector['icono'] = $iconos[$i++];
            $sector['cantidades'] = [0, 0, 0, 0];
            $sectores[$sector['id']] = $sector;
        }

        $estatus = [];

        foreach (Estatus::all()->sortBy('nombre') as $est) {
            $estatus[$est->id] = $est;
        }

        $cantidades = Proyectos::select(DB::raw('count(*) as cantidad'), 'sector_primario_id', 'estatus_id')
            ->groupBy('sector_primario_id', 'estatus_id')
            ->orderBy('sector_primario_id', 'estatus_id');

        if ($ano != 0){
            $cantidades->where('ano_ejecucion', $ano);
        }

        if ($estatu != 0){
            $cantidades->where('estatus_id', $estatu);
        }

        if ($sector_primario != 0){
            $cantidades->where('sector_primario_id', $sector_primario);
        }
        
        foreach ($cantidades->get() as $cantidad) {
            $sectores[$cantidad->sector_primario_id]['cantidades'][$cantidad->estatus_id] = intval($cantidad->cantidad);
        }

        foreach ($sectores as &$sector) {
            $sector['cantidades'][0] = array_sum($sector['cantidades']);
        }

        $sectores = array_values($sectores);

        $municipios = [];
        $total_proyectos_estatal = 0;
        foreach (municipio::all() as $municipio) {
            $cantidad = Proyectos::where('municipio_id', $municipio->id);
            if ($ano != 0){
                $cantidad->where('ano_ejecucion', $ano);
            }

            if ($estatu != 0){
                $cantidad->where('estatus_id', $estatu);
            }

            if ($sector_primario != 0){
                $cantidad->where('sector_primario_id', $sector_primario);
            }


            if (str_slug($municipio->nombre) == 'estado') {
                $total_proyectos_estatal = $cantidad->count();
            }

            $municipios[] = [
                'slug' => str_slug($municipio->nombre),
                'value' => $cantidad->count()
            ];
        }

        $proyectos = Proyectos::select('*');

        if ($ano != 0){
            $proyectos->where('ano_ejecucion', $ano);
        }

        if ($estatu != 0){
            $proyectos->where('estatus_id', $estatu);
        }

        if ($sector_primario != 0){
            $proyectos->where('sector_primario_id', $sector_primario);
        }

        
        $_proyectos                 = clone $proyectos;
        $total_proyectos            = $_proyectos->count();

        $_proyectos                 = clone $proyectos;
        $total_proyectos_concluidos = $_proyectos->where('estatus_id', 1)->count();

        $_proyectos                 = clone $proyectos;
        $total_proyectos_ejecucion  = $_proyectos->where('estatus_id', 2)->count();

        $_proyectos                 = clone $proyectos;
        $total_proyectos_iniciar    = $_proyectos->where('estatus_id', 3)->count();

        $_proyectos                 = clone $proyectos;
        $total_inversion            = $_proyectos->sum('inversion');

        return [
            'total_proyectos'            => number_format(floatval($total_proyectos), 0, '', '.'),
            'total_proyectos_ejecucion'  => number_format(floatval($total_proyectos_ejecucion), 0, '', '.'),
            'total_proyectos_concluidos' => number_format(floatval($total_proyectos_concluidos), 0, '', '.'),
            'total_proyectos_iniciar'    => number_format(floatval($total_proyectos_iniciar), 0, '', '.'),
            'total_inversion'            => number_format(floatval($total_inversion), 2, ',', '.'),
            'total_proyectos_estatal'    => $total_proyectos_estatal,
            'estatus'                    => $estatus,
            'sectores'                   => $sectores,
            'municipios'                 => $municipios
        ];
    }

    public function tareas($id = 0)
    {
        $Proyectos = Proyectos::find($id);
        
        $this->js = ['dhtmlxgantt/dhtmlxgantt.js'];
        $this->css = ['dhtmlxgantt/dhtmlxgantt.css'];

        return $this->view('proyecto::ProyectosTareas', [
            'Proyectos' => $Proyectos
        ]);
    }

    public function data_gantt()
    {
        $connector = new GanttConnector(null, "PHPLaravel");
        $connector->render_links(new ProyectosLinks(), "id", "source,target,type");
        $connector->render_table(new ProyectosTareas(), "id", "start_date,duration,text,progress,parent");
    }

    public function nuevo()
    {
        $Proyectos = new Proyectos();
        return $this->view('proyecto::Proyectos', [
            'layouts' => 'admin::layouts.popup',
            'Proyectos' => $Proyectos
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Proyectos = Proyectos::find($id);
        return $this->view('proyecto::Proyectos', [
            'layouts' => 'admin::layouts.popup',
            'Proyectos' => $Proyectos
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Proyectos = Proyectos::withTrashed()->find($id);
        } else {
            $Proyectos = Proyectos::find($id);
        }

        if ($Proyectos) {
            $Proyectos->parroquia_id = parroquia::where('municipio_id', $Proyectos->municipio_id)
                ->pluck('nombre', 'id')
                ->put('_', $Proyectos->parroquia_id);
            
            return array_merge($Proyectos->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function parroquias(Request $request)
    {
        $sql = parroquia::where('municipio_id', $request->id)
            ->pluck('nombre', 'id')
            ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'El Municipio no contiene Parroquias'];
        
        if ($sql) {
            $salida = ['s' => 's' , 'msj'=> 'Parroquias encontradas', 'select'=> $sql];
        }
        
        return $salida;
    }

    public function guardar(ProyectosRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try {
            $Proyectos = $id == 0 ? new Proyectos() : Proyectos::find($id);

            $Proyectos->fill($request->all());
            $Proyectos->save();
        } catch (QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch (Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $Proyectos->id,
            'texto' => $Proyectos->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try {
            Proyectos::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Proyectos::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Proyectos::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Proyectos::select([
            'proyectos.id',
            'proyectos.nombre',
            'estatus.nombre as estatus',
            'ente_ejecutor.nombre as ente_ejecutor',
            'municipio.nombre as municipio',
            'parroquia.nombre as parroquia',
            'sector_primario.nombre as sector_primario',
            'proyectos.ano_ejecucion',
            'proyectos.deleted_at'
        ])
        ->leftJoin('estatus', 'estatus.id', '=', 'proyectos.estatus_id')
        ->leftJoin('ente_ejecutor', 'ente_ejecutor.id', '=', 'proyectos.ente_ejecutor_id')
        ->leftJoin('municipio', 'municipio.id', '=', 'proyectos.municipio_id')
        ->leftJoin('parroquia', 'parroquia.id', '=', 'proyectos.parroquia_id')
        ->leftJoin('sector_primario', 'sector_primario.id', '=', 'proyectos.sector_primario_id');

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }

    public function carga(Request $request)
    {

       /* importates para la carga hay que tratar el excel el campo:

            % de avance quitarle el '%',
            Año de Ejecución cambiar
            Ano de Ejecucion
            No. Certificación cambiar
            no. certificacion

            los registros no pueden estar en una tabla


        */

        set_time_limit(0);
        DB::beginTransaction();
        try {

            $ruta = public_path('archivos/');
            $archivo = $request->file('subir');

            $mime = $archivo->getClientMimeType();

       /* if (!in_array($mime, $mimes)) {
            return "Error de Validación, la extension el archivo cargado debe ser: xls,xlsx,csv";
        }*/
        
        do {
            $nombre_archivo = str_random(20) . '.' . $archivo->getClientOriginalExtension();
        } while (is_file($ruta . $nombre_archivo));
            
        $archivo->move($ruta, $nombre_archivo);
        chmod($ruta . $nombre_archivo, 0777);
            //$ruta = public_path('archivos/');
            //$nombre_archivo = "archivo.xlsx";
                
            $excel = Excel::selectSheetsByIndex(0)->load($ruta . $nombre_archivo)->get();
            
            $municipios = [];
            foreach (Municipio::all() as $municipio) {
                $municipios[str_slug($municipio->nombre)] = [$municipio->id, $municipio->nombre];
            }

            $parroquias = [];
            foreach (Parroquia::all() as $parroquia) {
                $parroquias[str_slug($parroquia->nombre)] = [$parroquia->id, $parroquia->nombre];
            }

            $sectorprimario = [];
            foreach (SectorPrimario::all() as $sector) {
                $sectorprimario[str_slug($sector->nombre)] = [$sector->id, $sector->nombre];
            }

            $estatus = [];
            foreach (Estatus::all() as $estatu) {
                $estatus[str_slug($estatu->nombre)] = [$estatu->id, $estatu->nombre];
            }

            $fuentefinanciamiento = [];
            foreach (FuenteFinanciamiento::all() as $fuente) {
                $fuentefinanciamiento[str_slug($fuente->nombre)] = [$fuente->id, $fuente->nombre];
            }
            $enteejecutor = [];
            foreach (EnteEjecutor::all() as $ejecutor) {
                $enteejecutor[str_slug($ejecutor->nombre)] = [$ejecutor->id, $ejecutor->nombre];
            }

            DB::statement("SET foreign_key_checks=0");
            Proyectos::truncate();
            DB::statement("SET foreign_key_checks=1");

            foreach ($excel as $key => $value) {
                $avance = $value->de_avance;

                if (substr($avance, 0, 3) == '100'){
                    $avance = '100';
                }

                $parroquiaSlug = str_slug($value->parroquia);
                if ($parroquiaSlug == 'todos' || $parroquiaSlug == '') {
                    $parroquiaSlug = 'todas';
                }
                
                if ($value->nombre_del_proyecto == ''){
                    continue;
                }
                $cliente = Proyectos::firstOrNew([
                    'nombre'                   => $value->nombre_del_proyecto,
                    'numero_certificacion'     => is_null($value->no_certificacion) ? '' : $value->no_certificacion,
                    'estatus_id'               => $estatus[str_slug($value->estatus)][0],
                    'fuente_financiamiento_id' => $fuentefinanciamiento[str_slug($value->fuente_de_financiamiento)][0],
                    'inversion'                => $value->inversion,
                    'ente_ejecutor_id'         => $enteejecutor[str_slug($value->ente_ejecutor)][0],
                    'estados_id'               => 6,
                    'municipio_id'             => $municipios[str_slug($value->municipio)][0],
                    'parroquia_id'             => $parroquias[$parroquiaSlug][0],
                    'sector_primario_id'       => $sectorprimario[str_slug($value->sector_primario)][0],
                    'avance'                   => $avance,
                    'ano_ejecucion'            => $value->ano_de_ejecucion
                ]);

                $cliente->save();
            }
        } catch (QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch (Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }

        DB::commit();
        //return "listo! " . date('d/m/Y H:i:s');
        return ['s' => 's', 'msj' => trans('controller.incluir'),];
    }
}
