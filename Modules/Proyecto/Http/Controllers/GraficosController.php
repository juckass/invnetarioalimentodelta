<?php 

namespace Modules\Proyecto\Http\Controllers;

use Modules\Proyecto\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;
use Carbon\Carbon;

use Modules\Proyecto\Model\Proyectos;
use Modules\Clap\Model\Municipio;
use Modules\Clap\Model\Parroquia;

class GraficosController extends Controller
{
    protected $titulo = 'Graficas';
    
    public $librerias = [
        'jquery-ui',
        'jquery-ui-timepicker',
        'highcharts',
        'highcharts-more',
        'highcharts-drilldown',
    ];

    public $js = [
        'Graficas',
    ];

    public function index()
    {
        return $this->view('proyecto::Graficas');
    }

    public function cantidad()
    {
        return Proyectos::count();
    }

    public static function grafica(Request $request)
    {
        $series = [[
            'name' => 'Proyectos',
            'colorByPoint' => true,
            'data' => [[
                'name' => 'Total Proyectos',
                'drilldown' => str_slug('Proyectos', '_'),
                'y' => 0
            ]]
        ]];

        $total = 0;

        $query = DB::table('proyectos')
            ->select(DB::raw('count(*) as y'), 'municipio.nombre')
            ->leftJoin('municipio', 'municipio.id', '=', 'proyectos.municipio_id')
            ->groupBy('municipio.nombre');

        $i = 2;
        foreach ($query->get() as $municipio) {
            $j = $i++;
            $cantidad = floatval(str_replace(',', '.', $municipio->y));
            $slug = str_slug($municipio->nombre, '_');

            if ($slug == 'estado') {
                $j = 1;
                $municipio->nombre = 'Ámbito Estadal';
            }

            $series[0]['data'][$j] = [
                'name' => $municipio->nombre,
                'drilldown' => $slug,
                'y' => $cantidad
            ];

            $total += $cantidad;
        }

        $series[0]['data'] = array_values($series[0]['data']);
        $series[0]['data'][0]['y'] = $total;

        $drilldown = [
            'series' => [[
                'name' => 'Proyectos',
                'id' => str_slug('Proyectos', '_'),
                'data' => ['total', $total]
            ]]
        ];

        $query = DB::table('proyectos')
            ->select(DB::raw('count(*) as y'), 'parroquia.nombre')
            ->leftJoin('parroquia', 'parroquia.id', '=', 'proyectos.parroquia_id')
            ->groupBy('proyectos.municipio_id', 'proyectos.parroquia_id', 'parroquia.nombre');

        foreach (municipio::all() as $municipio) {
            $data = [];
            $_query = clone $query;
            $_query->where('proyectos.municipio_id', $municipio->id);

            foreach ($_query->get() as $emp) {
                $cantidad = floatval(str_replace(',', '.', $emp->y));

                if ($cantidad == 0) {
                    continue;
                }

                $data[] = [$emp->nombre, $cantidad];
            }

            $drilldown['series'][] = [
                'name' => $municipio->nombre,
                'id' => str_slug($municipio->nombre, '_'),
                'data' => $data
            ];
        }

        return [
            'series' => $series,
            'drilldown' => $drilldown
        ];
    }

    public static function graficaTorta(Request $request)
    {
        $series = [];

        $query = DB::table('proyectos')
            ->select(DB::raw('count(*) as y'), 'municipio.nombre')
            ->leftJoin('municipio', 'municipio.id', '=', 'proyectos.municipio_id')
            ->groupBy('municipio.nombre');

        foreach ($query->get() as $proyecto) {
            $cantidad = floatval(str_replace(',', '.', $proyecto->y));

            $series[] = [
                'name' => $proyecto->nombre,
                'y' => $cantidad
            ];
        }

        return $series;
    }

    public function municipios(Request $request)
    {
        return municipio::pluck('nombre', 'id');
    }
    
    public function parroquias(Request $request)
    {
        $sql = Parroquia::select('id', 'nombre')
                    ->where('municipio_id', $request->id)
                    ->pluck('nombre', 'id');

        $salida = ['s' => 'n' , 'msj' => 'El Municipio no contiene Parroquias'];
        
        if ($sql) {
            $salida = ['s' => 's' , 'msj' => 'Parroquias encontrados', 'parroquia_id' => $sql];
        }
        
        return $salida;
    }
}
