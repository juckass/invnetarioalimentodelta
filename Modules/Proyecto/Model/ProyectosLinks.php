<?php 

namespace Modules\Proyecto\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProyectosLinks extends Model
{
	use SoftDeletes;

    protected $table = 'proyectos_links';
    protected $fillable = ["source","target","type"];
    public $timestamps = false;
}