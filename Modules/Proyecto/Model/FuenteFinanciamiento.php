<?php namespace Modules\Proyecto\Model;

use Modules\Admin\Model\Modelo;



class FuenteFinanciamiento extends modelo
{
    protected $table = 'fuente_financiamiento';
    protected $fillable = ["nombre"];
    protected $campos = [
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Fuente Financiamiento'
        ]
    ];

    public function proyectos(){
		return $this->hasMany('Modules\Proyecto\Model\Proyectos');
	}
}