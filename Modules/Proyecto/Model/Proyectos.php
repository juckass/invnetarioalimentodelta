<?php namespace Modules\Proyecto\Model;

use Modules\Admin\Model\Modelo;

use Modules\Clap\Model\Estados;
use Modules\Clap\Model\Municipio;
use Modules\Clap\Model\Parroquia;

use Modules\Proyecto\Model\Estatus;
use Modules\Proyecto\Model\FuenteFinanciamiento;
use Modules\Proyecto\Model\EnteEjecutor;
use Modules\Proyecto\Model\SectorPrimario;

class Proyectos extends modelo
{
    protected $table = 'proyectos';
    protected $fillable = [
        "nombre",
        "numero_certificacion",
        "estatus_id",
        "fuente_financiamiento_id",
        "inversion",
        "ente_ejecutor_id",
        "estados_id",
        "municipio_id",
        "parroquia_id",
        "sector_primario_id",
        "avance",
        "ano_ejecucion"
    ];

    protected $campos = [
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Proyectos',
            'cont_class' => 'col-sm-12'
        ],
        'numero_certificacion' => [
            'type' => 'number',
            'label' => 'Numero Certificacion',
            'placeholder' => 'Numero Certificacion del Proyectos'
        ],
        'estatus_id' => [
            'type' => 'select',
            'label' => 'Estatus',
            'placeholder' => '- Seleccione un Estatus',
            'url' => 'estatus'
        ],
        'fuente_financiamiento_id' => [
            'type' => 'select',
            'label' => 'Fuente Financiamiento',
            'placeholder' => '- Seleccione un Fuente Financiamiento',
            'url' => 'fuente_financiamiento'
        ],
        'inversion' => [
            'type' => 'text',
            'label' => 'Inversion',
            'placeholder' => 'Inversion del Proyectos'
        ],
        'ente_ejecutor_id' => [
            'type' => 'select',
            'label' => 'Ente Ejecutor',
            'placeholder' => '- Seleccione un Ente Ejecutor',
            'url' => 'ente_ejecutor'
        ],
        'estado_id' => [
            'type' => 'select',
            'label' => 'Estado',
            'placeholder' => '- Seleccione un Estado',
            'url' => 'estado'
        ],
        'municipio_id' => [
            'type' => 'select',
            'label' => 'Municipio',
            'placeholder' => '- Seleccione un Municipio',
            'url' => 'municipio'
        ],
        'parroquia_id' => [
            'type' => 'select',
            'label' => 'Parroquia',
            'placeholder' => '- Seleccione un Parroquia',
            'url' => 'parroquia'
        ],
        'sector_primario_id' => [
            'type' => 'select',
            'label' => 'Sector Primario',
            'placeholder' => '- Seleccione un Sector Primario',
            'url' => 'sector_primario'
        ],
        'avance' => [
            'type' => 'text',
            'label' => 'Avance',
            'placeholder' => 'Avance del Proyectos'
        ],
        'ano_ejecucion' => [
            'type' => 'number',
            'label' => 'Ano Ejecucion',
            'placeholder' => 'Ano Ejecucion del Proyectos'
        ]
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['estatus_id']['options'] = Estatus::pluck('nombre', 'id');
		$this->campos['fuente_financiamiento_id']['options'] = FuenteFinanciamiento::pluck('nombre', 'id');
		$this->campos['ente_ejecutor_id']['options'] = EnteEjecutor::pluck('nombre', 'id');
        $this->campos['estado_id']['options'] = Estados::pluck('nombre', 'id');
		$this->campos['municipio_id']['options'] = Municipio::pluck('nombre', 'id');
		$this->campos['parroquia_id']['options'] = Parroquia::pluck('nombre', 'id');
		$this->campos['sector_primario_id']['options'] = SectorPrimario::pluck('nombre', 'id');
    }

    public function estados(){
        return $this->belongsTo('Modules\Clap\Model\Estados');
    }

    public function municipio(){
		return $this->belongsTo('Modules\Clap\Model\Municipio');
	}

	public function parroquia(){
		return $this->belongsTo('Modules\Clap\Model\Parroquia');
	}

	public function sector_primario(){
		return $this->belongsTo('Modules\Proyecto\Model\SectorPrimario');
	}

	public function estatus(){
		return $this->belongsTo('Modules\Proyecto\Model\Estatus');
	}

	public function fuente_financiamiento(){
		return $this->belongsTo('Modules\Proyecto\Model\FuenteFinanciamiento');
	}

	public function ente_ejecutor(){
		return $this->belongsTo('Modules\Proyecto\Model\EnteEjecutor');
	}
}