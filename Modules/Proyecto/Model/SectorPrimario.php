<?php namespace Modules\Proyecto\Model;

use Modules\Admin\Model\Modelo;



class SectorPrimario extends modelo
{
    protected $table = 'sector_primario';
    protected $fillable = ["nombre"];
    protected $campos = [
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Sector Primario'
        ]
    ];

    public function proyectos(){
		return $this->hasMany('Modules\Proyecto\Model\Proyectos');
	}
}