<?php

namespace Modules\Proyecto\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Modules\Proyecto\Model\Proyectos;

class ProyectosTareas extends Model
{
	use SoftDeletes;

    protected $table = 'proyectos_tareas';
    protected $fillable = ["proyectos_id"];
    public $timestamps = false;

    public function proyectos()
    {
		return $this->belongsTo('Modules\Proyecto\Model\Proyectos');
	}
}