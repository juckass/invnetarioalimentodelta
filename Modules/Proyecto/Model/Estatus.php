<?php namespace Modules\Proyecto\Model;

use Modules\Admin\Model\Modelo;



class Estatus extends modelo
{
    protected $table = 'estatus';
    protected $fillable = ["nombre"];
    protected $campos = [
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Estatus'
        ]
    ];

    public function proyectos(){
		return $this->hasMany('Modules\Proyecto\Model\Proyectos');
	}
}