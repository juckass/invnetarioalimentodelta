<?php namespace Modules\Proyecto\Model;

use Modules\Admin\Model\Modelo;



class EnteEjecutor extends modelo
{
    protected $table = 'ente_ejecutor';
    protected $fillable = ["nombre"];
    protected $campos = [
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Ente Ejecutor'
        ]
    ];

    public function proyectos(){
		return $this->hasMany('Modules\Proyecto\Model\Proyectos');
	}
}