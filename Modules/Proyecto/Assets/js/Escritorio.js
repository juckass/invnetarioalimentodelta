var app;

$(function(){
	app = new Vue({
		el: '#app_escritorio',
		data: datos,
		methods : {
			buscar : function(){
				$.get($url + 'data', {
					ano : parseInt(this.ano),
					estatu : parseInt(this.estatu),
					sector_primario : parseInt(this.sector_primario),
				}, function(r){
					app.$data.municipios = r.municipios;
					app.$data.sectores = r.sectores;
					app.$data.total_inversion = r.total_inversion;
					app.$data.total_proyectos = r.total_proyectos;
					app.$data.total_proyectos_concluidos = r.total_proyectos_concluidos;
					app.$data.total_proyectos_ejecucion = r.total_proyectos_ejecucion;
					app.$data.total_proyectos_estatal = r.total_proyectos_estatal;
					app.$data.total_proyectos_iniciar = r.total_proyectos_iniciar;

					generarMapa();
					generarGrafica();
					generarGraficaSectores();
				});
			},
			cambiarEstatus : function(estatus){
				app.estatu = parseInt(estatus);

				$.get($url + 'data', {
					ano : parseInt(this.ano),
					estatu : parseInt(this.estatu),
					sector_primario : parseInt(this.sector_primario),
				}, function(r){
					app.$data.municipios = r.municipios;
					app.$data.total_inversion = r.total_inversion;
					app.$data.total_proyectos_estatal = r.total_proyectos_estatal;

					generarMapa();
					generarGrafica();
					generarGraficaSectores();
				});
			},
			cambiarSector : function(sector_primario){
				app.sector_primario = parseInt(sector_primario);
				app.buscar();
			}
		}
	});

	Highcharts.setOptions({
		colors: ['#8E44AD', '#32C5D2', '#4DB3A2', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4']
	});
	
	$("#ano").change(function(){
		app.buscar();
	});

	$("#sector_primario").change(function(){
		app.cambiarSector($(this).val());
	});

	$("#estatu").change(function(){
		app.cambiarEstatus($(this).val());
	});

	generarMapa();
	generarGrafica();
	generarGraficaSectores();
});


function generarMapa(){
	$('#mapa').highcharts('Map', {
		'chart' : {
			'borderWidth' : 1,
			'animation': false
		},
		'credits': {
			'enabled' : false
		},
		'title': {
			'align': 'center',
			'text': '',
			'useHTML': false,
		},
		'legend': {
			'layout': 'vertical',
			'align': 'left',
			'verticalAlign': 'bottom'
		},
		'mapNavigation': {
			'enabled': true
		},
		'colorAxis': {},
		'series': [{
			'type' : 'map',
			'name': 'mapa',
			'animation': {
				'duration': 1000
			},
			
			'data' : app.municipios,
			'mapData' : Highcharts.maps['venezuela/bolivar'],
			'joinBy' : ['slug', 'slug'],
			'events': {
		 		'click': function (e) {
		 			
	 				$municipio = e.point.name;
		 			if(e.point.name === 'Bolivariano <br />Angostura'){
		 				$municipio = 'Bolivariano Angostura';
		 			}

		 			window.open(dire+'?municipio='+$municipio, '_blank');  
		 		}
			 },

			'dataLabels': {
				'enabled': true,
				'color': '#FFFFFF',
				'formatter': function () {
					if (this.point.value > 0) {
						//return this.key + " (" + this.point.value + ")";
						return this.key;
					}
				}
			},
			'states': {
				'hover': {
					'color': '#1A3C82'
				}
			}
		}]
	});
}

function generarGrafica(){
	var $data = [
		{ 'name': 'En Ejecucion', 'y': parseFloat(app.total_proyectos_ejecucion.replace('.', '')) },
		{ 'name': 'Concluidos', 'y': parseFloat(app.total_proyectos_concluidos.replace('.', '')) },
		{ 'name': 'Por Iniciar', 'y': parseFloat(app.total_proyectos_iniciar.replace('.', '')) },
    ];

	$('#grafica').highcharts({
		'chart': {
	        'plotBackgroundColor': null,
	        'plotBorderWidth': null,
	        'plotShadow': false,
	        'type': 'pie'
	    },
	    'title': {
	        'text': 'Proyectos por Estatus'
	    },
	    'tooltip': {
	        'pointFormat': '{series.name}: <b>{point.percentage:.1f}%</b>'
	    },
	    'plotOptions': {
	        'pie': {
	            'allowPointSelect': true,
	            'cursor': 'pointer',
	            'dataLabels': {
	                'enabled': true,
	                'format': '<b>{point.name}</b>: {point.percentage:.1f} %',
	                'style': {
	                    'color': (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
	                }
	            }
	        }
	    },
	    'series': [{
	        'name': 'Proyectos',
	        'colorByPoint': true,
	        'data': $data
	    }]
    });
}

function generarGraficaSectores() {
	var $data = [{
		name: 'Sectores',
		data: []
	}],
	$categorias = [];

	for (var i in app.sectores) {
		$categorias.push(app.sectores[i].nombre);
		$data[0].data.push(app.sectores[i].cantidades[app.estatu]);
	}


	$('#graficaSectores').highcharts({
		chart: {
			type: 'column'
		},
		title: {
			text: 'Proyectos por Sectores Primarios'
		},
		xAxis: {
			labels:{
				padding: 5
			},
			categories: $categorias,
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			labels: {
				overflow: 'justify'
			}
		},
		plotOptions: {
			column: {
				colorByPoint: true
			},
			series: {
				borderWidth: 0,
				dataLabels: {
					enabled: true,
				}
			}
		},
		credits: {
			enabled: true
		},
		series: $data
	});
}


function number_format (number, decimals, decPoint, thousandsSep) {
	number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
	var n = !isFinite(+number) ? 0 : +number;
	var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals);
	var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep;
	var dec = (typeof decPoint === 'undefined') ? '.' : decPoint;
	var s = '';

	var toFixedFix = function (n, prec) {
		var k = Math.pow(10, prec);
		return '' + (Math.round(n * k) / k).toFixed(prec);
	}

	// @todo: for IE parseFloat(0.55).toFixed(0) = 0;
	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
	if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	}
	if ((s[1] || '').length < prec) {
		s[1] = s[1] || '';
		s[1] += new Array(prec - s[1].length + 1).join('0');
	}

	return s.join(dec);
}