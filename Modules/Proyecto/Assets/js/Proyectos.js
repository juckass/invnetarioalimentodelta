var aplicacion, $form, tabla, $Ladda;
$(function() {
	aplicacion = new app('formulario', {
		'limpiar' : function(){
			tabla.fnDraw();
			$('#tareas', '#botonera').addClass('disabled');
		},
		'buscar' : function(op, form){
			$('#tareas', '#botonera').removeClass('disabled').attr({
				'href' : $url + 'tareas/' + aplicacion.id
			});
		}
	});

	$form = aplicacion.form;

	$('#municipio_id').change(function(){
		aplicacion.selectCascada($(this).val(), 'parroquia_id','parroquias');
	});

	$('#tareas', '#botonera').addClass('disabled');

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [
			{ "data" : "nombre", "name" : "proyectos.nombre" },
			{ "data" : "estatus", "name" : "estatus.nombre" },
			{ "data" : "ente_ejecutor", "name" : "ente_ejecutor.nombre" },
			{ "data" : "municipio", "name" : "municipio.nombre" },
			{ "data" : "parroquia", "name" : "parroquia.nombre" },
			{ "data" : "sector_primario", "name" : "sector_primario.nombre" },
			{ "data" : "ano_ejecucion", "name" : "proyectos.ano_ejecucion" }
		]
	});
	
	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});

	$("#subir").on('click', function(e){
	    e.preventDefault();
	    $("#upload:hidden").trigger('click');
	});

	$("#upload").on('change', function(){
		var l = Ladda.create($("#subir").get(0));
	 	l.start();

		var options2 = { 
			url : $url + 'archivo',
			type : 'POST',
			success: function(r){
				if (typeof(r) == 'string'){
					aviso(r);
					return;
				}

				$("input", ".tabla-inventario").each(function(){
					var empresa = this.getAttribute('data-empresa'),
					rubro = this.getAttribute('data-rubro');

					this.value = r.cantidades[empresa][rubro] || 0;
				});

				aviso(r);
			},
			complete : function(){
				l.stop();
			}
		}; 
		
		$('#carga').ajaxSubmit(options2); 
	});
});