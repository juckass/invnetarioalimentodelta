$(function() {
	aplicacion = new app('formulario',{
		'url' : $url,
		'submit' 	: function(){
			return true;
		},
		'limpiar' 	: function(){
			return false;
		},
	});

	$('#sector_primario_id').val(sector_primario_id);
	$('#ano_ejecucion').val(ano);
	$('#estatus_id').val(estatus_id);
	$('#municipio_id').val(municipio_id);

	if (sector_primario_id != ''){
		reporte();
	}
	
	if (municipio_id != ''){
		reporte();
	}
	
	
	$('#municipio_id').change(function(){
		aplicacion.rellenar({
			'parroquia_id' : [],
		});

		aplicacion.selectCascada($(this).val(), 'parroquia_id','parroquias');
	});

	//reporte();

	$('#exportar').click(function(){
		var excel = $('input[name=\'excel\']', '#formulario');
		
		excel.val('1');

		reporte(true);

		excel.val('0');
	});
	
	$('#imprimir').click(function(){
		reporte(true);
	});

	$("#buscar").click(function(){
		reporte();
	});
});

function reporte($form){
	$form = $form || false;

	if ($form){
		$('#formulario').submit();
		return;
	}
	
	var data = formData('#formulario');
	data.html = 1;

	$.ajax($url + 'imprimir', {
		data : data,
		type: 'POST',
		success : function(html){
			$("#reportehtml").html(html);
		}
	});
}