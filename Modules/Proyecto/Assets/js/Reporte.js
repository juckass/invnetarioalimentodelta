var app, vueapp;

$(function() {
	vueapp = new Vue({
		el: '#reporteHtml',
		data: {
			'proyectos' : [],
			'total' : 0
		},
		methods : {
			buscar : function(){
				
			}
		}
	});

	aplicacion = new app('formulario',{
		'url' : $url,
		'submit' 	: function(){
			return true;
		},
		'limpiar' 	: function(){
			return false;
		},
	});

	$('#sector_primario_id').val(sector_primario_id);
	$('#ano_ejecucion').val(ano);
	$('#estatus_id').val(estatus_id);
	$('#municipio_id').val(municipio_id);

	if (sector_primario_id != ''){
		reporte();
	}
	
	if (municipio_id != ''){
		$('#municipio_id').selectpicker('render').selectpicker('refresh');
		aplicacion.selectCascada(municipio_id, 'parroquia_id','parroquias');
		reporte();
	}
	
	
	$('#municipio_id').change(function(){
		
		aplicacion.rellenar({
			'parroquia_id' : [],
		});

		if($(this).val() == null){

			return false;
		}

		aplicacion.selectCascada($(this).val(), 'parroquia_id','parroquias');
	});

	//reporte();

	$('#exportar').click(function(){
		var excel = $('input[name=\'excel\']', '#formulario');
		
		excel.val('1');

		reporte(true);

		excel.val('0');
	});
	
	$('#imprimir').click(function(){
		reporte(true);
	});

	$("#buscar").click(function(){
		reporte();
	});
});

function reporte($form){
	$form = $form || false;

	if ($form){
		$('#formulario').submit();
		return;
	}
	
	var data = formData('#formulario');
	data.html = 1;

	vueapp.$data.proyectos = [];
	vueapp.$data.total = 0;
	$.blockUI({
		message: '<h1>Cargando Reporte</h1>',
		css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        }
	}); 

	$.ajax($url + 'imprimir', {
		data : data,
		type: 'POST',
		success : function(r){
			vueapp.$data.proyectos = r.proyectos;
			vueapp.$data.total = r.total;
			setTimeout($.unblockUI, 200);
			//$("#reportehtml").html(html);
		}
	});
}