$(function() {
	$("#formulario").submit(function(evt){
		return false;
	});

	$("#btn_graficar").click(function(){
		$.ajax($url + 'grafica', {
			data : $("#formulario").serialize(),
			success : generarGrafica
		});

		$.ajax($url + 'grafica-torta', {
			data : $("#formulario").serialize(),
			success : generarTorta
		});
	}).click();
});

function generarGrafica(r){
	$('#grafica').highcharts({
		chart: {
			type: 'column'
		},
		title: {
			text: 'Proyectos'
		},
		subtitle: {
			text: 'Proyectos.'
		},
		xAxis: {
			type: 'category',
			labels: {
				style: {
					fontSize: '13px',
					fontFamily: 'Verdana, sans-serif'
				}
			}
		},
		yAxis: {
			title: {
				text: 'Cantidad de Proyectos'
			}
		},
		legend: {
			enabled: false
		},
		plotOptions: {
			series: {
				borderWidth: 0,
				dataLabels: {
					enabled: true,
					format: '{point.y}'
				}
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b><br/>'
		},

		series: r.series,
		drilldown: r.drilldown
	});
}

function generarTorta(r){
	$('#grafica_torta').highcharts({
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title: {
			text: 'Proyectos por Municipios'
		},
		tooltip: {
			pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.percentage:.1f} %',
					style: {
						color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
					}
				}
			}
		},
		series:  [{
            name: 'Proyectos',
            colorByPoint: true,
            data: r
        }]
	});
}