var aplicacion, $form, tabla;



$(function() {

	aplicacion = new app("formulario", {

		'antes': function(accion) {

			$("#redes_id").change()

		},

		'limpiar': function() {

			$("input", ".tabla-inventario").val('0');

			$("#redes_id").change()

		},

		'buscar': function(r) {}

	});



	aplicacion.limpiar();

	$form = aplicacion.form;



	$("input", ".tabla-inventario").TouchSpin({

		max: 10000,

		decimals: 2,

		step: 0.01,

		verticalbuttons: true,

		initval: 0

	});



	$("input", ".tabla-inventario").val(0);



	$("#subir").on('click', function(e) {

		e.preventDefault();

		$("#upload:hidden").trigger('click');

	});



	$("#mes").on('change', function() {

		var fecha = $("#mes").val();

		var red   = $("#redes_id").val();	

		enviar(fecha, red);

	});



	$("option:first", "#redes_id").remove();

	$("#redes_id").on('change', function() {

		var red = $(this).val();

		$(".cont_data:not(.inv" + red + ")", ".table").hide();

		$(".cont_data:not(.inv" + red + ") input", ".table").prop('disabled', true);

		$(".inv" + red, ".table").show();

		$(".inv" + red + " input", ".table").prop('disabled', false);

	}).change();



	function enviar(fecha, red){

	
		$.ajax({

			url: $url + 'fecha',

			data:{  

				'fecha' :  fecha,

				'red'   :  red

			},

			success: function(r) {

				$("input", ".tabla-inventario").val('0');

				if ($.isArray(r)) {

					return;

				}


				$("input", ".tabla-inventario").each(function() {

					var empresa = this.getAttribute('data-empresa'),

						rubro = this.getAttribute('data-rubro');



					if (r[empresa] == undefined){

						this.value = 0;

					}else{

						this.value = r[empresa][rubro] || 0;

					}

				});

			}

		});		

	}

	

	$("#upload").on('change', function() {

		var l = Ladda.create($("#subir").get(0));

		l.start();

		var options2 = {

			url: $url + 'archivo',

			type: 'POST',

			success: function(r) {

				if (typeof(r) == 'string') {

					aviso(r);

					return;

				}

				$("input", ".tabla-inventario").each(function() {

					var empresa = this.getAttribute('data-empresa'),

						rubro = this.getAttribute('data-rubro');



					if (r[empresa] == undefined){

						this.value = 0;

					}else{

						this.value = r[empresa][rubro] || 0;

					}

				});

				aviso(r);

			},

			complete: function() {

				l.stop();

			}

		};

		$('#carga').ajaxSubmit(options2);

	});

});

