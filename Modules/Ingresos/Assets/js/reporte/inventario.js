$(function() {
	$("#fecha").datepicker({
		changeMonth: true,
		onSelect : function(){
			reporte();
		}
	}).change(function(){
		reporte();
	});

	reporte();
	
	$('#imprimir').click(function(){
		reporte(true);
	});

	$("#buscar").click(function(){
		reporte();
	});
});

function reporte($form){
	$form = $form || false;

	var $fecha = $("#fecha").val();

	if($fecha == ''){
		aviso('El campo Fecha es Requerido.', false);
		return false;
	}

	if ($form){
		$('#formulario').submit();
	}
	
	$.ajax($url + 'imprimir', {
		data : {
			fecha : $fecha,
			html : 1
		},
		type: 'POST',
		success : function(html){
			$("#reportehtml").html(html);
		}
	});
}