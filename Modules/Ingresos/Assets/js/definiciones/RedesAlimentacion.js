var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
	});

	$form = aplicacion.form;

	tabla = $('#tabla')
	.on('click', 'tbody tr', function(){
		aplicacion.buscar(this.id);
	})
	.dataTable({
		ajax: $url + 'datatable',
		columns: [{"data":"nombre","name":"nombre"}]
	});
});