$(function() {

	$tipo = $('#tipo_articulo').val("");
	$red = $('#red_alimentos').val("");
	seleccion();
	$("#formulario").submit(function(evt){
		return false;
	});

	$('input[name="rubro[]"]').each(function(){
		var self = $(this),
		label = self.next(),
		label_text = label.text();
		
		label.remove();
		self.iCheck({
			checkboxClass: 'icheckbox_line-blue',
			radioClass: 'iradio_inline',
			insert: '<div class="icheck_line-icon"></div>' + label_text
		});
	});

	$("#red_alimentos").on('change', function(){
		seleccion();
	});

	$("#tipo_articulo").on('change', function(){
		seleccion();
	});

	$('#tipos_rubros').bootstrapSwitch('state', true, true)
	$('#tipos_rubros').on('switchChange.bootstrapSwitch', function(event, state) {		
		seleccion();
	});

	$("#btn_graficar").click(function(){
		$('a[href="#tab_grafica_mes"]', ".portlet-grafica-mes").tab('show');
		$.ajax($url + 'graficaAnos', {
			data : $("#formulario").serialize(),
			success : generarGrafica
		});
	}).click();
});

function seleccion(){
	$state = $('#tipos_rubros').prop('checked');
	$tipo = $('#tipo_articulo').val();
	$red = $('#red_alimentos').val();
	$ano = $('#ano_select').val();

	if($tipo == ""){
		$tipo = 'todos';
	}

	if($red == ""){
		$red = 'todos';
	}

	if($state){
		//grafica
		$rubro='priorizado'; 
				
	}else{
		$rubro='todos';
		
	}

	$('#rubro').val($rubro);
	$('#tipo').val($tipo);	
	$('#red').val($red);
	$('#ano').val($ano);
	$("#btn_graficar").click();
}

function generarGrafica(r){
	$('#grafica').highcharts({
		title: {
			text: 'Comportamiento de los Ingresos'
		},
		subtitle: {
			text: 'Comportamiento de los Ingresos'
		},
		xAxis: {
			categories: r.categorias,
			tickmarkPlacement: 'on',
			title: {
				enabled: false
			}
		},
		yAxis: {
			title: {
				text: 'Toneladas de Alimentos'
			}
		},
		tooltip: {
			shared: true
		},
		series: r.series
	});
}