$(function() {
	$tipo = $('#tipo_articulo').val("");
	$red = $('#red_alimentos').val("");
	

	$("#red_alimentos").on('change', function(){
		seleccion();
	});

	$("#tipo_articulo").on('change', function(){
		seleccion();
	});

	$('#tipos_rubros').bootstrapSwitch('state', true, true)
	$('#tipos_rubros').on('switchChange.bootstrapSwitch', function(event, state) {		
		seleccion();
	});

	$("#formulario").submit(function(evt){
		return false;
	});

	$("#btn_graficar").click(function(){
		graficames();
	}).click();

	$("#mes").on('change', function(){
		seleccion();
		$mes = $('#mes').val();
		$('#mes_h').val($mes);
		$("#btn_graficar").click();
	});
	$("#ano").on('change', function(){
		seleccion();
	});

	seleccion();

	$(".btn-rubros").click(function(){
		var rubros = $("#rubros");
		rubros.toggle(400);
	});

	$('input[name="rubro[]"]').each(function(){
		var self = $(this),
		label = self.next(),
		label_text = label.text();

		label.remove();
		self.iCheck({
			checkboxClass: 'icheckbox_line-blue',
			radioClass: 'iradio_inline',
			insert: '<div class="icheck_line-icon"></div>' + label_text
		});
	});
});

Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
	return {
		linearGradient: {
			x1: 0,
			y1: 0,
			x2: 0,
			y2: 1
		},
		stops: [
			[0, color],
			[1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
		]
	};
});

function seleccion(){
	$state = $('#tipos_rubros').prop('checked');
	$tipo = $('#tipo_articulo').val();
	$red = $('#red_alimentos').val();
	$ano = $('#ano').val();

	if($tipo == ""){
		$tipo = 'todos';
	}

	if($red == ""){
		$red = 'todos';
	}

	if($state){
		//grafica
		$rubro='priorizado'; 
		$('.porcentaje').show();
				
	}else{
		$rubro='todos';
		$('.porcentaje').hide();	
	}
	$('#rubro').val($rubro);
	$('#tipo').val($tipo);	
	$('#red').val($red);
	$('#ano2').val($ano);
	grafica($rubro, $tipo, $red, $ano);
	$t =$('#mes').val();
	$('#mes_h').val($t);
	$("#btn_graficar").click();
	porcentajerubros($t, $rubro, $tipo, $red, $ano);
}
//primera grafica
function grafica(rubro,tipo,red, ano){ 
	$.ajax( $url + "grafica",{
		data : {
			'rubro' : rubro,
			'tipo'  : tipo,
			'red'	: red,
			'ano'   : ano
		},
		success : generarGrafica
	});
}
//segunda grafica 
function graficames(){
	$('a[href="#tab_grafica_mes"]', ".portlet-grafica-mes").tab('show')

	$.ajax($url + "graficames", {
		data : $("#formulario").serialize(),
		success : function(r){
			$('.porcentaje_ingreso').text(r.totalPorcentaje);
			$('.mes_porcent').text(r.mes);
			generarGraficames(r);
		}
	});
}
//tersera grafica
function porcentajerubros(mes,rubro,tipo,red,ano){
	$.ajax($url + "graficarubros", {
		data : {
			'mes': mes,
			'rubro': rubro,
			'tipo' : tipo,
			'red'  : red,
			'ano'  : ano
		},
		success : function(r){
			generarGraficarubro(r);
		}
	});
}

/*-----------------Funciones Graficas-----------------------------*/
function generarGrafica(r){
	$('#grafica').highcharts({
		chart: {
			type: 'column'
		},
		title: {
			text: ''
		},
		xAxis: {
			type: 'category',
			labels: {
				style: {
					fontSize: '13px',
					fontFamily: 'Verdana, sans-serif'
				}
			}
		},
		yAxis: {
			title: {
				text: 'Toneladas Metricas'
			}
		},
		legend: {
			enabled: false
		},
		plotOptions: {
			series: {
				borderWidth: 0,
				dataLabels: {
					enabled: true,
					format: '{point.y:.2f} Tm'
				}
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f} TM</b><br/>\
			<span style="color:{point.color}">Porcentaje</span>: <b>{point.p:.2f}%</b><br/>'
		},

		series: r.series,
		drilldown: r.drilldown
	});

}
function generarGraficames(r){
	$('#graficames').highcharts({
		chart: {
			type: 'column'
		},
		title: {
			text: ''
		},
		xAxis: {
			type: 'category',
			labels: {
				style: {
					fontSize: '13px',
					fontFamily: 'Verdana, sans-serif'
				}
			}
		},
		yAxis: {
			title: {
				text: 'Toneladas Metricas'
			}
		},
		legend: {
			enabled: false
		},
		plotOptions: {
			series: {
				borderWidth: 0,
				dataLabels: {
					enabled: true,
					format: '{point.y:.2f} Tm'
				}
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f} TM</b><br/>'
		},

		series: r.series,
		drilldown: r.drilldown
	});
}

function generarGraficarubro(r){
	//console.log(r);
   $('#graficarubros').highcharts({
		chart: {
			type: 'bar'
		},
		title: {
			text: 'Porcentaje de Ingresos de Alimentos Por Rubros'
		},
		subtitle: {
			text: 'Mes de ' + $("option[value='" + $("#mes").val() + "']", "#mes").text() 
		},
		xAxis: {
			labels:{
				padding: 5
			},
			categories: r.rubros,
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: ' %',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' %'
		},
		plotOptions: {
			bar: {
				colorByPoint: true,
				dataLabels: {
					text: ' %',
					enabled: true
				},
				groupPadding : 0.05
			},
			series: {
				borderWidth: 0,
				dataLabels: {
					enabled: true,
					format: '{point.y:.2f}% ({point.i:.2f} TM)'
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -80,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
			shadow: true
		},
		credits: {
			enabled: true
		},
		series: [{
			name: 'ingreso',
			data: r.ingresos
		}
		]
	});
}