<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngresosTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		
		
		Schema::create('ingresos_categoria', function(Blueprint $table){
			$table->increments('id');
			$table->string('nombre', 80)->unique();

			$table->timestamps();
			$table->softDeletes();     
		});

		Schema::create('ingresos_rubros', function(Blueprint $table){
			$table->increments('id');

			$table->string('nombre', 80)->unique();
			$table->integer('estatus')->unsigned();
			$table->integer('categoria_id')->unsigned();

			$table->timestamps();
			$table->softDeletes();

			$table->foreign('categoria_id')
				->references('id')->on('ingresos_categoria')
				->onDelete('cascade')->onUpdate('cascade');
		});

		Schema::create('ingresos_requerimiento', function(Blueprint $table){
			$table->increments('id');

			$table->integer('municipio_id')->unsigned();
			$table->integer('rubro_id')->unsigned();
			$table->decimal('cantidad', 8,2);

			$table->timestamps();
			$table->softDeletes();

			$table->foreign('municipio_id')
				->references('id')->on('municipio')
				->onDelete('cascade')->onUpdate('cascade');

			$table->foreign('rubro_id')
				->references('id')->on('ingresos_rubros')
				->onDelete('cascade')->onUpdate('cascade');
		});

		Schema::create('ingresos_inventario', function(Blueprint $table){
			$table->increments('id');

			$table->integer('empresa_id')->unsigned();
			$table->integer('ingresorubros_id')->unsigned();
			$table->integer('redes_id')->unsigned();
			$table->decimal('cantidad', 8,2);
			$table->string('mes');
			$table->integer('ano')->nullable();

			$table->timestamps();
			$table->softDeletes();
			
			$table->unique(['empresa_id','ingresorubros_id','mes','ano' ],'string_inventario_index_unique');

			$table->foreign('empresa_id')
				->references('id')->on('empresas')
				->onDelete('cascade')->onUpdate('cascade');

			$table->foreign('ingresorubros_id')
				->references('id')->on('ingresos_rubros')
				->onDelete('cascade')->onUpdate('cascade');

			$table->foreign('redes_id')
				->references('id')->on('redes_alimentacion')
				->onDelete('cascade')->onUpdate('cascade');        
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('ingresos_inventario');
		Schema::dropIfExists('ingresos_requerimiento');
		Schema::dropIfExists('ingresos_rubros');
		Schema::dropIfExists('ingresos_categoria');  
	}

}
