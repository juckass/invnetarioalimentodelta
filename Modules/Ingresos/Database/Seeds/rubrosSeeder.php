<?php namespace Modules\Ingresos\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Ingresos\Model\definiciones\rubros;

class rubrosSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('ingresos_categoria')->insert([
			'nombre' => 'Alimentos',
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		]);

		\DB::table('ingresos_categoria')->insert([
			'nombre' => 'No alimenticios',
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		]);

		$rubros = [
			['Condimentos', 2, 1],
			['Sardina En Lata', 2, 1],
			['Adobo', 2, 1],
			['Vinagre', 2, 1],
			['Salsa Inglesa', 2, 1],
			['Salsa De Tomate', 0, 1],
			['Salsa De Soya', 2, 1],
			['Salsa De Ajo ', 2, 1],
			['Quesos', 2, 1],
			['Mayonesa', 2, 1],
			['Jugos  Varios', 2, 1],
			['Haria De Maiz Tostado', 2, 1],
			['Leche Condesada', 2, 1],
			['Manteca', 2, 1],
			['Arveja ', 1, 1],
			['Sal domestica', 2, 1],
			['Pescado (Fresco)', 2, 1],
			['Panettones', 2, 1],
			['Jamon', 2, 1],
			['Torta Negra', 2, 1],
			['nectares y bebidas Lacteas', 2, 1],
			['Salsas Varias', 2, 1],
			['Cereal', 2, 1],
			['Huevos', 2, 1],
			['Frutas/Verduras', 2, 1],
			['Frijol', 2, 1],
			['Margarina', 1, 1],
			['Lentejas', 1, 1],
			['Leche / Leche en Polvo Completa', 1, 1],
			['Harina de Trigo', 1, 1],
			['Harina de Maiz Precocida', 1, 1],
			['Carne de Res', 1, 1],
			['Arroz Pulido', 1, 1],
			['Arvejas', 1, 1],
			['Azucar Blanco', 1, 1],
			['Caraotas Negras', 1, 1],
			['Mortadela', 1, 1],
			['Pastas', 1, 1],
			['Embutidos', 2, 1],
			['Carne De Cerdo', 2, 1],
			['Bebidas Lacteas', 2, 1],
			['Atun Lata', 2, 1],
			['Aceite Vegetal', 1, 1],
			['Sal', 1, 1],
			['Pollo', 1, 1],
			['Pernil', 1, 1],
			['Leche Pasteurizada y UHT', 1, 1],
			['Café Molido', 1, 1],
			['Vasos Plásticos', 2, 2],
			['Uniformes Escolares', 2, 2],
			['Pañales desechables', 2, 2],
			['Limpiador', 2, 2],
			['Jabón de tocador', 2, 2],
			['Detergente en polvo', 2, 2],
			['Papel higienico', 2, 2],
			['Articulos de limpieza', 2, 2],
			['Bolsas Plásticas', 2, 2],
			['Champu', 2, 2],
			['Cloro', 2, 2],
			['Desinfectante', 2, 2],
			['Desodorante', 2, 2],
			['Detergente líquido', 2, 2],
			['Crema Dental', 2, 2]
		];

		foreach ($rubros as $rubro) {
			rubros::create([
				'nombre' 		=> $rubro[0],
				'estatus' 		=> $rubro[1],
				'categoria_id' 	=> $rubro[2]
			]);
		}
	}
}



