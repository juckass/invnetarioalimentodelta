<?php namespace Modules\Ingresos\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class IngresosDatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call(rubrosSeeder::class);
		$this->call(redesAlimentacionSeeder::class);

		Model::reguard();
	}

}