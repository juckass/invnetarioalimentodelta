<?php namespace Modules\Ingresos\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Ingresos\Model\definiciones\RedesAlimentacion;

class redesAlimentacionSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$redes = [
			'Publicas',
			'Privadas',
			'Extranjeras'
		];

		foreach ($redes as $red) {
			RedesAlimentacion::create([
				'nombre' => $red
			]);
		}
	}
}



