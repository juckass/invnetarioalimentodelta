<?php

namespace Modules\Ingresos\Model;

use Modules\Admin\Model\Modelo;



class Categorias extends Modelo
{
    protected $table = 'categorias';
    protected $fillable = ["nombre"];
    protected $campos = [
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Categorias'
        ]
    ];
}