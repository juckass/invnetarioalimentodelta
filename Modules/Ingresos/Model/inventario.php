<?php  
namespace Modules\Ingresos\Model;
use Modules\Admin\Model\modelo;

use Carbon\Carbon;

class inventario extends modelo
{
	protected $table = 'ingresos_inventario';

	 /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'empresa_id',
		'ingresorubros_id',
		'cantidad',
		'mes',
		'mes_str',
		'ano'
	]; 

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

	

	public function empresas(){
		return $this->belongsTo('Modules\Inventario\Model\empresas');
	}
	
	public function rubros(){
		return $this->belongsTo('Modules\Ingresos\Model\definiciones\rubros');
	}
}