<?php

namespace Modules\Ingresos\Model;

use Modules\Admin\Model\Modelo;



class Rubros extends modelo
{
    protected $table = 'rubros';
    protected $fillable = ["nombre","estatus","categoria_id"];
    protected $campos = [
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Rubros'
        ],
        'estatus' => [
            'type' => 'number',
            'label' => 'Estatus',
            'placeholder' => 'Estatus del Rubros'
        ],
        'categoria_id' => [
            'type' => 'number',
            'label' => 'Categoria',
            'placeholder' => 'Categoria del Rubros'
        ]
    ];


    public function inventario()
	{
		return $this->hasMany('Modules\Inventario\Model\inventario');
	}
}