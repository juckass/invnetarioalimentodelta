<?php

namespace Modules\Ingresos\Model;

use Modules\Admin\Model\Modelo;



class RedesAlimentacion extends modelo
{
    protected $table = 'redes_alimentacion';
    protected $fillable = ["nombre"];
    protected $campos = [
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Redes Alimentacion'
        ]
    ];
}