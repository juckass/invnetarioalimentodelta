<?php
namespace Modules\Ingresos\Model\definiciones;

use Modules\Admin\Model\modelo;

class RedesAlimentacion extends modelo
{
	protected $table = 'redes_alimentacion';
    protected $fillable = ['id', 'nombre'];

    //protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}