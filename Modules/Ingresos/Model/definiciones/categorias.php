<?php

namespace Modules\Ingresos\Model\definiciones;

use Modules\Admin\Model\Modelo;



class categorias extends modelo
{
    protected $table = 'ingresos_categoria';
    protected $fillable = ["nombre"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Ingresos Categoria'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}