<?php

namespace Modules\Ingresos\Model\definiciones;

use Modules\Admin\Model\Modelo;



class Rubros extends modelo
{
    protected $table = 'ingresos_rubros';
    protected $fillable = ["nombre","estatus","categoria_id"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Ingresos Rubros'
    ],
    'estatus' => [
         'type'       => 'select',
            'label'      => 'estatus',
            'cont_class' => 'form-group col-md-4',
            'options'=>[
                '1' => 'Priorizada',
                '2' => 'No Priorizada'
            ] 
    ],
    'categoria_id' => [
       'type'        => 'select',
            'label'       => 'Categorias',
            'placeholder' => '- Seleccione un categoria',
            
    ]
    
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['categoria_id']['options'] = categorias::pluck('nombre', 'id');
        
    }

    
}