<?php

namespace Modules\Ingresos\Model\definiciones;

use Modules\Admin\Model\Modelo;
use Modules\Clap\Model\Municipio;


class IngresosRequerimiento extends modelo
{
    protected $table = 'ingresos_requerimiento';
    protected $fillable = ["municipio_id","rubro_id","cantidad"];
    protected $campos = [
    'municipio_id' => [
        'type'        => 'select',
        'label' => 'Municipio',
        'placeholder' => '- Seleccione un Municipio'
    ],
    'rubro_id' => [
       'type'        => 'select',
            'label'       => 'Categorias',
            'placeholder' => '- Seleccione un categoria',
    ],
    'cantidad' => [
        'type' => 'text',
        'label' => 'Cantidad',
        'placeholder' => 'Cantidad del Ingresos Requerimiento'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['rubro_id']['options'] = Rubros::pluck('nombre', 'id');
        $this->campos['municipio_id']['options'] = Municipio::pluck('nombre', 'id');
    }

    
}