@extends(isset($layouts) ? $layouts : 'admin::layouts.default')

@section('content-top')
    @include('admin::partials.botonera')
    
    @include('admin::partials.ubicacion', ['ubicacion' => ['Ingresos Rubros']])
    
    @include('admin::partials.modal-busqueda', [
        'titulo' => 'Buscar IngresosRubros.',
        'columnas' => [
            'Nombre' => '33.333333333333',
		'Estatus' => '33.333333333333',
		'Categoria' => '33.333333333333'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $IngresosRubros->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection