@extends('admin::layouts.default')
@section('content')
	@include('admin::partials.botonera')

	{{ Form::bsModalBusqueda([
		'Municipio' => '33.333333333333',
		'Rubro' 	=> '33.333333333333',
		'Cantidad' 	=> '33.333333333333'
	]) }}

	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url('/') }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Ingresos Requerimiento</span>
		</li>
	</ul>
	
	<div class="row">
		{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}

			{{ Form::bsSelect('municipio_id', $controller->municipios() ,'', [
				'label' 		=> 'Municipio',
				'required' 		=> 'required',
			]) }}

			{{ Form::bsSelect('rubro_id', $controller->rubros() ,'', [
				'label' 		=> 'Rubro',
				'required' 		=> 'required',
			]) }}

			{{ Form::bsText('cantidad', '', [
				'label' => 'Cantidad',
				'placeholder' => 'Cantidad',
				'required' => 'required'
			]) }}
		
		{!! Form::close() !!}
	</div>
@endsection