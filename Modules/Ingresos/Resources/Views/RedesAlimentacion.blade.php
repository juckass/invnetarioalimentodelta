@extends(isset($layouts) ? $layouts : 'admin::layouts.default')

@section('content-top')
    @include('admin::partials.botonera')
    
    @include('admin::partials.ubicacion', ['ubicacion' => ['Redes Alimentacion']])
    
    @include('admin::partials.modal-busqueda', [
        'titulo' => 'Buscar RedesAlimentacion.',
        'columnas' => [
            'Nombre' => '100'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $RedesAlimentacion->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection