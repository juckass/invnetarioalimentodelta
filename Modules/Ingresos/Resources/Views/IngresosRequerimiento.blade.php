@extends(isset($layouts) ? $layouts : 'admin::layouts.default')

@section('content-top')
    @include('admin::partials.botonera')
    
    @include('admin::partials.ubicacion', ['ubicacion' => ['Ingresos Requerimiento']])
    
    @include('admin::partials.modal-busqueda', [
        'titulo' => 'Buscar IngresosRequerimiento.',
        'columnas' => [
            'Municipio' => '33.333333333333',
		'Rubro' => '33.333333333333',
		'Cantidad' => '33.333333333333'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $IngresosRequerimiento->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection