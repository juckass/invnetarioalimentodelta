@extends('admin::layouts.default')
@section('content')
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Comportamiento</span>
		</li>
	</ul>
	<div class="row">
		<div class="col-sm-2">
			<h3>Tipos de Rubros</h3>
			<input id="tipos_rubros" type="checkbox" class="make-switch" data-on-text="Priorizado" data-off-text="Todos">
		</div>
		
		<div class="col-sm-3">
			<h3>Red de alimentos</h3>
			{{ Form::Select('red_alimentos', $controller->red(), '', [
				'id'=>'red_alimentos',
				'label' => '',
				'class' => 'form-control',
				'placeholder' => 'Todos',
			]) }}
		</div>
		<div class="col-sm-3">
			<h3>Tipos de Articulos</h3>
			{{ Form::Select('tipo_articulo', $controller->alimentos(), '', [
				'id'=>'tipo_articulo',
				'label' => '',
				'class' => 'form-control',
				'placeholder' => 'Todos',
			]) }}
		</div>
		<div class="col-sm-4">
			<h3>Año</h3>
			{{ Form::Select('ano_select', $anos, '', [
				'id'=>'ano_select',
				'label' => 'Año',
				'class' => 'form-control',
			]) }}
		</div>	
	</div>
	<br>	
	<div class="row">
		<div class="portlet portlet-grafica-mes box blue col-sm-12">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-bar-chart"></i>
					<span class="caption-subject bold uppercase"> Comportamiento Ingresos</span>
				</div>
				<div class="actions">
					<button id="btn_graficar" class="btn btn-circle btn-default btn-sm" title="Graficar">
						<i class="fa fa-bar-chart"></i>
						<span class="visible-lg-inline visible-md-inline">Graficar</span>
					</button>
				</div>
			</div>
			<div class="portlet-body tabs-below">
				<div class="tabbable-line">
				<ul class="nav nav-tabs">
						<li class="active">
							<a href="#tab_grafica_mes" data-toggle="tab"> Grafica </a>
						</li>
						<li>
							<a href="#tab_grafica_mes_opciones" data-toggle="tab"> Rubros </a>
						</li>
					</ul>
					<div class="tab-content">
						<div id="tab_grafica_mes" class="tab-pane active">
							<div id="grafica" class="col-sm-12"></div>
						</div>
						<div id="tab_grafica_mes_opciones" class="tab-pane" style="overflow: auto;">
							{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST']) !!}
								
								<input type="hidden" id="tipo" name="op" value="">
								<input type="hidden" id="red" name="red" value="">
								<input type="hidden" id="ano" name="ano" value="">
								<input type="hidden" id="rubro" name="tipo_rubro" value="">

								@foreach($controller->rubros() as $id => $texto)
								<div class="col-md-4 col-sm-6">
									<input name="rubro[]" value="{{ $id }}" type="checkbox" />
									<label>{{ $texto }}</label>
								</div>
								@endforeach
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection