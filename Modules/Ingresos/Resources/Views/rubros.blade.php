@extends(isset($layouts) ? $layouts : 'admin::layouts.default')

@section('content-top')
    @include('admin::partials.botonera')
    
    @include('admin::partials.ubicacion', ['ubicacion' => ['Rubros']])
    
    @include('admin::partials.modal-busqueda', [
        'titulo' => 'Buscar Rubros.',
        'columnas' => [
        'Nombre' => '100'
		
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Rubros->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection