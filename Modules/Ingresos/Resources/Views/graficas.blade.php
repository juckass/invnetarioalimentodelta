@extends('admin::layouts.default') 
@section('content')
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Ingresos<i class="fa fa-circle"></i></span>
		</li>
		<li>
			<span>Graficas</span>
		</li>
	</ul>
	<div class="row">
		<div class="col-sm-12 porcentaje" id="porcentaje">
			<h2>Ingreso del Mes de <span class="mes_porcent"></span> :  <span class="porcentaje_ingreso"> </span></h2>
			<hr>	
		</div>
		<div class="col-sm-2">
			<h3>Tipos de Rubros</h3>
			<input id="tipos_rubros" type="checkbox" class="make-switch" data-on-text="Priorizado" data-off-text="Todos">
		</div>
		
		<div class="col-sm-3">
			<h3>Red de alimentos</h3>
			{{ Form::Select('red_alimentos', 
				$controller->red(), '', [
					'label' => '',
					'class' => 'form-control',
					'placeholder' => 'Todos',
					'id'=>'red_alimentos'
			]) }}
		</div>
		<div class="col-sm-3">
			<h3>Tipos de Articulos</h3>
			{{ Form::Select('tipo_articulo', 
				$controller->alimentos(), '', [
					'label' => '',
					'class' => 'form-control',
					'placeholder' => 'Todos',
					'id'=>'tipo_articulo'
			]) }}
		</div>	<div class="col-sm-2">
			<h3>Mes</h3>
			{{ Form::select('mes',$controller->meses, $mes, [
				'label' => 'Mes',
				'required' => 'required',
				'class' => 'form-control',
				'id'=>'mes'
			]) }}
		</div>
		<div class="col-sm-2">
			<h3>Año</h3>
			{{ Form::select('ano',$controller->ano(), $ano, [
				'label' => 'Año',
				'required' => 'required',
				'class' => 'form-control',
				'id'=>'ano'
			]) }}
		</div>
	</div>	

	<div class="row" style="margin-top: 30px;">
		<div class="portlet box bg-red-thunderbird bg-font-red-thunderbird col-sm-12">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-bar-chart"></i>
					<span class="caption-subject bold uppercase"> Ingresos de Alimentos</span>
					<span class="caption-helper font-white">Grafica por Meses</span>
				</div>
			</div>
			<div class="portlet-body tabs-below">
				<div id="grafica"></div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="portlet portlet-grafica-mes box bg-red-thunderbird bg-font-red-thunderbird col-sm-12">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-bar-chart"></i>
					<span class="caption-subject bold uppercase"> Ingresos de Alimentos</span>
					<span class="caption-helper font-white">
						Mes de <span class="mes_porcent"></span> ( <span class="porcentaje_ingreso"></span> )
					</span>
				</div>
				<div class="actions">
					<button id="btn_graficar" class="btn btn-circle btn-default btn-sm" title="Graficar">
						<i class="fa fa-bar-chart"></i>
						<span class="visible-lg-inline visible-md-inline">Graficar</span>
					</button>
				</div>
			</div>
			<div class="portlet-body tabs-below">
				<div class="tabbable-line">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#tab_grafica_mes" data-toggle="tab"> Grafica </a>
						</li>
						<li>
							<a href="#tab_grafica_mes_opciones" data-toggle="tab"> Opciones </a>
						</li>
					</ul>
					<div class="tab-content">
						<div id="tab_grafica_mes" class="tab-pane active">
							<div class="col-sm-12 porcentaje" id="porcentaje"></div>
							<div id="graficames"></div>
						</div>
						<div id="tab_grafica_mes_opciones" class="tab-pane" style="overflow: auto;">
							{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST']) !!}
								<input type="hidden" id="mes_h" name="mes" value="{{ $mes }}">
								<input type="hidden" id="rubro" name="rubro_select" value="">
								<input type="hidden" id="tipo" name="op" value="">
								<input type="hidden" id="red" name="red" value="">
								<input type="hidden" id="ano2" name="ano" value="">

								@foreach($controller->rubros() as $id => $texto)
								<div class="col-md-4 col-sm-6">
									<input name="rubro[]" value="{{ $id }}" type="checkbox" />
									<label>{{ $texto }}</label>
								</div>
								@endforeach
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="portlet box bg-red-thunderbird bg-font-red-thunderbird col-sm-12">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-bar-chart"></i>
					<span class="caption-subject bold uppercase"> Ingreso Por Rubros</span>
					<span class="caption-helper font-white">Porcentaje de Ingreso por Rubros</span>
				</div>
			</div>
			<div class="portlet-body tabs-below">
				<div id="graficarubros"></div>
			</div>
		</div>
	</div>
@endsection
@push('css')
<style type="text/css" media="screen">
.icheckbox_line-blue{
	white-space: nowrap;      /* CSS3 */   
	white-space: -moz-nowrap; /* Firefox */    
	white-space: -nowrap;     /* Opera <7 */   
	white-space: -o-nowrap;   /* Opera 7 */    
	word-wrap: break-word;      /* IE */
}
.collapse{
	margin-bottom: 20px;
}

#graficarubros{
	height: 1000px;
}
.btn-rubros{
	margin-top: 25px;
}
#grafica{
	margin-top: 25px;
}
</style>
@endpush

