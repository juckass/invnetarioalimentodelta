<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => Config::get('admin.prefix')], function() {
	Route::group(['prefix' => 'ingresos'], function(){ 
		Route::group(['prefix' => 'definiciones'], function(){ 
			Route::group(['prefix' => 'categoria'], function() {
				Route::get('/', 				'definiciones\IngresosCategoriaController@index');
				Route::post('guardar',			'definiciones\IngresosCategoriaController@guardar');
				Route::put('guardar/{id}',		'definiciones\IngresosCategoriaController@guardar');
				Route::get('buscar/{id}', 		'definiciones\IngresosCategoriaController@buscar');
				Route::get('datatable', 		'definiciones\IngresosCategoriaController@datatable');	
				Route::delete('eliminar/{id}', 	'definiciones\IngresosCategoriaController@eliminar');

			});
			Route::group(['prefix' => 'Rubors_Requerimientos'], function() {
				Route::get('/', 				'definiciones\IngresosRequerimientoController@index');
				Route::post('guardar',			'definiciones\IngresosRequerimientoController@guardar');
				Route::put('guardar/{id}',		'definiciones\IngresosRequerimientoController@guardar');
				Route::get('buscar/{id}', 		'definiciones\IngresosRequerimientoController@buscar');
				Route::get('datatable', 		'definiciones\IngresosRequerimientoController@datatable');	
				Route::delete('eliminar/{id}', 	'definiciones\IngresosRequerimientoController@eliminar');

			});
			Route::group(['prefix' => 'rubros'], function() {
				Route::get('/', 				'definiciones\RubrosController@index');
				Route::post('guardar',			'definiciones\RubrosController@guardar');
				Route::put('guardar/{id}',		'definiciones\RubrosController@guardar');
				Route::get('buscar/{id}', 		'definiciones\RubrosController@buscar');
				Route::get('datatable', 		'definiciones\RubrosController@datatable');	
				Route::delete('eliminar/{id}', 	'definiciones\RubrosController@eliminar');

			});
			Route::group(['prefix' => 'redesalimentacion'], function() {
				Route::get('/', 				'definiciones\RedesAlimentacionController@index');
				Route::post('guardar',			'definiciones\RedesAlimentacionController@guardar');
				Route::put('guardar/{id}',		'definiciones\RedesAlimentacionController@guardar');
				Route::get('buscar/{id}', 		'definiciones\RedesAlimentacionController@buscar');
				Route::get('datatable', 		'definiciones\RedesAlimentacionController@datatable');	
				Route::delete('eliminar/{id}', 	'definiciones\RedesAlimentacionController@eliminar');

			});
		});

		Route::group(['prefix' => 'carga'], function(){ 
			Route::get('/', 				'InventarioController@index');
			Route::post('guardar',			'InventarioController@guardar');
			Route::get('fecha', 			'InventarioController@fecha');
			Route::post('archivo/', 		'InventarioController@archivo');
		});

		Route::group(['prefix' => 'graficas/inventario'], function(){ 
			Route::get('/', 					'GraficasController@index');
			Route::get('/graficames',  		'GraficasController@graficames');
			Route::get('/grafica', 				'GraficasController@grafica');
			Route::get('/graficarubros', 		'GraficasController@graficarubros');
			//Route::post('imprimir',			'ReporteController@imprimir');
		});	

		Route::group(['prefix' => 'comportamiento'], function(){ 
			Route::get('/', 					'ComportamientoController@index');
			Route::get('/graficaAnos',  		'ComportamientoController@graficaAnos');
			Route::get('/graficaRango', 		'ComportamientoController@graficaRango');
			//Route::post('imprimir',			'ReporteController@imprimir');
		});	
	});


	Route::group(['prefix' => 'categorias'], function() {
		Route::get('/', 				'CategoriasController@index');
		Route::get('nuevo', 			'CategoriasController@nuevo');
		Route::get('cambiar/{id}', 		'CategoriasController@cambiar');
		
		Route::get('buscar/{id}', 		'CategoriasController@buscar');

		Route::post('guardar',			'CategoriasController@guardar');
		Route::put('guardar/{id}', 		'CategoriasController@guardar');

		Route::delete('eliminar/{id}', 	'CategoriasController@eliminar');
		Route::post('restaurar/{id}', 	'CategoriasController@restaurar');
		Route::delete('destruir/{id}', 	'CategoriasController@destruir');

		Route::get('datatable', 		'CategoriasController@datatable');
	});

    //{{route}}
});