<?php

$menu['ingresos'] = [
	[
		'nombre' 	=> 'Ingresos',
		'direccion' => '#Ingresos',
		'icono' 	=> 'fa fa-bullseye',
		'menu' 		=> [
			[
				'nombre' 	=> 'Definiciones',
				'direccion' => '#Definicionesingresos',
				'icono' 	=> 'fa fa-list',
				'menu' 		=>[

					[
						'nombre' 	=> 'Rubros',
						'direccion' => 'ingresos/definiciones/rubros',
						'icono' 	=> 'fa fa-cutlery'
					],	

					[
						'nombre' 	=> 'Categoria',
						'direccion' => 'ingresos/definiciones/categoria',
						'icono' 	=> 'fa fa-list'
					],
					[
						'nombre' 	=> 'Rubors Requerimientos',
						'direccion' => 'ingresos/definiciones/Rubors_Requerimientos',
						'icono' 	=> 'fa fa-asterisk'
					],[ 
						'nombre' 	=> 'Redes de Alimentacion',
						'direccion' => 'ingresos/definiciones/redesalimentacion',
						'icono' 	=> 'fa fa-asterisk'
					]
				]			
			],
			[
				'nombre' 	=> 'Carga',
				'direccion' => 'ingresos/carga',
				'icono' 	=> 'fa fa-list-alt',
			],
			[
				'nombre' 	=> 'Graficas',
				'direccion' => 'ingresos/graficas/inventario',
				'icono' 	=> 'fa fa-bar-chart',
			],
			[
				'nombre' 	=> 'Comportamiento',
				'direccion' => 'ingresos/comportamiento',
				'icono' 	=> 'fa fa-line-chart',
			]
		]
	]
];