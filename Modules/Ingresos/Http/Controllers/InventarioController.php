<?php namespace Modules\Ingresos\Http\Controllers;

use Modules\Ingresos\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;
use Carbon\Carbon;
use Excel; 
use Modules\Inventario\Model\definiciones\Empresas;
use Modules\Ingresos\Model\definiciones\rubros;
use Modules\Ingresos\Model\inventario;
use Modules\Ingresos\Model\definiciones\categorias;
use Modules\Ingresos\Model\definiciones\RedesAlimentacion as redes;

use Modules\Ingresos\Http\Requests\inventarioRequest;

class InventarioController extends Controller {
	protected $titulo = 'Inventario';
	
	public $librerias = [
		'jquery-ui',
		'jquery-ui-timepicker',
		'ladda',
		'touchspin'
	];

	public $js=[
		'inventario',
	];
	public $css=[
		'inventario',
	];

	public $meses = [		
		1 =>'Enero',
		'Febrero', 
		'Marzo',
		'Abril',
		'Mayo',				 
		'Junio',
		'Julio',
		'Agosto',
		'Septiembre',
		'Octubre',
		'Noviembre',
		'Diciembre',
	];

	public function index()
	{
		$empresas   = Empresas::all();
		$rubros     = rubros::orderBy('nombre', 'ASC')->get();


		return $this->view('ingresos::inventario', [
		   'empresas' =>   $empresas,
		   'rubros'  =>   $rubros,
		]);
	}
	public  function categoria(Request $request){

		$rubros = rubros::where('categoria_id', '=',$request->id)->orderBy('nombre', 'ASC')->get();

		return $rubros;
	}

	public function guardar(inventarioRequest $request){
		DB::beginTransaction(); //Start transaction!

		try{
			$mes = $request->mes;
			$inventario = $request->input("inv");

			foreach ($inventario as $id_empresa => $inv) {
				foreach ($inv as $id_rubro => $cantidad) {
					if(!$cantidad == 0 ){
						$cantidad = floatval(str_replace(',', '.', $cantidad));
					}					

					$inven = inventario::firstOrCreate([
						'mes' => $mes,
						'mes_str' => strtolower($this->meses[$mes]),
						'empresa_id' => $id_empresa,
						'ingresorubros_id' => $id_rubro,
						'ano' => date('Y')
					]);

					$inven->cantidad = floatval($cantidad);
					$inven->save();
				}
			}
		}catch(\Exception $e){
			DB::rollback();
			return $e->errorInfo[2];	
		}

		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}
 
	public static function fecha(Request $request)
	{
		$fecha = $request->fecha; 
		

		$query = DB::table('ingresos_inventario')
			->select('ingresos_inventario.empresa_id', 'ingresos_inventario.ingresorubros_id', 'ingresos_inventario.cantidad')
			->whereNull('ingresos_inventario.deleted_at')
			->where('ingresos_inventario.mes', '=', $fecha)
			->where('ingresos_inventario.ano', '=', date('Y'));
	
		$inventario = $query->get();

		$cantidades	= [];

		if(!empty($inventario)){
			$empresas 	= Empresas::all();
			$rubros     = rubros::all();
		
			foreach ($inventario as $inv) {
				$cantidades[$inv->empresa_id][$inv->ingresorubros_id] = $inv->cantidad;
			}
		}

		return $cantidades;
	}

	public function archivo(Request $request){
		$mimes = [
			'text/csv',
			'application/vnd.ms-excel',
			'application/vnd.oasis.opendocument.spreadsheet',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
		];

		$ruta = public_path('archivos/');
		$archivo = $request->file('subir');

		$mime = $archivo->getClientMimeType();

		if (!in_array($mime, $mimes)) {
			return "Error de Validación, la extension el archivo cargado debe ser: xls,xlsx,csv";
		}
		
		do {
			$nombre_archivo = str_random(20) . '.' . $archivo->getClientOriginalExtension();
		} while (is_file($ruta . $nombre_archivo));
			
		$archivo->move($ruta, $nombre_archivo);
		chmod($ruta . $nombre_archivo, 0777);

		$excel = Excel::selectSheetsByIndex(0)->load($ruta . $nombre_archivo)->get();

		$empresas = [];
		foreach (Empresas::all() as $empresa) {
			$empresa->nombre = str_slug($empresa->nombre);

			$empresas[$empresa->nombre] = $empresa->id;
		}

		$rubros = [];
		foreach (rubros::all() as $rubro) {
			$rubro->nombre = str_slug($rubro->nombre);

			$rubros[$rubro->nombre] = $rubro->id;
		}
			
		$cantidades	= [];
	 
		try {
			foreach ($excel as $key => $value){
				$value->rubros = str_slug($value->rubros);
				$idRubro = $rubros[$value->rubros];

				$idEmpresa = 0;

				foreach ($empresas as $empresa => $idEmpresa) {
					$str = str_replace('-', '_', str_slug($empresa));
					
					$valor = $value->$str;
					if(is_float($valor)) {
						$valor = round($valor, '2');
					}

					if (is_null($value->$str)){
						$valor = 0;
					}

					$cantidades[$idEmpresa][$idRubro] = $valor;
				}
			}
		} catch (Exception $e) { }

		return ['s' => 's', 'msj' => 'Carga Completa', 'cantidades' => $cantidades ];
	}

	public function red() {
		return redes::pluck('nombre', 'id');
	}
}