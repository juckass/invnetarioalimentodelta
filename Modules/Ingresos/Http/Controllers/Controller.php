<?php namespace Modules\Ingresos\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

abstract class Controller extends BaseController {
	protected $titulo = 'Backend';

	public $app = 'alimentos';

	protected $patch_js = [
		'public/js',
		'public/plugins',
		'Modules/Ingresos/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'Modules/Ingresos/Assets/css',
	];
}