<?php

namespace Modules\Ingresos\Http\Controllers\definiciones;

//Controlador Padre
use Modules\Ingresos\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Ingresos\Http\Requests\IngresosRequerimientoRequest;

//Modelos
use Modules\Ingresos\Model\definiciones\IngresosRequerimiento;

class IngresosRequerimientoController extends Controller
{
    protected $titulo = 'Ingresos Requerimiento';

    public $js = [
        'IngresosRequerimiento'
    ];
    
    public $css = [
        'IngresosRequerimiento'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('ingresos::IngresosRequerimiento', [
            'IngresosRequerimiento' => new IngresosRequerimiento()
        ]);
    }

    public function nuevo()
    {
        $IngresosRequerimiento = new IngresosRequerimiento();
        return $this->view('ingresos::IngresosRequerimiento', [
            'layouts' => 'admin::layouts.popup',
            'IngresosRequerimiento' => $IngresosRequerimiento
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $IngresosRequerimiento = IngresosRequerimiento::find($id);
        return $this->view('ingresos::IngresosRequerimiento', [
            'layouts' => 'admin::layouts.popup',
            'IngresosRequerimiento' => $IngresosRequerimiento
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $IngresosRequerimiento = IngresosRequerimiento::withTrashed()->find($id);
        } else {
            $IngresosRequerimiento = IngresosRequerimiento::find($id);
        }

        if ($IngresosRequerimiento) {
            return array_merge($IngresosRequerimiento->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(IngresosRequerimientoRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $IngresosRequerimiento = $id == 0 ? new IngresosRequerimiento() : IngresosRequerimiento::find($id);

            $IngresosRequerimiento->fill($request->all());
            $IngresosRequerimiento->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $IngresosRequerimiento->id,
            'texto' => $IngresosRequerimiento->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            IngresosRequerimiento::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            IngresosRequerimiento::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            IngresosRequerimiento::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = IngresosRequerimiento::select([
            'ingresos_requerimiento.id', 
            'municipio.nombre as municipio', 
            'ingresos_rubros.nombre as rubros', 
            'ingresos_requerimiento.cantidad', 
            'ingresos_requerimiento.deleted_at'
            
        ])->join('municipio', 'municipio.id','=','ingresos_requerimiento.municipio_id')
          ->join('ingresos_rubros', 'ingresos_rubros.id','=','ingresos_requerimiento.rubro_id')
          ->where('municipio.estados_id',9);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}