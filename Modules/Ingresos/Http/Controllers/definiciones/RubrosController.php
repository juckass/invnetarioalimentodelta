<?php

namespace Modules\Ingresos\Http\Controllers\definiciones;

//Controlador Padre
use Modules\Ingresos\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Ingresos\Http\Requests\IngresosRubrosRequest;

//Modelos
use Modules\Ingresos\Model\definiciones\Rubros as IngresosRubros;

class RubrosController extends Controller
{
    protected $titulo = 'Ingresos Rubros';

    public $js = [
        'IngresosRubros'
    ];
    
    public $css = [
        'IngresosRubros'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('ingresos::Rubros', [
            'Rubros' => new IngresosRubros()
        ]);
    }

    public function nuevo()
    {
        $IngresosRubros = new IngresosRubros();
        return $this->view('ingresos::Rubros', [
            'layouts' => 'admin::layouts.popup',
            'Rubros' => $IngresosRubros
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $IngresosRubros = IngresosRubros::find($id);
        return $this->view('ingresos::Rubros', [
            'layouts' => 'admin::layouts.popup',
            'Rubros' => $IngresosRubros
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $IngresosRubros = IngresosRubros::withTrashed()->find($id);
        } else {
            $IngresosRubros = IngresosRubros::find($id);
        }

        if ($IngresosRubros) {
            return array_merge($IngresosRubros->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(IngresosRubrosRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $IngresosRubros = $id == 0 ? new IngresosRubros() : IngresosRubros::find($id);

            $IngresosRubros->fill($request->all());
            $IngresosRubros->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $IngresosRubros->id,
            'texto' => $IngresosRubros->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            IngresosRubros::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            IngresosRubros::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            IngresosRubros::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = IngresosRubros::select([
            'id', 'nombre', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}