<?php

namespace Modules\Ingresos\Http\Controllers\definiciones;

//Controlador Padre
use Modules\Ingresos\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Ingresos\Http\Requests\RedesAlimentacionRequest;

//Modelos
use Modules\Ingresos\Model\definiciones\RedesAlimentacion;

class RedesAlimentacionController extends Controller
{
    protected $titulo = 'Redes Alimentacion';

    public $js = [
        'RedesAlimentacion'
    ];
    
    public $css = [
        'RedesAlimentacion'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('ingresos::RedesAlimentacion', [
            'RedesAlimentacion' => new RedesAlimentacion()
        ]);
    }

    public function nuevo()
    {
        $RedesAlimentacion = new RedesAlimentacion();
        return $this->view('ingresos::RedesAlimentacion', [
            'layouts' => 'admin::layouts.popup',
            'RedesAlimentacion' => $RedesAlimentacion
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $RedesAlimentacion = RedesAlimentacion::find($id);
        return $this->view('ingresos::RedesAlimentacion', [
            'layouts' => 'admin::layouts.popup',
            'RedesAlimentacion' => $RedesAlimentacion
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $RedesAlimentacion = RedesAlimentacion::withTrashed()->find($id);
        } else {
            $RedesAlimentacion = RedesAlimentacion::find($id);
        }

        if ($RedesAlimentacion) {
            return array_merge($RedesAlimentacion->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(RedesAlimentacionRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $RedesAlimentacion = $id == 0 ? new RedesAlimentacion() : RedesAlimentacion::find($id);

            $RedesAlimentacion->fill($request->all());
            $RedesAlimentacion->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $RedesAlimentacion->id,
            'texto' => $RedesAlimentacion->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            RedesAlimentacion::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            RedesAlimentacion::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            RedesAlimentacion::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = RedesAlimentacion::select([
            'id', 'nombre', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}