<?php namespace Modules\Ingresos\Http\Controllers;

use Modules\Ingresos\Http\Controllers\Controller;
use Illuminate\Http\Request;
 
use DB;
use Carbon\Carbon;

use Modules\Inventario\Model\definiciones\categorias;
use Modules\Inventario\Model\definiciones\Empresas;
use Modules\Ingresos\Model\definiciones\rubros;
use Modules\Ingresos\Model\inventario;
use Modules\Ingresos\Model\definiciones\categorias as cate;
use Modules\Ingresos\Model\definiciones\RedesAlimentacion as redes;

class GraficasController extends Controller {
	protected $titulo = 'Graficas';
	
	public $librerias = [
		'jquery-ui',
		'jquery-ui-timepicker',
		'highcharts',
		'highcharts-drilldown',
		'highcharts-3d',
		'icheck',
		'bootstrap-switch'
	];

	public $js = [
		'graficas',
	];

	public $meses = [		
		1 => 'Enero',
		'Febrero', 
		'Marzo',
		'Abril',
		'Mayo',				 
		'Junio',
		'Julio',
		'Agosto',
		'Septiembre',
		'Octubre',
		'Noviembre',
		'Diciembre',
	];

	public function index()
	{
		$ano = date('Y');
		$query = DB::table('ingresos_inventario');
		$num = $query->count();
		if ($num > 0){
			$num = $query->where('ano', $ano)->max('mes');
		}

		return $this->view('ingresos::graficas',[
			'mes' => $num,
			'ano' => $ano
		]);
	}

	public function ultimomes()
	{
		return ['mes' => $num];
	}

	public function grafica(Request $request){
		$rubro 	=	$request->rubro;
		$tipo	=	$request->tipo;
		$red	=	$request->red;
		$ano    =   $request->ano;

		/* Requerimientos por rubro y por municipio */

		$requerimiento = DB::table('ingresos_requerimiento')
			->whereNull('deleted_at')
			->groupBy('rubro_id')
			->pluck(DB::RAW('SUM(cantidad) as cantidad'), 'rubro_id');
		
		$rubrosRequeridos = [];
		$totalRequerimientos = 0;
		$totalPorcentaje = 0;


		$query = DB::table('ingresos_inventario')
			->select('ingresos_inventario.mes', 'ingresos_inventario.cantidad', 'ingresos_inventario.ingresorubros_id as rubro_id', 'empresas.redes_id')
			->leftJoin('empresas', 'empresas.id', '=', 'ingresos_inventario.empresa_id')
			->leftJoin('ingresos_rubros', 'ingresos_rubros.id', '=', 'ingresos_inventario.ingresorubros_id')
			->where('ingresos_inventario.ano', $ano);

		if($rubro == 'priorizado'){
			$query->where('ingresos_rubros.estatus', 1);
		}

		if($tipo !== 'todos'){
			$query->where('ingresos_rubros.categoria_id', $tipo);
		}

		/*if($red !== 'todos'){
			$query->where('empresas.redes_id', $red);
		}*/

		$query->whereNull('ingresos_inventario.deleted_at')
			  ->whereNull('ingresos_rubros.deleted_at')
			  ->whereNull('empresas.deleted_at');

		$series = [[
			'name' => 'Alimentos',
			'colorByPoint' => true,
			'data' => []
		]];
		
		foreach ($this->meses as $mes_id => $mes) {
			$series[0]['data'][$mes_id - 1] = [
				'name' => substr($mes, 0, 3),
				'drilldown' => str_slug($mes, '_'),
				'y' => 0,
				'p' => 0,
				'rubros' => [],
				'demanda' => 0
			];
		}
		
		foreach ($query->get() as $inventario) {
			$cantidad = floatval(str_replace(',', '.', $inventario->cantidad));
						
			if ($red === 'todos' || $inventario->redes_id == $red){
				$series[0]['data'][$inventario->mes - 1]['y'] += $cantidad;
			}

			if ($cantidad > 0){
				$series[0]['data'][$inventario->mes - 1]['rubros'][] = $inventario->rubro_id;
				$series[0]['data'][$inventario->mes - 1]['demanda'] += $requerimiento[$inventario->rubro_id];
			}
		}

		
		foreach ($series[0]['data'] as $mes => $value) {
			$series[0]['data'][$mes]['rubros'] = array_unique($series[0]['data'][$mes]['rubros']);

			$totalRequerimientos = 0;

			foreach($series[0]['data'][$mes]['rubros'] as $rubros){
				$totalRequerimientos += floatval($requerimiento[$rubros]);
			}

			$series[0]['data'][$mes]['demanda'] = $totalRequerimientos;

			if ($totalRequerimientos > 0){
				$series[0]['data'][$mes]['p'] = $series[0]['data'][$mes]['y'] * 100 / $totalRequerimientos;
			}
		}

		/* Datos de la grafica */

		$query = DB::table('ingresos_inventario')
			->select('redes_alimentacion.nombre as empresa', DB::RAW('SUM(ingresos_inventario.cantidad) as cantidad'))
			->leftJoin('empresas', 'empresas.id', '=', 'ingresos_inventario.empresa_id')
			->leftJoin('redes_alimentacion', 'empresas.redes_id', '=', 'redes_alimentacion.id')
			->leftJoin('ingresos_rubros', 'ingresos_rubros.id', '=', 'ingresos_inventario.ingresorubros_id')
			->where('ingresos_inventario.ano', $ano);

		if($rubro == 'priorizado'){
			$query->where('ingresos_rubros.estatus', 1);
		}

		if($red !== 'todos'){
			$query->where('empresas.redes_id', $red);
		}

		if($tipo !== 'todos'){
			$query->where('ingresos_rubros.categoria_id', $tipo);
		}
			
		$query->whereNull('ingresos_inventario.deleted_at')
			->whereNull('ingresos_rubros.deleted_at')
			->whereNull('empresas.deleted_at')
			->groupBy('empresas.redes_id', 'redes_alimentacion.nombre');

		$drilldown = [
			'series' => []
		];

		foreach ($this->meses as $mes_id => $mes) {
			$data = [];
			$_query = clone $query;
			$_query->where('ingresos_inventario.mes', $mes_id);
			
			foreach ($_query->get() as $inventario) {
				$cantidad = floatval(str_replace(',', '.', $inventario->cantidad));

				if ($cantidad == 0){
					continue;
				}

				$p = 0;
				
				if ($series[0]['data'][$mes_id - 1]['demanda'] > 0){
					$p = $cantidad * 100 / $series[0]['data'][$mes_id - 1]['demanda'];
				}

				$data[] = [
					'name' => $inventario->empresa, 
					'y' => $cantidad,
					'p' => $p
				];
			}

			$drilldown['series'][] = [
				'name' => $mes,
				'id' => str_slug($mes, '_'),
				'data' => $data
			];
		}

		return [
			'series' => $series,
			'drilldown' => $drilldown
		];
	}

	public function graficames(Request $request)
	{
		$fecha  =   $request->mes;
		$rubro  =	$request->rubro_select;
		$tipo	=	$request->op;
		$red	=   $request->red;
		$ano	=   $request->ano;


		$query = DB::table('ingresos_inventario')
			->select('empresas.nombre as name', DB::RAW('SUM(ingresos_inventario.cantidad) as y'))
			->leftJoin('empresas', 'empresas.id', '=', 'ingresos_inventario.empresa_id')
			->leftJoin('ingresos_rubros', 'ingresos_rubros.id', '=', 'ingresos_inventario.ingresorubros_id')
			->where('ingresos_inventario.ano', $ano);

		if($rubro == 'priorizado'){
			$query->where('ingresos_rubros.estatus','=',1);
		}

		if($red!=='todos'){
			$query->where('empresas.redes_id','=',$red);
		}

		if($tipo!=='todos'){
			$query->where('ingresos_rubros.categoria_id','=',$tipo);
		}

		$query->whereNull('ingresos_inventario.deleted_at')
			->whereNull('ingresos_rubros.deleted_at')
			->whereNull('empresas.deleted_at')
			->where('ingresos_inventario.mes', '=', $fecha)
			->groupBy('ingresos_inventario.empresa_id', 'empresas.nombre');

		if($request->has('rubro')){
			$query->whereIn('ingresorubros_id', $request->rubro);
		}

		$series = [[
			'name' => 'Alimentos',
			'colorByPoint' => true,
			'data' => [[
				'name' => 'Total del<br>Estado',
				'drilldown' => str_slug('Total del Estado', '_'),
				'y' => 0
			]]
		]];
		
		$total = 0;

		foreach ($query->get() as $inventario) {
			$cantidad = floatval(str_replace(',', '.', $inventario->y));
			
			$nombre = $inventario->name;

			$nombre = str_replace(" ", "<br>", $nombre);

			$series[0]['data'][] = [
				'name' => $nombre,
				'drilldown' => str_slug($inventario->name, '_'),
				'y' => $cantidad
			];

			$total += $cantidad;
		}

		$series[0]['data'][0]['y'] = $total;

		/* Requerimientos por rubro y por municipio */

		$requerimiento = DB::table('ingresos_requerimiento')
			->whereNull('deleted_at')
			->groupBy('rubro_id')
			->pluck(DB::RAW('SUM(cantidad) as cantidad'), 'rubro_id');
		
		$rubrosRequeridos = [];
		$totalRequerimientos = 0;
		$totalPorcentaje = 0;
		
		/* Datos del Drilldown */

		$query = DB::table('ingresos_inventario')
			->leftJoin('ingresos_rubros', 'ingresos_rubros.id', '=', 'ingresos_inventario.ingresorubros_id')
			->leftJoin('empresas', 'empresas.id', '=', 'ingresos_inventario.empresa_id')
			->where('ingresos_inventario.ano', $ano);

		
		if($rubro == 'priorizado'){
			$query->where('ingresos_rubros.estatus','=',1);
		}

		if($red!=='todos'){
			$query->where('empresas.redes_id','=',$red);
		}

		if($tipo!=='todos'){
			$query->where('ingresos_rubros.categoria_id','=',$tipo);
		}


		$query ->whereNull('ingresos_inventario.deleted_at')
			->whereNull('ingresos_rubros.deleted_at')
			->whereNull('empresas.deleted_at')
			->where('ingresos_inventario.mes', '=', $fecha);
			
		if($request->has('rubro')){
			$query->whereIn('ingresorubros_id', $request->rubro);
		}


		$drilldown = [
			'series' => [],
		];

		$data = [];

		$_query = clone $query;
		$_query
			->select('ingresos_rubros.nombre as rubro', DB::RAW('SUM(ingresos_inventario.cantidad) as cantidad'))
			->groupBy('ingresos_rubros.id', 'ingresos_rubros.nombre');
		
		foreach ($_query->get() as $inventario) {
			$cantidad = floatval(str_replace(',', '.', $inventario->cantidad));

			if ($cantidad == 0){
				continue;
			}

			$data[] = [$inventario->rubro, $cantidad];
		}

		$drilldown['series'][] = [
			'name' => 'Total del <br>Estado',
			'id' => str_slug('Total del Estado', '_'),
			'data' => $data
		];

		$query->select('ingresos_rubros.id', 'ingresos_rubros.nombre as rubro', 'ingresos_inventario.cantidad');

		foreach (Empresas::all() as $empresa) {
			$data = [];
			$_query = clone $query;
			$_query->where('empresas.id', $empresa->id);

			foreach ($_query->get() as $inventario) {
				$cantidad = floatval(str_replace(',', '.', $inventario->cantidad));

				if ($cantidad == 0){
					continue;
				}

				$rubrosRequeridos[] = $inventario->id;
				
				$data[] = [$inventario->rubro, $cantidad];
			}

			$drilldown['series'][] = [
				'name' => $empresa->nombre,
				'id' => str_slug($empresa->nombre, '_'),
				'data' => $data
			];
		}

		$rubrosRequeridos = array_unique($rubrosRequeridos);

		foreach ($rubrosRequeridos as $id) {
			$totalRequerimientos += floatval($requerimiento[$id]);
		}
		
		if ($totalRequerimientos != 0){
			$totalPorcentaje = $total * 100 / $totalRequerimientos;
		}

		return [
			'total' => $total,
			'totalPorcentaje' => number_format($totalPorcentaje, 2, ',', '.') . ' %',
			'totalRequerido' => $totalRequerimientos,
			'mes' => isset($this->meses[$fecha]) ? $this->meses[$fecha] : 1,

			'series' => $series,
			'drilldown' => $drilldown
		];
	}
	public function graficarubros(Request $request)
	{ 	 
	
		$rubro  =	$request->rubro;
		$tipo	=	$request->tipo;
		$red	=   $request->red;
		$ano    =   $request->ano;

		$query = DB::table('ingresos_inventario')
	  		->select(
	  			'ingresos_rubros.id',
	  			'ingresos_rubros.nombre',
	  			DB::RAW('SUM(ingresos_inventario.cantidad) as ingreso')
	  		)
			->leftJoin('ingresos_rubros', 'ingresos_rubros.id', '=', 'ingresos_inventario.ingresorubros_id')
			->leftJoin('empresas', 'empresas.id', '=', 'ingresos_inventario.empresa_id')
			->groupBy('ingresos_rubros.id', 'ingresos_rubros.nombre')
			->where('ingresos_inventario.ano', $ano)
			->whereNull('ingresos_inventario.deleted_at')
			->whereNull('empresas.deleted_at')
			->whereNull('ingresos_rubros.deleted_at');
			
		if($rubro == 'priorizado'){
			$query->where('ingresos_rubros.estatus','=',1);
		}

		if($tipo!=='todos'){
			$query->where('ingresos_rubros.categoria_id','=',$tipo);
		}

		if($red!=='todos'){
			$query->where('empresas.redes_id','=',$red);
		}

		$query->where('ingresos_inventario.mes', '=', $request->mes)
			->orderby('nombre', 'asc');	

		$coleccion = collect([]);
		
		$rubros    = [];
		$ingresos  = [];
		$requerido = [];

		$requerimiento = DB::table('ingresos_requerimiento')
			->whereNull('deleted_at')
			->groupBy('rubro_id')
			->pluck(DB::RAW('SUM(cantidad) as cantidad'), 'rubro_id');

		foreach ($query->get() as  $value) {
			$cantidad = floatval(str_replace(',', '.', $value->ingreso));
			$reque = floatval(str_replace(',', '.', $requerimiento[$value->id]));

			if ($reque == 0){
				continue;
			}
			
			$arr = [
				'cantidad' => 100,
				'rubros'   => $value->nombre,
				'ingresos' => [
					'y' => round($value->ingreso * 100 / $reque, 2),
					'r' => round($reque, 2),
					'i' => round($value->ingreso, 2)
				]
			];
		
			$coleccion->push($arr);
		}

		return [
			'rubros'   =>  $coleccion->sortBy('ingresos')->pluck('rubros'),
			'ingresos' =>  $coleccion->sortBy('ingresos')->pluck('ingresos'),
			'requerido'=>  $coleccion->sortBy('ingresos')->pluck('cantidad') 
		];
	}

	public function rubros() {
		return rubros::pluck('nombre', 'id');
	}
	public function alimentos() {
		return cate::pluck('nombre', 'id');
	}
	public function red() {
		return redes::pluck('nombre', 'id');
	}

	public function ano(){

		return  DB::table('ingresos_inventario')
		->select('ano')
		->whereNull('ingresos_inventario.deleted_at')
		->distinct()->pluck('ano', 'ano');

	}
}