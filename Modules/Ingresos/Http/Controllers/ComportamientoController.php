<?php namespace Modules\Ingresos\Http\Controllers;

use Modules\Ingresos\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;
use Carbon\Carbon;

use Modules\Inventario\Model\definiciones\Empresas;
use Modules\Ingresos\Model\definiciones\rubros;
use Modules\Ingresos\Model\inventario;
use Modules\Ingresos\Model\definiciones\categorias as cate;
use Modules\Ingresos\Model\definiciones\RedesAlimentacion as redes;

class ComportamientoController extends Controller {
	protected $titulo = 'Comportamiento';
	
	public $librerias = [
		'jquery-ui',
		'jquery-ui-timepicker',
		'highcharts',
		'highcharts-drilldown',
		'icheck',
		'bootstrap-switch'
	];

	public $js = ['comportamiento.js'];
	public $css = ['comportamiento.css'];

	public $meses = [
		'Enero',
		'Febrero',
		'Marzo',
		'Abril',
		'Mayo',
		'Junio',
		'Julio',
		'Agosto',
		'Septiembre',
		'Octubre',
		'Noviembre',
		'Diciembre'
	];

	public function index()
	{
		$sql = DB::table('ingresos_inventario')
			->select('ano')
			->groupBy('ano')
			->orderBy('ano', 'desc')
			->get();

		$anos = [];

		foreach ($sql as $value) {
			$anos[$value->ano] = $value->ano;
		}

		return $this->view('ingresos::comportamiento', [
			//'meses' => $this->meses,
			'anos' => $anos
		]);
	}

	public function graficaAnos(Request $request)
	{
		$rubro 	=	$request->tipo_rubro;
		$tipo	=	$request->op;
		$red	=	$request->red;
		//$ano	=	$request->ano;

 		$query = DB::table('ingresos_inventario')
			->select(DB::RAW('SUM(ingresos_inventario.cantidad) as cantidad'), 'ingresos_inventario.mes', 'ingresos_inventario.ano', 'ingresos_inventario.empresa_id')
			->leftJoin('empresas', 'empresas.id', '=', 'ingresos_inventario.empresa_id')
			->leftJoin('ingresos_rubros', 'ingresos_rubros.id', '=', 'ingresos_inventario.ingresorubros_id')
			//->where('ano', $ano);
			->whereNull('ingresos_inventario.deleted_at')
			->whereNull('empresas.deleted_at')
			->whereNull('ingresos_rubros.deleted_at')

			->orderBy('ingresos_inventario.ano')
			->orderBy('ingresos_inventario.mes')
			->orderBy('ingresos_inventario.empresa_id')
			->groupBy('ingresos_inventario.mes', 'ingresos_inventario.ano', 'ingresos_inventario.empresa_id');

		if($rubro == 'priorizado'){
			$query->where('ingresos_rubros.estatus', 1);
		}

		if($tipo !== 'todos'){
			$query->where('ingresos_rubros.categoria_id', $tipo);
		}

		if($red !== 'todos'){
			$query->where('empresas.redes_id', $red);
		}
		

		if($request->has('rubro')){
			$query->whereIn('ingresorubros_id', $request->rubro);
		}

		$series = [[
			'name' => 'Total Bolívar',
			'data' => []
		]];

		/* Total del estado */
		//$empresas = Empresas::where('redes_id', 1)->get();
		$categorias = [];
		$empresas = Empresas::all()->sortBy('id')->pluck('nombre', 'id');
		
		$datas = $query->get();
		foreach ($datas as $data) {
			$fecha = $this->meses[$data->mes - 1] . ' ' . $data->ano;
			$categorias[$fecha] = $fecha;

			if (!isset($series[$data->empresa_id])) {
				$series[$data->empresa_id]['name'] = $empresas[$data->empresa_id];
				$series[$data->empresa_id]['data'] = [];
			}
		}

		$categorias = array_values($categorias);
		foreach ($series as $empresa_id => $serie) {
			foreach ($categorias as $categoria) {
				$series[$empresa_id]['data'][$categoria] = 0;
			}
		}

		foreach ($datas as $data) {
			$fecha = $this->meses[$data->mes - 1] . ' ' . $data->ano;
			$series[$data->empresa_id]['data'][$fecha] = floatval($data->cantidad);
			$series[0]['data'][$fecha] += floatval($data->cantidad);
		}

		$series = array_values($series);
		foreach ($series as $key => $value) {
			$series[$key]['data'] = array_values($series[$key]['data']);
		}
		
		return [
			'categorias' => $categorias,
			'series' => $series
		];
	}

	public static function graficaRango(Request $request)
	{
		$fechadesde = Carbon::createFromFormat('d/m/Y', $request->fechadesde)->format("Y-m-d");
		$fechahasta = Carbon::createFromFormat('d/m/Y', $request->fechahasta)->format("Y-m-d");

		$query = DB::table('inventario')
			->select('inventario.fecha')
			->whereNull('inventario.deleted_at')
			->whereBetween('inventario.fecha', [$fechadesde, $fechahasta])
			->groupBy('inventario.fecha');

		$categorias = [];
		foreach ($query->get() as $inventario) {
			$categorias[] = Carbon::parse($inventario->fecha)->format('d/m/Y');
		}
 
		$query = DB::table('inventario')
			->select(DB::RAW('SUM(inventario.cantidad) as cantidad'), 'inventario.fecha')
			->leftJoin('empresas', 'empresas.id', '=', 'inventario.empresa_id')
			->whereNull('inventario.deleted_at')
			->whereNull('empresas.deleted_at')
			->whereBetween('inventario.fecha', [$fechadesde, $fechahasta])
			->where('empresas.redes_id', 1)
			->orderBy('inventario.fecha')
			->groupBy('inventario.fecha');

		if($request->has('rubro')){
			$query->whereIn('rubros_id', $request->rubro);
		}

		$series = [[
			'name' => 'Total Bolívar',
			'data' => []
		]];

		/* Total del estado */
		$_query = clone $query;

		foreach ($_query->get() as $inventario) {
			$cantidad = floatval(str_replace(',', '.', $inventario->cantidad));

			$series[0]['data'][] = $cantidad;
		}


		/* Resumen por Empresa */


		$query->groupBy('inventario.empresa_id');
		$empresas = Empresas::all();
		//$empresas = Empresas::where('redes_id', 1)->get();
		foreach ($empresas as $empresa) {
			$serie = [
				'name' => $empresa->nombre,
				'data' => []
			];

			$_query = clone $query;
			$_query->where('empresas.id', $empresa->id);

			foreach ($_query->get() as $inventario) {
				$cantidad = floatval(str_replace(',', '.', $inventario->cantidad));

				$serie['data'][] = $cantidad;
			}

			$series[] = $serie;
		}
		
		return [
			'categorias' => $categorias,
			'series' => $series
		];
	}
	
	public function rubros() {
		return rubros::pluck('nombre', 'id');
	}
	public function alimentos() {
		return cate::pluck('nombre', 'id');
	}
	public function red() {
		return redes::pluck('nombre', 'id');
	}
}