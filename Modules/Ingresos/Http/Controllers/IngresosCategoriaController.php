<?php

namespace Modules\Ingresos\Http\Controllers;

//Controlador Padre
use Modules\Ingresos\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Ingresos\Http\Requests\IngresosCategoriaRequest;

//Modelos
use Modules\Ingresos\Model\Categorias as IngresosCategoria;

class IngresosCategoriaController extends Controller
{
    protected $titulo = 'Ingresos Categoria';

    public $js = [
        'definiciones/categorias'
    ];
    
    public $css = [
        'Categorias'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('ingresos::categorias', [
            'Categoria' => new IngresosCategoria()
        ]);
    }

    public function nuevo()
    {
        $IngresosCategoria = new IngresosCategoria();
        return $this->view('ingresos::categorias', [
            'layouts' => 'admin::layouts.popup',
            'IngresosCategoria' => $IngresosCategoria
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $IngresosCategoria = IngresosCategoria::find($id);
        return $this->view('ingresos::categorias', [
            'layouts' => 'admin::layouts.popup',
            'IngresosCategoria' => $IngresosCategoria
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $IngresosCategoria = IngresosCategoria::withTrashed()->find($id);
        } else {
            $IngresosCategoria = IngresosCategoria::find($id);
        }

        if ($IngresosCategoria) {
            return array_merge($IngresosCategoria->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(IngresosCategoriaRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $IngresosCategoria = $id == 0 ? new IngresosCategoria() : IngresosCategoria::find($id);

            $IngresosCategoria->fill($request->all());
            $IngresosCategoria->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $IngresosCategoria->id,
            'texto' => $IngresosCategoria->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            IngresosCategoria::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            IngresosCategoria::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            IngresosCategoria::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = IngresosCategoria::select([
            'id', 'nombre', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}