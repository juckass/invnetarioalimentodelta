<?php

namespace Modules\Ingresos\Http\Requests\definiciones;

use App\Http\Requests\Request;
 
class IngresosRequerimientoRequest extends Request {
	protected $tabla = 'ingresos_requerimiento';
	protected $reglasArr = [
		'municipio_id' => ['required', 'integer'], 
		'rubro_id' => ['required', 'integer'], 
		'cantidad' => ['required']
	];

	public function rules(){
		return $this->reglas();
	}
}