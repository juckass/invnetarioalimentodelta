<?php namespace Modules\Ingresos\Http\Requests\definiciones;

use App\Http\Requests\Request;

class CrearCategoriaRequest extends Request {
	protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:50', 'unique:ingresos_categoria'],
	];
}