<?php

namespace Modules\Ingresos\Http\Requests;

use App\Http\Requests\Request;

class IngresosCategoriaRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:100']
	];
}