<?php

namespace Modules\Ingresos\Http\Requests;

use App\Http\Requests\Request;

class RedesAlimentacionRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:100']
	];
}