<?php

namespace Modules\Ingresos\Http\Requests;

use App\Http\Requests\Request;


class inventarioRequest extends Request {
	protected $reglasArr = [
		'mes' => ['required'],
		'inv.*.*' =>  ['required', 'regex:/^\d{1,8}([\.\,]\d{1,3})?$/'],
	];
}