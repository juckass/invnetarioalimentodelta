<?php

namespace Modules\Ingresos\Http\Requests;

use App\Http\Requests\Request;

class IngresosRequerimientoRequest extends Request {
    protected $reglasArr = [
		'municipio_id' => ['required', 'integer'], 
		'rubro_id' => ['required', 'integer'], 
		'cantidad' => ['required']
	];
}