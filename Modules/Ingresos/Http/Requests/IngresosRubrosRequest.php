<?php

namespace Modules\Ingresos\Http\Requests;

use App\Http\Requests\Request;

class IngresosRubrosRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:80', 'unique:ingresos_rubros,nombre'], 
		'estatus' => ['integer'], 
		'categoria_id' => ['required', 'integer']
	];
}