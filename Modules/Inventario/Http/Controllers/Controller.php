<?php namespace Modules\Inventario\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

abstract class Controller extends BaseController {
	protected $titulo = 'Backend';

	public $app = 'alimentos';

	protected $patch_js = [
		'public/js',
		'public/plugins',
		'Modules/Inventario/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'Modules/Inventario/Assets/css',
	];
}