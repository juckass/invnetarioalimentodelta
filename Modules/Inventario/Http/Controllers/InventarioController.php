<?php namespace Modules\Inventario\Http\Controllers;

use Modules\Inventario\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;
use Carbon\Carbon;
use Excel; 
use Modules\Inventario\Model\definiciones\Empresas;
use Modules\Inventario\Model\definiciones\rubros;
use Modules\Inventario\Model\inventario as inventario;

use Modules\Inventario\Http\Requests\inventarioRequest;

class InventarioController extends Controller {
	protected $titulo = 'Inventario';
	
	public $librerias = [
		'jquery-ui',
		'jquery-ui-timepicker',
		'ladda',
		'touchspin'
	];

	public $js=[
		'inventario'
	];
	public $css=[
		'inventario'
	];
	public function index()
	{
		$empresas   = Empresas::where('redes_id', '=', '1')->get();
		$rubros     = rubros::orderBy('nombre', 'ASC')->get();
		
		return $this->view('inventario::inventario', [
			'empresas' =>   $empresas,
			'rubros'  =>   $rubros,
		]);
	}
	
	public function guardar(inventarioRequest $request){
		DB::beginTransaction(); //Start transaction!

		try{
			$fecha = $request->input("fecha");
			$fecha_inv = Carbon::createFromFormat('d/m/Y', $fecha)->format("Y-m-d");
			$inventario = $request->input("inv");
			
			foreach ($inventario as $id_empresa => $inv) {
				foreach ($inv as $id_rubro => $cantidad) {
					$cantidad = floatval(str_replace(',', '.', $cantidad));

					$inven = inventario::firstOrNew([
						'fecha' => $fecha_inv,
						'empresa_id' => $id_empresa,
						'rubros_id' => $id_rubro,
						'ano' => date('Y'),
					]);

					$inven->cantidad = $cantidad;

					$inven->save();
				}
			}
		}catch(\Exception $e){
			DB::rollback();
			return $e->errorInfo[2];	
		}

		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}
 
	public static function fecha(Request $request)
	{
		$fecha =  Carbon::createFromFormat('d/m/Y', $request->fecha)->format("Y-m-d"); 
		$query = DB::table('inventario')
			->select('inventario.empresa_id', 'inventario.rubros_id', 'inventario.cantidad')
			->whereNull('inventario.deleted_at')
			->where('inventario.fecha', '=', $fecha)
			->where('inventario.ano', '=', date('Y'));

		
		$inventario = $query->get();

		$cantidades	= [];

		if(!empty($inventario)){
			$empresas 	= Empresas::all();
			$rubros     = rubros::all();
		
			foreach ($inventario as $inv) {
				$cantidades[$inv->empresa_id][$inv->rubros_id] = $inv->cantidad;
			}
		}

		return $cantidades;
	}

	public function archivo(Request $request){
		$mimes = [
			'text/csv',
			'application/vnd.ms-excel',
			'application/vnd.oasis.opendocument.spreadsheet',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
		];

		$ruta = public_path('archivos/');
		$archivo = $request->file('subir');

		$mime = $archivo->getClientMimeType();

		if (!in_array($mime, $mimes)) {
			return "Error de Validación, la extension el archivo cargado debe ser: xls,xlsx,csv";
		}
		
		do {
			$nombre_archivo = str_random(20) . '.' . $archivo->getClientOriginalExtension();
		} while (is_file($ruta . $nombre_archivo));
			
		$archivo->move($ruta, $nombre_archivo);
		chmod($ruta . $nombre_archivo, 0777);

		$excel = Excel::selectSheetsByIndex(0)->load($ruta . $nombre_archivo)->get();

		$empresas = [];
		$no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
		$permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");

		foreach (Empresas::all() as $empresa) {
			$empresa->nombre = str_replace($no_permitidas, $permitidas, $empresa->nombre);
			$empresa->nombre = str_slug($empresa->nombre);

			$empresas[$empresa->nombre] = $empresa->id;
		}

		$rubros = [];
		foreach (rubros::all() as $rubro) {
			$rubro->nombre = str_slug($rubro->nombre);

			$rubros[$rubro->nombre] = $rubro->id;
		}
			
		$cantidades	= [];
	 
		try {
			foreach ($excel as $key => $value){
				$value->rubros = str_slug($value->rubros);
				$idRubro = $rubros[$value->rubros];

				$idEmpresa = 0;

				foreach ($empresas as $empresa => $idEmpresa) {
					$str = str_replace('-', '_', str_slug($empresa));
					
					$valor = $value->$str;
					if(is_float($valor)) {
						$valor = round($valor, '2');
					}

					if (is_null($value->$str)){
						$valor = 0;
					}

					$cantidades[$idEmpresa][$idRubro] = $valor;
				}
			}
		} catch (Exception $e) { }
		
		return ['s' => 's', 'msj' => 'Carga Completa', 'cantidades' => $cantidades ];
	}
}