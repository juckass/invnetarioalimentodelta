<?php namespace Modules\Inventario\Http\Controllers;

use Modules\Inventario\Http\Controllers\Controller;
use Illuminate\Http\Request;
 
use DB;
use Carbon\Carbon;

use Modules\Inventario\Model\definiciones\categorias;
use Modules\Inventario\Model\definiciones\Empresas;
use Modules\Inventario\Model\definiciones\rubros;
use Modules\Inventario\Model\inventario;

class GraficasController extends Controller {
	protected $titulo = 'Graficas';
	
	public $librerias = [
		'jquery-ui',
		'jquery-ui-timepicker',
	];
	public $js = [
		'graficas',
	];
	public function index()
	{
		$ultimaFecha = DB::table('inventario')->select(DB::raw('max(fecha) as fecha'))->first();
		
		return $this->view('inventario::graficas', [
			'ultimaFecha' => Carbon::parse($ultimaFecha->fecha)->format('d/m/Y')
		]);
	}
	


	public static function graficaCategoria(Request $request)
	{
		$fecha =  Carbon::createFromFormat('d/m/Y', $request->fecha)->format("Y-m-d");
		$query = DB::table('inventario')
			->select('categorias.nombre as name' , DB::RAW('SUM(inventario.cantidad) as y'))
			->leftJoin('empresas', 'empresas.id', '=', 'inventario.empresa_id')
			->leftJoin('rubros', 'rubros.id', '=', 'inventario.rubros_id')
			->leftJoin('categorias', 'rubros.categoria_id', '=', 'categorias.id')

			->whereNull('inventario.deleted_at')
			->whereNull('empresas.deleted_at')
			->whereNull('rubros.deleted_at')
			->whereNull('categorias.deleted_at')
			->where('inventario.fecha', '=', $fecha)
			->groupBy('categorias.id');

		$series = [[
			'name' => 'Alimentos',
			'colorByPoint' => true,
			'data' => [[
				'name' => 'Total del Estado',
				'drilldown' => str_slug('Total del Estado', '_'),
				'y' => 0
			]]
		]];
		
		$total = 0;

		foreach ($query->get() as $inventario) {
			$cantidad = floatval(str_replace(',', '.', $inventario->y));

			$series[0]['data'][] = [
				'name' => $inventario->name,
				'drilldown' => str_slug($inventario->name, '_'),
				'y' => $cantidad
			];

			$total += $cantidad;
		}

		$series[0]['data'][0]['y'] = $total;

		/*-----------------------------------------------------------------------------------------------------*/
		$query = DB::table('inventario')
			->select('empresas.nombre as empresas', DB::RAW('SUM(inventario.cantidad) as cantidad'))
			->leftJoin('rubros', 'rubros.id', '=', 'inventario.rubros_id')
			->leftJoin('categorias', 'rubros.categoria_id', '=', 'categorias.id')
			->leftJoin('empresas', 'empresas.id', '=', 'inventario.empresa_id')
			->whereNull('inventario.deleted_at')
			->whereNull('rubros.deleted_at')
			->whereNull('categorias.deleted_at')
			->whereNull('empresas.deleted_at')
			->where('inventario.fecha', '=', $fecha)
			->groupBy('empresas.id');

		if($request->has('rubro')){
			$query->whereIn('rubros_id', $request->rubro);
		}

		$drilldown = [
			'series' => [],
		];

		$_query = clone $query;
		
		foreach ($_query->get() as $inventario) {
			$cantidad = floatval(str_replace(',', '.', $inventario->cantidad));

			if ($cantidad == 0){
				continue;
			}

			$data[] = [$inventario->empresas, $cantidad];
		}

		$drilldown['series'][] = [
			'name' => 'Total del Estado',
			'id' => str_slug('Total del Estado', '_'),
			'data' => $data
		];

		foreach (categorias::all() as $categorias) {
			$data = [];
			$_query = clone $query;
			$_query->where('categorias.id', intval($categorias->id));

			foreach ($_query->get() as $inventario) {
				$cantidad = floatval(str_replace(',', '.', $inventario->cantidad));

				if ($cantidad == 0){
					continue;
				}

				$data[] = [$inventario->empresas, $cantidad];
			}

			$drilldown['series'][] = [
				'name' => $categorias->nombre,
				'id' => str_slug($categorias->nombre, '_'),
				'data' => $data
			];
		}

		return [
			'series' => $series,
			'drilldown' => $drilldown
		];
	}

	public static function graficaEmpresas(Request $request)
	{
		$fecha =  Carbon::createFromFormat('d/m/Y', $request->fecha)->format("Y-m-d");

		$query = DB::table('inventario')
			->select('empresas.nombre as name', DB::RAW('SUM(inventario.cantidad) as y'))
			->leftJoin('empresas', 'empresas.id', '=', 'inventario.empresa_id')
			->whereNull('inventario.deleted_at')
			->whereNull('empresas.deleted_at')
			->where('inventario.fecha', '=', $fecha)
			->groupBy('empresas.nombre');

		if($request->has('rubro')){
			$query->whereIn('rubros_id', $request->rubro);
		}
		
		$series = [[
			'name' => 'Alimentos',
			'colorByPoint' => true,
			'data' => [[
				'name' => 'Total del Estado',
				'drilldown' => str_slug('Total del Estado', '_'),
				'y' => 0
			]]
		]];
		
		$total = 0;

		foreach ($query->get() as $inventario) {
			$cantidad = floatval(str_replace(',', '.', $inventario->y));

			$series[0]['data'][] = [
				'name' => $inventario->name,
				'drilldown' => str_slug($inventario->name, '_'),
				'y' => $cantidad
			];

			$total += $cantidad;
		}

		$series[0]['data'][0]['y'] = $total;

		$query = DB::table('inventario')
			->leftJoin('rubros', 'rubros.id', '=', 'inventario.rubros_id')
			->leftJoin('empresas', 'empresas.id', '=', 'inventario.empresa_id')
			->whereNull('inventario.deleted_at')
			->whereNull('empresas.deleted_at')
			->whereNull('rubros.deleted_at')
			->where('inventario.fecha', '=', $fecha);

		if($request->has('rubro')){
			$query->whereIn('rubros_id', $request->rubro);
		}

		$drilldown = [
			'series' => [],
		];

		$data = [];

		$_query = clone $query;
		$_query
		->select('rubros.nombre as rubro', DB::RAW('SUM(inventario.cantidad) as cantidad'))
		->groupBy('rubros.nombre');
		
		foreach ($_query->get() as $inventario) {
			$cantidad = floatval(str_replace(',', '.', $inventario->cantidad));

			if ($cantidad == 0){
				continue;
			}

			$data[] = [$inventario->rubro, $cantidad];
		}

		$drilldown['series'][] = [
			'name' => 'Total del Estado',
			'id' => str_slug('Total del Estado', '_'),
			'data' => $data
		];

		$query->select('rubros.nombre as rubro', 'inventario.cantidad');

		foreach (Empresas::all() as $empresa) {
			$data = [];
			$_query = clone $query;
			$_query->where('empresas.id', $empresa->id);

			foreach ($_query->get() as $inventario) {
				$cantidad = floatval(str_replace(',', '.', $inventario->cantidad));

				if ($cantidad == 0){
					continue;
				}

				$data[] = [$inventario->rubro, $cantidad];
			}

			$drilldown['series'][] = [
				'name' => $empresa->nombre,
				'id' => str_slug($empresa->nombre, '_'),
				'data' => $data
			];
		}

		return [
			'series' => $series,
			'drilldown' => $drilldown
		];
	}

	public function rubros() {
		return rubros::pluck('nombre', 'id');
	}
}