<?php

namespace Modules\Inventario\Http\Controllers\definiciones;

//Controlador Padre
use Modules\Inventario\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Inventario\Http\Requests\EmpresasRequest;

//Modelos
use Modules\Inventario\Model\definiciones\Empresas;
use Modules\Ingresos\Model\definiciones\RedesAlimentacion as redes;

class EmpresasController extends Controller
{
    protected $titulo = 'Empresas';

    public $js = [
        'definiciones/empresas'
    ];
    
    public $css = [
        'definiciones/empresas'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('inventario::empresas', [
            'Empresas' => new Empresas()
        ]);
    }

    public function nuevo()
    {
        $Empresas = new Empresas();
        return $this->view('inventario::empresas', [
            'layouts' => 'admin::layouts.popup',
            'Empresas' => $Empresas
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Empresas = Empresas::find($id);
        return $this->view('inventario::empresas', [
            'layouts' => 'admin::layouts.popup',
            'Empresas' => $Empresas
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Empresas = Empresas::withTrashed()->find($id);
        } else {
            $Empresas = Empresas::find($id);
        }

        if ($Empresas) {
            return array_merge($Empresas->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(EmpresasRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Empresas = $id == 0 ? new Empresas() : Empresas::find($id);

            $Empresas->fill($request->all());
            $Empresas->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $Empresas->id,
            'texto' => $Empresas->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Empresas::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Empresas::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Empresas::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Empresas::select([
            'id', 'nombre', 'redes_id', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
    public function red() {
        return redes::pluck('nombre', 'id');
    }

}