<?php namespace Modules\Inventario\Http\Controllers\definiciones;
  
use DB;
use Illuminate\Http\Request;

use Yajra\Datatables\Datatables;

use Modules\Inventario\Http\Controllers\Controller;

use Modules\Inventario\Model\definiciones\rubros as modelo;
use Modules\Inventario\Model\definiciones\categorias;

use Modules\Inventario\Http\Requests\RubrosRequest as Rubros_request;


class RubrosController extends Controller {
	
	protected $titulo = 'Rubros';
	
	public $librerias = [
		'datatables',
	];

	public $js = [
		'definiciones/rubros'
	];

	public function getIndex()
	{
		return view('inventario::rubros', $this->_app());
	}
	
	public function getBuscar(Request $request, $id = 0) {
		$rs = modelo::find($id);

		if ($rs) {
			return array_merge($rs->toArray(), [
				's' => 's',
				'msj' => trans('controller.buscar'),
			]);
		}

		return trans('controller.nobuscar');
	}

	
	public function postCrear(Rubros_request $request){

		DB::beginTransaction();
		try {
			$rs = modelo::create($request->all());

		} catch (Exception $e) {
			DB::rollback();
			return $e->errorInfo[2];
		}

		DB::commit();
		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function putActualizar(Request $request, $id = 0) {
		DB::beginTransaction();
		try {
			//dd($request->all())
			$rs = modelo::find($id)->update([
				"nombre" 		 => $request->nombre,
				"estatus"		 =>	$request->estatus,
  				"categoria_id"	 => $request->categoria_id
			]);
		} catch (Exception $e) {
			DB::rollback();
			return $e->errorInfo[2];
		}

		DB::commit();
		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function deleteEliminar(Request $request, $id = 0) {
		try {
			$rs = modelo::destroy(intval($id));
		} catch (Exception $e) {
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

	
	public function getDatatable() {
		$sql = modelo::select('id', 'nombre', 'categoria_id');
		return Datatables::of($sql)->setRowId('id')->make(true);
	}
	public function categorias() {
		return categorias::pluck('nombre', 'id');
	}

}  
