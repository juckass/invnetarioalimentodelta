<?php

namespace Modules\Inventario\Http\Controllers\definiciones;

//Controlador Padre
use Modules\Inventario\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Inventario\Http\Requests\RubrosRequest;

//Modelos
use Modules\Inventario\Model\definiciones\rubros as Rubros;
use Modules\Inventario\Model\definiciones\categorias;

class RubrosController extends Controller
{
    protected $titulo = 'Rubros';

    public $js = [
       'definiciones/rubros'
    ];
    
    public $css = [
        'Rubros'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('inventario::rubros', [
            'Rubros' => new Rubros()
        ]);
    }

    public function nuevo()
    {
        $Rubros = new Rubros();
        return $this->view('inventario::rubros', [
            'layouts' => 'admin::layouts.popup',
            'Rubros' => $Rubros
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Rubros = Rubros::find($id);
        return $this->view('inventario::rubros', [
            'layouts' => 'admin::layouts.popup',
            'Rubros' => $Rubros
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Rubros = Rubros::withTrashed()->find($id);
        } else {
            $Rubros = Rubros::find($id);
        }

        if ($Rubros) {
            return array_merge($Rubros->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(RubrosRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Rubros = $id == 0 ? new Rubros() : Rubros::find($id);

            $Rubros->fill($request->all());
            $Rubros->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $Rubros->id,
            'texto' => $Rubros->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Rubros::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Rubros::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Rubros::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Rubros::select([
            'id', 'nombre', 'estatus', 'categoria_id', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }

    public function categorias() {
        return categorias::pluck('nombre', 'id');
    }

}