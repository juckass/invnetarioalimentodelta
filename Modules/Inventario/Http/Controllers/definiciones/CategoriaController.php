<?php

namespace Modules\Inventario\Http\Controllers\definiciones;

//Controlador Padre
use Modules\Inventario\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Inventario\Http\Requests\CategoriasRequest;

//Modelos
use Modules\Inventario\Model\definiciones\categorias;

class CategoriaController extends Controller
{
    protected $titulo = 'Categorias';

    public $js = [
        'definiciones/categorias'
    ];
    
    public $css = [
        'definiciones/categorias'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('inventario::categorias', [
            'Categorias' => new Categorias()
        ]);
    }

    public function nuevo()
    {
        $Categorias = new Categorias();
        return $this->view('inventario::categorias', [
            'layouts' => 'admin::layouts.popup',
            'Categorias' => $Categorias
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Categorias = Categorias::find($id);
        return $this->view('inventario::categorias', [
            'layouts' => 'admin::layouts.popup',
            'Categorias' => $Categorias
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Categorias = Categorias::withTrashed()->find($id);
        } else {
            $Categorias = Categorias::find($id);
        }

        if ($Categorias) {
            return array_merge($Categorias->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(CategoriasRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Categorias = $id == 0 ? new Categorias() : Categorias::find($id);

            $Categorias->fill($request->all());
            $Categorias->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $Categorias->id,
            'texto' => $Categorias->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Categorias::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Categorias::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Categorias::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Categorias::select([
            'id', 'nombre', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}