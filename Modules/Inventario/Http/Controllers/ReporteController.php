<?php namespace Modules\Inventario\Http\Controllers;



use DB;

use Carbon\Carbon;

use Modules\Inventario\Http\Controllers\Controller;

use Illuminate\Http\Request;



use Modules\Inventario\Model\definiciones\Empresas as empresa;

use Modules\Inventario\Model\definiciones\rubros as rubros;

use Modules\Inventario\Model\inventario;



use Modules\Inventario\Http\Requests\reporteRequest;





class ReporteController extends Controller {

	

	protected $titulo = 'Reporte';



	public $librerias = [

		'jquery-ui',

		'jquery-ui-timepicker',

	];



	public $js=[



		'reporte/inventario'

	];



	public function index()

	{

		$ultimaFecha = DB::table('inventario')->select(DB::raw('max(fecha) as fecha'))->first();

		return $this->view('inventario::reporte', [

			'ultimaFecha' => Carbon::parse($ultimaFecha->fecha)->format('d/m/Y')

		]);

	}

	

	public function imprimir(reporteRequest $request){

		$fecha =  Carbon::createFromFormat('d/m/Y', $request['fecha'])->format("Y-m-d"); 



		$query = DB::table('inventario')

			->select('inventario.empresa_id', 'inventario.rubros_id', 'inventario.cantidad')

			->whereNull('inventario.deleted_at')

			->where('inventario.fecha', '=', $fecha);

	

		$empresas    = empresa::where('redes_id', 1)->get();

		$rubros      = rubros::all();

		$cantidades  = [];



		$total = 0;

		$totalRubro = [];

		$totalEmpresas = [];



		foreach ($query->get() as $inventario) {

			$cantidades[$inventario->empresa_id][$inventario->rubros_id] = $inventario->cantidad;



			$total += $inventario->cantidad;



			//Acumulador de Totales de cantidad de alimento por Rubros

			if (!isset($totalRubro[$inventario->rubros_id])){

				$totalRubro[$inventario->rubros_id] = 0;

			}

			$totalRubro[$inventario->rubros_id] += $inventario->cantidad;



			//Acumulador de Totales de cantidad de alimento por empresas

			if (!isset($totalEmpresas[$inventario->empresa_id])){

				$totalEmpresas[$inventario->empresa_id] = 0;

			}

			$totalEmpresas[$inventario->empresa_id] += $inventario->cantidad;

		}



		//dd($cantidades);





		$html = view('inventario::pdf.reporte', [

           'cantidades'	=> $cantidades,

			'fecha'			=> $request['fecha'],

			'empresas'		=> $empresas,

			'rubros'		=> $rubros,

			'total'			=> $total,

			'totalRubro'	=> $totalRubro,

			'totalEmpresas'	=> $totalEmpresas,

			'html' 			=> $request->has('html')

        ])->render();



        if ($request->has('html')) {

            return $html;

        }



        $pdf = \PDF::loadHTML($html);

        return $pdf->download('reporte.pdf');

	}

}  





 		