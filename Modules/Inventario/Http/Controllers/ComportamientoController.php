<?php namespace Modules\Inventario\Http\Controllers;

use Modules\Inventario\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;
use Carbon\Carbon;

use Modules\Inventario\Model\definiciones\Empresas as empresas;
use Modules\Inventario\Model\definiciones\rubros;
use Modules\Inventario\Model\inventario;

class ComportamientoController extends Controller {
	protected $titulo = 'Comportamiento';
	
	public $librerias = [
		'jquery-ui',
		'jquery-ui-timepicker',
		'highcharts',
		'highcharts-drilldown',
		'icheck'
	];

	public $js = [
		'comportamiento'
	];

	public $css = [
		'comportamiento'
	];

	public function index()
	{
		$fecha = DB::table('inventario')->select(DB::raw('max(fecha) as fecha'))->first();
		
		$ultimaFecha = Carbon::parse($fecha->fecha);
		$primeraFecha = Carbon::parse($fecha->fecha)->subWeeks(2);

		$sql = DB::table('inventario')
			->select(DB::raw('EXTRACT(YEAR FROM fecha) as anos'))
			->groupBy(DB::raw('EXTRACT(YEAR FROM fecha)'))
			->get();

		$anos = [];

		foreach ($sql as $value) {
			$anos[$value->anos] = $value->anos;
		}
		
		return $this->view('inventario::comportamiento', [
			'ultimaFecha' => $ultimaFecha->format('d/m/Y'),
			'primeraFecha' => $primeraFecha->format('d/m/Y'),
			'anos' => $anos
		]);
	}

	public static function graficaAnos(Request $request)
	{
		$ano = $request->ano;
		
		$meses = [
			'Enero',
			'Febrero',
			'Marzo',
			'Abril',
			'Mayo',
			'Junio',
			'Julio',
			'Agosto',
			'Septiembre',
			'Octubre',
			'Noviembre',
			'Diciembre'
		];
 		
		/*
SELECT
	empresa_id,
	sum(cantidad) as cantidad,
	EXTRACT(MONTH FROM inventario.fecha) AS mes,
	EXTRACT(YEAR FROM inventario.fecha) AS ano
FROM
	inventario
GROUP BY 
	empresa_id,
	EXTRACT(MONTH FROM inventario.fecha),
	EXTRACT(YEAR FROM inventario.fecha)
ORDER BY 
	EXTRACT(YEAR FROM inventario.fecha),
	EXTRACT(MONTH FROM inventario.fecha)
		*/
		
 		$query = DB::table('inventario')
			->select(
				'inventario.empresa_id',
				DB::RAW('sum(inventario.cantidad) as cantidad'),
				DB::RAW('EXTRACT(MONTH FROM inventario.fecha) as mes'),
				DB::RAW('EXTRACT(YEAR FROM inventario.fecha) as ano')
			)
			->leftJoin('empresas', 'empresas.id', '=', 'inventario.empresa_id')
			->whereNull('inventario.deleted_at')
			->whereNull('empresas.deleted_at')
			->groupBy(
				'inventario.empresa_id',
				DB::RAW('EXTRACT(MONTH FROM inventario.fecha)'),
				DB::RAW('EXTRACT(YEAR FROM inventario.fecha)')
			)
			->orderBy(DB::RAW('EXTRACT(YEAR FROM inventario.fecha)'))
			->orderBy(DB::RAW('EXTRACT(MONTH FROM inventario.fecha)'));
	
		if($request->has('rubro')){
			$query->whereIn('inventario.rubros_id', $request->rubro);
		}

		$series = [[
			'name' => 'Total Delta Amacuro',
			'data' => []
		]];

		/* Total del estado */
		$categorias = [];
		$empresas = Empresas::all()->sortBy('id')->pluck('nombre', 'id');
		
		$datas = $query->get();
		foreach ($datas as $data) {
			$fecha = $meses[$data->mes - 1] . ' ' . $data->ano;
			$categorias[$fecha] = $fecha;

			if (!isset($series[$data->empresa_id])) {
				$series[$data->empresa_id]['name'] = $empresas[$data->empresa_id];
				$series[$data->empresa_id]['data'] = [];
			}
		}

		$categorias = array_values($categorias);
		foreach ($series as $empresa_id => $serie) {
			foreach ($categorias as $categoria) {
				$series[$empresa_id]['data'][$categoria] = 0;
			}
		}

		foreach ($datas as $data) {
			$fecha = $meses[$data->mes - 1] . ' ' . $data->ano;
			$series[$data->empresa_id]['data'][$fecha] = floatval($data->cantidad);
			$series[0]['data'][$fecha] += floatval($data->cantidad);
		}

		$series = array_values($series);
		foreach ($series as $key => $value) {
			$series[$key]['data'] = array_values($series[$key]['data']);
		}
		
		return [
			'categorias' => $categorias,
			'series' => $series
		];

		$_query = clone $query;
		$i = 0;

		foreach ($meses as $mes) {
			$series[$i]['data'][] = 0;
		}

		foreach (empresas::all() as $empresa) {
			$i++;
			$series[$i]['name'] = $empresa->nombre;
			foreach ($meses as $mes) {
				$series[$i]['data'][] = 0;
			}
		}

		/* Resumen por Empresa */
		$query->groupBy('inventario.empresa_id', 'fecha');
		$i = 0;
		$empresas = empresas::all();
		//$empresas = empresas::where('redes_id', 1)->get();
		foreach ($empresas as $empresa) {
			//$series[$i]['name'] = $empresa->nombre;

			$_query = clone $query;
			$_query->where('empresas.id', $empresa->id);

			foreach ($_query->get() as $inventario) {
				$cantidad = floatval(str_replace(',', '.', $inventario->cantidad));

				$series[$i]['data'][intval($inventario->mes) - 1] = $cantidad;
			}

			$i++;
		}
		
		for ($i = 0, $ci = count($series); $i < $ci; $i++) {
			for ($j = 0, $cj = count($series[$i]['data']); $j < $cj; $j++) { 
				$series[$i]['data'][$j] += $series[$i]['data'][$j];
			}
		}
		
	
		return [
			'categorias' => array_values($meses),
			'series' => $series
		];
	}

	public static function graficaRango(Request $request)
	{
		$fechadesde = Carbon::createFromFormat('d/m/Y', $request->fechadesde)->format("Y-m-d");
		$fechahasta = Carbon::createFromFormat('d/m/Y', $request->fechahasta)->format("Y-m-d");

		$query = DB::table('inventario')
			->select('inventario.fecha')
			->whereNull('inventario.deleted_at')
			->whereBetween('inventario.fecha', [$fechadesde, $fechahasta])
			->groupBy('inventario.fecha');

		$categorias = [];
		foreach ($query->get() as $inventario) {
			$categorias[] = Carbon::parse($inventario->fecha)->format('d/m/Y');
		}
 
		$query = DB::table('inventario')
			->select(DB::RAW('SUM(inventario.cantidad) as cantidad'), 'inventario.fecha')
			->leftJoin('empresas', 'empresas.id', '=', 'inventario.empresa_id')
			->whereNull('inventario.deleted_at')
			->whereNull('empresas.deleted_at')
			->where('empresas.redes_id', 1)
			->whereBetween('inventario.fecha', [$fechadesde, $fechahasta])
			->orderBy('inventario.fecha')
			->groupBy('inventario.fecha');

		if($request->has('rubro')){
			$query->whereIn('rubros_id', $request->rubro);
		}

		$series = [[
			'name' => 'Total Bolívar',
			'data' => []
		]];

		/* Total del estado */
		$_query = clone $query;

		foreach ($_query->get() as $inventario) {
			$cantidad = floatval(str_replace(',', '.', $inventario->cantidad));

			$series[0]['data'][] = $cantidad;
		}


		/* Resumen por Empresa */


		$query->groupBy('inventario.empresa_id');
		$empresas = empresas::all();
		//$empresas = empresas::where('redes_id', 1)->get();
		foreach ($empresas as $empresa) {
			$serie = [
				'name' => $empresa->nombre,
				'data' => []
			];

			$_query = clone $query;
			$_query->where('empresas.id', $empresa->id);

			foreach ($_query->get() as $inventario) {
				$cantidad = floatval(str_replace(',', '.', $inventario->cantidad));

				$serie['data'][] = $cantidad;
			}

			$series[] = $serie;
		}
		
		return [
			'categorias' => $categorias,
			'series' => $series
		];
	}
	
	public function rubros() {
		return rubros::pluck('nombre', 'id');
	}
}