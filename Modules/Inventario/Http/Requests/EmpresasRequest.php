<?php

namespace Modules\Inventario\Http\Requests;

use App\Http\Requests\Request;

class EmpresasRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:80', 'unique:empresas,nombre'], 
		'redes_id' => ['required', 'integer']
	];
}