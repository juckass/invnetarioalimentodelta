<?php

namespace Modules\Inventario\Http\Requests;

use App\Http\Requests\Request;


class CrearEmpresasRequest extends Request {
	protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:50', 'unique:empresas'],
	];
}