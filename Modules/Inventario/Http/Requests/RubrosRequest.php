<?php

namespace Modules\Inventario\Http\Requests;

use App\Http\Requests\Request;

class RubrosRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:80', 'unique:rubros,nombre'], 
		'estatus' => ['integer'], 
		'categoria_id' => ['integer']
	];
}