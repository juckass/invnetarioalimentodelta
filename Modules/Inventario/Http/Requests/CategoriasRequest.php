<?php

namespace Modules\Inventario\Http\Requests;

use App\Http\Requests\Request;

class CategoriasRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:80', 'unique:categorias,nombre']
	];
}