<?php

namespace Modules\Inventario\Http\Requests;

use App\Http\Requests\Request;


class reporteRequest extends Request {
	protected $reglasArr = [
		'fecha' => ['required', 'date_format:d/m/Y'],
	];
}