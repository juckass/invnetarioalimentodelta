<?php

namespace Modules\Inventario\Http\Requests;

use App\Http\Requests\Request;


class inventarioRequest extends Request {
	protected $reglasArr = [
		'fecha' => ['required', 'date_format:d/m/Y'],
		'inv.*.*' =>  ['required', 'regex:/^\d{1,8}([\.\,]\d{1,3})?$/'],
	];
}