<?php

$menu['inventario'][] = [
	'nombre' 	=> 'Inventario',
	'direccion' => '#Inventario',
	'icono' 	=> 'fa fa-cart-arrow-down ',
	'menu' 		=> [
		[
			'nombre' 	=> 'Definiciones',
			'direccion' => '#Definicionesinventario',
			'icono' 	=> 'fa fa-list',
			'menu' 		=>[

				[
					'nombre' 	=> 'Categoria',
					'direccion' => 'inventario/definiciones/categoria',
					'icono' 	=> 'fa fa-list'
				],
				[
					'nombre' 	=> 'Empresas',
					'direccion' => 'inventario/definiciones/empresas',
					'icono' 	=> 'fa fa-industry'
				],
				[
					'nombre' 	=> 'Rubros',
					'direccion' => 'inventario/definiciones/rubros',
					'icono' 	=> 'fa fa-cutlery'
				]
			]			
		],
		[
			'nombre' 	=> 'Inventario',
			'direccion' => 'inventario/reporte',
			'icono' 	=> 'fa fa-print',
		],
		[
			'nombre' 	=> 'Carga',
			'direccion' => 'inventario/carga',
			'icono' 	=> 'fa fa-list-alt',
		],
		[
			'nombre' 	=> 'Graficas',
			'direccion' => 'inventario/graficos',
			'icono' 	=> 'fa fa-bar-chart',
		],
		[
			'nombre' 	=> 'Comportamiento',
			'direccion' => 'inventario/comportamiento',
			'icono' 	=> 'fa fa-line-chart',
		]
	]
];