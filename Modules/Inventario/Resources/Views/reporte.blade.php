@extends('admin::layouts.default')
@section('content')
	<div id="botonera">
		<button id="buscar" class="btn green" title="{{ Lang::get('backend.btn_group.search.title') }}">
			<i class="fa fa-search"></i>
			<span class="visible-lg-inline visible-md-inline">{{ Lang::get('backend.btn_group.search.btn') }}</span>
		</button>

		<button id="imprimir" class="btn btn-info" title="{{ Lang::get('backend.btn_group.print.title') }}">
			<i class="fa fa-print"></i>
			<span class="visible-lg-inline visible-md-inline">{{ Lang::get('backend.btn_group.print.btn') }}</span>
		</button>
	</div>

	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Reportes<i class="fa fa-circle"></i></span>
		</li>
		<li>
			<span>Inventario</span>
		</li>
	</ul>

	<div class="row">
		{!! Form::open([
			'id' => 'formulario',
			'name' => 'formulario',
			'method' => 'POST',
			'target' => '_blank',
			'url' => 'inventario/reporte/imprimir'
		]) !!}
			{{ Form::bsText('fecha', $ultimaFecha, [
				'label' => 'Fecha',
				'required' => 'required'
			]) }}
		{!! Form::close() !!}
	</div>

	<div id="reportehtml"></div>
@endsection