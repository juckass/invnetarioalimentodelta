@extends('admin::layouts.default')
@section('content')
	@include('admin::partials.botonera')
		
	{{ Form::bsModalBusqueda([
		'Categoria' => '100',
	], [
		'titulo' => 'Buscar Categoria.'
	]) }}


	<!--  tabla buscardor -->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Categorias</span>
		</li>
	</ul>

	<div class="row">
		{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST']) !!}
			{{ Form::bsText('nombre', '', [
				'label' => 'Nombre de la Categoria',
				'placeholder' => 'Nombre de la Categoria',
				'required' => 'required'
			]) }}
		{!! Form::close() !!}
	</div>
@endsection