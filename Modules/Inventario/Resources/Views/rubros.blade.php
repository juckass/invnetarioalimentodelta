@extends('admin::layouts.default')
@section('content')
	@include('admin::partials.botonera')
	<!--  tabla buscardor -->
	{{ Form::bsModalBusqueda([
		'Rubros' => '100',		
	], [
		'titulo' => 'Buscar Rubros.'
	]) }}

	<!--  tabla buscardor -->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Rubros</span>
		</li>
	</ul>
	<div class="row">
		{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST']) !!}
			{{ Form::bsText('nombre', '', [
				'label' => 'Nombre del rubro',
				'placeholder' => 'Nombre del rubro',
				'required' => 'required'
			]) }}
			{{ Form::bsSelect('categoria_id', $controller->categorias(), '', [
				'label' => 'Categoria',
				'required' => 'required'
			]) }}
			{{ Form::bsSelect('estatus', [
				'1' => 'Prioritario',
				'2' => 'No Prioritario'
				], '', [
				'label' => 'estatus',
				'required' => 'required'
			]) }}
		{!! Form::close() !!}
	</div>
@endsection