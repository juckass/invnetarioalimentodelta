@extends('admin::layouts.default')
@section('content')
	<div id="botonera">
		<button id="btn_graficar" class="btn btn-app btn-info" title="Graficar">
			<i class="fa fa-bar-chart"></i>
			<span class="visible-lg-inline visible-md-inline">Graficar</span>
		</button>
	</div>

	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Comportamiento</span>
		</li>
	</ul>

	<div class="row">
		{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST']) !!}
			{{ Form::bsSelect('tipo', 
				[
					'anos'=>'Año',
					'rango'=> 'Rango de fecha',
				], '', [
					'label' => 'Tipo de Grafica',
					'required' => 'required'
			]) }}

			<div  id="anos">
				{{ Form::bsSelect('ano', $anos, '', [
					'label' => 'Año',
					'required' => 'required'
				]) }}
			</div>

			<div id="rango">
				{{ Form::bsText('fechadesde', $primeraFecha, [
					'label' => 'Desde',
					'required' => 'required'
				]) }}

				{{ Form::bsText('fechahasta', $ultimaFecha, [
					'label' => 'Hasta',
					'required' => 'required'
				]) }}
			</div>
			
			<br/>
			<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
			  Seleccionar Rubros
			</button>
			<div class="collapse col-sm-12" id="collapseExample">
			  	<?php		
			  		$i = 0;
			  	?>
			  	<div class="col-md-4 col-sm-6">
					@foreach($controller->rubros() as $id => $texto)
						@if (++$i % 4 === 0)
							<?php $i = 1; ?>
							</div><div class="col-md-4 col-sm-6">
						@endif

						<input name="rubro[]" value="{{ $id }}" type="checkbox" />
						<label>{{ $texto }}</label>
					@endforeach
				</div>
			</div>
		{!! Form::close() !!}
		
		<div id="grafica" class="col-sm-12"></div>
	</div>
@endsection