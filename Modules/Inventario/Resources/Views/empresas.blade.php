@extends('admin::layouts.default')
@section('content')
	@include('admin::partials.botonera')
		
	{{ Form::bsModalBusqueda([
		'Empresas' => '100',
	], [
		'titulo' => 'Buscar Empresas.'
	]) }}


	<!--  tabla buscardor -->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Empresas</span>
		</li>
	</ul>

	<div class="row">
		{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST']) !!}
			{{ Form::bsText('nombre', '', [
				'label' => 'Nombre de la Empresa',
				'placeholder' => 'Nombre de la Empresa',
				'required' => 'required'
			]) }}

			{{ Form::bsSelect('redes_id', 
				$controller->red(), '', [
					'label' => 'Red de alimentos',
					'required' => 'required'
			]) }}
			
		{!! Form::close() !!}
	</div>
@endsection  