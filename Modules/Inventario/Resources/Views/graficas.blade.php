@extends('admin::layouts.default')
@section('content')
	<div id="botonera">
		<button id="btn_graficar" class="btn btn-app btn-info" title="Graficar">
			<i class="fa fa-bar-chart"></i>
			<span class="visible-lg-inline visible-md-inline">Graficar</span>
		</button>
	</div>

	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Graficas</span>
		</li>
	</ul>

	<div class="row">
		{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST']) !!}
				
			{{ Form::bsText('fecha', $ultimaFecha, [
				'label' => 'Fecha',
				'required' => 'required'
			]) }}

			{{ Form::bsSelect('tipo', 
				[	
					'empresa'=> 'Empresas',
					'categoria'=>'Categoria',
				], '', [
					'label' => 'Tipo de Grafica',
					'required' => 'required'
			]) }}

			<br/>
			<div class="ws">
				<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
					  Seleccionar Rubros
				</button>
				<div class="collapse col-sm-12" id="collapseExample">
				  	<?php
				  		$i = 0;
				  	?>
				  	<div class="col-md-4 col-sm-6">
						@foreach($controller->rubros() as $id => $texto)
							@if (++$i % 4 === 0)
								<?php $i = 1; ?>
								</div><div class="col-md-4 col-sm-6">
							@endif

							<input name="rubro[]" value="{{ $id }}" type="checkbox" />
							<label>{{ $texto }}</label>
						@endforeach
					</div>
				</div>
			</div>	
		{!! Form::close() !!}
		<div id="grafica" class="col-sm-12"></div>
	</div>
@endsection
@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/all.css">
<style type="text/css" media="screen">
.icheckbox_line-blue{
	white-space: nowrap;      /* CSS3 */   
	white-space: -moz-nowrap; /* Firefox */    
	white-space: -nowrap;     /* Opera <7 */   
	white-space: -o-nowrap;   /* Opera 7 */    
	word-wrap: break-word;      /* IE */
}
.collapse{
	margin-bottom: 20px;
}
</style>

@endpush
@push('js')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/4.2.5/highcharts.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/4.2.5/modules/drilldown.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/4.2.5/highcharts-more.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>

@endpush