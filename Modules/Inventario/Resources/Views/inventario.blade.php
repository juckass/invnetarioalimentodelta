@extends('admin::layouts.default')
@section('content')
	@include('admin::partials.botonera')
	
	<!--  tabla buscardor -->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Inventario</span>
		</li>
	</ul>

	<div class="row">
		{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST']) !!}
			{{ Form::bsText('fecha', '', [
				'label' => 'Fecha',
				'required' => 'required'
			]) }}

			<input type="hidden"  id="actualizar" name="actualizar" value="" />

			<div class="col-md-12"></div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive">
				<table border="1" class="table table-striped table-hover tabla-inventario">
					<caption></caption>
					<thead>
						<tr>
							<th style="width: 30%;">&nbsp;</th>
							@foreach($empresas as $empresa)
								<th class="service" style="width: {{ 70/count($empresas) }}%;">{{ $empresa->nombre}}</th>
							@endforeach 	
						</tr>
					</thead>
					<tbody>
						@foreach($rubros as $rubro)
						<tr>
							<td>{{ $rubro->nombre}}</td>
							@foreach($empresas as $empresa)
								<td class="cont_input">
									<input value="0,00" name="inv[{{ $empresa->id }}][{{ $rubro->id }}]" type="text" data-empresa="{{ $empresa->id }}" data-rubro="{{ $rubro->id }}" />
								</td>
							@endforeach 
						</tr>
						@endforeach 
					</tbody>
				</table>
			</div>
		{!! Form::close() !!}

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h4>Cargar Archivo <small>Excel (.csv, .xls, .xlsx, .ods)</small></h4>
			<hr>
			{!! Form::open(['id' => 'carga', 'name' => 'carga', 'method' => 'POST']) !!}
				<input id="upload" name="subir" type="file"/>

				<button id="subir" type="button" class="btn btn-primary mt-ladda-btn ladda-button" data-style="expand-right">
					<span class="ladda-label">
						<i class="icon-arrow-right"></i> Carga archivo Excel
					</span>
				</button>
			{!! Form::close() !!}
		</div>
	</div>
@endsection