<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventarioTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('redes_alimentacion', function(Blueprint $table){
            $table->increments('id');
            $table->string('nombre', 80)->unique();

            $table->timestamps();
            $table->softDeletes();
        });
           
        Schema::create('categorias', function(Blueprint $table){
            $table->increments('id');
            $table->string('nombre', 80)->unique();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('empresas', function(Blueprint $table){
            $table->increments('id');
            $table->string('nombre', 80)->unique();
            $table->integer('redes_id')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();

             $table->foreign('redes_id')
                ->references('id')->on('redes_alimentacion')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('rubros', function(Blueprint $table){
            $table->increments('id');
            $table->string('nombre', 80)->unique();
            $table->integer('estatus')->unsigned();
            $table->integer('categoria_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('categoria_id')
                ->references('id')->on('categorias')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('inventario', function(Blueprint $table){
            $table->increments('id');
            $table->integer('empresa_id')->unsigned();
            $table->integer('rubros_id')->unsigned();
            $table->integer('ano')->nullable();
            $table->decimal('cantidad', 8, 2);
            $table->date('fecha');
            $table->unique(['empresa_id','rubros_id','fecha', 'ano' ],'inventario_index_unique');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('empresa_id')
                ->references('id')->on('empresas')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('rubros_id')
                ->references('id')->on('rubros')
                ->onDelete('cascade')->onUpdate('cascade');    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventario');
        Schema::dropIfExists('rubros');
        Schema::dropIfExists('empresas');
        Schema::dropIfExists('categorias');
        Schema::dropIfExists('redes_alimentacion');
    }
}
