<?php namespace Modules\Inventario\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Inventario\Model\definiciones\rubros;
use Modules\Inventario\Model\definiciones\categorias;

class RubrosSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		
		$categorias = [
			['Granos'],
			['Harinas'],
			['Carnes'],
			['Leche'],
			['Aceites'],
			['Arroz / Pasta'],
			['Azucar']
		];

		foreach ($categorias as $categoria) {
			categorias::create([
				'nombre' => $categoria[0]
			]);
		}
		

		$rubros = [
			['Aceite no regulado', 1, 5],
			['Aceite regulado', 1, 5],
			['Arroz blanco presentacion no regulada', 1, 6],
			['Arroz blanco presentacion regulada', 1, 6],
			['Arvejas', 1, 1],
			['Azucar domestica', 1, 7],
			['Café molido presentacion no regulada', 1, 1],
			['Café molido presentacion regulada', 1, 1],
			['Caraota negra', 1, 1],
			['Carne de bovino en canal', 1, 3],
			['Carne de bovino depostada', 1, 3],
			['Harina de maiz precocida no regulada', 1, 2],
			['Harina de maiz precocida regulada', 1, 2],
			['Harina de trigo familiar', 1, 2],
			['Leche en polvo completa - uso domestico', 1, 4],
			['Lentejas', 1, 1],
			['Margarina', 1, 5],
			['Pasta alimenticia no regulada', 1, 6],
			['Pollo beneficiado entero', 1, 3],
			['Leche UHT', 1, 4],
			['Pasta alimenticia regulada', 1, 6]
		];

		foreach ($rubros as $rubro) {
			rubros::create([
				'nombre' 		=> $rubro[0],
				'estatus' 		=> $rubro[1],
				'categoria_id' 	=> $rubro[2]
			]);
		}
	}
}