<?php namespace Modules\Inventario\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Admin\Model\app_menu as menu;

class menuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$inventario = menu::create([
			'nombre' => 'Inventario',
			'padre' => 0,
			'posicion' => 2,
			'direccion' => '#inventario',
			'icono' => 'fa fa-cart-arrow-down'
		]);

    	$menu = menu::create([
			'nombre' => 'Definiciones',
			'padre' => $inventario->id,
			'posicion' => 0,
			'direccion' => '',
			'icono' => 'fa fa-list'
		]);

			menu::create([
				'nombre' => 'Categoria',
				'padre' => $menu->id,
				'posicion' => 0,
				'direccion' => 'definiciones/categorias',
				'icono' => 'fa fa-list'
			]);

			menu::create([
				'nombre' => 'Redes de Alimentacion',
				'padre' => $menu->id,
				'posicion' => 2,
				'direccion' => 'ingresos/definiciones/redes',
				'icono' => 'fa fa-asterisk'
			]);
		
			menu::create([
				'nombre' => 'Empresas',
				'padre' => $menu->id,
				'posicion' => 1,
				'direccion' => 'definiciones/empresas',
				'icono' => 'fa fa-industry'
			]);

			menu::create([
				'nombre' => 'Rubros',
				'padre' => $menu->id,
				'posicion' => 2,
				'direccion' => 'definiciones/rubros',
				'icono' => 'fa fa-cutlery'
			]);


		menu::create([
			'nombre' => 'Inventario',
			'padre' => $inventario->id,
			'posicion' => 1,
			'direccion' => 'reporte/inventario',
			'icono' => 'fa fa-print'
		]);

		menu::create([
			'nombre' => 'Carga',
			'padre' => $inventario->id,
			'posicion' => 2,
			'direccion' => 'inventario',
			'icono' => 'fa fa-list-alt'
		]);


		menu::create([
			'nombre' => 'Graficas',
			'padre' => $inventario->id,
			'posicion' => 3,
			'direccion' => 'graficas/inventario',
			'icono' => 'fa fa-bar-chart'
		]);

		menu::create([
			'nombre' => 'Comportamiento',
			'padre' => $inventario->id,
			'posicion' => 4,
			'direccion' => 'comportamiento',
			'icono' => 'fa fa-line-chart'
		]);
    }
}
