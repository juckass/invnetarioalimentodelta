<?php namespace Modules\Inventario\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class InventarioDatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		
		$this->call(EmpresasSeeder::class);
		$this->call(RubrosSeeder::class);
		
	}

}