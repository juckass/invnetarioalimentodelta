<?php

namespace Modules\Inventario\Model\definiciones;
use Modules\Admin\Model\modelo;

class rubros extends modelo
{
	protected $table = 'rubros';

    protected $fillable = [
        'nombre','categoria_id','estatus'
    ];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
