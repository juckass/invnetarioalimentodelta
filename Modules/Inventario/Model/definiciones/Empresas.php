<?php
namespace Modules\Inventario\Model\definiciones;
use Modules\Admin\Model\modelo;

class Empresas extends modelo
{
	protected $table = 'empresas';

    protected $fillable = [
        'nombre',
        'redes_id'
    ];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
