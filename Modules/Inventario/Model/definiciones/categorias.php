<?php
namespace Modules\Inventario\Model\definiciones;
use Modules\Admin\Model\modelo;

class categorias extends modelo
{
	protected $table = 'categorias';

    protected $fillable = [
        'nombre'
    ];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
