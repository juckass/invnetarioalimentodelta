<?php  
namespace Modules\Inventario\Model;
use Modules\Admin\Model\modelo;

use Carbon\Carbon;

class inventario extends modelo
{
	protected $table = 'inventario';

	protected $fillable = [
		'empresa_id',
		'rubros_id',
		'cantidad',
		'fecha',
		'ano'
	]; 

	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];


	public function setFechaAttribute($value)
	{
		// 2016-06-27
		$formato = 'd/m/Y';
		if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
			$formato = 'Y-m-d';
		}
		
		$this->attributes['fecha'] = Carbon::createFromFormat($formato, $value);
	}
	
	public function getFechaAtAttribute($value){
		return Carbon::parse($value)->format('d/m/Y');
	}

	public function empresas(){
		return $this->belongsTo('App\Model\empresas');
	}
	
	public function rubros(){
		return $this->belongsTo('App\Model\rubros');
	}
}