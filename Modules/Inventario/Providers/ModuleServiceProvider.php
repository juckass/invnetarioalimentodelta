<?php

namespace Modules\Inventario\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'inventario');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'inventario');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'inventario');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
