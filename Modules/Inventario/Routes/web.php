<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => Config::get('admin.prefix')], function() {
	Route::get('inventario_alimento', function(){
		return Redirect('/');
	});
	
	Route::group(['prefix' => 'inventario'], function(){ 
		Route::group(['prefix' => 'definiciones'], function(){ 
			Route::group(['prefix' => 'categoria'], function() {
				Route::get('/', 				'definiciones\CategoriaController@index');
				Route::post('guardar',			'definiciones\CategoriaController@guardar');
				Route::put('guardar/{id}',		'definiciones\CategoriaController@guardar');
				Route::get('buscar/{id}', 		'definiciones\CategoriaController@buscar');
				Route::get('datatable', 		'definiciones\CategoriaController@datatable');	
				Route::delete('eliminar/{id}', 	'definiciones\CategoriaController@eliminar');

			});
			Route::group(['prefix' => 'empresas'], function() {
				Route::get('/', 				'definiciones\EmpresasController@index');
				Route::post('guardar',			'definiciones\EmpresasController@guardar');
				Route::put('guardar/{id}',		'definiciones\EmpresasController@guardar');
				Route::get('buscar/{id}', 		'definiciones\EmpresasController@buscar');
				Route::get('datatable', 		'definiciones\EmpresasController@datatable');	
				Route::delete('eliminar/{id}', 	'definiciones\EmpresasController@eliminar');

			});
			Route::group(['prefix' => 'rubros'], function() {
				Route::get('/', 				'definiciones\RubrosController@index');
				Route::post('guardar',			'definiciones\RubrosController@guardar');
				Route::put('guardar/{id}',		'definiciones\RubrosController@guardar');
				Route::get('buscar/{id}', 		'definiciones\RubrosController@buscar');
				Route::get('datatable', 		'definiciones\RubrosController@datatable');	
				Route::delete('eliminar/{id}', 	'definiciones\RubrosController@eliminar');

			});
		});
		Route::group(['prefix' => 'reporte'], function(){ 
			Route::get('/', 				'ReporteController@index');
			Route::post('imprimir',			'ReporteController@imprimir');
		});	

		Route::group(['prefix' => 'carga'], function(){ 
			Route::get('/', 				'InventarioController@index');
			Route::post('guardar',			'InventarioController@guardar');
			Route::get('fecha', 			'InventarioController@fecha');
			Route::post('archivo/', 		'InventarioController@archivo');
		});	
		Route::group(['prefix' => 'graficos'], function(){ 
			Route::get('/', 				 'GraficasController@index');
			Route::get('/graficaEmpresas',  'GraficasController@graficaEmpresas');
			Route::get('/graficaCategoria', 'GraficasController@graficaCategoria');
			//Route::post('imprimir',			'ReporteController@imprimir');
		});
		Route::group(['prefix' => 'comportamiento'], function(){ 
			Route::get('/', 					'ComportamientoController@index');
			Route::get('/graficaAnos',  		'ComportamientoController@graficaAnos');
			Route::get('/graficaRango', 		'ComportamientoController@graficaRango');
			//Route::post('imprimir',			'ReporteController@imprimir');
		});	
	});

    //{{route}}
});