$(function() {

	$("#formulario").submit(function(evt){
		return false;
	});

	$("option:first", "#tipo").remove();

	$("#tipo").on('change', function(){
		
		$tipo = $('#tipo').val();
		
		if($tipo == "empresa"){
			$(".ws").css('display', 'block');
			$grafica = $url + "graficaEmpresas";	 
		}else if($tipo == "categoria"){
			$(".ws").css('display', 'none');
			$grafica = $url + "graficaCategoria";	 
		}
		$("#btn_graficar").click();
	}).change();

	$("#fecha").datepicker({
		changeMonth: true,
		onSelect : function(){
			$("#btn_graficar").click();
		}
	}).change(function(){
		$("#btn_graficar").click();
	});
	$('input[name="rubro[]"]').each(function(){
		var self = $(this),
		label = self.next(),
		label_text = label.text();
		console.log(label);

		label.remove();
		self.iCheck({
			checkboxClass: 'icheckbox_line-blue',
			radioClass: 'iradio_inline',
			insert: '<div class="icheck_line-icon"></div>' + label_text
		});
	});


	$("#btn_graficar").click(function(){
		var $fecha = $("#fecha").val();
		var $rubro = $("#rubros").val();


		if($fecha == ''){
			aviso('El campo Fecha es Requerido.', false);
			return false;
		}

		$.ajax($grafica, {
			data : $("#formulario").serialize(),
			success : generarGrafica
		});
	}).click();
});

function generarGrafica(r){
	$('#grafica').highcharts({
		chart: {
			type: 'column'
		},
		title: {
			text: 'Inventario de Alimentos'
		},
		subtitle: {
			text: 'Inventario al dia de ' + $("#fecha").val() + '.'
		},
		xAxis: {
			type: 'category',
			labels: {
				rotation: -45,
				style: {
					fontSize: '13px',
					fontFamily: 'Verdana, sans-serif'
				}
			}
		},
		yAxis: {
			title: {
				text: 'Toneladas de Alimentos'
			}
		},
		legend: {
			enabled: false
		},
		plotOptions: {
			series: {
				borderWidth: 0,
				dataLabels: {
					enabled: true,
					format: '{point.y:.2f}'
				}
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b><br/>'
		},

		series: r.series,
		drilldown: r.drilldown
	});
}