var aplicacion, $form, tabla = false, to;
$(function() {
	aplicacion = new app("formulario", {
		'antes' : function(accion){
			
		},
		'limpiar' : function(){

			tabla.fnDraw();
		},
		'buscar' : function(r){
			
		}
	});

	$form = aplicacion.form;

	//$("#cedula", $form).numeric({ min : 0 });
	//$("#fecha_nacimiento", $form).datepicker();
	/*$('#fecha_nacimiento', $form).daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    });*/
	//$("#telefono", $form).mask("0999-999-9999");

	//buscar 
	tabla = $('#tabla')
	.on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	})
	.removeClass('display')
	.addClass('table table-striped table-bordered')
	.dataTable({
		processing: true,
		serverSide: true,
		ajax: $url + "datatable",
		oLanguage: datatableEspanol,
		columns: [
			{ data: 'nombre', name: 'nombre' },
			
		]
	});
});