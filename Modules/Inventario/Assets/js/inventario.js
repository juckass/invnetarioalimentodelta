var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app("formulario", {
		'antes' : function(accion){	
		},
		'limpiar' : function(){
			$("input", ".tabla-inventario").val('0');
		},
		'buscar' : function(r){	
		}
	});

	aplicacion.limpiar();
	//datos_prueba();

	$form = aplicacion.form;
	$("#fecha", $form).datepicker();

	$("input", ".tabla-inventario").TouchSpin({
		max: 10000,
		decimals: 2,
		step: 0.01,
		verticalbuttons: true,
		initval: 0
    });

	$("input", ".tabla-inventario").val('0');

	$("#subir").on('click', function(e){
	    e.preventDefault();
	    $("#upload:hidden").trigger('click');
	});

	$("#fecha").on('change', function(){
		var fecha = $("#fecha").val();
		$.ajax({
			url : $url + 'fecha',
			data: 'fecha='+fecha,
			method: "Get",
			success : function(r){
				$("input", ".tabla-inventario").val('0');

				if ($.isArray(r)){
					return;			
				}

				$("input", ".tabla-inventario").each(function(){
					var empresa = this.getAttribute('data-empresa'),
					rubro = this.getAttribute('data-rubro');

					this.value = r[empresa][rubro] || 0;
				});
			}
		});
	});

	$("#upload").on('change', function(){
		var l = Ladda.create($("#subir").get(0));
	 	l.start();

		var options2 = { 
			url : $url + 'archivo',
			type : 'POST',
			success: function(r){
				if (typeof(r) == 'string'){
					aviso(r);
					return;
				}

				$("input", ".tabla-inventario").each(function(){
					var empresa = this.getAttribute('data-empresa'),
					rubro = this.getAttribute('data-rubro');

					this.value = r.cantidades[empresa][rubro] || 0;
				});

				aviso(r);
			},
			complete : function(){
				l.stop();
			}
		}; 
		
		$('#carga').ajaxSubmit(options2); 
	});
});