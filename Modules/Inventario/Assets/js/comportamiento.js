$(function() {
	$("#formulario").submit(function(evt){
		return false;
	});

	$("option:first", "#tipo").remove();
	$("option:first", "#ano").remove();
	
	$("#tipo").on('change', function(){
		var tipo = $('#tipo').val();

		if(tipo == "anos"){
			$("#anos").css('display', 'block');
			$("#rango").css('display', 'none');
		}else if(tipo == "rango"){
			$("#rango").css('display', 'block');
			$("#anos").css('display', 'none');
		}

		$("#btn_graficar").click();
	}).change();

	$("#fechadesde").datepicker({
		changeMonth: true,
		onSelect : function(){
			$("#btn_graficar").click();
		} 
	}).change(function(){
		$("#btn_graficar").click();
	});

	$("#fechahasta").datepicker({
		changeMonth: true,
		onSelect : function(){
			$("#btn_graficar").click();
		}
	}).change(function(){
		$("#btn_graficar").click();
	});

	$('input[name="rubro[]"]').each(function(){
		var self = $(this),
		label = self.next(),
		label_text = label.text();
		
		label.remove();
		self.iCheck({
			checkboxClass: 'icheckbox_line-blue',
			radioClass: 'iradio_inline',
			insert: '<div class="icheck_line-icon"></div>' + label_text
		});
	});

	$("#btn_graficar").click(function(){
		var $urlGrafica = 'graficaAnos';
		if ($("#tipo").val() == 'rango'){
			$urlGrafica = 'graficaRango';

			if($("#fechadesde").val() == '' || $("#fechahasta").val() == ''){
				aviso('El campo Fecha es Requerido.', false);
				return false;
			}
		}

		$.ajax($url + $urlGrafica, {
			data : $("#formulario").serialize(),
			success : generarGrafica
		});
	}).click();
});

function generarGrafica(r){
	$('#grafica').highcharts({
		title: {
			text: 'Grafico de Comportamiento de Inventario por Empresa'
		},
		xAxis: {
			categories: r.categorias,
			tickmarkPlacement: 'on',
			title: {
				enabled: false
			}
		},
		yAxis: {
			title: {
				text: 'Toneladas de Alimentos'
			}
		},
		tooltip: {
			shared: true
		},
		series: r.series
	});
	//console.log(r.series);
}